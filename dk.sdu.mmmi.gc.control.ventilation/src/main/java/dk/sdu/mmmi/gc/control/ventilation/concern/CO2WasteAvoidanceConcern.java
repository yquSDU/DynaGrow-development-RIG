package dk.sdu.mmmi.gc.control.ventilation.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class CO2WasteAvoidanceConcern extends AbstractConcern {

    private double cost = 0;

    public CO2WasteAvoidanceConcern(ControlDomain g) {
        super("Avoid wasteful CO₂ dosing", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {
        // do not dosing when ventilating
        double co2SP = s.getValue(CO2GoalOutput.class).value();

        // TODO: Make min co2 limit as an user input
        if (isVentilating(s)) {
            cost = abs(co2SP - 350.0) != 0.0 ? 1 : 0;
        }

        return new PPM(cost).value();
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

//        if (isDosing(s)) {
//            // Dosing - don't ventilate.
//            r.put(VentilationTempOutput.class, "Dosing");
//        }

        if (isVentilating(s)) {
            // Ventilation - don't dose.
            r.put(CO2GoalOutput.class, "Ventilating");
        }

        return r;
    }

//    private boolean isDosing(Solution s) {
//        double co2SP = s.getValue(CO2GoalOutput.class).value();
//        double co2 = s.getValue(CO2Input.class).value();
//        return co2 - 50.0 < co2SP;
//    }

    private boolean isVentilating(Solution s) {
        double t = s.getValue(IndoorTemperatureInput.class).value();
        double ventSP = s.getValue(VentilationTempOutput.class).value();
        return ventSP < t + 2.0;
    }
}
