package dk.sdu.mmmi.gc.impl.okosimulation;

import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Provides a realistic set of el prices for any Date object. The provided data
 * is based on the elprices in dkwest from 2008.
 *
 * @author mrj
 */
public class ElPrices {

    private static final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
    private static final String CURRENCY = "EUR";
    private static final ElPrices INSTANCE = new ElPrices();
    private Map<Integer, MoneyMegaWattHour> data = null;

    private ElPrices() {
    }

    public static ElPrices getInstance() {
        return INSTANCE;
    }

    public synchronized MoneyMegaWattHour getPrice(Date date) {
        // Initialize data if needed.
        if (!isInitialized()) {
            initialize();
        }

        // Get data.
        MoneyMegaWattHour r = data.get(hourOfYear(date));
        if (r == null) {
            throw new IllegalStateException("Null not expected.");
        }
        return r;
    }

    private synchronized boolean isInitialized() {
        return data != null;
    }

    private synchronized void initialize() {
        System.out.println("INITIALIZE EL PRICES");
        try {
            // Reset map.
            data = Collections.synchronizedMap(new HashMap<Integer, MoneyMegaWattHour>());

            // Read from csv.
            URL url = getClass().getResource("elprices_dkwest_2008.csv");
            importFromCSV(url);
        } catch (Exception x) {
            x.printStackTrace(System.err);
        }
        
        System.out.println("DONE");
    }

    private synchronized void importFromCSV(URL url) throws Exception {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            // Read lines.
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(";");

                // Read date from line.
                Date d = DF.parse(tokens[1]);

                // Read hour data from line.
                for (int h = 0; h < 24; h++) {
                    double price = Double.parseDouble(tokens[h + 2]);
                    //System.out.print(".");
                    data.put(hourOfYear(d) + h, new MoneyMegaWattHour(price, CURRENCY));
                }
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }
    private static final Calendar C = Calendar.getInstance();

    private static int hourOfYear(Date d) {
        C.setTime(d);
        return C.get(Calendar.HOUR_OF_DAY)
                + (C.get(Calendar.DAY_OF_YEAR) - 1) * 24;
    }
}
