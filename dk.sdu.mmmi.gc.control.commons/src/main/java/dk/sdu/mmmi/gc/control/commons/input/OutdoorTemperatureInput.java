package dk.sdu.mmmi.gc.control.commons.input;

import dk.sdu.mmmi.gc.control.commons.input.CO2Input;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jcs
 */
public class OutdoorTemperatureInput extends AbstractInput<Celcius> {

    private final ControlDomain g;

    public OutdoorTemperatureInput(ControlDomain g) {
        super("Outdoor temperature");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        HALConnector hal = HAL.get(g);
        if (hal != null) {
            try {
                Celcius newValue = hal.readOutdoorTemperature();
                setValue(newValue);
                store(t, newValue);
            } catch (HALException ex) {
                Logger.getLogger(CO2Input.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void store(Date t, Celcius result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertOutdoorTemperature(new Sample<>(t, result));
    }
}
