package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.weather.IWeatherServiceDriver;
import dk.sdu.mmmi.controleum.api.weather.WeatherCommunicationException;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.gui.ConfigureLightForecastDBAction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import javax.swing.Action;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

/**
 *
 * @author jcs
 */
public class OutdoorLightForecastInput extends AbstractInput<OutdoorLightForecast> {

    private static final Logger logger = getLogger(OutdoorLightForecastInput.class.getName());
    private final ControlDomain g;
    private final Link<Action> action;

    public OutdoorLightForecastInput(ControlDomain g) {
        super("Outdoor light forecast");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureLightForecastDBAction(g));
    }

    @Override
    public void doUpdateValue(Date from) {
        try {

            TimeStamp endTime = context(g).one(FrameEndInput.class).getValue();

            // Update data in local db if not just called
            updateForecast(startOfHour(from), endTime.toDate());

        } catch (WeatherCommunicationException ex) {

            logger.log(Level.INFO, "No access to weather forecast.", ex);

            NotificationDisplayer.getDefault().
                    notify("No Outdoor light forecast!",
                            ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/warning.gif", true),
                            "Click to deactivate the Outdoor light forecast input?",
                            context(this).one(Action.class));
        }
    }

    //
    // Private
    //
    private void updateForecast(Date from, Date to) throws WeatherCommunicationException {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        LightForecastConfig cfg = db.selectLightForecastConfig();

        // Get data remotely if possible
        if (cfg.isActive()) {
            IWeatherServiceDriver weatherDB = Lookup.getDefault().lookup(IWeatherServiceDriver.class);
            Sample<OutdoorLightForecast> remoteForecast = weatherDB.getOutdoorLightForecast(cfg, from, to);

            // Store the data
            db.insertOutdoorLightForecast(remoteForecast);
        }

        // Update input
        OutdoorLightForecast s = db.selectOutdoorLightForecast(from, to);
        if (s != null) {
            setValue(s);
        }
    }
}
