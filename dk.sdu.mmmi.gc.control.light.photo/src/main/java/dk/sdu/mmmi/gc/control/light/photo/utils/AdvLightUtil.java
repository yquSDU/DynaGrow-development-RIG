package dk.sdu.mmmi.gc.control.light.photo.utils;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.commons.input.LightTransmissionFactorInput;
import dk.sdu.mmmi.gc.control.commons.input.PhotoOptimalPercentageInput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.combineNatArtLight;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.convertToIndoorPAR;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.light.input.LampIntensityInput;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import java.util.List;

/**
 *
 * @author jcs
 */
public class AdvLightUtil {

    /**
     * @return Expected photo. sum from natural and artifical light for the
     * remaining day. Note: The returned result may be cached to avoid using
     * unnecessary calculations in the fitness methods.
     */
    public static MMolSqrMeter calcExpectedCombinedPhotoSum(ControlDomain g, LightPlan lightPlan) {

        MMolSqrMeter expCombinedToday = context(lightPlan).one(MMolSqrMeter.class);

        // Not Cached
        if (expCombinedToday == null) {
            Percent photoPct = context(g).one(PhotoOptimalPercentageInput.class).getValue();
            UMolSqrtMeterSecond lampIntensity = context(g).one(LampIntensityInput.class).getValue();
            Percent ghTransmission = context(g).one(LightTransmissionFactorInput.class).getValue();

            // Convert forcasted outdoor light to indoor PAR light
            OutdoorLightForecast lightForecast = context(g).one(OutdoorLightForecastInput.class).getValue();
            List<Sample<UMolSqrtMeterSecond>> parNatLightLevels = convertToIndoorPAR(lightForecast, ghTransmission);

            // Art. + Nat. light
            List<Sample<UMolSqrtMeterSecond>> parLightLevels = combineNatArtLight(parNatLightLevels, lightPlan, lampIntensity);

            // Calc. combined photo. sum by light plan by taking nat. light forecast for remaining day into account
            PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
            Duration ctrlDelay = context(g).one(ControlManager.class).getDelay();

            expCombinedToday = photoUtil.calcPhotoSum(parLightLevels, photoPct, ctrlDelay);

            // Cache
            context(lightPlan).add(MMolSqrMeter.class, expCombinedToday);
        }

        return expCombinedToday;
    }
}
