package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.gui.charts.ChartManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

/**
 *
 * @author jcs
 */
public class CreateChartAction extends AbstractAction {

    private final ControlDomain g;
    private final ChartManager cm;

    public CreateChartAction(ControlDomain g) {
        super("Create new Chart");
        this.g = g;
        this.cm = context(g).one(ChartManager.class);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        // Ask for name
        NotifyDescriptor.InputLine d = new NotifyDescriptor.InputLine("Chart name", "Chart name");
        if (DialogDisplayer.getDefault().notify(d) == NotifyDescriptor.OK_OPTION) {

            cm.createChart(d.getInputText());
        }
    }
}
