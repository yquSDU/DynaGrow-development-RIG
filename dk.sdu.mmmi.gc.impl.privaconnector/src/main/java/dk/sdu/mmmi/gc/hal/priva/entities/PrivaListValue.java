/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva.entities;

/**
 *
 * @author ancla
 */
public class PrivaListValue {

    private final byte[] name;
    private final byte[] value;

    public PrivaListValue(byte[] name, byte[] value) {
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name
     */
    public byte[] getName() {
        return name;
    }

    /**
     * @return the value
     */
    public byte[] getValue() {
        return value;
    }

    public int getValueAsInt() {
        int val = 0;
        for (int i = 0; i < value.length; i++) {
            val += ((int) value[i] & 0xffL) << (8 * i);
        }
        return val;
    }

    @Override
    public String toString() {

        return "PrivaListValue - Name: " + new String(name) + " || Value: " + getValueAsInt();
    }
}
