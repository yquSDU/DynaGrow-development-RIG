package dk.sdu.mmmi.gc.impl.climatedb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.impl.entities.config.ConcernConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.ControlConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.OutputConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.NegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.WINDOW_SIZE;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfXDaysAfter;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfDay;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.climatedb.util.JDBC;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PhotoSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PrivaConfig;
import dk.sdu.mmmi.gc.impl.entities.config.SuperLinkConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mrj & jcs
 */
public class DerbyContextDataAccess implements ContextDataAccess, ClimateDataAccess {

    private final Connection con;
    private final int contextID;
    private final String name;
    private final Gson gson;

    public DerbyContextDataAccess(Connection con, int contextID, String name) {
        this.con = con;
        this.contextID = contextID;
        this.name = name;
        this.gson = new GsonBuilder()
            //.setDateFormat("E, dd MMM yyyy HH:mm:ss z")
            .create();
    }

    @Override
    public int getID() {
        return contextID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void cleanClimateData() {
        String[] delete = new String[]{
            "air_temperature", "co2",
            "light_intensity", "heating_threshold", "ventilation_threshold",
            "co2_threshold", "light_status", "general_screen_position",
            "daily_avg_temp_goal", "daily_avg_temp",
            "decreasing_vent_temp_per_min", "light_sum_hours_goal",
            "humidity", "max_humidity", "max_air_temperature",
            "min_air_temperature", "outdoor_light", "max_outdoor_light",
            "outdoor_temperature", "photo_opt",
            "lamp_effect",
            "expected_natural_par_sum_today", "expected_natural_photo_sum_today",
            "expected_natural_photo_sum_period",
            "lamp_intensity", "light_time_achieved_today", "light_time_achieved_period",
            "light_transmission_factor", "par_sum_achieved_today", "par_sum_achieved_period",
            "par_sum_day_goal", "photo_sum_achieved_today",
            "photo_sum_day_goal", "screen_transmission_factor",
            "fixed_day_light_plan", "energy_balance",
            "negotiation_statistics", "concern_negotiation_result",
            "optimal_photo", "actual_photo", "preferred_day_temp",
            "preferred_night_temp", "preferred_day_co2", "preferred_night_co2",
            "preferred_day_screen_position", "preferred_night_screen_position",
            "preferred_day_ventilation_sp", "preferred_night_ventilation_sp",
            "screen_closing_pct", "proposed_light_plan",
            "photo_balance_yesterday", "par_balance_yesterday", "min_light_art_light_intensity",
            "indoor_light_configuration", "photo_sum_achieved_period"};

        for (String s : delete) {
            try (PreparedStatement ps = con.prepareStatement(
                    "DELETE FROM " + s + " WHERE context_id = ?")) {
                ps.setInt(1, contextID);
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not clean climate DB.", ex);
            }
        }
    }

    //
    // Setter
    //
    @Override
    public void setSuperLinkConfiguration(SuperLinkConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO superlink_configuration "
                + "(context_id, time, hostname, domain, username, password, department, light_groups, indoorlight_code) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));

            ps.setString(3, cfg.getHostname());
            ps.setString(4, cfg.getDomain());
            ps.setString(5, cfg.getUsername());
            ps.setString(6, cfg.getPassword());
            ps.setString(7, cfg.getDepartment());
            ps.setString(8, cfg.getLightGroupsAsString());
            ps.setString(9, cfg.getIndoorLightCode());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save SuperLinkConfiguration to DB.", ex);
        }
    }

    @Override
    public void setControlConfiguration(ControlConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO control_configuration "
                + "(context_id, time, config) "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save ControlConfiguration to DB.", ex);
        }
    }

    @Override
    public void setConcernConfiguration(String id, ConcernConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO concern_configuration "
                + "(context_id, time, concern_id, is_enabled, priority) "
                + "VALUES (?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, id);
            ps.setInt(4, cfg.isEnabled() ? 1 : 0);
            ps.setInt(5, cfg.getPriority());
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save ConcernConfiguration to DB.", ex);
        }
    }

    @Override
    public void setOutputConfiguration(String id, OutputConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO output_configuration "
                + "(context_id, time, output_id, is_enabled) "
                + "VALUES (?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, id);
            ps.setInt(4, cfg.isEnabled() ? 1 : 0);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save OutputConfiguration to DB.", ex);
        }
    }

    //
    // Getter
    //
    @Override
    public SuperLinkConfig getSuperLinkConfiguration() {
        SuperLinkConfig cfg = new SuperLinkConfig();

        try (PreparedStatement ps = con.prepareStatement(
        "SELECT time, hostname, domain, username, password, department, light_groups, indoorlight_code "
        + "FROM superlink_configuration "
                + "WHERE context_id = ? "
                + "ORDER BY time DESC")) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cfg.setHostname(rs.getString(2));
                cfg.setDomain(rs.getString(3));
                cfg.setUsername(rs.getString(4));
                cfg.setPassword(rs.getString(5));
                cfg.setDepartment(rs.getString(6));
                cfg.setLightGroups(rs.getString(7));
                cfg.setIndoorLightCode(rs.getString(8));
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve SuperLinkConfiguration from DB.", ex);
        }
        return cfg;
    }

    @Override
    public ControlConfig getControlConfiguration() {

        ControlConfig cfg = new ControlConfig.Builder().build();

        try (PreparedStatement ps = con.prepareStatement("SELECT time, config "
                + "FROM control_configuration "
                + "WHERE context_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cfg = gson.fromJson(rs.getString(2), ControlConfig.class);
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve ControlConfig from DB.", ex);
        }
        return cfg;
    }

    @Override
    public ConcernConfig getConcernConfiguration(String id) {

        ConcernConfig cfg = null;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, is_enabled, priority "
                + "FROM concern_configuration "
                + "WHERE context_id = ? AND concern_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int p = rs.getInt(3);
                cfg = new ConcernConfig.Builder().setEnabled(rs.getInt(2) > 0)
                        .setPriority(p).build();
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve ConcernConfig from DB.", ex);
        }

        return cfg;
    }

    @Override
    public OutputConfig getOutputConfiguration(String id) {
        OutputConfig cfg = new OutputConfig();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, is_enabled "
                + "FROM output_configuration "
                + "WHERE context_id = ? AND output_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cfg.setEnabled(rs.getInt(2) > 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve OutputConfig from DB.", ex);
        }
        return cfg;
    }

    @Override
    public ChartConfig getChartConfig(String id) {
        ChartConfig cfg = new ChartConfig();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT chart_config "
                + "FROM chart_configuration "
                + "WHERE context_id = ? AND id = ? ");) {
            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cfg = gson.fromJson(rs.getString(1), ChartConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve ChartConfig by ID from DB.", ex);
        }
        return cfg;
    }

    @Override
    public PARSumAchievedConfig selectPARSumAchievedConfig() {
        PARSumAchievedConfig r = null;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT par_sum_achieved_config "
                + "FROM par_sum_achieved_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), PARSumAchievedConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }

        // Insert default
        if (r == null) {
            r = new PARSumAchievedConfig();
            insertPARSumAchievedConfig(r);
        }
        return r;
    }

    @Override
    public PhotoSumAchievedConfig selectPhotoSumAchievedConfig() {
        PhotoSumAchievedConfig r = null;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT photo_sum_achieved_config "
                + "FROM photo_sum_achieved_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), PhotoSumAchievedConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }

        // Insert default
        if (r == null) {
            r = new PhotoSumAchievedConfig();
            insertPhotoSumAchievedConfig(r);
        }
        return r;
    }
    //
    // Updates
    //

    @Override
    public void updateLightForecastConfig(LightForecastConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE light_forecast_configuration "
                + "SET light_forecast_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not update LightForecastConfig in DB.", ex);
        }
    }

    @Override
    public void updateElPriceForecastConfig(ElPriceForecastConfig c) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE elprice_configuration "
                + "SET elprice_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(c));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not update ElpriceForecastConfig in DB.", ex);
        }
    }

    @Override
    public void updateChart(ChartConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE chart_configuration "
                + "SET chart_config = ? "
                + "WHERE context_id = ? AND id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);
            ps.setInt(3, cfg.getID());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve ChartConfig in DB.", ex);
        }
    }

    @Override
    public void updatePARSumAchievedConfig(PARSumAchievedConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE par_sum_achieved_configuration "
                + "SET par_sum_achieved_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve PARSumAchievedConfig in DB.", ex);
        }
    }

    @Override
    public void updatePhotoSumAchievedConfig(PhotoSumAchievedConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE photo_sum_achieved_configuration "
                + "SET photo_sum_achieved_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve PARSumAchievedConfig in DB.", ex);
        }
    }

    //
    // Inserts
    //
    @Override
    public void insertLightForecastConfig(LightForecastConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO light_forecast_configuration "
                + "(context_id, light_forecast_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert LightForecastConfig into DB.", ex);
        }
    }

    @Override
    public void insertPARSumAchievedConfig(PARSumAchievedConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO par_sum_achieved_configuration "
                + "(context_id, par_sum_achieved_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert PARSumAchievedConfig into DB.", ex);
        }
    }

    @Override
    public void insertPhotoSumAchievedConfig(PhotoSumAchievedConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO photo_sum_achieved_configuration "
                + "(context_id, photo_sum_achieved_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert PhotoSumAchievedConfig into DB.", ex);
        }
    }

    @Override
    public void insertElPriceForecastConfig(ElPriceForecastConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO elprice_configuration "
                + "(context_id, elprice_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert ElPriceForecastConfig into DB.", ex);
        }
    }

    @Override
    public ChartConfig insertChart(ChartConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO chart_configuration "
                + "(context_id, chart_config) "
                + "VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();

            ResultSet s = ps.getGeneratedKeys();
            if (s.next()) {
                cfg.setID(s.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert ChartConfig into DB.", ex);
        }

        return cfg;
    }

    @Override
    public void insertGreenhouseSize(Sample<SqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "greenhouse_size", "sqrmeter");
    }

    @Override
    public void insertAirTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "air_temperature", "celcius");
    }

    @Override
    public void insertCO2(Sample<PPM> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "co2", "ppm");
    }

    @Override
    public void insertLightIntensity(Sample<UMolSqrtMeterSecond> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "light_intensity", "umol");
    }

    @Override
    public void insertHeatingThreshold(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "heating_threshold", "celcius");
    }

    @Override
    public void insertVentilationThreshold(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "ventilation_threshold", "celcius");
    }

    @Override
    public void insertCO2Threshold(Sample<PPM> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "co2_threshold", "ppm");
    }

    @Override
    public void insertLightStatus(Sample<Switch> s) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO light_status "
                + "(context_id, time, switch) "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(s.getTimestamp()));
            setSwitch(ps, 3, s.getSample());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert LightStatus into DB.", ex);
        }
    }

    @Override
    public void insertGeneralScreenPosition(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "general_screen_position", "factor");
    }

    @Override
    public void insertWindowsOpening(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "windows_opening", "factor");
    }

    @Override
    public void insertDailyAverageTemperatureGoal(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "daily_avg_temp_goal", "celcius");
    }

    @Override
    public void insertDailyAverageTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "daily_avg_temp", "celcius");
    }

    @Override
    public void insertDecreasingVentilationTemperaturePerMinute(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "decreasing_vent_temp_per_min", "celcius");
    }

    @Override
    public void insertLightSumHoursGoal(Sample<Duration> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "light_sum_hours_goal", "hours");
    }

    @Override
    public void insertHumidity(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "humidity", "factor");
    }

    @Override
    public void insertMaxHumidity(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "max_humidity", "factor");
    }

    @Override
    public void insertMaxAirTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "max_air_temperature", "celcius");
    }

    @Override
    public void insertMinAirTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "min_air_temperature", "celcius");
    }

    @Override
    public void insertOutdoorLight(Sample<WattSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "outdoor_light", "wattm2");
    }

    @Override
    public void insertMaxOutdoorLight(Sample<WattSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "max_outdoor_light", "wattm2");
    }

    @Override
    public void insertOutdoorTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "outdoor_temperature", "celcius");
    }

    @Override
    public void insertPhotoOptimization(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "photo_opt", "factor");
    }

    @Override
    public void insertElPriceForecast(Sample<ElPriceMWhForecast> s) {
        insertCombinedElForecast(s, "combined_el_prices");
    }

    @Override
    public void insertOutdoorLightForecast(Sample<OutdoorLightForecast> s) {

        boolean first = true;

        try (PreparedStatement ps1 = con.prepareStatement(
                "DELETE FROM combined_light_forecast WHERE context_id = ? AND time >= ?");
                PreparedStatement ps2 = con.prepareStatement(
                        "INSERT INTO combined_light_forecast "
                        + "(context_id, time, wattm2) "
                        + "VALUES (?, ?, ?)");) {

            for (Sample<WattSqrMeter> entry : s.getSample().get()) {

                Date time = entry.getTimestamp();
                WattSqrMeter watt = entry.getSample();

                // Delete current data covered by forecast on first iteration.
                if (first) {

                    ps1.setInt(1, contextID);
                    ps1.setTimestamp(2, JDBC.toTime(time));
                    ps1.executeUpdate();

                    first = false;
                }

                // Insert new data.
                ps2.setInt(1, contextID);
                ps2.setTimestamp(2, JDBC.toTime(time));
                setWattM2(ps2, 3, watt);
                ResultSet keys = ps2.getGeneratedKeys();
                ps2.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert CombinedOutdoorLightForecast into DB.", ex);
        }
    }

    @Override
    public void insertInstalledLampEffect(Sample<WattSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "lamp_effect", "wattm2");
    }

    @Override
    public void insertExpectedNaturalPARSumToday(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "expected_natural_par_sum_today", "mol_m2");
    }

    @Override
    public void insertExpectedNaturalPARSumPeriod(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "expected_natural_par_sum_period", "mol_m2");
    }

    @Override
    public void insertExpectedNaturalPhotoSumToday(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "expected_natural_photo_sum_today", "mmol_m2");
    }

    @Override
    public void insertExpectedNaturalPhotoSumPeriod(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "expected_natural_photo_sum_period", "mmol_m2");
    }

    @Override
    public void insertExpectedCombinedPhotoSumPeriod(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "expected_combined_photo_sum_period", "mmol_m2");
    }

    @Override
    public void insertLampIntensity(Sample<UMolSqrtMeterSecond> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "lamp_intensity", "umol_m2_s");
    }

    @Override
    public void insertLightTimeAchievedToday(Sample<Duration> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "light_time_achieved_today", "ms");
    }

    @Override
    public void insertLightTimeAchievedPeriod(Sample<Duration> s) {
        insertSample(s.getTimestamp(), s.getSample().getMS(),
                "light_time_achieved_period", "ms");
    }

    @Override
    public void insertLightTransmissionFactor(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "light_transmission_factor", "factor");
    }

    @Override
    public void insertPARSumAchievedToday(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "par_sum_achieved_today", "mol_m2");
    }

    @Override
    public void insertPARSumAchievedPeriod(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "par_sum_achieved_period", "mol_m2");
    }

    @Override
    public void insertPARSumDayGoal(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "par_sum_day_goal", "mol_m2");
    }

    @Override
    public void insertPhotoSumAchievedToday(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "photo_sum_achieved_today", "mmol_m2");
    }

    @Override
    public void insertPhotoSumAchievedPeriod(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "photo_sum_achieved_period", "mmol_m2");
    }

    @Override
    public void insertPhotoSumDayGoal(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "photo_sum_day_goal", "mmol_m2");
    }

    @Override
    public void insertScreenPhotoRatio(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "screen_photo_loss_ratio", "pct");
    }

    @Override
    public void insertScreenClosingPct(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "screen_closing_pct", "pct");
    }

    @Override
    public void insertScreenTransmissionFactor(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "screen_transmission_factor", "factor");
    }

    @Override
    public void insertFixedLightPlan(Sample<FixedDayLightPlan> s) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO fixed_day_light_plan "
                + "(context_id, time, light_plan) "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(s.getTimestamp()));
            ps.setString(3, gson.toJson(s.getSample()));
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert X into DB.", ex);
        }
    }

    @Override
    public void insertNegotiationResult(Sample<NegotiationResult> s) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO negotiation_statistics "
                + "(context_id, time, generations, duration_ms) "
                + "VALUES (?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(s.getTimestamp()));
            ps.setInt(3, s.getSample().getGenerationsExecuted());
            ps.setLong(4, s.getSample().getTimeSpendMS());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert X into DB.", ex);
        }
    }

    @Override
    public void insertConcernNegotiationResult(Sample<ConcernNegotiationResult> s) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO concern_negotiation_result "
                + "(context_id, concern_id, time, fitness, value_help) "
                + "VALUES (?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, s.getSample().getConcernID());
            ps.setTimestamp(3, JDBC.toTime(s.getTimestamp()));
            ps.setDouble(4, s.getSample().getCost());

            Type type = new TypeToken<Map<String, String>>() {
            }.getType();

            ps.setString(5, gson.toJson(s.getSample().getValueHelp(), type));
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert X into DB.", ex);
        }
    }

    @Override
    public void insertEnergyBalance(Sample<Energy> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "energy_balance", "balance");
    }

    @Override
    public void insertActualPhoto(Sample<UMolSqrtMeterSecond> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "actual_photo", "mmol_m2_s");
    }

    @Override
    public void insertOptimalPhoto(Sample<UMolSqrtMeterSecond> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "optimal_photo", "mmol_m2_s");
    }

    @Override
    public void insertPreferredDayTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_day_temp", "celcius");
    }

    @Override
    public void insertPreferredNightTemperature(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_night_temp", "celcius");
    }

    @Override
    public void insertPreferredDayCO2(Sample<PPM> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_day_co2", "ppm");
    }

    @Override
    public void insertPreferredNightCO2(Sample<PPM> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_night_co2", "ppm");
    }

    @Override
    public void insertPreferredDayScreenPosition(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_day_screen_position", "factor");
    }

    @Override
    public void insertPreferredNightScreenPosition(Sample<Percent> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_night_screen_position", "factor");
    }

    @Override
    public void insertPreferredDayVentilationSP(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_day_ventilation_sp", "celcius");
    }

    @Override
    public void insertPreferredNightVentilationSP(Sample<Celcius> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "preferred_night_ventilation_sp", "celcius");
    }

    //
    // Selects
    //
    @Override
    public Sample<SqrMeter> selectGreenhouseSize(Date t) {
        return selectSample(t, "greenhouse_size", "sqrmeter", SqrMeter.class, Double.class, 100.0);
    }

    @Override
    public LightForecastConfig selectLightForecastConfig() {
        LightForecastConfig r = null;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT light_forecast_config "
                + "FROM light_forecast_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), LightForecastConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }

        // Insert default
        if (r == null) {
            r = new LightForecastConfig();
            insertLightForecastConfig(r);
        }
        return r;
    }

    @Override
    public ElPriceForecastConfig selectElPriceForecastConfig() {
        ElPriceForecastConfig r = null;
        try (PreparedStatement ps = con.prepareStatement(
                "SELECT elprice_config "
                + "FROM elprice_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), ElPriceForecastConfig.class);
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }

        // Insert default
        if (r == null) {
            r = new ElPriceForecastConfig();
            insertElPriceForecastConfig(r);

        }
        return r;
    }

    @Override
    public List<ChartConfig> selectCharts() {

        List<ChartConfig> r = new ArrayList<>();
        try (PreparedStatement ps = con.prepareStatement(
                "SELECT id, chart_config "
                + "FROM chart_configuration "
                + "WHERE context_id = ? "
                + "ORDER BY id");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ChartConfig cfg = gson.fromJson(rs.getString(2), ChartConfig.class);
                cfg.setID(rs.getInt(1));
                r.add(cfg);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public List<Sample<ConcernNegotiationResult>> selectConcernNegotiationResult(String concernID, Date fr, Date to) {
        List<Sample<ConcernNegotiationResult>> r = new ArrayList<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, concern_id, fitness, value_help "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time");) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                ConcernNegotiationResult cnr = new ConcernNegotiationResult(rs.getString(2), rs.getDouble(3), valueHelp);

                r.add(new Sample<>(d, cnr));
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<ConcernNegotiationResult> selectConcernNegotiationResult(String concernID, Date t) {

        Sample<ConcernNegotiationResult> r = null;

        try (
                PreparedStatement ps = con.prepareStatement(
                        "SELECT time, concern_id, fitness, value_help "
                        + "FROM concern_negotiation_result "
                        + "WHERE context_id = ? "
                        + "AND concern_id = ? "
                        + "AND time <= ? "
                        + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(t));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));

                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                ConcernNegotiationResult cnr
                        = new ConcernNegotiationResult(rs.getString(2),
                                rs.getDouble(3),
                                valueHelp);

                r = new Sample<>(d, cnr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<FixedDayLightPlan> selectFixedLightPlan(Date t) {
        Sample<FixedDayLightPlan> r = new Sample<>(new FixedDayLightPlan());
        try (
                PreparedStatement ps = con.prepareStatement(
                        "SELECT time, light_plan "
                        + "FROM fixed_day_light_plan "
                        + "WHERE context_id = ? "
                        + "AND time <= ? "
                        + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(t));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));

                r = new Sample<>(d, gson.fromJson(rs.getString(2), FixedDayLightPlan.class));
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<Celcius> selectAirTemperature(Date t) {
        return selectSample(t, "air_temperature", "celcius", Celcius.class, Double.class, 20.0d);
    }

    @Override
    public List<Sample<Celcius>> selectAirTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "air_temperature", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<PPM> selectCO2(Date t) {
        return selectSample(t, "co2", "ppm", PPM.class, Double.class, 350.0);
    }

    @Override
    public List<Sample<PPM>> selectCO2(Date fr, Date to) {
        return selectSampleList(fr, to, "co2", "ppm", PPM.class, Double.class);
    }

    @Override
    public Sample<UMolSqrtMeterSecond> selectLightIntensity(Date t) {
        return selectSample(t, "light_intensity", "umol", UMolSqrtMeterSecond.class, Double.class, 100.0);
    }

    @Override
    public List<Sample<UMolSqrtMeterSecond>> selectLightIntensity(Date fr, Date to) {
        return selectSampleList(fr, to, "light_intensity", "umol", UMolSqrtMeterSecond.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectHeatingThreshold(Date t) {
        return selectSample(t, "heating_threshold", "celcius", Celcius.class, Double.class, 15.0);
    }

    @Override
    public List<Sample<Celcius>> selectHeatingThreshold(Date fr, Date to) {
        return selectSampleList(fr, to, "heating_threshold", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectVentilationThreshold(Date t) {
        return selectSample(t, "ventilation_threshold", "celcius", Celcius.class, Double.class, 20.0);
    }

    @Override
    public List<Sample<Celcius>> selectVentilationThreshold(Date fr, Date to) {
        return selectSampleList(fr, to, "ventilation_threshold", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<PPM> selectCO2Threshold(Date t) {
        return selectSample(t, "co2_threshold", "ppm", PPM.class, Double.class, 500.0);
    }

    @Override
    public List<Sample<PPM>> selectCO2Threshold(Date fr, Date to) {
        return selectSampleList(fr, to, "co2_threshold", "ppm", PPM.class, Double.class);
    }

    @Override
    public Sample<Switch> selectLightStatus(Date t) {

        Sample<Switch> r = new Sample<>(Switch.OFF);

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, switch "
                + "FROM light_status "
                + "WHERE context_id = ? "
                + "AND time <= ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(t));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                Short c = rs.getShort(2);
                r = new Sample<>(d, newSwitchIfNotNull(c));
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select LightStatus from DB.", ex);
        }
        return r;
    }

    @Override
    public List<Sample<Switch>> selectLightStatus(Date fr, Date to) {

        List<Sample<Switch>> r = new ArrayList<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, switch "
                + "FROM light_status "
                + "WHERE context_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time");) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                Short t = rs.getShort(2);
                r.add(new Sample<>(d, newSwitchIfNotNull(t)));
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select LightStatus list from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<Percent> selectGeneralScreenPosition(Date t) {
        return selectSample(t, "general_screen_position", "factor", Percent.class, Double.class, 0.0);
    }

    @Override
    public List<Sample<Percent>> selectGeneralScreenPosition(Date fr, Date to) {
        return selectSampleList(fr, to, "general_screen_position", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Percent> selectWindowsOpening(Date t) {
        return selectSample(t, "windows_opening", "factor", Percent.class, Double.class, 0.0);
    }

    @Override
    public List<Sample<Percent>> selectWindowsOpening(Date fr, Date to) {
        return selectSampleList(fr, to, "windows_opening", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectDailyAverageTemperatureGoal(Date t) {
        return selectSample(t, "daily_avg_temp_goal", "celcius", Celcius.class, Double.class, 20d);
    }

    @Override
    public List<Sample<Celcius>> selectDailyAverageTemperatureGoal(Date fr, Date to) {
        return selectSampleList(fr, to, "daily_avg_temp_goal", "celcius", Celcius.class, Double.class);
    }

    @Override
    public List<Sample<Celcius>> selectDailyAverageTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "daily_avg_temp", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectDecreasingVentilationTemperaturePerMinute(Date t) {
        return selectSample(t, "decreasing_vent_temp_per_min", "celcius", Celcius.class, Double.class, 0.2);
    }

    @Override
    public List<Sample<Celcius>> selectDecreasingVentilationTemperaturePerMinute(Date fr, Date to) {
        return selectSampleList(fr, to, "decreasing_vent_temp_per_min", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Duration> selectLightSumHoursGoal(Date t) {
        return selectSample(t, "light_sum_hours_goal", "hours", Duration.class, Long.class, 6 * 3600000l);
    }

    @Override
    public List<Sample<Duration>> selectLightSumHoursGoal(Date fr, Date to) {
        return selectSampleList(fr, to, "light_sum_hours_goal", "hours", Duration.class, Long.class);
    }

    @Override
    public Sample<Percent> selectHumidity(Date t) {
        return selectSample(t, "humidity", "factor", Percent.class, Double.class, 80.0d);
    }

    @Override
    public List<Sample<Percent>> selectHumidity(Date fr, Date to) {
        return selectSampleList(fr, to, "humidity", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Percent> selectMaxHumidity(Date t) {
        return selectSample(t, "max_humidity", "factor", Percent.class, Double.class, 90.0);
    }

    @Override
    public List<Sample<Percent>> selectMaxHumidity(Date fr, Date to) {
        return selectSampleList(fr, to, "max_humidity", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectMaxAirTemperature(Date t) {
        return selectSample(t, "max_air_temperature", "celcius", Celcius.class, Double.class, 30.0);
    }

    @Override
    public List<Sample<Celcius>> selectMaxAirTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "max_air_temperature", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectMinAirTemperature(Date t) {
        return selectSample(t, "min_air_temperature", "celcius", Celcius.class, Double.class, 15.0);
    }

    @Override
    public List<Sample<Celcius>> selectMinAirTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "min_air_temperature", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<WattSqrMeter> selectOutdoorLight(Date t) {
        return selectSample(t, "outdoor_light", "wattm2", WattSqrMeter.class, Double.class, 500.0);
    }

    @Override
    public List<Sample<WattSqrMeter>> selectOutdoorLight(Date fr, Date to) {
        return selectSampleList(fr, to, "outdoor_light", "wattm2", WattSqrMeter.class, Double.class);
    }

    @Override
    public Sample<WattSqrMeter> selectMaxOutdoorLight(Date t) {
        return selectSample(t, "max_outdoor_light", "wattm2", WattSqrMeter.class, Double.class, 1200d);
    }

    @Override
    public List<Sample<WattSqrMeter>> selectMaxOutdoorLight(Date fr, Date to) {
        return selectSampleList(fr, to, "max_outdoor_light", "wattm2", WattSqrMeter.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectOutdoorTemperature(Date t) {
        return selectSample(t, "outdoor_temperature", "celcius", Celcius.class, Double.class, 20d);
    }

    @Override
    public List<Sample<Celcius>> selectOutdoorTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "outdoor_temperature", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Percent> selectPhotoOptimizationGoal(Date t) {
        return selectSample(t, "photo_opt", "factor", Percent.class, Double.class, 80d);
    }

    @Override
    public List<Sample<Percent>> selectPhotoOptimizationGoal(Date fr, Date to) {
        return selectSampleList(fr, to, "photo_opt", "factor", Percent.class, Double.class);
    }

    @Override
    public ElPriceMWhForecast selectElPriceForecast(Date fr, Date to) {

        return selectCombinedElForecast(fr, to, "combined_el_prices");
    }

    @Override
    public OutdoorLightForecast selectOutdoorLightForecast(Date fr, Date to) {
        return new OutdoorLightForecast(selectSampleList(fr, to, "combined_light_forecast", "wattm2", WattSqrMeter.class, Double.class));
    }

    @Override
    public Sample<WattSqrMeter> selectInstalledLampEffect(Date t) {
        return selectSample(t, "lamp_effect", "wattm2", WattSqrMeter.class, Double.class, 60d);
    }

    @Override
    public List<Sample<WattSqrMeter>> selectInstalledLampEffect(Date fr, Date to) {
        return selectSampleList(fr, to, "lamp_effect", "wattm2", WattSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MolSqrMeter> selectExpectedNaturalPARSumToday(Date t) {
        return selectSample(t, "expected_natural_par_sum_today", "mol_m2", MolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MolSqrMeter>> selectExpectedNaturalPARSumToday(Date fr, Date to) {
        return selectSampleList(fr, to, "expected_natural_par_sum_today", "mol_m2", MolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MolSqrMeter> selectExpectedNaturalPARSumPeriod(Date t) {
        return selectSample(t, "expected_natural_par_sum_period", "mol_m2", MolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MolSqrMeter>> selectExpectedNaturalPARSumPeriod(Date fr, Date to) {
        return selectSampleList(fr, to, "expected_natural_par_sum_period", "mol_m2", MolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MMolSqrMeter> selectExpectedNaturalPhotoSumToday(Date t) {
        return selectSample(t, "expected_natural_photo_sum_today", "mmol_m2", MMolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MMolSqrMeter>> selectExpectedNaturalPhotoSumToday(Date fr, Date to) {
        return selectSampleList(fr, to, "expected_natural_photo_sum_today", "mmol_m2", MMolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<UMolSqrtMeterSecond> selectLampIntensity(Date t) {
        return selectSample(t, "lamp_intensity", "umol_m2_s", UMolSqrtMeterSecond.class, Double.class, 100d);
    }

    @Override
    public List<Sample<UMolSqrtMeterSecond>> selectLampIntensity(Date fr, Date to) {
        return selectSampleList(fr, to, "lamp_intensity", "umol_m2_s", UMolSqrtMeterSecond.class, Double.class);
    }

    @Override
    public Sample<Duration> selectLightTimeAchievedToday(Date t) {
        return selectSample(t, "light_time_achieved_today", "ms", Duration.class, Long.class, 0l);
    }

    @Override
    public List<Sample<Duration>> selectLightTimeAchievedToday(Date fr, Date to) {
        return selectSampleList(fr, to, "light_time_achieved_today", "ms", Duration.class, Long.class);
    }

    @Override
    public List<Sample<Duration>> selectLightTimeAchievedPeriod(Date fr, Date to) {
        return selectSampleList(fr, to, "light_time_achieved_period", "ms", Duration.class, Long.class);
    }

    @Override
    public Sample<Percent> selectScreenPhotoRatio(Date t) {
        return selectSample(t, "screen_photo_loss_ratio", "pct", Percent.class, Double.class, 10d);
    }

    @Override
    public Sample<Percent> selectScreenClosingPct(Date t) {
        return selectSample(t, "screen_closing_pct", "pct", Percent.class, Double.class, 97d);
    }

    @Override
    public Sample<Percent> selectLightTransmissionFactor(Date t) {
        return selectSample(t, "light_transmission_factor", "factor", Percent.class, Double.class, 60d);
    }

    @Override
    public List<Sample<Percent>> selectLightTransmissionFactor(Date fr, Date to) {
        return selectSampleList(fr, to, "light_transmission_factor", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<MolSqrMeter> selectPARSumAchievedToday(Date t) {
        return selectSample(t, "par_sum_achieved_today", "mol_m2", MolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MolSqrMeter>> selectPARSumAchievedToday(Date fr, Date to) {
        return selectSampleList(fr, to, "par_sum_achieved_today", "mol_m2", MolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MolSqrMeter> selectPARSumAchievedPeriod(Date t) {
        return selectSample(t, "par_sum_achieved_period", "mol_m2", MolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MolSqrMeter>> selectPARSumAchievedPeriod(Date from, Date to) {
        return selectSampleList(from, to, "par_sum_achieved_period", "mol_m2", MolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MolSqrMeter> selectPARSumDayGoal(Date t) {
        return selectSample(t, "par_sum_day_goal", "mol_m2", MolSqrMeter.class, Double.class, 10d);
    }

    @Override
    public List<Sample<MolSqrMeter>> selectPARSumDayGoal(Date fr, Date to) {
        return selectSampleList(fr, to, "par_sum_day_goal", "mol_m2", MolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MMolSqrMeter> selectPhotoSumAchievedToday(Date t) {
        return selectSample(t, "photo_sum_achieved_today", "mmol_m2", MMolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MMolSqrMeter>> selectPhotoSumAchievedToday(Date fr, Date to) {
        return selectSampleList(fr, to, "photo_sum_achieved_today", "mmol_m2", MMolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MMolSqrMeter> selectPhotoSumAchievedPeriod(Date t) {
        return selectSample(t, "photo_sum_achieved_period", "mmol_m2", MMolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public List<Sample<MMolSqrMeter>> selectPhotoSumAchievedPeriod(Date fr, Date to) {
        return selectSampleList(fr, to, "photo_sum_achieved_period", "mmol_m2", MMolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<MMolSqrMeter> selectPhotoSumDayGoal(Date t) {
        return selectSample(t, "photo_sum_day_goal", "mmol_m2", MMolSqrMeter.class, Double.class, 600d);
    }

    @Override
    public List<Sample<MMolSqrMeter>> selectPhotoSumDayGoal(Date fr, Date to) {
        return selectSampleList(fr, to, "photo_sum_day_goal", "mmol_m2", MMolSqrMeter.class, Double.class);
    }

    @Override
    public Sample<Percent> selectScreenTransmissionFactor(Date t) {
        return selectSample(t, "screen_transmission_factor", "factor", Percent.class, Double.class, 20d);
    }

    @Override
    public List<Sample<Percent>> selectScreenTransmissionFactor(Date fr, Date to) {
        return selectSampleList(fr, to, "screen_transmission_factor", "factor", Percent.class, Double.class);
    }

    @Override
    public List<Sample<NegotiationResult>> selectNegotiationResult(Date fr, Date to) {
        List<Sample<NegotiationResult>> r = new ArrayList<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, generations, duration_ms "
                + "FROM negotiation_statistics "
                + "WHERE context_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time")) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                NegotiationResult nr = new NegotiationResult(
                        rs.getInt(2),
                        rs.getLong(3));

                r.add(new Sample<>(d, nr));
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select NegotiationResult list from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<NegotiationResult> selectNegotiationResult(Date t) {

        Sample<NegotiationResult> r = new Sample<>(new NegotiationResult(0, 0));

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, generations, duration_ms"
                + "FROM negotiation_statistics "
                + "WHERE context_id = ? "
                + "AND time <= ? "
                + "ORDER BY time DESC")) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(t));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                NegotiationResult nr = new NegotiationResult(
                        rs.getInt(2),
                        rs.getLong(3));
                r = new Sample<>(d, nr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select NegotiationResult from DB.", ex);
        }

        return r;
    }

    @Override
    public Sample<Energy> selectEnergyBalance(Date t) {
        return selectSample(t, "energy_balance", "balance", Energy.class, Double.class, 0d);
    }

    @Override
    public List<Sample<Energy>> selectEnergyBalance(Date fr, Date to) {
        return selectSampleList(fr, to, "energy_balance", "balance", Energy.class, Double.class);
    }

    @Override
    public Sample<UMolSqrtMeterSecond> selectActualPhoto(Date t) {
        return selectSample(t, "actual_photo", "mmol_m2_s", UMolSqrtMeterSecond.class, Double.class, 0d);
    }

    @Override
    public List<Sample<UMolSqrtMeterSecond>> selectActualPhoto(Date fr, Date to) {
        return selectSampleList(fr, to, "actual_photo", "mmol_m2_s", UMolSqrtMeterSecond.class, Double.class);
    }

    @Override
    public Sample<UMolSqrtMeterSecond> selectOptimalPhoto(Date t) {
        return selectSample(t, "optimal_photo", "mmol_m2_s", UMolSqrtMeterSecond.class, Double.class, 0d);
    }

    @Override
    public List<Sample<UMolSqrtMeterSecond>> selectOptimalPhoto(Date fr, Date to) {
        return selectSampleList(fr, to, "optimal_photo", "mmol_m2_s", UMolSqrtMeterSecond.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectPreferredDayTemperature(Date t) {
        return selectSample(t, "preferred_day_temp", "celcius", Celcius.class, Double.class, 19d);
    }

    @Override
    public List<Sample<Celcius>> selectPreferredDayTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_day_temp", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectPreferredNightTemperature(Date t) {
        return selectSample(t, "preferred_night_temp", "celcius", Celcius.class, Double.class, 18d);
    }

    @Override
    public List<Sample<Celcius>> selectPreferredNightTemperature(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_night_temp", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<PPM> selectPreferredDayCO2(Date t) {
        return selectSample(t, "preferred_day_co2", "ppm", PPM.class, Double.class, 750d);
    }

    @Override
    public List<Sample<PPM>> selectPreferredDayCO2(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_day_co2", "ppm", PPM.class, Double.class);
    }

    @Override
    public Sample<PPM> selectPreferredNightCO2(Date t) {
        return selectSample(t, "preferred_night_co2", "ppm", PPM.class, Double.class, 350d);
    }

    @Override
    public List<Sample<PPM>> selectPreferredNightCO2(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_night_co2", "ppm", PPM.class, Double.class);
    }

    @Override
    public Sample<Percent> selectPreferredDayScreenPosition(Date t) {
        return selectSample(t, "preferred_day_screen_position", "factor", Percent.class, Double.class, 0d);
    }

    @Override
    public List<Sample<Percent>> selectPreferredDayScreenPosition(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_day_screen_position", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Percent> selectPreferredNightScreenPosition(Date t) {
        return selectSample(t, "preferred_night_screen_position", "factor", Percent.class, Double.class, 0d);
    }

    @Override
    public List<Sample<Percent>> selectPreferredNightScreenPosition(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_night_screen_position", "factor", Percent.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectPreferredDayVentilationSP(Date t) {
        return selectSample(t, "preferred_day_ventilation_sp", "celcius", Celcius.class, Double.class, 28d);
    }

    @Override
    public List<Sample<Celcius>> selectPreferredDayVentilationSP(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_day_ventilation_sp", "celcius", Celcius.class, Double.class);
    }

    @Override
    public Sample<Celcius> selectPreferredNightVentilationSP(Date t) {
        return selectSample(t, "preferred_night_ventilation_sp", "celcius", Celcius.class, Double.class, 24d);
    }

    @Override
    public List<Sample<Celcius>> selectPreferredNightVentilationSP(Date fr, Date to) {
        return selectSampleList(fr, to, "preferred_night_ventilation_sp", "celcius", Celcius.class, Double.class);
    }

    //
    // Deletes
    //
    @Override
    public void deleteChart(ChartConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "DELETE FROM chart_configuration "
                + "WHERE id = ?")) {
            ps.setInt(1, cfg.getID());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not delete Chart from DB.", ex);
        }
    }

    @Override
    public Double selectConcernMinFitness(String concernID, Date fr, Date to) {

        Double r = 0.0;
        try (PreparedStatement ps = con.prepareStatement(
                "SELECT MIN(fitness) "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? ")) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                r = rs.getDouble(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernMinFitness from DB.", ex);
        }
        return r;
    }

    @Override
    public Double selectConcernMaxFitness(String concernID, Date fr, Date to) {

        Double r = 1.0;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT MAX(fitness) "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? ")) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                r = rs.getDouble(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernMaxFitness from DB.", ex);
        }
        return r;
    }

    @Override
    public Map<Date, List<ConcernNegotiationResult>> selectConcernsNegotiationResult(Date fr, Date to) {

        Map<Date, List<ConcernNegotiationResult>> r = new TreeMap<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, concern_id, fitness, value_help "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time")) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));

                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                ConcernNegotiationResult cnr
                        = new ConcernNegotiationResult(rs.getString(2),
                                rs.getDouble(3),
                                valueHelp);

                if (r.containsKey(d)) {
                    r.get(d).add(cnr);
                } else {
                    List<ConcernNegotiationResult> l = new ArrayList<>();
                    l.add(cnr);
                    r.put(d, l);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernsNegotiationResult from DB.", ex);
        }

        return r;
    }

    @Override
    public void insertProposedLightPlan(LightPlan sample) {

        Date time = sample.getElementStart(0);

        // Delete current data covered by light on first iteration.
        try (PreparedStatement ps1 = con.prepareStatement(
                "DELETE FROM proposed_light_plan WHERE context_id = ? AND time >= ?");
                PreparedStatement ps2 = con.prepareStatement(
                        "INSERT INTO proposed_light_plan "
                        + "(context_id, time, light_plan) "
                        + "VALUES (?, ?, ?)");) {

            ps1.setInt(1, contextID);
            ps1.setTimestamp(2, JDBC.toTime(time));
            ps1.executeUpdate();

            // Insert new data.
            ps2.setInt(1, contextID);
            ps2.setTimestamp(2, JDBC.toTime(time));
            ps2.setString(3, gson.toJson(sample));

            ps2.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert ProposedLightPlan into DB.", ex);
        }
    }

    @Override
    public LightPlan selectProposedLightPlan(Date from, Duration lightInterval) {

        // Default light plan
        LightPlan r = new LightPlan(from, endOfXDaysAfter(from, WINDOW_SIZE - 1), lightInterval);

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT light_plan "
                + "FROM proposed_light_plan "
                + "WHERE context_id = ? "
                + "AND time <= ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(startOfHour(from)));

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                r = gson.fromJson(rs.getString(1), LightPlan.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ProposedLightPlan from DB.", ex);
        }
        return r;
    }

    @Override
    public void insertPhotoBalanceYesterday(Sample<MMolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "photo_balance_yesterday", "mmol_m2");
    }

    @Override
    public Sample<MMolSqrMeter> selectPhotoBalanceYesterday(Date t) {
        return selectSample(t, "photo_balance_yesterday", "mmol_m2", MMolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public Sample<MolSqrMeter> selectPARBalanceYesterday(Date t) {
        return selectSample(t, "par_balance_yesterday", "mol_m2", MolSqrMeter.class, Double.class, 0d);
    }

    @Override
    public void insertPARBalanceYesterday(Sample<MolSqrMeter> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "par_balance_yesterday", "mol_m2");
    }

    @Override
    public LightPlanConfig selectLightPlanConfig() {
        LightPlanConfig r = new LightPlanConfig.Builder().build();

        try (
                PreparedStatement ps = con.prepareStatement(
                        "SELECT light_plan_config "
                        + "FROM light_plan_configuration "
                        + "WHERE context_id = ? "
                        + "ORDER BY time DESC")) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                r = gson.fromJson(rs.getString(1), LightPlanConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).
                    log(Level.SEVERE, "Could not select LightPlanConfig from DB.", ex);
        }

        // Insert default
        if (r == null) {
            insertLightPlanConfig(r);
        }
        return r;
    }

    @Override
    public void insertLightPlanConfig(LightPlanConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO light_plan_configuration "
                + "(context_id, time, light_plan_config) "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).
                    log(Level.SEVERE, "Could not insert LightPlanConfig into DB.", ex);
        }
    }

    @Override
    public Sample<UMolSqrtMeterSecond> selectMinLightForArtLight(Date t) {
        return selectSample(t, "min_light_art_light_intensity", "umol_m2_s", UMolSqrtMeterSecond.class, Double.class, 100d);
    }

    @Override
    public void insertMinLightForArtLight(Sample<UMolSqrtMeterSecond> s) {
        insertSample(s.getTimestamp(), s.getSample().value(),
                "min_light_art_light_intensity", "umol_m2_s");

    }

    @Override
    public List<Sample<UMolSqrtMeterSecond>> selectMinLightForArtLight(Date fr, Date to) {
        return selectSampleList(fr, to, "min_light_art_light_intensity", "umol_m2_s", UMolSqrtMeterSecond.class, Double.class);
    }

    @Override
    public void updateIndoorLightConfig(IndoorLightConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE indoor_light_configuration "
                + "SET indoor_light_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not update IndoorLightConfig in DB.", ex);
        }

    }

    @Override
    public IndoorLightConfig selectIndoorLightConfig() {
        IndoorLightConfig r = null;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT indoor_light_config "
                + "FROM indoor_light_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), IndoorLightConfig.class);
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select IndoorLightConfig from DB.", ex);
        }

        // Insert default
        if (r == null) {
            r = new IndoorLightConfig();
            insertIndoorLightConfig(r);
        }
        return r;
    }

    @Override
    public void insertIndoorLightConfig(IndoorLightConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO indoor_light_configuration "
                + "(context_id, indoor_light_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert IndoorLightConfig into DB.", ex);
        }
    }

    @Override
    public ElSpotPriceForecastConfig selectElSpotPriceForecastConfig() {
        ElSpotPriceForecastConfig r = null;
        try (PreparedStatement ps = con.prepareStatement(
                "SELECT el_spotprice_config "
                + "FROM el_spotprice_configuration "
                + "WHERE context_id = ? ");) {

            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = gson.fromJson(rs.getString(1), ElSpotPriceForecastConfig.class);
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, format("Could not select %s from DB.", ElSpotPriceForecastConfig.class.getSimpleName()), ex);
        }

        // Insert default
        if (r == null) {
            r = new ElSpotPriceForecastConfig();
            insertElSpotPriceForecastConfig(r);

        }
        return r;
    }

    @Override
    public void updateElSpotPriceForecastConfig(ElSpotPriceForecastConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE el_spotprice_configuration "
                + "SET el_spotprice_config = ? "
                + "WHERE context_id = ?")) {

            ps.setString(1, gson.toJson(cfg));
            ps.setInt(2, contextID);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, format("Could not update %s in DB.", ElSpotPriceForecastConfig.class.getSimpleName()), ex);
        }
    }

    private void insertElSpotPriceForecastConfig(ElSpotPriceForecastConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO el_spotprice_configuration "
                + "(context_id, el_spotprice_config) "
                + "VALUES (?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, format("Could not insert %s into DB.", ElSpotPriceForecastConfig.class.getSimpleName()), ex);
        }
    }

    @Override
    public ElPriceMWhForecast selectElSpotPriceForecast(Date from, Date to) {
        return selectCombinedElForecast(from, to, "combined_el_spotprices");
    }

    @Override
    public void insertElSpotPriceForecast(Sample<ElPriceMWhForecast> s) {
        insertCombinedElForecast(s, "combined_el_spotprices");
    }

    @Override
    public void insertHybridElPriceForecast(Sample<ElPriceMWhForecast> s) {
        insertCombinedElForecast(s, "hybrid_el_prices");
    }

    @Override
    public ElPriceMWhForecast selectHybridElPriceForecast(Date fr, Date to) {
        return selectCombinedElForecast(fr, to, "hybrid_el_prices");
    }

    @Override
    public void insertMinCO2(Sample<PPM> sample) {
        insertSample(sample.getTimestamp(), sample.getSample().value(),
                "min_co2", "ppm");
    }

    @Override
    public Sample<PPM> selectMinCO2(Date t) {
        return selectSample(t, "min_co2", "ppm", PPM.class, Double.class, 300.0);

    }

    @Override
    public void insertMaxCO2(Sample<PPM> sample) {
        insertSample(sample.getTimestamp(), sample.getSample().value(),
                "max_co2", "ppm");
    }

    @Override
    public Sample<PPM> selectMaxCO2(Date t) {
        return selectSample(t, "max_co2", "ppm", PPM.class, Double.class, 1500.0);
    }

    @Override
    public Sample<TimeStamp> selectFrameStart(Date t) {
        return selectSample(t, "frame_start", "timest", TimeStamp.class, Date.class, startOfDay(t));
    }

    @Override
    public void insertFrameStart(Date t, TimeStamp newStartDate) {
        insertSample(t, newStartDate.toDate(), "frame_start", "timest");
    }

    @Override
    public Sample<TimeStamp> selectFrameEnd(Date t) {
        return selectSample(t, "frame_end", "timest", TimeStamp.class, Date.class, endOfXDaysAfter(t, WINDOW_SIZE - 1));
    }

    @Override
    public void insertFrameEnd(Date t, TimeStamp newStartDate) {
        insertSample(t, newStartDate.toDate(), "frame_end", "timest");
    }

    //
    // PRIVATE
    //
    private <Unit, V> Sample<Unit> selectSample(Date t, String table, String unit, Class<Unit> unitClazz, Class<V> valClazz, V defaultValue) {

        Sample<Unit> r = null;
        try {
            // try default value
            r = new Sample<>(t, unitClazz.getDeclaredConstructor(valClazz).newInstance(defaultValue));

            try (PreparedStatement ps = con.prepareStatement(
                    "SELECT time, " + unit
                    + " FROM " + table
                    + " WHERE context_id = ?"
                    + " AND time <= ? "
                    + "ORDER BY time DESC")) {

                ps.setMaxRows(1);
                ps.setInt(1, contextID);
                ps.setTimestamp(2, JDBC.toTime(t));

                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    Date d = JDBC.fromTime(rs.getTimestamp(1));
                    V v = rs.getObject(2, valClazz);
                    Unit u = unitClazz.getDeclaredConstructor(valClazz).newInstance(v);
                    r = new Sample<>(d, u);
                }

            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).
                        log(Level.SEVERE, "Could not select " + table + " from DB.", ex);
            }

        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(DerbyContextDataAccess.class.getName()).
                    log(Level.SEVERE, "Could not init sample because of type problems.", ex);
        }
        return r;
    }

    private <U, V> List<Sample<U>> selectSampleList(Date fr, Date to, String tableName, String unitName, Class<U> unitClazz, Class<V> valClazz) {

        List<Sample<U>> r = new ArrayList<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, " + unitName
                + " FROM " + tableName
                + " WHERE context_id = ?"
                + " AND time >= ? AND time < ?"
                + " ORDER BY time");) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                V v = rs.getObject(2, valClazz);
                U u = unitClazz.getDeclaredConstructor(valClazz).newInstance(v);
                r.add(new Sample<>(d, u));
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select " + tableName + " from DB.", ex);
        }
        return r;
    }

    private void insertCombinedElForecast(Sample<ElPriceMWhForecast> s, String tableName) {
        boolean first = true;
        try (PreparedStatement ps1 = con.prepareStatement(
                format("DELETE FROM %s WHERE context_id = ? AND time >= ?", tableName));
                PreparedStatement ps2 = con.prepareStatement(
                        format("INSERT INTO %s "
                                + "(context_id, time, price, currency, area) "
                                + "VALUES (?, ?, ?, ?, ?)", tableName));) {

            for (Entry<Date, MoneyMegaWattHour> entry : s.getSample().getForecastData().entrySet()) {
                Date time = entry.getKey();
                MoneyMegaWattHour price = entry.getValue();

                // Delete current data covered by forecast on first iteration.
                if (first) {
                    ps1.setInt(1, contextID);
                    ps1.setTimestamp(2, JDBC.toTime(time));
                    ps1.executeUpdate();
                }

                first = false;

                // Insert new data.
                ps2.setInt(1, contextID);
                ps2.setTimestamp(2, JDBC.toTime(time));
                setMoneyMegaWattHours(ps2, 3, price);
                ps2.setString(4, s.getSample().getCurrency());
                ps2.setString(5, s.getSample().getArea());
                ResultSet t = ps2.getGeneratedKeys();
                ps2.executeUpdate();
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert CombinedElPriceForecast into DB.", ex);
        }
    }

    private ElPriceMWhForecast selectCombinedElForecast(Date fr, Date to, String tableName) {
        Map<Date, Double> samples = new TreeMap<>();
        String currency = null;
        String area = null;

        try (PreparedStatement ps = con.prepareStatement(
                format("SELECT time, price, currency, area "
                        + "FROM %s "
                        + "WHERE context_id = ? "
                        + "AND time >= ? AND time < ? "
                        + "ORDER BY time", tableName))) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            ResultSet rs = ps.executeQuery();

            samples = new TreeMap<>();
            while (rs.next()) {
                Date d = JDBC.fromTime(rs.getTimestamp(1));
                Double t = rs.getDouble(2);

                currency = rs.getString(3);
                area = rs.getString(4);
                samples.put(d, t);
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select CombinedElForecast list from DB.", ex);
        }

        return new ElPriceMWhForecast(samples, area, currency);
    }

    private void insertSample(Date time, Object value, String tableName, String unitName) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO " + tableName
                + "(context_id, time, " + unitName + ") "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(time));

            ps.setObject(3, value);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert " + tableName + " to DB.", ex);
        }
    }

    private static Switch newSwitchIfNotNull(Short v) {
        return v == null ? null : v != 0 ? Switch.ON : Switch.OFF;
    }

    private static Duration newDurationIfNotNull(Long v) {
        return v == null ? null : new Duration(v);
    }

    private static MoneyMegaWattHour newMoneyMegaWattHourIfNotNull(Double v) {
        return v == null ? null : new MoneyMegaWattHour(v, "DKK");
    }

    private void setWattM2(PreparedStatement ps, int i, WattSqrMeter v)
            throws SQLException {
        if (v == null) {
            ps.setNull(i, Types.DOUBLE);
        } else {
            ps.setDouble(i, v.value());
        }
    }

    private void setSwitch(PreparedStatement ps, int i, Switch v)
            throws SQLException {
        if (v == null) {
            ps.setNull(i, Types.SMALLINT);
        } else {
            ps.setShort(i, (short) (v.isOn() ? 1 : 0));
        }
    }

    private void setMoneyMegaWattHours(PreparedStatement ps, int i, MoneyMegaWattHour v)
            throws SQLException {
        if (v == null) {
            ps.setNull(i, Types.DOUBLE);
        } else {
            ps.setDouble(i, v.getValue().value());
        }
    }

    @Override
    public PrivaConfig getPrivaConfiguration() {
        PrivaConfig cfg = new PrivaConfig();

        try (PreparedStatement ps = con.prepareStatement(
        "SELECT time, pcu, indoorlight_scad, co2_scad, heating_scad, humidity_scad, light_groups, indoortemp_scad, outdoorlight_scad, outdoortemp_scad, vent_scad "
                + "FROM priva_configuration WHERE context_id = ? ORDER BY time DESC")) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cfg.setPcuName(rs.getString(2));
                cfg.setIndoor_light_intensity_scad_id(rs.getString(3));
                cfg.setCo2_scad_id(rs.getString(4));
                cfg.setHeating_threshold_scad_id(rs.getString(5));
                cfg.setHumidity_scad_id(rs.getString(6));
                cfg.setLightGroups(rs.getString(7));
                cfg.setIndoor_temp_scad_id(rs.getString(8));
                cfg.setOutdoor_light_intensity_scad_id(rs.getString(9));
                cfg.setOutdoor_temp_scad_id(rs.getString(10));
                cfg.setVentilation_threshold_scad_id(rs.getString(11));
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve PrivaConfiguration from DB.", ex);
        }
        return cfg;
    }

    @Override
    public void setPrivaConfiguration(PrivaConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
        "INSERT INTO priva_configuration (context_id, "
        + "time, pcu, indoorlight_scad, co2_scad, heating_scad, humidity_scad, light_groups, indoortemp_scad, outdoorlight_scad, outdoortemp_scad, vent_scad) "                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));

            ps.setString(3, cfg.getPcuName());
            ps.setString(4, cfg.getIndoor_light_intensity_scad_id());
            ps.setString(5, cfg.getCo2_scad_id());
            ps.setString(6, cfg.getHeating_threshold_scad_id());
            ps.setString(7, cfg.getHumidity_scad_id());
            ps.setString(8, cfg.getLightGroupsAsString());
            ps.setString(9, cfg.getIndoor_temp_scad_id());
            ps.setString(10, cfg.getOutdoor_light_intensity_scad_id());
            ps.setString(11, cfg.getOutdoor_temp_scad_id());
            ps.setString(12, cfg.getVentilation_threshold_scad_id());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save PrivaConfiguration to DB.", ex);
        }
    }

}
