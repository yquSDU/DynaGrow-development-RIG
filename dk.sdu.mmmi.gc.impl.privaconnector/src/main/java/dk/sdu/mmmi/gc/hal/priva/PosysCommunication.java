/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva;

import com.sun.jna.Memory;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;
import dk.sdu.mmmi.gc.hal.priva.acquaintance.PrivaCommException;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaListValue;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaConversionInfo;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaFormattedByteValue;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaValueList;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author erso Created on 26-05-2010, 11:08:21
 */
public class PosysCommunication {

    // Declared as Singleton class
    private static PosysCommunication instance = null;
    private ShortByReference writeId = new ShortByReference((short) 0);

    private PosysCommunication() {
    }

    public static PosysCommunication getInstance() {
        if (instance == null) {
            instance = new PosysCommunication();
        }
        return instance;
    }
    // Error codes from POSYS.H:
    private static String[] POSYS_ERRORS = new String[]{
        "PS_E_OK", "PS_E_SIZE", "PS_E_INIT", "PS_E_COMMUNICATION",
        "PS_E_OPEN", "PS_E_HANDLE", "PS_E_RANGE", "PS_E_SCAN",
        "PS_E_READ", "PS_E_WRITE", "PS_E_READ_ONLY", "PS_E_IN_USE",
        "PS_E_NOT_ALLOWED", "PS_E_INDEX", "PS_E_TYPE", "PS_E_OUTDATED",
        "PS_E_NOT_SUPPORTED", "PS_E_NOT_HIDABLE"
    };
    // Constants from POSYS.H:
    private static final int PS_HOST_STRING_SIZE = 13;		// \0 inclusive
    private static final int PS_SCAD_STRING_SIZE = 25;		// \0 inclusive
    private static final int PS_VALUE_STRING_SIZE = 25;		// \0 inclusive (same len as scad)
    private static final int PS_UNIT_STRING_SIZE = 20;		// \0 inclusive
    private static final int PS_LOCATION_STRING_SIZE = 32;	// \0 inclusive
    private static final int PS_NAME_STRING_SIZE = 20;		// \0 inclusive
    private static final int PS_VERSION_KEY_SIZE = 10;		// \0 inclusive
    private static final int PS_TXT_STRING_SIZE = 40;		// \0 inclusive
    private static final int PS_HUGE_STRING_SIZE = 512;		// \0 inclusive
    private static final int PS_CAT_STR_LEN = 12;		// \0 inclusive
    private static final int PS_FORMAT_STRING_SIZE = 20;
    private static final int PS_FORMULA_STRING_SIZE = 80;
    private String[] scadList;
    private Memory memory;

    private ShortByReference pres_handle = new ShortByReference();

    private int numScads = 0;

    public void getAlloc(String[] scadList) throws PrivaCommException {
        this.scadList = scadList;
        IntByReference allocSize = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_get_alloc_size(scadList.length, allocSize);
        if (err == 0) {
            memory = new Memory(allocSize.getValue());
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_alloc_size(): Error Code ({0}) {1}\tAllocated bytes: {2}\n", new Object[]{err, POSYS_ERRORS[err], allocSize.getValue()});;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to allocate memory. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }

    }

    public void open(String host) throws PrivaCommException {
        IntByReference ri = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_open(host, memory, scadList, scadList.length, ri);
        if (err == 0) {
            numScads = ri.getValue();
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_open(): Error Code ({0}) {1}\tSuccesful SCAD decoded: {2}", new Object[]{err, POSYS_ERRORS[err], ri.getValue()});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to open connection. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public String[] readValues() throws PrivaCommException {
        String[] results = new String[scadList.length];
        Arrays.fill(results, getEmptyValueString());
        IntByReference ri = new IntByReference();

        int err = PosysLibrary.INSTANCE.ps_read_values(memory, results, ri);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_read_values(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return results;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to read values. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public int[] readRawValues() throws PrivaCommException {
        //long[] results = new long[scadList.length];
        int[] results = new int[scadList.length];

        IntByReference ri = new IntByReference();

        int err = PosysLibrary.INSTANCE.psx_read_raw_values(memory, results, ri);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_read_raw_values(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return results;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to read raw values. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public void close() throws PrivaCommException {
        int err = PosysLibrary.INSTANCE.ps_close();
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_close(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to close connection. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    private String getEmptyValueString() {
        char[] chars = new char[PS_VALUE_STRING_SIZE];
        return new String(chars);
    }

    public void getWritePermissions(String host) throws PrivaCommException {
//    int ps_get_write_permission (String host_name, ShortByReference write_id);
        int err = PosysLibrary.INSTANCE.ps_get_write_permission(host, writeId);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_write_permission(): Error Code ({0}) {1}\nWrite id: {2}", new Object[]{err, POSYS_ERRORS[err], writeId.getValue()});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get write permission. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public void writeValues(String[] values) throws PrivaCommException {
        //int psw_write_values
        //(Pointer hdl, String[] values, IntByReference ri, short write_id);
        IntByReference ri = new IntByReference();
        int err = PosysLibrary.INSTANCE.psw_write_values(memory, values, ri, writeId.getValue());
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psw_write_values(): Error Code ({0}) {1}\nValue: {2}\nSuccesses: {3}", new Object[]{err, POSYS_ERRORS[err], values[0], ri.getValue()});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to write values. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public void releaseWritePermissions(String host) throws PrivaCommException {
//    int ps_release_write_permission (String host_name, ShortByReference write_id);
        int err = PosysLibrary.INSTANCE.ps_release_write_permission(host, writeId);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_release_write_permission(): Error Code ({0}) {1}\nWrite id: {2}", new Object[]{err, POSYS_ERRORS[err], writeId.getValue()});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to release write permission. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public String getVersionKey(String host) throws PrivaCommException {
        byte[] version = new byte[PS_VERSION_KEY_SIZE];

        int err = PosysLibrary.INSTANCE.ps_get_version_key(host, version);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_version_key(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return new String(version);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to write values. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public String[] getLimits(int idx) throws PrivaCommException {
        byte[] lower = new byte[PS_UNIT_STRING_SIZE];
        byte[] upper = new byte[PS_UNIT_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.ps_get_limits(memory, idx, lower, upper);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_limits(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return convertToStringArray(lower, upper);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get limits. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public PrivaConversionInfo getGetConversionInfo(int idx) throws PrivaCommException {
        byte[] format = new byte[PS_FORMAT_STRING_SIZE];
        byte[] formula = new byte[PS_FORMULA_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.psx_get_conv_info(memory, idx, format, formula);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_get_conv_info(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});

            return new PrivaConversionInfo(new String(formula), new String(format));
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get conversion info. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public String getInputFormula(int idx) throws PrivaCommException {
        byte[] formula = new byte[PS_FORMULA_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.psx_get_input_formula(memory, idx, formula);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_get_input_formula(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return new String(formula);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get input formula. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public boolean isNumeric(int idx) throws PrivaCommException {
        IntByReference res = new IntByReference();
        int err = PosysLibrary.INSTANCE.psx_isnumeric(memory, idx, res);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_isnumeric(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return res.getValue() == 1;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get information on type. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public boolean isAttrib(int idx) throws PrivaCommException {
        IntByReference res = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_isattrib(memory, idx, res);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_isattrib(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return res.getValue() == 1;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to information on attribute. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public void getPresHandle(int idx) throws PrivaCommException {
        int err = PosysLibrary.INSTANCE.psx_get_pres_hdl(memory, idx, pres_handle);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_get_pres_hdl(): Error Code ({0}) {1}. Value of handle: {2}", new Object[]{err, POSYS_ERRORS[err], pres_handle.getValue()});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get presentation handle. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public String[] getPresInfo(String host) throws PrivaCommException {
        byte[] format = new byte[PS_FORMAT_STRING_SIZE];
        byte[] input_formula = new byte[PS_FORMULA_STRING_SIZE];
        byte[] output_formula = new byte[PS_FORMULA_STRING_SIZE];
        byte[] unit_str = new byte[PS_UNIT_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.psx_get_pres_info(host, pres_handle.getValue(), format, input_formula, output_formula, unit_str);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_get_pres_info(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get presentation info. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
        return convertToStringArray(format, input_formula, output_formula, unit_str);
    }

    public String[] getDescription(int idx) throws PrivaCommException {
        byte[] location = new byte[PS_LOCATION_STRING_SIZE];
        byte[] name = new byte[PS_NAME_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.ps_get_description(memory, idx, location, name);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_get_description: Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return convertToStringArray(location, name);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get description. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }
    //    int psx_format(int raw, String format, String formula, byte[] res);

    //  int psx_scan (String value, String format, String formula, int raw);
    public String rawToFormatted(int raw, PrivaConversionInfo conversionInfo) throws PrivaCommException {
        byte[] res = new byte[PS_VALUE_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.psx_format(raw, conversionInfo.getFormat(), conversionInfo.getFormula(), res);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_format(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return new String(res).trim();
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to convert to formatted value. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public int formattedToRaw(String value, String format, String formula) throws PrivaCommException {
        byte[] res = new byte[PS_VALUE_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.psx_scan(value, format, formula, res);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function psx_scan(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});

            ByteBuffer buffer = ByteBuffer.wrap(res);
            buffer.order(ByteOrder.LITTLE_ENDIAN);  // if you want little-endian
            int result = buffer.getShort();
            return result;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to convert to raw value. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    private String[] convertToStringArray(byte[]  
        ... args)
    {
        String[] res = new String[args.length];
        int i = 0;
        for (byte[] arg : args) {
            res[i] = new String(args[i]);
            i++;
        }
        return res;
    }

    public PrivaFormattedByteValue attribSplit(int idx, String value) throws PrivaCommException {
        byte[] val = new byte[PS_VALUE_STRING_SIZE];
        //ByteByReference attr = new ByteByReference();
        byte[] attr = new byte[PS_UNIT_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.ps_attrib_split(memory, idx, value, val, attr);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_attrib_split(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return new PrivaFormattedByteValue(val, attr);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to split value and attribute. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public String attribMerge(byte[] val, byte[] attrib) throws PrivaCommException {
        byte[] combined = new byte[PS_VALUE_STRING_SIZE];
        int err = PosysLibrary.INSTANCE.ps_attrib_merge(combined, val, attrib);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_attrib_merge(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return new String(combined);
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to merge value and attribute. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");

        }
    }

    public boolean isWritePermissionRequired(String host) throws PrivaCommException {
        int err = PosysLibrary.INSTANCE.ps_is_write_permission_required(host);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_is_write_permission_required(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return false;
        } else {
            return true;
        }
    }

    public boolean isToggle(int idx) throws PrivaCommException {
        IntByReference res = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_istoggle(memory, idx, res);
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_istoggle(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return res.getValue() == 1;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to information on toggle. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public PrivaValueList createToggleValueList(int idx) throws PrivaCommException {
        IntByReference allocSize = new IntByReference();
        IntByReference count = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_get_toggle_size(memory, idx, count, allocSize);
        if (err == 0) {

            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_toggle_size(): Error Code ({0}) {1}\tAllocated bytes: {2}\n", new Object[]{err, POSYS_ERRORS[err], allocSize.getValue()});;
            return new PrivaValueList(count.getValue(), new Memory(allocSize.getValue()));
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get toggle list size. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public PrivaValueList configureToggleValueList(int idx, PrivaValueList list) throws PrivaCommException {
        int err = PosysLibrary.INSTANCE.ps_get_toggle_list(memory, idx, list.getSize());
        if (err == 0) {

            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_toggle_list(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return list;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get toggle list. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }
    
     public PrivaValueList createAttributeValueList(int idx) throws PrivaCommException {
        IntByReference allocSize = new IntByReference();
        IntByReference count = new IntByReference();
        int err = PosysLibrary.INSTANCE.ps_get_attrib_toggle_size(memory, idx, count, allocSize);
        if (err == 0) {

            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_attrib_toggle_size(): Error Code ({0}) {1}\tAllocated bytes: {2}\n", new Object[]{err, POSYS_ERRORS[err], allocSize.getValue()});;
            return new PrivaValueList(count.getValue(), new Memory(allocSize.getValue()));
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get attribute toggle list size. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public PrivaValueList configureAttributeValueList(int idx, PrivaValueList list) throws PrivaCommException {
        int err = PosysLibrary.INSTANCE.ps_get_attrib_toggle_list(memory, idx, list.getSize());
        if (err == 0) {
            Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_toggle_list(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            return list;
        } else {
            throw new PrivaCommException("Unexpected error code from Priva when trying to get attribute toggle list. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
        }
    }

    public PrivaValueList fillValueList(PrivaValueList list) throws PrivaCommException {

        for (int i = 0; i < list.getCount(); i++) {
            byte[] toggleName = new byte[PS_NAME_STRING_SIZE];
            byte[] val = new byte[4];
            int err = PosysLibrary.INSTANCE.ps_get_toggle(list.getSize(), i, toggleName, val);
            list.add(new PrivaListValue(toggleName, val));
            if (err == 0) {

                Logger.getLogger(PosysCommunication.class.getName()).log(Level.INFO, "Function ps_get_toggle(): Error Code ({0}) {1}", new Object[]{err, POSYS_ERRORS[err]});
            } else {
                throw new PrivaCommException("Unexpected error code from Priva when trying to get value from toggle list. Error Code (" + err + ") {" + POSYS_ERRORS[err] + "}");
            }
        }
        return list;
    }

    /**
     * @return the numScads
     */
    public int getNumScads() {
        return numScads;
    }

}
