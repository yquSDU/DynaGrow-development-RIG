package dk.sdu.mmmi.gc.control.light.par.gui;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class ConfigurePARSumAction extends AbstractAction implements Presenter.Popup {

    private final ControlDomain g;
    private final Input input;

    public ConfigurePARSumAction(ControlDomain g, Input input) {
        super("Configure");
        this.g = g;
        this.input = input;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        PARSumAchievedConfig cfg = context(g).one(PARSumAchievedConfig.class);

        PARSumAchievedConfigPanel pnl = new PARSumAchievedConfigPanel(cfg);
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure PAR sum achieved today");

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            updateConfig(pnl.get());
            input.setValue(cfg.getPARSum());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private void updateConfig(PARSumAchievedConfig cfg) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.updatePARSumAchievedConfig(cfg);
    }

}
