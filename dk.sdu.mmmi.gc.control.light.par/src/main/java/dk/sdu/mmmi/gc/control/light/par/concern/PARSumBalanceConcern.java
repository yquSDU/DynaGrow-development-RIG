package dk.sdu.mmmi.gc.control.light.par.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.WINDOW_SIZE;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.calcLightplanPARSum;
import dk.sdu.mmmi.gc.control.light.input.LampIntensityInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.par.input.ExpNaturalPARSumPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumDayGoalInput;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class PARSumBalanceConcern extends AbstractConcern {

    private MolSqrMeter totalInPlan;
    private MolSqrMeter balance;
    private MolSqrMeter expNatPARSum;
    private MolSqrMeter goal;
    private MolSqrMeter achievedPARSum;

    public PARSumBalanceConcern(ControlDomain g) {
        super("Achieve PAR sum balance", g, 5);
    }

    @Override
    public double evaluate(Solution s) {

        // Inputs
        // Goal window = DLI * (2 day past + today + 2 days future)
        goal = s.getValue(PARSumDayGoalInput.class).multiply(WINDOW_SIZE);
        achievedPARSum = s.getValue(PARSumAchievedPeriodInput.class);
        expNatPARSum = s.getValue(ExpNaturalPARSumPeriodInput.class);

        UMolSqrtMeterSecond lampIntensity = s.getValue(LampIntensityInput.class);

        LightPlan p = s.getValue(LightPlanTodayOutput.class);
        totalInPlan = calcLightplanPARSum(p, lampIntensity);

        // PAR sum balance today
        balance = achievedPARSum.add(expNatPARSum).add(totalInPlan).subtract(goal);

        return abs(balance.roundedValue());
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class,
                String.format("ExpectedNat(%.1f) + ExpectedArt(%.1f) + Achieved(%.1f)"
                        + " - Goal(%.1f) = Balance(%.1f)",
                        expNatPARSum.value(), totalInPlan.value(),
                        achievedPARSum.value(),
                        goal.value(), balance.value()));
        return r;
    }

}
