/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.gc.hal.priva.entities;

/**
 *
 * @author ancla
 */
public class PrivaConversionInfo {
    private final String formula;
    private final String format;
    
    public PrivaConversionInfo(String formula, String format)
    {
        this.formula = formula;
        this.format = format;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }
    
    @Override
    public String toString()
    {
        return "Format:[" + format.trim() + "]\tFormula:[" + formula.trim() + "]";
    }
}
