package dk.sdu.mmmi.gc.control.light.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author jcs
 */
public class LightTimeAchievedToday extends AbstractInput<Duration> {

    private static final Logger logger = getLogger(LightTimeAchievedToday.class.getName());
    private final ControlDomain g;

    public LightTimeAchievedToday(ControlDomain g) {
        super("Light-time achieved today");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        Duration d = retrieve(t);
        setValue(d);
        store(t, d);
    }

    private Duration retrieve(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        List<Sample<Switch>> lightPlan = db.selectLightStatus(DateUtil.startOfDay(t), t);
        long delay = context(g).one(ControlManager.class).getDelay().toMS();

        long totalLight = 0;
        for (Sample<Switch> hour : lightPlan) {
            if (hour.getSample().isOn()) {
                totalLight += delay;
            }
        }

        return new Duration(totalLight);
    }

    private void store(Date t, Duration ms) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertLightTimeAchievedToday(new Sample<>(t, ms));
    }
}
