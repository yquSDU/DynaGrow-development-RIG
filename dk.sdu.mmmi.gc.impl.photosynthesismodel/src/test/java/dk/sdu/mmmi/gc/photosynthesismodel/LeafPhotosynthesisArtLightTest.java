package dk.sdu.mmmi.gc.photosynthesismodel;


import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LeafPhotosynthesisArtLightTest {

    private LeafPhotosynthesisArtLight model;

    @Before
    public void setUp() throws Exception {
        model = new LeafPhotosynthesisArtLight();
    }

    @After
    public void tearDown() throws Exception {
        model = null;
    }

    @Test
    public void testCalculate() {

        double temp = 25;
        double co2 = 500;
        double par = 2300;

        double Pgn = model.calculate(temp, co2, par, new PlantSpecificPhotoParams(100, 1.1, 0.7, 0.3, 0.45, 50));

        System.out.println("" + Pgn);
        Assert.assertEquals(27.01730, Pgn, 0.1); // Confirmed by Oliver
    }
}
