package dk.sdu.mmmi.gc.gui.explorer;

import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.gui.actions.CreateChartAction;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 * @author mrj
 */
public class ChartsNode extends AbstractNode {

    private final Action prefferedAction;

    public ChartsNode(ControlDomain g) {
        super(Children.LEAF);
        setDisplayName("Charts");
        setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/chart_curve.png");

        this.prefferedAction = new CreateChartAction(g);

        setChildren(new ChartsNodeChildren(g));
    }

    @Override
    public Action getPreferredAction() {
        return prefferedAction;
    }

    @Override
    public Action[] getActions(boolean context) {
        return Lists.newArrayList(prefferedAction).toArray(new Action[]{});
    }
}
