package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import static org.openide.util.Utilities.actionsGlobalContext;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class CleanDataAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain domain;

    public CleanDataAction(ControlDomain d) {
        super("Delete climate data");
        this.domain = d;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {

        NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation(
                "Are you sure you want to delete all climate data in this greenhouse?",
                NotifyDescriptor.Confirmation.OK_CANCEL_OPTION);

        Object r = DialogDisplayer.getDefault().notify(nd);

        if (r.equals(NotifyDescriptor.OK_OPTION)) {
            Lookup l = actionsGlobalContext();
            for (ControlDomain g : l.lookupAll(ControlDomain.class)) {
                cleanClimateData(g);
            }

            NotifyDescriptor.Message msg = new NotifyDescriptor.Message("All climate data deleted.");
            DialogDisplayer.getDefault().notify(msg);
        }
    }

    @Override
    public boolean isEnabled() {
        return !getControlManager().isAutoRunning()
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/database_delete.png", false));
        return mi;
    }

    private void cleanClimateData(ControlDomain g) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.cleanClimateData();
    }

    private ControlManager getControlManager() {
        return context(domain).one(ControlManager.class);
    }
}
