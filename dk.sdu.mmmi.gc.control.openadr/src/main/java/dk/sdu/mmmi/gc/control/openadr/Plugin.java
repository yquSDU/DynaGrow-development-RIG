/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.control.openadr;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.gc.control.openadr.concern.LightDemandResponseConcern;
import dk.sdu.mmmi.gc.control.openadr.input.DemandResponseLightPlanInput;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author ancla
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final DisposableList d = new DisposableList();

    @Override
    public Disposable create(ControlDomain domain) {
        Input drinput = new DemandResponseLightPlanInput(domain);
        d.add(context(domain).add(Input.class, drinput));
        
        Concern drConcern = new LightDemandResponseConcern(domain);
        d.add(context(domain).add(Concern.class, drConcern));
        return d;
    }

}
