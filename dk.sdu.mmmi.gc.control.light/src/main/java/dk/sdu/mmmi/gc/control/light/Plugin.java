package dk.sdu.mmmi.gc.control.light;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.control.light.concern.AchieveLightTimeSumConcern;
import dk.sdu.mmmi.gc.control.light.concern.FixedLightPlanConcern;
import dk.sdu.mmmi.gc.control.light.concern.LightIntervalConcern;
import dk.sdu.mmmi.gc.control.light.concern.LightSubIntervalConcern;
import dk.sdu.mmmi.gc.control.light.concern.MinLightForArtLightConcern;
import dk.sdu.mmmi.gc.control.light.concern.MinSwitchConcern;
import dk.sdu.mmmi.gc.control.light.concern.PreferCheapLightConcern;
import dk.sdu.mmmi.gc.control.light.concern.PreferLightDuringNightConcern;
import dk.sdu.mmmi.gc.control.light.concern.PreferNoLightingConcern;
import dk.sdu.mmmi.gc.control.light.gui.ConfigureLightPlanAction;
import dk.sdu.mmmi.gc.control.light.input.ElPriceMWhForecastInput;
import dk.sdu.mmmi.gc.control.light.input.ElSpotPriceMWhForecastInput;
import dk.sdu.mmmi.gc.control.light.input.FixedLightPlanInput;
import dk.sdu.mmmi.gc.control.light.input.FrameEndInput;
import dk.sdu.mmmi.gc.control.light.input.FrameStartInput;
import dk.sdu.mmmi.gc.control.light.input.GreenhouseSizeInput;
import dk.sdu.mmmi.gc.control.light.input.HybridElPriceForecastInput;
import dk.sdu.mmmi.gc.control.light.input.InstalledLampEffectInput;
import dk.sdu.mmmi.gc.control.light.input.LampIntensityInput;
import dk.sdu.mmmi.gc.control.light.input.LightTimeAchievedPeriod;
import dk.sdu.mmmi.gc.control.light.input.LightTimeAchievedToday;
import dk.sdu.mmmi.gc.control.light.input.MinLightForArtLightInput;
import dk.sdu.mmmi.gc.control.light.input.MinimumLightTimeSumInput;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import dk.sdu.mmmi.gc.control.light.input.ProposedLightPlanInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.series.CombinedElPriceForecastSeries;
import dk.sdu.mmmi.gc.control.light.series.CombinedElSpotPriceForecastSeries;
import dk.sdu.mmmi.gc.control.light.series.CombinedOutdoorLightForecastSeries;
import dk.sdu.mmmi.gc.control.light.series.HybridElPriceForecastSeries;
import dk.sdu.mmmi.gc.control.light.series.InstalledLampEffectSeries;
import dk.sdu.mmmi.gc.control.light.series.LampIntensitySeries;
import dk.sdu.mmmi.gc.control.light.series.LightStatusSeries;
import dk.sdu.mmmi.gc.control.light.series.LightTimeAchievedPeriodSeries;
import dk.sdu.mmmi.gc.control.light.series.LightTimeAchievedTodaySeries;
import dk.sdu.mmmi.gc.control.light.series.MinLightForArtLightSeries;
import dk.sdu.mmmi.gc.control.light.series.MinimumLightTimeSumSeries;
import dk.sdu.mmmi.gc.control.light.series.ProposedLightPlanSeries;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author mrj, jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final DisposableList d = new DisposableList();
    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {

        // Configurations

        // Concerns
        //createAchieveLightTimeSumConcern(g);
        //createPreferLightDuringNightConcern(g);
        //createPreferNoLightingConcern(g);
        createMinLightForArtLightConcern(g);
        createFixedLightPlanConcern(g);
        createLightIntervalConcern(g);
        //createLightSubIntervalConcern(g);
        createMinSwitchConcern(g);
        createPreferCheapLightConcern(g);

        // Inputs
        createFrameWindow(g); // this has to be added before other derived inputs
        minimumLightForArtLightInput(g);
        //minimumLightTimeSumInput(g);
        //lightTimeAchievedToday(g);
        fixedLightPlanInput(g);
        proposedLightPlanInput(g);
        createElPriceMWhForecastInput(g);
        createElSpotPriceMWhForecastInput(g);
        createHybridPriceMWhForecastInput(g);
        createInstalledLampEffectInput(g);
        createGreenhouseSizeInput(g);
        createOutdoorLightForecastInput(g);
        createLampIntensityInput(g);

        // Outputs
        lightPlanTodayOutput(g);

        return d;
    }

    //
    // Configs
    //

    //
    // OUTPUTS
    //
    private void lightPlanTodayOutput(ControlDomain g) {
        LightPlanTodayOutput lightPlanOutput = new LightPlanTodayOutput(g);
        d.add(context(g).add(Output.class, lightPlanOutput));
        d.add(context(lightPlanOutput).add(Action.class, new ConfigureLightPlanAction(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new LightStatusSeries(g)));
    }

    //
    // INPUTS
    //
    private void proposedLightPlanInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new ProposedLightPlanInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ProposedLightPlanSeries(g)));
    }

    private void fixedLightPlanInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new FixedLightPlanInput(g)));
    }

    private void lightTimeAchievedToday(ControlDomain g) {
        d.add(context(g).add(Input.class, new LightTimeAchievedToday(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new LightTimeAchievedTodaySeries(g)));

        d.add(context(g).add(Input.class, new LightTimeAchievedPeriod(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new LightTimeAchievedPeriodSeries(g)));
    }

    private void minimumLightTimeSumInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new MinimumLightTimeSumInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new MinimumLightTimeSumSeries(g)));
    }

    private void createLampIntensityInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new LampIntensityInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new LampIntensitySeries(g)));
    }

    private void createElPriceMWhForecastInput(ControlDomain g) {
        ElPriceMWhForecastInput elPriceMWhForecastInput = new ElPriceMWhForecastInput(g);
        d.add(context(g).add(Input.class, elPriceMWhForecastInput));
        // TODO: Remove d.add(context(g).add(DoubleTimeSeries.class, new LatestElPriceForecastSeries(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new CombinedElPriceForecastSeries(g)));
    }

    private void createElSpotPriceMWhForecastInput(ControlDomain g) {
        ElSpotPriceMWhForecastInput elSpotPriceMWhForecastInput = new ElSpotPriceMWhForecastInput(g);
        d.add(context(g).add(Input.class, elSpotPriceMWhForecastInput));
        d.add(context(g).add(DoubleTimeSeries.class, new CombinedElSpotPriceForecastSeries(g)));
    }

    private void createHybridPriceMWhForecastInput(ControlDomain g) {
        HybridElPriceForecastInput hybridForecastInput = new HybridElPriceForecastInput(g);
        d.add(context(g).add(Input.class, hybridForecastInput));
        d.add(context(g).add(DoubleTimeSeries.class, new HybridElPriceForecastSeries(g)));
    }

    private void createOutdoorLightForecastInput(ControlDomain g) {
        // Important: OutputLightForecastInput has to be added to context before ExpNaturalPARSumTodayInput, ExpNaturalPhotoSumTodayInput and ExpCombinedPhotoSumTodayInput
        OutdoorLightForecastInput outdoorLightForecastInput = new OutdoorLightForecastInput(g);
        d.add(context(g).add(Input.class, outdoorLightForecastInput));
        //d.add(context(g).add(DoubleTimeSeries.class, new LatestOutdoorLightForecastSeries(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new CombinedOutdoorLightForecastSeries(g)));        
    }

    private void createGreenhouseSizeInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new GreenhouseSizeInput(g)));
    }

    private void createInstalledLampEffectInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new InstalledLampEffectInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new InstalledLampEffectSeries(g)));
    }

    private void minimumLightForArtLightInput(ControlDomain g) {
        d.add(context(g).add(Input.class, new MinLightForArtLightInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new MinLightForArtLightSeries(g)));
    }

    //
    // CONCERNS
    //
    private void createLightIntervalConcern(ControlDomain g) {
        LightIntervalConcern c = new LightIntervalConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createMinSwitchConcern(ControlDomain g) {
        MinSwitchConcern c = new MinSwitchConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createLightSubIntervalConcern(ControlDomain g) {
        LightSubIntervalConcern c = new LightSubIntervalConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createFixedLightPlanConcern(ControlDomain g) {
        FixedLightPlanConcern c = new FixedLightPlanConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createPreferNoLightingConcern(ControlDomain g) {
        PreferNoLightingConcern c = new PreferNoLightingConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createAchieveLightTimeSumConcern(ControlDomain g) {
        AchieveLightTimeSumConcern c = new AchieveLightTimeSumConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createPreferCheapLightConcern(ControlDomain g) {
        PreferCheapLightConcern preferCheapLightConcern = new PreferCheapLightConcern(g);
        cm.loadConcernConfig(g, preferCheapLightConcern);
        d.add(context(g).add(Concern.class, preferCheapLightConcern));
        addTimeSeries(g, preferCheapLightConcern);
    }

    private void createMinLightForArtLightConcern(ControlDomain g) {
        MinLightForArtLightConcern c = new MinLightForArtLightConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void createPreferLightDuringNightConcern(ControlDomain g) {
        PreferLightDuringNightConcern c = new PreferLightDuringNightConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        addTimeSeries(g, c);
    }

    private void addTimeSeries(ControlDomain g, Concern c) {
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, c)));
    }

    private void createFrameWindow(ControlDomain g) {
        d.add(context(g).add(Input.class, new FrameEndInput(g)));
        d.add(context(g).add(Input.class, new FrameStartInput(g)));        
    }

}
