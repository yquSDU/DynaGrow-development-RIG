package dk.sdu.mmmi.gc.control.commons.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jcs
 */
public class OutdoorLightInput extends AbstractInput<WattSqrMeter> {

    private final ControlDomain g;

    public OutdoorLightInput(ControlDomain g) {
        super("Outdoor light");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        HALConnector hal = HAL.get(g);
        if (hal != null) {
            try {
                WattSqrMeter v = hal.readOutdoorLightIntensity();
                setValue(v);
                store(t, v);
            } catch (HALException ex) {
                Logger.getLogger(OutdoorLightInput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void store(Date t, WattSqrMeter result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertOutdoorLight(new Sample<>(t, result));
    }
}
