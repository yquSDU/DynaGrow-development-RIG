package dk.sdu.mmmi.gc.gui.house;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import org.openide.windows.TopComponent;

/**
 * Need help, see this: http://wiki.netbeans.org/DevFaqNonSingletonTopComponents
 *
 * @author mrj
 */
public class HouseTopComponent extends TopComponent {

    private final ControlDomain g;
    private final GreenhouseComponent gc;
    private final GreenhouseComponentUpdater updater;

    public HouseTopComponent(ControlDomain g) {
        this.g = g;

        // Set component name.
        ContextDataAccess da = context(g).one(ContextDataAccess.class);
        if (da != null) {
            String name = da.getName();
            setName(name);
        }

        // Configure components.
        gc = new GreenhouseComponent(g);
        JScrollPane sp = new JScrollPane(gc);

        // Configure layout.
        setLayout(new BorderLayout());
        add(sp, BorderLayout.CENTER);

        // Updater.
        updater = new GreenhouseComponentUpdater(g, gc);
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_NEVER;
    }

    @Override
    protected void componentOpened() {
        updater.start();
    }

    @Override
    protected void componentClosed() {
        updater.stop();
    }
}
