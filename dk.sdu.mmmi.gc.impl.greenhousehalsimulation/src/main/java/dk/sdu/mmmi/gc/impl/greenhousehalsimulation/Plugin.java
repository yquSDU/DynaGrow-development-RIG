package dk.sdu.mmmi.gc.impl.greenhousehalsimulation;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import java.util.Calendar;
import java.util.Date;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        d.add(context(g).add(HALConnector.class, new DefaultHAL(g)));
        SimulationContext context = new DefualtSimulationContext(g);
        // ================================
        //qqq  Uncomment these lines when need to add fake hybrid electricity prices 
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // Date range beginning is 00:00 of the current date
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 3);        // create data for 3 days
        // Date range end is plus 3 days ahead
        Date endDate = calendar.getTime();
        context.init(startDate, endDate);
        // ================================
        d.add(context(g).add(SimulationContext.class, context));
        
        d.add(context(g).add(SimulationContext.class, new DefualtSimulationContext(g)));

        return d;
    }
}
