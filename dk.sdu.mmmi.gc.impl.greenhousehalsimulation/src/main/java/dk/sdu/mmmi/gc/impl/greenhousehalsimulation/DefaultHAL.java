package dk.sdu.mmmi.gc.impl.greenhousehalsimulation;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author mrj
 */
public final class DefaultHAL implements HALConnector {

    public static final Random R = new Random();
    private final ControlDomain g;
    private Date time = new Date();
    private final ClimateDataAccess db;

    public DefaultHAL(ControlDomain g) {
        this.g = g;
        this.db = context(g).one(ClimateDataAccess.class);

        writeCO2Threshold(new PPM(350d));
        writeGeneralScreenPosition(new Percent(0d));
        writeHeatingThreshold(new Celcius(18d));
        writeLightStatus(Switch.OFF);
        writeVentilationThreshold(new Celcius(30d));

        readTemperature();
        readOutdoorTemperature();
        readCO2();
        readOutdoorLightIntensity();
        readLightIntensity();
        readHumidity();
    }

    @Override
    public Status getStatus() {
        return Status.DEGRATED_SERVICE;
    }

    @Override
    public Celcius readTemperature() {

        double v = db.selectAirTemperature(time).getSample().value();
        double mutated = mutate(v, 1, 30, 20);

        Sample<Celcius> r = new Sample<>(time, new Celcius(mutated));
        return r.getSample();
    }

    @Override
    public Celcius readOutdoorTemperature() {

        double v = db.selectOutdoorTemperature(time).getSample().value();
        double mutated = mutate(v, 1, 15, 10);;

        Sample<Celcius> r = new Sample<>(time, new Celcius(mutated));
        return r.getSample();
    }

    @Override
    public PPM readCO2() {
        double v = db.selectCO2(time).getSample().value();
        double mutated = mutate(v, 20, 1000, 350);

        Sample<PPM> r = new Sample<>(time, new PPM(mutated));
        return r.getSample();
    }

    @Override
    public UMolSqrtMeterSecond readLightIntensity() {
        double v = db.selectLightIntensity(time).getSample().value();
        double mutated = mutate(v, 5, 1000, 200);

        Sample<UMolSqrtMeterSecond> r = new Sample<>(time, new UMolSqrtMeterSecond(mutated));
        return r.getSample();
    }

    @Override
    public Percent readHumidity() {
        double v = db.selectHumidity(time).getSample().value();
        double mutated = mutate(v, 2, 100, 50);

        Sample<Percent> r = new Sample<>(time, new Percent(mutated));
        return r.getSample();
    }

    @Override
    public WattSqrMeter readOutdoorLightIntensity() {
        double v = db.selectOutdoorLight(time).getSample().value();
        double mutated = mutate(v, 5, 500, 100);

        Sample<WattSqrMeter> r = new Sample<>(time, new WattSqrMeter(mutated));
        return r.getSample();
    }

    @Override
    public Switch readArtficialLightStatus(String lightGroup) {
        return db.selectLightStatus(time).getSample();
    }

    @Override
    public Percent readWindowOpening() {
        return db.selectWindowsOpening(time).getSample();
    }

    @Override
    public void writeHeatingThreshold(Celcius c) {
    }

    @Override
    public void writeVentilationThreshold(Celcius c) {
    }

    @Override
    public void writeCO2Threshold(PPM p) {
    }

    @Override
    public void writeLightStatus(Switch s) {
    }

    @Override
    public void writeGeneralScreenPosition(Percent p) {
    }

    @Override
    public void dispose() {

    }

    private double mutate(double v, int DELTA, int MAX_VALUE, int MIN_VALUE) {
        double mutated;
        if (R.nextBoolean()) {
            mutated = v + DELTA * R.nextDouble();
        } else {
            mutated = v - DELTA * R.nextDouble();
        }
        // Border cases
        if (mutated > MAX_VALUE) {
            mutated = MAX_VALUE;
        } else if (mutated < MIN_VALUE) {
            mutated = MIN_VALUE;
        }
        return mutated;
    }

    void setTime(Date cursor) {
        this.time = cursor;
    }

    @Override
    public List<String> readLightGroups() throws HALException {
        ArrayList<String> r = new ArrayList<>();
        r.add("");
        return r;
    }
}
