package dk.sdu.mmmi.gc.gui.explorer;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import org.openide.nodes.AbstractNode;

/**
 * @author mrj
 */
public class OutputsNode extends AbstractNode {

    public OutputsNode(ControlDomain g) {
        super(new OutputsNodeChildren(g));
        setDisplayName("Outputs");
        setIconBaseWithExtension("dk/sdu/mmmi/gc/impl/greenhouseicons/Output.png");
    }
}
