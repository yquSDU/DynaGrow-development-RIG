package dk.sdu.mmmi.gc.gui.charts;

import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mrj
 */
public class DataUtil {

    public static List<DoubleTimeValue> addNoDataZones(List<DoubleTimeValue> in, long maxPeriodLengthMS) {

        List<DoubleTimeValue> r = new ArrayList<>(in.size());
        Date last = null;
        for (DoubleTimeValue v : in) {
            if (last != null) {
                Date now = v.getTime();
                long diff = now.getTime() - last.getTime();
                if (diff > maxPeriodLengthMS) {
                    r.add(new DoubleTimeValue(new Date(last.getTime() + 1), null));
                    r.add(new DoubleTimeValue(new Date(now.getTime() - 1), null));
                }
            }
            r.add(v);
            last = v.getTime();
        }

        return r;
    }
}
