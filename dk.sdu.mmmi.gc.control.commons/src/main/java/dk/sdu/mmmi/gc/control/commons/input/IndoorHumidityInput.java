package dk.sdu.mmmi.gc.control.commons.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jcs
 */
public class IndoorHumidityInput extends AbstractInput<Percent> {

    private final ControlDomain g;

    public IndoorHumidityInput(ControlDomain g) {
        super("Indoor humidity");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        HALConnector hal = HAL.get(g);
        if (hal != null) {
            try {
                Percent v = hal.readHumidity();
                setValue(v);
                persist(t, v);
            } catch (HALException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void persist(Date t, Percent result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertHumidity(new Sample<>(t, result));
    }
}
