package dk.sdu.mmmi.gc.impl.entities.results;

import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jcs
 */
public class FixedDayLightPlan {

    private final List<Switch> hours = new ArrayList<>(24);

    public FixedDayLightPlan() {
        for (int i = 0; i < 24; i++) {
            hours.add(i, Switch.NA);
        }
    }

    public FixedDayLightPlan(FixedDayLightPlan lightPlan) {
        for (Switch s : lightPlan.getHours()) {
            hours.add(s);
        }
    }

    public List<Switch> getHours() {
        return hours;
    }

    public Switch getHour(int i) {
        return hours.get(i);
    }

    public void setHour(int hourIdx, Switch s) {
        hours.set(hourIdx, s);
    }

    public FixedDayLightPlan set(int hourIdx, Switch s) {
        hours.set(hourIdx, s);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Switch s : hours) {
            sb.append(s);
        }
        sb.append("]");
        return sb.toString();
    }
}
