/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;

/**
 *
 * @author jcs
 */
public class LightSubIntervalConcern extends AbstractConcern {

    private static final int INTERVAL_LENGHT = 3;

    public LightSubIntervalConcern(ControlDomain g) {
        super("Light Subinterval", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        boolean accept = true;
        LightPlan newProposedPlan = s.getValue(LightPlanTodayOutput.class);

        for (int i = 0; i < newProposedPlan.size() - INTERVAL_LENGHT; i += INTERVAL_LENGHT) {
            Switch firstElement = newProposedPlan.getElement(i);

            for (int j = i; j < i + INTERVAL_LENGHT; j++) {
                Switch intervalElement = newProposedPlan.getElement(j);

                if (firstElement.isOn() && !intervalElement.isOn()) {
                    accept = false;
                }
            }

        }

        return accept ? 0 : 1;
    }
}
