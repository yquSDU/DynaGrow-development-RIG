package dk.sdu.mmmi.gc.control.commons.utils;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.MegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Money;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.Watt;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import static java.math.BigDecimal.ZERO;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jcs
 */
public class EnergyUtil {

    private static final Logger logger = Logger.getLogger(EnergyUtil.class.getName());

    public static Energy getEnergyBalance(ControlDomain g, Date t) throws HALException, SQLException {

        HALConnector hal = HAL.get(g);
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        if (hal == null || db == null) {
            return null;
        }

        // Indoor light.
//        UMolSqrtMeterSecond indoorLight = LightUtil.getIndoorLight(g, t);
//        if(indoorLight == null) {
//            return null;
//        }
        // Outdoor light.
        WattSqrMeter outdoorLight = hal.readOutdoorLightIntensity();
        if (outdoorLight == null) {
            return null;
        }

        // Outdoor temp.
        Celcius outdoorTemp = hal.readOutdoorTemperature();
        if (outdoorTemp == null) {
            return null;
        }

        // Indoor temp.
        Celcius indoorTemp = hal.readTemperature();
        if (indoorTemp == null) {
            return null;
        }

        // Greenhouse transmission.
        Sample<Percent> r = db.selectLightTransmissionFactor(t);
        if (r == null) {
            return null;
        }

        Percent glassTrans = r.getSample();
        if (glassTrans == null) {
            return null;
        }

        //double energyBalance = EnergyUtil.calcEnergyBalance(indoorLight, outdoorLight, outdoorTemp, greenTrans);
        double energyBalance = EnergyUtil.calcEnergyBalance3(outdoorLight, outdoorTemp, indoorTemp, glassTrans);
        return new Energy(energyBalance);
    }

//  // Old IntelliGrow code (Pascal)
//  { beregner lysmængde i hus på baggrund af udvendigt lys, umol m-2 s-1}
//
//  OutIrr_umol :=  OutIrr * 2.3;  { i umol m-2s-1 }
//
//  IrrWithoutScreens := OutIrr_umol * GreenhouseTrn/100;
//  IrrWithScreens    := IrrWithoutScreens * Scr1IrrTrn/100;
//
//  ComponentDlg.AddHeaderCurrentReport('Indstrålingsforhold: ');
//  ComponentDlg.AddCurrentReport('');
//  MyMsg := 'Udenfor væksthuset: ' + IGFloatToStr(OutIrr,2) + ' Wm-2' + MyCr +
//           'Udenfor væksthuset: ' + IGFloatToStr(OutIrr_umol,2) + ' umol m-2 s-1' + MyCr +
//           'I væksthuset med gardiner: ' + IGFloatToStr(IrrWithScreens,2) + ' umol m-2 s-1' + MyCr +
//           'I væksthuset uden gardiner: ' + IGFloatToStr(IrrWithoutScreens,2) + ' umol m-2 s-1' + MyCr;
//  ComponentDlg.AddCurrentReport(MyMsg);
//  ComponentDlg.AddCurrentReport('');
//
//  { bergner energibalancemodel }
//  c:= 0.3; { konstant for lysets virkning på energiforbruget }
//
//  EnergyBalance := c*OutIrr-(u_glas-u_gardin)* (AirTmpIn-AirTmpOut);
//
    /**
     * Derived from Jesper's old Pascal code (IntelliGrow).
     */
    private static double calcEnergyBalance3(WattSqrMeter outdoorLight, Celcius outdoorAirTemp, Celcius indoorTemp, Percent trans) {

        double u_glass = 6.0;
        double light = outdoorLight.value() * trans.asFactor();

        //  c:= 0.3; { konstant for lysets virkning på energiforbruget }
        double c = 0.3;

        //  EnergyBalance := c*OutIrr-(u_glas-u_gardin)* (AirTmpIn-AirTmpOut);
        double b = c * light - u_glass * (indoorTemp.value() - outdoorAirTemp.value());
        return b;
    }

//    // Old PREDICT code:
//    private double calcEnergyBalance() {
//        // Convert from PAR in umol to sun spectrum in W m^-2
//        double totalLightAfterShading = par.get()/2.3;
//
//        double energyBalance = LIGHT_ENERGY * totalLightAfterShading -
//                u_glass.get() * (getTIn() - airTmpOut.get());
//        return energyBalance;
//    }
//
//    private double getTIn() {
//        double tOut = airTmpOut.get();
//        double iOut = outIrr.get();
//        // light
//        double transmission = greenhouseIrrTrn.get() / 100; // tglass + tscreens
//        double k = u_glass.get(); // uglas + uscreens
//
//        double tIn = tOut + (1 / 3) * transmission * (iOut / k);
//        return tIn;
//    }
    /**
     * Calculate the price of an artificial light-plan based on electricity
     * prices (unitCostList) and connected lamp load.
     *
     * consumedEnergy [MWh] = totalLampLoad [MW] * timeOfOperation [hour]
     *
     * energyCost [EUR] = consumedEnergy [MWh] * unitCostOfEnergy [EUR/MWh]
     *
     * @param plan Artificial light-plan
     * @param unitCostList Unit cost list of electricity prices
     * @param totalLampLoad Total connected lamp load
     * @param lpCfg
     * @return Price of artificial light-plan
     */
    public static Money calcPrice(LightPlan plan, ElPriceMWhForecast unitCostList, Watt totalLampLoad, LightPlanConfig lpCfg) {

        Money energyCostSum;

        // Check for cached result
        if (lpCfg.isElPriceCacheEnabled()) {
            energyCostSum = context(plan).one(Money.class);

            if (energyCostSum != null) {
                return energyCostSum;
            }
        }

        // Calculate cost
        energyCostSum = new Money(ZERO, unitCostList.getCurrency());

        for (int i = 0; i < plan.size(); i++) {

            Switch light = plan.getElement(i);

            if (light.isOn()) {
                Duration time = lpCfg.getTimeSlotDuration();
                MegaWattHour consumedEnergy = totalLampLoad.toMegaWatt().times(time);
                MoneyMegaWattHour unitCost = unitCostList.getPrice(startOfHour(plan.getElementStart(i)));

                if (unitCost == null) {
                    logger.log(Level.SEVERE,
                            String.format("The price in list %s for an "
                                    + "element %s §(%s) in the plan could not be found",
                                    unitCostList, i, plan.getElementStart(i)));
                        // The price of an element in the plan could not be
                    // calculated. We don't want to use partial prices for
                    // our plans. Thus, we return null.
                    return null;
                }

                Money energyCost = unitCost.times(consumedEnergy);
                energyCostSum = energyCostSum.plus(energyCost);
            }
        }

        // Cache the result if cache is enabled
        if (lpCfg.isElPriceCacheEnabled()) {
            context(plan).add(Money.class, energyCostSum);
        }

        return energyCostSum;
    }
}
