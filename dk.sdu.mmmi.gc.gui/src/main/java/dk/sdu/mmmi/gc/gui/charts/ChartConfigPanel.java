package dk.sdu.mmmi.gc.gui.charts;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author mrj
 */
public class ChartConfigPanel extends JPanel {

    private final JTable tbl;
    private final Model mdl;
    private final ChartConfig chart;
    private final ControlDomain g;

    public ChartConfigPanel(ControlDomain g, ChartConfig chart) {
        this.g = g;
        this.chart = chart;
        this.mdl = new Model();
        this.tbl = new JTable(mdl);
        setLayout(new BorderLayout());
        JScrollPane scroll = new JScrollPane(tbl);
        add(scroll, BorderLayout.CENTER);
        setPreferredSize(new Dimension(400, 400));

        // Event Service
        mdl.update();
    }

    private class Model extends DefaultTableModel {

        private List<DoubleTimeSeries> data = new ArrayList<DoubleTimeSeries>();

        public Model() {
            addColumn("Series");
            addColumn("Enabled");
        }

        private void update() {
            data = new ArrayList<DoubleTimeSeries>(context(g).all(DoubleTimeSeries.class));
            Collections.sort(data, DoubleTimeSeries.ASC_ORDER);
            setRowCount(data.size());
        }

        @Override
        public Class<?> getColumnClass(int i) {
            return (i != 1) ? String.class : Boolean.class;
        }

        @Override
        public Object getValueAt(int row, int column) {
            DoubleTimeSeries sc = data.get(row);
            switch (column) {
                case 0:
                    return sc.getName() + " " + sc.getUnit();
                case 1:
                    return chart.getSeries().contains(sc.getID());
                default:
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return column == 1;
        }

        @Override
        public void setValueAt(Object o, int row, int column) {
            DoubleTimeSeries dts = data.get(row);
            switch (column) {
                case 1:
                    if ((Boolean) o) {
                        chart.addSeries(dts.getID());
                    } else {
                        chart.removeSeries(dts.getID());
                    }
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            context(g).one(ChartManager.class).update(chart);
        }
    }
}
