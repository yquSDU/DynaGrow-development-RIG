package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlListener;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.gc.gui.actions.CleanDataAction;
import dk.sdu.mmmi.gc.gui.actions.ConfigureControlAction;
import dk.sdu.mmmi.gc.gui.actions.DeleteGreenhouseAction;
import dk.sdu.mmmi.gc.gui.actions.DoSimulateControlAction;
import dk.sdu.mmmi.gc.gui.actions.ExportTestDataAction;
import dk.sdu.mmmi.gc.gui.actions.RenameGreenhouseAction;
import dk.sdu.mmmi.gc.gui.actions.ShowHouseAction;
import dk.sdu.mmmi.gc.gui.actions.StartAutoControlAction;
import dk.sdu.mmmi.gc.gui.actions.StartControlAction;
import dk.sdu.mmmi.gc.gui.actions.StopAutoControlAction;
import dk.sdu.mmmi.gc.gui.actions.StopControlAction;
import dk.sdu.mmmi.gc.gui.actions.StopSimulationAction;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 * @author mrj & jcs
 */
public class GreenhouseNode extends AbstractNode implements ControlListener, ControleumListener {

    private final ControlDomain g;
    private final DisposableList disposables = new DisposableList();
    private final ControlManager gm;

    public GreenhouseNode(ControlDomain g) {
        super(new GreenhouseNodeChildren(g), Lookups.singleton(g));
        gm = context(g).one(ControlManager.class);
        this.g = g;

        disposables.add(context(g).add(Action.class, new ExportTestDataAction(g)));
        disposables.add(context(g).add(Action.class, new RenameGreenhouseAction(g)));
        disposables.add(context(g).add(Action.class, new CleanDataAction(g)));
        disposables.add(context(g).add(Action.class, new DeleteGreenhouseAction(g)));
        disposables.add(context(g).add(Action.class, new StartControlAction(g)));
        disposables.add(context(g).add(Action.class, new StopControlAction(g)));
        disposables.add(context(g).add(Action.class, new StartAutoControlAction(g)));
        disposables.add(context(g).add(Action.class, new StopAutoControlAction(g)));
        disposables.add(context(g).add(Action.class, new DoSimulateControlAction(g)));
        disposables.add(context(g).add(Action.class, new StopSimulationAction(g)));
        disposables.add(context(g).add(Action.class, new ConfigureControlAction(g)));

        // Register listeners
        disposables.add(context(g).add(ControleumListener.class, this));
        disposables.add(context(g).add(ControlListener.class, this));

        String name = g.getName();
        setDisplayName(name);
        update();
    }

    @Override
    public SystemAction getDefaultAction() {
        return SystemAction.get(ShowHouseAction.class);
    }

    @Override
    public Action[] getActions(boolean context) {
        return context(g).all(Action.class).toArray(new Action[]{});
    }

    private void update() {
        String icon = "dk/sdu/mmmi/gc/silk/house_stop.png";

        if (gm.isSimulating()) {
            icon = "dk/sdu/mmmi/gc/silk/house_sim.png";
        } else if (gm.isControlling()) {
            icon = "dk/sdu/mmmi/gc/silk/house_play.png";
        } else if (gm.isAutoRunning()) {
            icon = "dk/sdu/mmmi/gc/silk/house_auto.png";
        }
        setIconBaseWithExtension(icon);
    }

    @Override
    public void onControlConfigChanged() {
        update();
    }

    @Override
    public void onControlDomainAdded(ControleumEvent e) {
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
        setDisplayName(e.getControlDomain().getName());
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        disposables.dispose();
    }
}
