package dk.sdu.mmmi.gc.impl.okosimulation;

import com.mathworks.toolbox.javabuilder.internal.MCRConfiguration;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Date;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import sdusimulator.API;

/**
 * MatLab API:
 *
 * [output,input]=SimStart(delta,init,input,output,SimDuration)
 *
 * Delta=minutter tidsinterval, 5 min = 300 Init=0 (nyt), 1 (kørende)
 * output,input (struct som sendt tidligere); første gang du kalder, sendt
 * output med ønskede inputs SimDuration=Simulation time i timer!
 *
 * input.spAirHeat=18; input.spAirVent=20; input.spCO2=1000; input.spRHgh=85;
 * input.spScreenPos=0; input.LampCapacityInst=400; input.NoLampsSqm=0.15;
 * input.ArtficialLightStatus=1; input.dayno=30; %starter med Day 1
 * input.hour=10; %fra 1 to 24 output.Tair=20; output.RHair=80;
 * output.CO2air=400; SimDuration=100;
 *
 * [output,input]=SimStart(300,0,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 *
 * TODO How do we get access to weather forecast? A new method:
 * GetLightForecast(day, hour) : light
 *
 * @author mrj
 */
public class Main {

    public static void main(String[] args) {
        env();
        api();
        test();
    }

    private static void env() {
        System.out.println("== ENV ==");
        System.out.println("MCRROOT: " + System.getenv("MCRROOT"));
        System.out.println("PATH: " + System.getenv("PATH"));
        System.out.println("DYLD_LIBRARY_PATH: " + System.getenv("DYLD_LIBRARY_PATH"));
        System.out.println("XAPPLRESDIR: " + System.getenv("XAPPLRESDIR"));
        System.out.println("Installed MCR: " + MCRConfiguration.isInstalledMCR());
    }

    private static void api() {
        try {
            System.out.println("== API constructors ==");
            for (Constructor c : API.class.getDeclaredConstructors()) {
                System.out.println(c);
            }
            System.out.println("== API methods ==");
            for (Method m : API.class.getDeclaredMethods()) {
                System.out.println(m);
            }
        } catch (Exception x) {
            x.printStackTrace(System.err);
        }
    }

    /**
     * [output,input]=SimStart(300,0,input,output,SimDuration)
     * [output,input]=SimStart(300,1,input,output,SimDuration)
     * [output,input]=SimStart(300,1,input,output,SimDuration)
     * [output,input]=SimStart(300,1,input,output,SimDuration)
     */
   public static void test() {
        System.out.println("== Simulation ==");
        int durationDays = 3;
        Date from = new Date();
        Date to = new Date(from.getTime() + 24 * 60 * 60 * 1000 * durationDays);
        System.out.println(from + " to " + to);
        Simulation s = new Simulation(from, to, 5 * 60 * 1000);
        try {
            while (!s.isSimulationCompleted()) {
                System.out.format("== %s ==\n", s.getCurrentSimulationTime());
                Inputs i = new Inputs();
                s.control(i);
            }
        } catch (Exception x) {
            x.printStackTrace(System.err);
        } finally {
            s.dispose();
        }
    }
}
