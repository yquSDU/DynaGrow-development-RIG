package dk.sdu.mmmi.gc.impl.okosimulation;

import org.openide.modules.ModuleInstall;

public class Installer extends ModuleInstall {

    @Override
    public void restored() {
        try {
            APISingleton.getInstance().getAPI();
        } catch (Exception x) {
            x.printStackTrace(System.err);
        }
    }

    @Override
    public boolean closing() {
        APISingleton.getInstance().dispose();
        return true;
    }
}
