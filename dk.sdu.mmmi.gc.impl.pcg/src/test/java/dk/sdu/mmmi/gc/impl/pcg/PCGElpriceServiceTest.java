package dk.sdu.mmmi.gc.impl.pcg;

import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;

public class PCGElpriceServiceTest {

    private SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private EnergyPriceService sut = Lookup.getDefault().lookup(EnergyPriceService.class);
    private static File userDir = new File(System.getProperty("user.home") + "/Downloads");

    @BeforeClass
    public static void setUpClass() throws IOException {

        FileHelper.setUpClass(userDir, "BelpexFilter.csv");
    }

    @AfterClass
    public static void tearDownClass() throws IOException {
        FileHelper.tearDownClass(userDir, "BelpexFilter.csv");
    }


    @Test
    public void getPrices() throws EnergyPriceDatabaseException, ParseException {
        // Test
        Map<Date, Double> result = sut.getPrices(null, SDF.parse("1/02/2019 00:00"), SDF.parse("1/02/2019 23:00"));

        // Asserts
        Assert.assertTrue(result.values().size() == 24);
    }
}