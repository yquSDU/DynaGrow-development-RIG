package dk.sdu.mmmi.gc.photosynthesismodel;

/**
 *
 * @author jasoe04
 */
public interface PhotosynthesisModel {

    /**
     * Calculates the photosynthesis rate dependent on temperature, CO2 and sun.
     *
     * TODO: Light should be given in watt PAR (W m^-2) instead of in PPFD PAR spectre(μmol m^-2 s^-1)
     *
     * @param temp Canopy/leaf temperature [°C]
     * @param co2 Greenhouse CO2 concentration [PPM (parts per million)]
     * @param par Photosynthetic Active Radiation in [μmol m^-2 s^-1]
     * @param R_d Dark respiration rate [mg CO2/m2/s]
     * @return Net assimilation rate as a function of temp. and CO2 in [μmol m^-2 s^-1]
     */
    public double calculate(double temp, double co2, double par, PlantSpecificPhotoParams plantParams);

}
