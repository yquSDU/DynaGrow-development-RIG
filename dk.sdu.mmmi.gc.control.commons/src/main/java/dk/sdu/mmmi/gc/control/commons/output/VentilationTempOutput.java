package dk.sdu.mmmi.gc.control.commons.output;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.BinaryOutput;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.bitSetToDouble;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.doubleToBitSet;
import dk.sdu.mmmi.controleum.control.commons.AbstractOutput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.BitSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrj & jcs
 */
public class VentilationTempOutput extends AbstractOutput<Celcius> implements BinaryOutput<Celcius> {

    private final static double DELTA = 2;
    private static final int MAX_VALUE = 40;
    private static final int MIN_VALUE = 10;
    private static final Logger LOGGER = Logger.getLogger(VentilationTempOutput.class.getName());
    private int bitSize = 32;

    public VentilationTempOutput(ControlDomain g) {
        super("Ventilation setpoint", g);
    }

    @Override
    public Celcius getRandomValue(Date t) {
        return new Celcius(R.nextDouble() * (MAX_VALUE - MIN_VALUE) + MIN_VALUE); // [10;40]
    }

    @Override
    public Celcius getMutatedValue(Celcius v) {

        double m;

        boolean positive = R.nextBoolean();

        // [-2;2]
        if (positive) {
            m = v.value() + DELTA * R.nextDouble();
        } else {
            m = v.value() - DELTA * R.nextDouble();
        }

        // Border cases
        if (m > MAX_VALUE) {
            m = MAX_VALUE;
        } else if (m < MIN_VALUE) {
            m = MIN_VALUE;
        }

        return new Celcius(m);
    }

    @Override
    public void doCommitValue(Date t, Celcius result) {

        // HAL if enabled.
        if (isEnabled()) {
            HALConnector hal = HAL.get(domain);
            if (hal != null) {
                try {
                    hal.writeVentilationThreshold(result);
                } catch (HALException ex) {
                    LOGGER.log(Level.SEVERE, "Ventilation threshold output could not be written.", ex);
                }
            }
        }

        // DB.
        ClimateDataAccess db = context(domain).one(ClimateDataAccess.class);
        db.insertVentilationThreshold(new Sample<>(t, result));
    }

    @Override
    public BitSet asBitSet() {
        Double val = getValue().value();
        BitSet bs = new BitSet();
        doubleToBitSet(bs, 0, bitSize, MIN_VALUE, MAX_VALUE, val);
        return bs;

    }

    @Override
    public void setBitSet(BitSet bitset) {
        setValue(new Celcius(bitSetToDouble(bitset, 0, bitSize, MIN_VALUE, MAX_VALUE)));
    }

    @Override
    public int bitSize() {
        return bitSize;
    }
}
