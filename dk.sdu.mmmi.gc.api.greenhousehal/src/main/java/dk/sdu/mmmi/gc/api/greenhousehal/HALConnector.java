package dk.sdu.mmmi.gc.api.greenhousehal;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.util.List;

/**
 * @author mrj
 */
public interface HALConnector extends Disposable {

    Status getStatus();

    List<String> readLightGroups() throws HALException;

    Celcius readTemperature() throws HALException;

    Celcius readOutdoorTemperature() throws HALException;

    PPM readCO2() throws HALException;

    /**
     * @return Indoor PAR Light.
     */
    UMolSqrtMeterSecond readLightIntensity() throws HALException;

    Percent readHumidity() throws HALException;

    /**
     * @return Sun Light.
     */
    WattSqrMeter readOutdoorLightIntensity() throws HALException;

    Switch readArtficialLightStatus(String lightGroup) throws HALException;

    Percent readWindowOpening() throws HALException;

    void writeHeatingThreshold(Celcius c) throws HALException;

    void writeVentilationThreshold(Celcius c) throws HALException;

    void writeCO2Threshold(PPM p) throws HALException;

    void writeLightStatus(Switch s) throws HALException;

    void writeGeneralScreenPosition(Percent p) throws HALException;

    enum Status {

        OPTIMAL_SERVICE(0), DEGRATED_SERVICE(1), NO_SERVICE(2);
        final int priority;

        Status(int p) {
            this.priority = p;
        }

        public int getPriority() {
            return priority;
        }
    }
}
