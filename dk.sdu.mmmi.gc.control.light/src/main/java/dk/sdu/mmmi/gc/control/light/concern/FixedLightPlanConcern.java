package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.control.light.input.FixedLightPlanInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import static java.lang.String.format;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj & jcs
 */
public class FixedLightPlanConcern extends AbstractConcern {

    private int numProblems;

    public FixedLightPlanConcern(ControlDomain g) {
        super("Fixed light hours", g, HARD_PRIORITY);
    }

    /**
     * This method is implemented only to make the GA run faster. It helps
     * differentiate between solutions that are close to being acceptable and
     * solutions that are not.
     */
    @Override
    public double evaluate(Solution s) {

        numProblems = 0;

        FixedDayLightPlan fixed = s.getValue(FixedLightPlanInput.class);
        LightPlan p = s.getValue(LightPlanTodayOutput.class);

        for (int i = 0; i < p.size(); i++) {

            Switch lightSwitch = p.getElement(i);

            int elementHour = DateUtil.hourOfDate(p.getElementStart(i));
            Switch req = fixed.getHour(elementHour);

            // Problem when req is not fulfilled
            if (req != null && !req.isNotAvailable()
                    && (req.isOn() == lightSwitch.isOff()
                    || req.isOff() == lightSwitch.isOn())) {
                numProblems++;
            }
        }

        //System.out.println("fit="+fit);
        return numProblems;
    }

//    @Override
//    public boolean accept(Solution s) {
//
//        FixedDayLightPlan fixed = s.getValue(FixedLightPlanInput.class);
//        LightPlan p = s.getValue(LightPlanTodayOutput.class);
//
//        for (int i = 0; i < p.size(); i++) {
//
//            int elementHour = DateUtil.hourOfDate(p.getElementStart(i));
//            Switch req = fixed.getHour(elementHour);
//
//            boolean on = p.getElement(i).isOn();
//            if (req != null && !req.isNotAvailable() && req.isOn() != on) {
//                return false;
//            }
//        }
//        return true;
//    }
    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> valueHelp = new HashMap<>();
        String helpMsg = format("Acceptable = %s, %d", numProblems == 0, numProblems);
        valueHelp.put(LightPlanTodayOutput.class, helpMsg);
        return valueHelp;
    }
}
