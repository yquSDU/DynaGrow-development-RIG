package dk.sdu.mmmi.gc.photosynthesismodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Class containing methods for calculating maximal photosynthesis
 * using mathematical model(s) of the photosynthesis.
 * 
 * @author jcs & mrj
 */
class PhotosynthesisCalculator2 implements PhotosynthesisCalculator {

    private final double[] tempAxis;
    private final double[] co2Axis;
    private double optimalPhotosynthesis;
    private double optimalCO2;
    private double optimalTemperature;
    private final PhotosynthesisModel model;
    /**
     * If searchCO2First = true then each CO2 level is searched first 
     * for each temp. level.
     * If searchCO2 == false then each temp. level is searched first for
     * each CO2 level.
     */
    private final boolean searchCO2First;

    /**
     *
     * @param model Photosynthesis model
     * @param minTemp Min temp. of the temp. axis in the matrix
     * @param maxTemp Max. temp of the temp axis in the matrix
     * @param minCO2 Min CO2 of the CO2 axis in the matrix
     * @param maxCO2 Max CO2 of the CO2 axis in the matrix
     * @param searchCO2First Search direction of the matrix. If true then CO2 is
     * search first else it is temp.
     */
    public PhotosynthesisCalculator2(PhotosynthesisModel model, double minTemp, double maxTemp, double minCO2, double maxCO2, boolean searchCO2First) {
        this(model, createAxis(minTemp, maxTemp, 0.5d), createAxis(minCO2, maxCO2, 20d), searchCO2First);
    }

    private PhotosynthesisCalculator2(PhotosynthesisModel model, double[] tempAxis, double[] co2Axis, boolean searchCO2First) {
        this.model = model;
        this.tempAxis = tempAxis;
        this.co2Axis = co2Axis;
        this.searchCO2First = searchCO2First;
    }


    /**
     * @see dk.sdu.mmmi.gc.photosynthesismodel.PhotosynthesisCalculator#calculateOptimalClimate(double, double, dk.sdu.mmmi.gc.photosynthesismodel.PlantSpecificPhotoParams) 
     */
    public void calculateOptimalClimate(double par, double pctFnMax, PlantSpecificPhotoParams plantParams) {
        // Initialize rates matrix.
        double[][] rates = new double[tempAxis.length][co2Axis.length];

        // Maintain info on higest rate;
        double maxRate = Double.NEGATIVE_INFINITY;

        // Calculate rates.
        for (int tIdx = 0; tIdx < tempAxis.length; tIdx++) {
            double t = tempAxis[tIdx];

            for (int cIdx = 0; cIdx < co2Axis.length; cIdx++) {
                double c = co2Axis[cIdx];

                // Calculate current rate.
                double rate = model.calculate(t, c, par, plantParams);
                rates[tIdx][cIdx] = rate;

                // Update max rate.
                if (rate > maxRate) {
                    maxRate = rate;
                }
            }
        }

        // Calculate goal rate.
        double goalRate = maxRate > 0 ? (pctFnMax / 100.0) * maxRate : maxRate + (pctFnMax / 100.0) * maxRate;

        // Find first solution to goal rate.
        double resultRate = 0;
        int goalTempIdx = -1;
        int goalCO2Idx = -1;

        // Temp. and co2 optimal search property
        if (searchCO2First) {
            outer:
            while (goalTempIdx < tempAxis.length - 1) {
                goalTempIdx++;
                goalCO2Idx = -1;
                while (goalCO2Idx < co2Axis.length - 1) {
                    goalCO2Idx++;
                    resultRate = rates[goalTempIdx][goalCO2Idx];
                    if (resultRate >= goalRate) {
                        break outer;
                    }
                }
            }
        } else {

            outer:
            while (goalCO2Idx < co2Axis.length - 1) {
                goalCO2Idx++;
                goalTempIdx = -1;
                while (goalTempIdx < tempAxis.length - 1) {
                    goalTempIdx++;
                    resultRate = rates[goalTempIdx][goalCO2Idx];
                    if (resultRate >= goalRate) {
                        break outer;
                    }
                }
            }
        }

        // Save results.
        optimalTemperature = tempAxis[goalTempIdx];
        optimalCO2 = par > 25 ? co2Axis[goalCO2Idx]: 350; // This is a hack to solve problem with high CO2 levels at low light levels.
        optimalPhotosynthesis = resultRate;

//        printResult(rates, par, pctFnMax, maxRate, goalRate, resultRate, goalTempIdx, goalCO2Idx);

    }

    public double getOptimalCO2() {
        return optimalCO2;
    }

    public double getMaxPhotosynthesis() {
        return optimalPhotosynthesis;
    }

    public double getOptimalTemperature() {
        return optimalTemperature;
    }

    private static double[] createAxis(final double min, final double max, double step) {
        List<Double> r = new ArrayList<Double>();
        double i = min;

        while (i <= max) {
            r.add(i);
            i += step;
        }
        double[] rr = new double[r.size()];
        for (int j = 0; j < r.size(); j++) {
            rr[j] = r.get(j);
        }
        return rr;
    }

    private void printResult(double[][] rates, double par, double pctFnMax, double maxRate, double goalRate, double resultRate, double goalTempIdx, double goalCO2Idx) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%1$14s", ""));
        for (int x = 0; x < tempAxis.length; x++) {
            sb.append(String.format("%1$14s", tempAxis[x]));
        }
        sb.append("\n");
        for (int y = 0; y < co2Axis.length; y++) {
            sb.append(String.format("%1$14s ", co2Axis[y]));
            for (int x = 0; x < tempAxis.length; x++) {
                double rate = rates[x][y];                
                if(x==goalTempIdx && y == goalCO2Idx){
                    sb.append(String.format("%1$13.3f*", rate));
                }
                else{
                    sb.append(String.format("%1$13.3f ", rate));
                }
            }
            sb.append("\n");
        }
        System.out.println("" + tempAxis.length + ":" + co2Axis.length);
        System.out.println("======");
        System.out.println("PAR=" + par + " µmol m^-2 s^-1");
        System.out.println("Optimization=" + pctFnMax + "%");
        System.out.println("max rate=" + maxRate);
        System.out.println("goal rate=" + goalRate);
        System.out.println("result rate=" + resultRate);
        //System.out.println("decreased result rate=" + resultRateDecreased);
        System.out.println(sb.toString());
        System.out.println("======");
    }
}
