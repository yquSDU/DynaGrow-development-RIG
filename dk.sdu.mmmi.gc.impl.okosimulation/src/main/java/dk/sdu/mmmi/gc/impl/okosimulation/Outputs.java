package dk.sdu.mmmi.gc.impl.okosimulation;

import com.mathworks.toolbox.javabuilder.MWStructArray;
import java.util.HashMap;

/**
 *
 * @author mrj
 */
public class Outputs {

    private double RHair = 80;
    private double CO2air = 400;
    private double Tair = 20;
    private double Tout;
    private double radiIn;
    private double outLightW;
    private double Tpipe = 20;
    private double alpha = 1; // Windows opening in percent [1-100]
    private double gvnt1 = 0.0;
    private double rbH2O = 50;
    private double riH2O = 100;
    private double CO2uptake_m2 = 0;

    public Outputs() {
    }

    public Outputs(MWStructArray a, double outLightW) {
        //System.out.println("OUTPUTS");
        //System.out.println(a);
        RHair = Util.getStructDouble(a, "RHair");
        CO2air = Util.getStructDouble(a, "CO2air");
        Tair = Util.getStructDouble(a, "Tair");
        Tout = Util.getStructDouble(a, "Tout");
        radiIn = Util.getStructDouble(a, "radiIn");
        Tpipe = Util.getStructDouble(a, "Tpipe");
        alpha = Util.getStructDouble(a, "alpha");
        gvnt1 = Util.getStructDouble(a, "gvnt1");
        rbH2O = Util.getStructDouble(a, "rbH2O");
        riH2O = Util.getStructDouble(a, "riH2O");
        CO2uptake_m2 = Util.getStructDouble(a, "CO2uptake_m2");
        this.outLightW = outLightW;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> r = new HashMap<String, Object>();
        r.put("RHair", RHair);
        r.put("CO2air", CO2air);
        r.put("Tair", Tair);
        r.put("Tout", Tout);
        r.put("radiIn", radiIn);
        r.put("Tpipe", Tpipe);
        r.put("alpha", alpha);
        r.put("gvnt1", gvnt1);
        r.put("rbH2O", rbH2O);
        r.put("riH2O", riH2O);
        r.put("CO2uptake_m2", CO2uptake_m2);
        return r;
    }

    public double getCO2air() {
        return CO2air;
    }

    public void setCO2air(double CO2air) {
        this.CO2air = CO2air;
    }

    public double getRHair() {
        return RHair;
    }

    public void setRHair(double RHair) {
        this.RHair = RHair;
    }

    public double getTair() {
        return Tair;
    }

    public void setTair(double Tair) {
        this.Tair = Tair;
    }

    public double getTout() {
        return Tout;
    }

    public void setTout(double Tout) {
        this.Tout = Tout;
    }

    public double getRadiIn() {
        return radiIn;
    }

    public void setRadiIn(double radiIn) {
        this.radiIn = radiIn;
    }

    public double getOutLightW() {
        return outLightW;
    }

    public void setOutLightW(double outLightW) {
        this.outLightW = outLightW;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public void setTpipe(double Tpipe) {
        this.Tpipe = Tpipe;
    }

    public double getTpipe() {
        return Tpipe;
    }

    public double getGvnt1() {
        return gvnt1;
    }

    public void setGvnt1(double gvnt1) {
        this.gvnt1 = gvnt1;
    }
}
