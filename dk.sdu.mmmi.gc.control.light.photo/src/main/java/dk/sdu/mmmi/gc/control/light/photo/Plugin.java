package dk.sdu.mmmi.gc.control.light.photo;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.photo.concern.AchievePhotoSumConcern;
import dk.sdu.mmmi.gc.control.light.photo.concern.PhotoSumBalanceConcern;
import dk.sdu.mmmi.gc.control.light.photo.concern.PreferPhotoGrowthConcern;
import dk.sdu.mmmi.gc.control.light.photo.input.ExpCombinedPhotoSumPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.ExpNaturalPhotoSumPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoBalanceYesterdayInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumAchievedTodayInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumDayGoalInput;
import dk.sdu.mmmi.gc.control.light.photo.series.ExpectedNaturalPhotoSumSeries;
import dk.sdu.mmmi.gc.control.light.photo.series.PhotoSumAchievedPeriodSeries;
import dk.sdu.mmmi.gc.control.light.photo.series.PhotoSumAchievedTodaySeries;
import dk.sdu.mmmi.gc.control.light.photo.series.PhotoSumGoalSeries;
import dk.sdu.mmmi.gc.impl.entities.config.PhotoSumAchievedConfig;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain domain) {

        DisposableList d = new DisposableList();

        // Concerns
        createAchievePhotoSumConcern(d, domain);
        createAchievePhotoSumBalanceConcern(d, domain);
        createPreferPhotoGrowthConcern(d, domain);

        // Inputs
        createPhotoSumDayGoalInput(d, domain);
        createPhotoSumAchievedTodayInput(d, domain);
        createPhotoSumAchievedPeriodInput(d, domain);
        createExpNaturalPhotoSumTodayInput(d, domain);
        createExpCombinedPhotoSumTodayInput(d, domain);
        createPhotoBalanceYesterdayInput(d, domain);

        retrievePhotoSumAchievedConfig(d, domain);

        return d;
    }

    //
    // Configs
    //
    private PhotoSumAchievedConfig retrievePhotoSumAchievedConfig(DisposableList d, ControlDomain g) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        PhotoSumAchievedConfig cfg = db.selectPhotoSumAchievedConfig();
        d.add(context(g).add(PhotoSumAchievedConfig.class, cfg));
        return cfg;
    }

    //
    // Inputs
    //
    private void createPhotoBalanceYesterdayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PhotoBalanceYesterdayInput(g)));
    }

    private void createExpCombinedPhotoSumTodayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ExpCombinedPhotoSumPeriodInput(g)));
    }

    private void createExpNaturalPhotoSumTodayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ExpNaturalPhotoSumPeriodInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ExpectedNaturalPhotoSumSeries(g)));
    }

    private void createPhotoSumAchievedTodayInput(DisposableList d, ControlDomain domain) {
        d.add(context(domain).add(Input.class, new PhotoSumAchievedTodayInput(domain)));
        d.add(context(domain).add(DoubleTimeSeries.class, new PhotoSumAchievedTodaySeries(domain)));
    }


    private void createPhotoSumAchievedPeriodInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PhotoSumAchievedPeriodInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PhotoSumAchievedPeriodSeries(g)));
    }

    private void createPhotoSumDayGoalInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PhotoSumDayGoalInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PhotoSumGoalSeries(g)));
    }

    //
    // Concerns
    //
    private AchievePhotoSumConcern createAchievePhotoSumConcern(DisposableList d, ControlDomain g) {
        AchievePhotoSumConcern achievePhotoSumConcern = new AchievePhotoSumConcern(g);
        cm.loadConcernConfig(g, achievePhotoSumConcern);
        d.add(context(g).add(Concern.class, achievePhotoSumConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, achievePhotoSumConcern)));
        return achievePhotoSumConcern;
    }

    private PhotoSumBalanceConcern createAchievePhotoSumBalanceConcern(DisposableList d, ControlDomain g) {
        PhotoSumBalanceConcern photoSumBalanceConcern = new PhotoSumBalanceConcern(g);
        cm.loadConcernConfig(g, photoSumBalanceConcern);
        d.add(context(g).add(Concern.class, photoSumBalanceConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, photoSumBalanceConcern)));
        return photoSumBalanceConcern;
    }

    private PreferPhotoGrowthConcern createPreferPhotoGrowthConcern(DisposableList d, ControlDomain g) {
        PreferPhotoGrowthConcern c = new PreferPhotoGrowthConcern(g);
        cm.loadConcernConfig(g, c);
        d.add(context(g).add(Concern.class, c));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, c)));
        return c;
    }
}
