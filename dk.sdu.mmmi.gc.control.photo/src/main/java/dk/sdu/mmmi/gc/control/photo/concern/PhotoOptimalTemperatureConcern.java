package dk.sdu.mmmi.gc.control.photo.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.input.PhotoOptimalPercentageInput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class PhotoOptimalTemperatureConcern extends AbstractConcern {

    public PhotoOptimalTemperatureConcern(ControlDomain g) {
        super("Photo. optimal temp.", g, 3);
    }

    @Override
    public double evaluate(Solution s) {
        Celcius sp = s.getValue(HeatingSetpointOutput.class);
        Celcius opt = getOptTemp(s);

        return abs(opt.subtract(sp).roundedValue());
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        Celcius opt = getOptTemp(s);

        if (opt != null) {
            r.put(HeatingSetpointOutput.class,
                    String.format("prefer %s", opt));
        }
        return r;
    }

    private Celcius getOptTemp(Solution s) {
        UMolSqrtMeterSecond light = s.getValue(IndoorLightInput.class);
        Percent p = s.getValue(PhotoOptimalPercentageInput.class);

        if (light == null || p == null) {
            return null;
        }

        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
        Celcius photoOptTemp = photoUtil.calcPhotoOptimalTemp(light, p);
        return photoOptTemp;
    }
}
