package dk.sdu.mmmi.gc.control.commons.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class CO2MinimumInput extends AbstractInput<PPM> {

    private final ControlDomain g;
    private final Link<Action> action;

    public CO2MinimumInput(ControlDomain g) {
        super("Minimum CO2");
        assert (g != null);
        this.g = g;
        this.action = context(this).add(Action.class, new CO2MinimumInput.ConfigureAction());

    }

    @Override
    public void doUpdateValue(Date t) {
        PPM v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, PPM result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertMinCO2(new Sample<PPM>(t, result));
    }

    private PPM retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<PPM> r = db.selectMinCO2(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            PPM oldValue = CO2MinimumInput.this.getValue();
            PPM newValue = DialogUtil.promtPPM(oldValue, "Min CO2");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
