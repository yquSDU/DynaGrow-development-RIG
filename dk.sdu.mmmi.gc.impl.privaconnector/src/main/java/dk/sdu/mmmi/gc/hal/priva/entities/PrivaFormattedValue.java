/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva.entities;

/**
 *
 * @author ancla
 */
public class PrivaFormattedValue {

    private final byte[] attrib;
    private final String value;

    public PrivaFormattedValue(String value, byte[] attrib) {
        this.value = value;
        this.attrib = attrib;
    }

    public PrivaFormattedValue(PrivaFormattedByteValue attribSplit) {
        this.attrib = attribSplit.getAttrib();
        this.value = new String(attribSplit.getValue()).trim();
    }

    /**
     * @return the attrib
     */
    public byte[] getAttrib() {
        return attrib;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    
}
