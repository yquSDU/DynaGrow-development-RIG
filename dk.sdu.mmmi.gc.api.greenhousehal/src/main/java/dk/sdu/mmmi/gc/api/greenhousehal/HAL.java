package dk.sdu.mmmi.gc.api.greenhousehal;

import static com.decouplink.Utilities.context;

/**
 * @author mrj
 */
public class HAL {

    /**
     * Get the best connector for a Greenhouse.
     */
    public static synchronized HALConnector get(Object g) {

        HALConnector r = context(g).one(HALConnector.class);

        return r;
    }
}
