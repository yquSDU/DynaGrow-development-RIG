package dk.sdu.mmmi.gc.gui.explorer;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;

/**
 * @author mrj
 */
public class BaseNode extends AbstractNode {

    private boolean emph = false;

    public BaseNode(Lookup lookup) {
        super(Children.LEAF, lookup);
    }

    public void setEmphasized(boolean emph) {
        if(this.emph != emph) {
            this.emph = emph;
            fireDisplayNameChange(null, getDisplayName());
        }
    }

    @Override
    public String getHtmlDisplayName() {
        if(emph) {
            return "<font color='3333FF'>" + getDisplayName() + "</font>";
        } else {
            return getDisplayName();
        }
    }
}
