package dk.sdu.mmmi.gc.impl.entities.config;

import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;

/**
 *
 * @author jcs
 */
public class PhotoSumAchievedConfig {

    private boolean isActive = true;
    private MMolSqrMeter photoSum = new MMolSqrMeter(0.0d);

    public PhotoSumAchievedConfig() {
    }

    public PhotoSumAchievedConfig(PhotoSumAchievedConfig cfg) {
        this.isActive = cfg.isActive;
        this.photoSum = cfg.getPhotoSum();
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setPhotoSum(double photoSum) {
        this.photoSum = new MMolSqrMeter(photoSum);
    }

    public boolean isActive() {
        return isActive;
    }

    public MMolSqrMeter getPhotoSum() {
        return photoSum;
    }
}
