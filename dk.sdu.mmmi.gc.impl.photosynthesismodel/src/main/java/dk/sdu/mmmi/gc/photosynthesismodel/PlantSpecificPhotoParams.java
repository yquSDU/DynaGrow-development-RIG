package dk.sdu.mmmi.gc.photosynthesismodel;

/**
 *
 * @author jcs
 */
public class PlantSpecificPhotoParams {

    /**
     * Boundary layer resistance, [s/m]
     * The default value is 100 s/m
     */
    private final double rbH20;

    /**
     * Dark respiration rate [umol CO2/m2/s] at 25 degrees
     * The default value is 1.1 umol CO2/m2/s
     */
    private final double rd25;

    /**
     * Convexity(theta) degree of curvature of CO2 response of light saturated
     * net photosynthesis [degree].
     * The default value is 0.7 degree  
     */
    private final double theta;

    /**
     * Fraction of PAR absorbered from the non photosynthetic plant material [%].
     * The default value is 30 %
     */
    private final double f;

    /**
     * Chlorophyll density [g Chl m^-2]
     * The default value is 0.45
     */
    private final double rhoChl;

    /**
     * Stomatal resistance, [s/m]
     * The default values is 50
     */
    private final double rsH2O;

    public PlantSpecificPhotoParams() {
        this(100d, 1.1d, 0.7d, 0.3d, 0.45d, 50d);
    }

    public PlantSpecificPhotoParams(double r_b_H20, double r_d_25, double theta, double f, double RHO_Chl, double r_s_H20) {
        this.rbH20 = r_b_H20;
        this.rd25 = r_d_25;
        this.theta = theta;
        this.f = f;
        this.rhoChl = RHO_Chl;
        this.rsH2O = r_s_H20;
    }

    public double getRbH2O() {
        return rbH20;
    }

    public double getRd25() {
        return rd25;
    }

    public double getTheta() {
        return theta;
    }

    public double getF() {
        return f;
    }

    public double getRhoChl() {
        return rhoChl;
    }

    double getRsH2O() {
        return rsH2O;
    }
}
