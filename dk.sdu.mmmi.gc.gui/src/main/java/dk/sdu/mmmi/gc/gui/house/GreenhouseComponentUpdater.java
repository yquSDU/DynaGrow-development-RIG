package dk.sdu.mmmi.gc.gui.house;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess; 
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.sql.SQLException;
import java.util.Date;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class GreenhouseComponentUpdater {

    private final ControlDomain g;
    private final GreenhouseComponent component;
    private Link<?> dispose;

    public GreenhouseComponentUpdater(ControlDomain g, GreenhouseComponent component) {
        this.g = g;
        this.component = component;
    }

    public void start() {
        if (dispose == null) {
            // Update now.
            update();

            // Update in the future.
            SearchObserver o = new SearchObserver() {
                @Override
                public void onSearchCompleted(Solution s) {
                    update();
                }
            };
            dispose = context(g).add(SearchObserver.class, o);
        }
    }

    public void stop() {
        if (dispose != null) {
            // Stop updating.
            dispose.dispose();
        }
    }

    private void update() {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            Date d = new Date();
            try {
                GreenhouseState s = createState(db, d);
                component.setState(s);
            } catch (SQLException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private GreenhouseState createState(ClimateDataAccess db, Date d) throws SQLException {
        GreenhouseState s = new GreenhouseState();

        // Screen position.
        Sample<Percent> screen = db.selectGeneralScreenPosition(d);
        s.setScreenPosition(screen != null ? screen.getSample() : null);

        // Ventilation.
        Sample<Celcius> air = db.selectAirTemperature(d);
        Sample<Celcius> airThreshold = db.selectVentilationThreshold(d);
        if (air != null && airThreshold != null) {
            Celcius airV = air.getSample();
            Celcius airThresholdV = airThreshold.getSample();
            s.setVentilating(airV.value() > airThresholdV.value());
        } else {
            s.setVentilating(false);
        }

        // Temperature.
        s.setIndoorTemperature(air != null ? air.getSample() : null);

        // Heating.
        Sample<Celcius> heating = db.selectHeatingThreshold(d);
        s.setIndoorTemperatureSP(heating != null ? heating.getSample() : null);

        // Photosynthesis.
        s.setPhotosynthesisOptimization(null); // TODO

        Sample<Percent> photoGoal = db.selectPhotoOptimizationGoal(d);
        s.setPhotosynthesisOptimizationGoal(photoGoal != null ? photoGoal.getSample() : null);

        // CO2.
        Sample<PPM> co2 = db.selectCO2(d);
        s.setCo2(co2 != null ? co2.getSample() : null);

        Sample<PPM> co2sp = db.selectCO2(d);
        s.setCo2SP(co2sp != null ? co2sp.getSample() : null);

        // Humidity.
        Sample<Percent> humidity = db.selectHumidity(d);
        s.setHumidity(humidity != null ? humidity.getSample() : null);

        // Irradiation.
        Sample<WattSqrMeter> irr = db.selectOutdoorLight(d);
        s.setOutdoorIrradiation(irr != null ? irr.getSample() : null);

        // Outdoor temperature.
        Sample<Celcius> outdoorT = db.selectOutdoorTemperature(d);
        s.setOutdoorTemperature(outdoorT != null ? outdoorT.getSample() : null);

        // Outdoor irradiation.
        Sample<UMolSqrtMeterSecond> irrIn = db.selectLightIntensity(d);
        s.setIndoorIrradiation(irrIn != null ? irrIn.getSample() : null);

        Sample<Switch> lamps = db.selectLightStatus(d);
        s.setLightOn(lamps != null ? lamps.getSample().isOn() : false);

        return s;
    }
}
