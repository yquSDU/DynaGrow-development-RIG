Maven Components
----------------

The primary contents of this repository is maven components. Each component is
organized in a folder corresponding to the components artefact-id - e.g.
com.decouplink.

The top-level pom.xml aggregates all maven components in the repository. This
file may be used to clean/compile/test/install everything - e.g. type "mvn
compile".

The subfolder "scripts/" contains utilities that do not fit into any particular
maven component. Please put your scripts here instead of poputing the root
folder.

If you can any questions, try to write any of these emails:

Martin Rytter, mlrj@mmmi.sdu.dk
Michael Rasmussen, jemr@mmmi.sdu.dk
Jan Corfixen Sørensen, jcs@mmmmi.sdk
Bo Nørregaard Jørgensen, bnj@mmmi.sdu.dk

