package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

/**
 * @author mrj
 */
public class EnableConcernAction extends AbstractAction implements HelpCtx.Provider {

    private final Concern concern;
    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);
    private final ControlDomain g;

    public EnableConcernAction(ControlDomain g, Concern concern) {
        super("Enable");
        this.g = g;
        this.concern = concern;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return !concern.isEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        if (concern != null) {
            concern.setEnabled(true);
            cm.saveConcernConfig(g, concern);
        }
    }
}
