package dk.sdu.mmmi.gc.control.light.par.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.calcPARSum;
import dk.sdu.mmmi.gc.control.light.input.FrameStartInput;
import dk.sdu.mmmi.gc.control.light.par.gui.ConfigurePARSumAction;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;

/**
 * PAR sum achieved within the sliding window.
 *
 * @author jcs
 */
public class PARSumAchievedPeriodInput extends AbstractInput<MolSqrMeter> {

    private final ControlDomain g;
    private static final Logger logger = Logger.getLogger(PARSumAchievedPeriodInput.class.getName());
    private final Link<Action> action;

    public PARSumAchievedPeriodInput(ControlDomain greenhouse) {
        super("PAR Light Integral achieved (window)");
        this.g = greenhouse;
        this.action = context(this).add(Action.class, new ConfigurePARSumAction(g, this));
    }

    @Override
    public void doUpdateValue(Date t) {
        MolSqrMeter sum = retrieve(t);
        if (sum != null) {
            setValue(sum);
            store(t, sum);
        }
    }

    private MolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        Duration interval = context(g).one(ControlManager.class).getDelay();

        TimeStamp startTime = context(g).one(FrameStartInput.class).getValue();

        List<Sample<UMolSqrtMeterSecond>> lightLevels = db.selectLightIntensity(startTime.toDate(), t);

        PARSumAchievedConfig cfg = db.selectPARSumAchievedConfig();

        MolSqrMeter sum;

        if (cfg.isActive()) {
            sum = calcPARSum(lightLevels, interval);
        } else {
            sum = cfg.getPARSum();
        }

        return sum;
    }

    private void store(Date t, MolSqrMeter ms) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPARSumAchievedPeriod(new Sample<>(t, ms));
    }

    private void updateConfig(PARSumAchievedConfig cfg) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.updatePARSumAchievedConfig(cfg);
    }

}
