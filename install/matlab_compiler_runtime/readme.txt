To make mablab simulation work
------------------------------

How to make the MATLAB Compiler Runtime work on your mac.

1. Install MCRInstaller.dmg on your mac!

2. Add the following lines to /etc/launchd.conf:
(i.e. use: sudo vim /etc/launch.conf)

setenv MCRROOT /Applications/MATLAB/MATLAB_Compiler_Runtime
setenv DYLD_LIBRARY_PATH /Applications/MATLAB/MATLAB_Compiler_Runtime/v715/runtime/maci64:/Applications/MATLAB/MATLAB_Compiler_Runtime/v715/sys/os/maci64:/Applications/MATLAB/MATLAB_Compiler_Runtime/v715/bin/maci64:/System/Library/Frameworks/JavaVM.framework/JavaVM:/System/Library/Frameworks/JavaVM.framework/Libraries
setenv XAPPLRESDIR /Applications/MATLAB/MATLAB_Compiler_Runtime/v715/X11/app-defaults

NOTE: This will make matlab work when the parent process in launched from the
GUI (i.e. via launchctrl). However, it will not work from the terminal! Also
note, that due to DYLD_LIBRARY_PATH from other applications may stop working,
when launched from the GUI (e.g. Gimp!). These applications will still work when
launched from the terminale (e.g. /Applications/<app>/...).
