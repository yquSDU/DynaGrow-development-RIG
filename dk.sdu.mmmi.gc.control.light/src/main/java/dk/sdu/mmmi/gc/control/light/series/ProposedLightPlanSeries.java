package dk.sdu.mmmi.gc.control.light.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.control.commons.utils.MathUtil;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class ProposedLightPlanSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public ProposedLightPlanSeries(ControlDomain g) {
        this.g = g;
        setName("Proposed light plan");
        setUnit("[on/off]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {
        List<DoubleTimeValue> r = new ArrayList<>();

        Duration timeslotDuration = getLightPlanConfig().getTimeSlotDuration();
        List<Sample<Switch>> proposedLightPlan = getClimateDataAccess().selectProposedLightPlan(to, timeslotDuration).getList();

        Map<Long, Double> raw = MathUtil.interpolate(proposedLightPlan, timeslotDuration);

        for (Map.Entry<Long, Double> s : raw.entrySet()) {
            Date t = new Date(s.getKey());
            if ((t.before(to) || t.equals(to))
                    && (t.after(from) || t.equals(from))) {
                double v = s.getValue();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }

    @Override
    public long getMaxPeriodLenghtMS() {
        return getLightPlanConfig().getTimeSlotDuration().toMS();
    }

    private LightPlanConfig getLightPlanConfig() {
        return context(g).one(LightPlanTodayOutput.class).getConfig();
    }

    private ClimateDataAccess getClimateDataAccess() {
        return context(g).one(ClimateDataAccess.class);
    }
}
