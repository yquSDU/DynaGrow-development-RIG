package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.control.TestData;
import com.decouplink.Utilities;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author yqu
 */
public class ExportTestDataAction extends AbstractAction implements Presenter.Popup {

    private final ControlDomain domain;

    public ExportTestDataAction(ControlDomain d) {
        super("Export Test Data");
        this.domain = d;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String dir = System.getProperty("user.home");
        FileChooserBuilder b = new FileChooserBuilder(dir);
        b.setTitle("Export Test Data");
        b.setDefaultWorkingDirectory(new File(dir));
        b.setApproveText("Export");
        File xmlFile = b.showSaveDialog();
        if (xmlFile == null) {
            return;
        }
        if (xmlFile.exists() && !xmlFile.isFile()) {
            xmlFile.mkdirs();
            xmlFile = new File(xmlFile, "testdata_v1.xml");
        }
        Collection<? extends Input> inputs = Utilities.context(domain).all(Input.class);
        Map<String, Object> inputsMap = new HashMap<String, Object>();
        for (Input input : inputs) {
            inputsMap.put(input.getName(), input.getValue());
        }
        Collection<? extends Output> outputs = Utilities.context(domain).all(Output.class);
        XStream xstream = new XStream(new DomDriver());
        try (OutputStream outputStream = new FileOutputStream(xmlFile)) {
            TestData testData = new TestData();
            testData.setInputsMap(inputsMap);
            if (outputs != null && !outputs.isEmpty()) {
                String result = outputs.iterator().next().getValue().toString();
                Pattern pattern = Pattern.compile("\\[(\\d+)\\].*");
                Matcher matcher = pattern.matcher(result);
                if (matcher.matches()) {
                    result = matcher.group(1);
                }
                testData.setResult(result);
            }
            xstream.toXML(testData, outputStream);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        //mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/database_delete.png", false));
        return mi;
    }

}
