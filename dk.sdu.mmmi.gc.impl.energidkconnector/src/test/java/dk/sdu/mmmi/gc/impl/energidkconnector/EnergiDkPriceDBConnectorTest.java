/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.impl.energidkconnector;

import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class EnergiDkPriceDBConnectorTest {

    private final SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd hh");

    public EnergiDkPriceDBConnectorTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPrices method, of class EnergiDkPriceDBConnector.
     */
    @Test
    public void testGetPrices() throws Exception {
        System.out.println("getPrices");

        // SETUP
        Date start = DF.parse("2020-02-4 00");
        Date end = DF.parse("2020-02-5 23");

        EnergiDkPriceDBConnector instance = new EnergiDkPriceDBConnector();

        // TEST
        Map<Date, Double> result = instance.getPrices(EnergyPriceDatabaseArea.DK_EAST, start, end);

        // ASSERTS
        assertEquals(48, result.size());
    }

}
