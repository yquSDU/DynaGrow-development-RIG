package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Lamp intensity in PAR spectra [umol m^-2 s^-1]
 *
 * @author jcs
 */
public class LampIntensityInput extends AbstractInput<UMolSqrtMeterSecond> {

    private final ControlDomain g;
    private final Link<Action> action;

    public LampIntensityInput(ControlDomain g) {
        super("Lamp PAR");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        UMolSqrtMeterSecond v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, UMolSqrtMeterSecond result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertLampIntensity(new Sample<>(t, result));
    }

    private UMolSqrtMeterSecond retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<UMolSqrtMeterSecond> r = db.selectLampIntensity(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            UMolSqrtMeterSecond oldValue = LampIntensityInput.this.getValue();
            UMolSqrtMeterSecond newValue = DialogUtil.promtUMol(oldValue, "Lamp Intensity");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
