package dk.sdu.mmmi.gc.control.light.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.WINDOW_SIZE;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfXDaysAfter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.Date;

/**
 * A 3 days sliding window is defined by the FrameStartInput and FrameEndInput
 * @author corfixen
 */
public class FrameEndInput extends AbstractInput<TimeStamp> {

    private final ClimateDataAccess db;

    public FrameEndInput(ControlDomain g) {
        super("Frame End");
        db = context(g).one(ClimateDataAccess.class);
    }

    @Override
    public void doUpdateValue(Date t) {

        long endMS = retrieve(t).toMS();

        // rollover after x days
        if (t.getTime() >= endMS) {
            endMS = endOfXDaysAfter(t, WINDOW_SIZE-2).getTime();
            setValue(new TimeStamp(endMS));
        }
        store(t, endMS);
    }

    private TimeStamp retrieve(Date t) {
        Sample<TimeStamp> v = db.selectFrameEnd(t);
        setValue(v.getSample());
        return v.getSample();
    }

    private void store(Date t, long newStartDate) {
        db.insertFrameEnd(t, new TimeStamp(newStartDate));
    }

}
