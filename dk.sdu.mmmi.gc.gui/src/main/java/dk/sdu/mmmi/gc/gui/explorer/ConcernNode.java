package dk.sdu.mmmi.gc.gui.explorer;

import static com.decouplink.Utilities.context;
import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ConcernListener;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.gc.gui.actions.DisableConcernAction;
import dk.sdu.mmmi.gc.gui.actions.EnableConcernAction;
import dk.sdu.mmmi.gc.gui.actions.PrioritizeConcernAction;
import dk.sdu.mmmi.gc.gui.help.ElementHelpAction;
import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.util.lookup.Lookups;

/**
 * @author mrj
 */
public class ConcernNode extends BaseNode implements SearchObserver, ConcernListener {

    private final Concern concern;
    private final Collection<AbstractAction> actions;

    public ConcernNode(ControlDomain g, Concern concern) {
        super(Lookups.singleton(concern));
        this.concern = concern;

        // Actions
        this.actions = Lists.newArrayList(new DisableConcernAction(g, concern),
                new EnableConcernAction(g, concern), new ElementHelpAction(concern));

        if (concern.getPriority() != Concern.HARD_PRIORITY) {
            actions.add(new PrioritizeConcernAction(g, concern));
        }

        // Listeners
        context(g).add(SearchObserver.class, this);

        update();
    }

    @Override
    public final void onSearchCompleted(Solution s) {
        update();
    }

    @Override
    public void onConcernChanged(Concern c) {
        update();
    }

    @Override
    public Action[] getActions(boolean context) {
        return actions.toArray(new Action[]{});
    }

    private void update() {
        if (concern.isEnabled()) {

            // find emotion
            String icon = "dk/sdu/mmmi/gc/silk/emoticon_smile_green_";
            if (!(concern.getEvaluationResult() <= 0.01)) {
                icon = "dk/sdu/mmmi/gc/silk/emoticon_neutral_";
            }
//            if (!concern.isAccepted()) {
//                icon = "dk/sdu/mmmi/gc/silk/emoticon_unhappy_red_";
//            }

            // find concern type
            if (concern.getPriority() == Concern.HARD_PRIORITY) {
                icon = icon + "hard.png";
            } else {
                icon = icon + "soft.png";
            }

            setIconBaseWithExtension(icon);
        } else {
            setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_smile_gray.png");
        }
        setDisplayName(String.format("%s (%d)", concern.toString(), concern.getPriority()));
    }
}
