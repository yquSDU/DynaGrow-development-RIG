package dk.sdu.mmmi.gc.commons.utils;

import static com.google.common.collect.Lists.newArrayList;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.gc.commons.utils.TestHelper.co2Sample;
import static dk.sdu.mmmi.gc.commons.utils.TestHelper.lightSample;
import static dk.sdu.mmmi.gc.commons.utils.TestHelper.tempSample;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import java.text.ParseException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class PhotoUtilTest {

    private PhotoUtil photoUtil;

    public PhotoUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        photoUtil = new PhotoUtil();

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calcPhotoSum method, of class PhotoUtil.
     */
    @Test
    public void testCalcAchievedPhotoSum() throws ParseException {
        System.out.println("calcAchievedPhotoSum");

        //SETUP: 5 min. interval data
        List<Sample<Celcius>> tempHistory
                = newArrayList(tempSample("00:00:00", 19.0), tempSample("00:05:00", 20.0), tempSample("00:10:00", 21.0));

        List<Sample<PPM>> co2History
                = newArrayList(co2Sample("00:00:00", 350.0), co2Sample("00:05:00", 370.0), co2Sample("00:10:00", 400.0));

        List<Sample<UMolSqrtMeterSecond>> lightLevels
                = newArrayList(lightSample("00:00:00", 600.0), lightSample("00:05:00", 700.0), lightSample("00:10:00", 800.0));

        MMolSqrMeter expResult = new MMolSqrMeter(15.6);

        // TEST
        MMolSqrMeter result = photoUtil.calcAchievedPhotoSum(tempHistory, co2History, lightLevels, new Duration(300 * 1000L));


        // ASSSERTS
        assertEquals(expResult.value(), result.value(), 0.1);
    }
}
