package dk.sdu.mmmi.gc.control.light.photo.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.LightUtil;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.Date;
import java.util.List;

/**
 * Expected Photo. sum from natural and supplemental light (combined).
 *
 * @author jcs
 */
public class ExpCombinedPhotoSumPeriodInput extends AbstractInput<MMolSqrMeter> {

    private final ControlDomain g;

    public ExpCombinedPhotoSumPeriodInput(ControlDomain g) {
        super("Exp. Combined photo. sum (remaining day + 2 days)");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        MMolSqrMeter totalPhoto = new MMolSqrMeter(0d);
        // Inputs and Outputs
        Duration lightDuration = context(g).one(LightPlanTodayOutput.class).getConfig().getTimeSlotDuration();

        LightPlan plan = db.selectProposedLightPlan(t, lightDuration);

        // Light plan has been proposed
        if (!plan.getStatusNow().isNotAvailable()) {
            Percent ghTransmission = db.selectLightTransmissionFactor(t).getSample();
            Percent photoPct = db.selectPhotoOptimizationGoal(t).getSample();

            // Get the forecast for remaining day.
            OutdoorLightForecast lightForecast = context(g).one(OutdoorLightForecastInput.class).getValue();

            // Convert forecasted outdoor light to indoor PAR light
            List<Sample<UMolSqrtMeterSecond>> parNatLightLevels = LightUtil.convertToIndoorPAR(lightForecast, ghTransmission);

            // Art. light + Nat. light
            UMolSqrtMeterSecond lampIntensity = db.selectLampIntensity(t).getSample();
            List<Sample<UMolSqrtMeterSecond>> parLightLevels = LightUtil.combineNatArtLight(parNatLightLevels, plan, lampIntensity);

            Duration ctrlDelay = context(g).one(ControlManager.class).getDelay();

            // Calculate the photo sum for natural + art. light
            PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
            totalPhoto = photoUtil.calcPhotoSum(parLightLevels, photoPct, ctrlDelay);

        }
        setValue(totalPhoto);
        store(t, totalPhoto);
    }

    private void store(Date t, MMolSqrMeter mol) {
        // Store prices in internal db
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertExpectedCombinedPhotoSumPeriod(new Sample<>(t, mol));
    }
}
