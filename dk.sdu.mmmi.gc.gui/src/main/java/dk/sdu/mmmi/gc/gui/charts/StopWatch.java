package dk.sdu.mmmi.gc.gui.charts;

/**
 * @author mrj
 */
public class StopWatch {

    private final String name;
    private final long start;

    public StopWatch(String name) {
        this.name = name;
        this.start = System.currentTimeMillis();
    }

    public void stop() {
        long dur = System.currentTimeMillis() - start;
        if(dur > 100) {
            System.out.format("WARNING: %s took %d ms!\n", name, dur);
        }
    }
}
