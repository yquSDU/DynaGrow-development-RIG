package dk.sdu.mmmi.gc.control.ventilation;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.control.ventilation.concern.AllwaysVentilateAboveMaxConcern;
import dk.sdu.mmmi.gc.control.ventilation.concern.CO2WasteAvoidanceConcern;
import dk.sdu.mmmi.gc.control.ventilation.concern.HeatingWasteAvoidanceConcern;
import dk.sdu.mmmi.gc.control.ventilation.concern.VentilationRampConcern;
import dk.sdu.mmmi.gc.control.ventilation.input.DecreasingVentilationTemperaturePerMinuteInput;
import dk.sdu.mmmi.gc.control.ventilation.input.LatestVentilationStartedTimeInput;
import dk.sdu.mmmi.gc.control.ventilation.series.DecreasingVentilationTemperaturePerMinuteSeries;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Concerns
        createCO2WasteAvoidanceConcern(g, d);
        createHeatingWasteAvoidanceConcern(g, d);
        createAllwaysVentilateAboveMaxConcern(g, d);
        createVentilationRampConcern(g, d);

        return d;
    }

    private void createVentilationRampConcern(ControlDomain g, DisposableList d) {
        Concern ventilationRampConcern = new VentilationRampConcern(g);
        cm.loadConcernConfig(g, ventilationRampConcern);
        d.add(context(g).add(Input.class, new LatestVentilationStartedTimeInput(g)));
        d.add(context(g).add(Input.class, new DecreasingVentilationTemperaturePerMinuteInput(g))); // 1 celcius per 5 min.
        d.add(context(g).add(Concern.class, ventilationRampConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, ventilationRampConcern)));
        d.add(context(g).add(DoubleTimeSeries.class, new DecreasingVentilationTemperaturePerMinuteSeries(g)));

    }

    private void createAllwaysVentilateAboveMaxConcern(ControlDomain g, DisposableList d) {
        Concern allwaysVentilateAboveMaxConcern = new AllwaysVentilateAboveMaxConcern(g);
        cm.loadConcernConfig(g, allwaysVentilateAboveMaxConcern);
        d.add(context(g).add(Concern.class, allwaysVentilateAboveMaxConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, allwaysVentilateAboveMaxConcern)));
    }

    private void createHeatingWasteAvoidanceConcern(ControlDomain g, DisposableList d) {
        HeatingWasteAvoidanceConcern heatingWasteAvoidanceConcern = new HeatingWasteAvoidanceConcern(g);
        cm.loadConcernConfig(g, heatingWasteAvoidanceConcern);
        d.add(context(g).add(Concern.class, heatingWasteAvoidanceConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, heatingWasteAvoidanceConcern)));
    }

    private void createCO2WasteAvoidanceConcern(ControlDomain g, DisposableList d) {
        CO2WasteAvoidanceConcern co2WasteAvoidanceConcern = new CO2WasteAvoidanceConcern(g);
        cm.loadConcernConfig(g, co2WasteAvoidanceConcern);
        d.add(context(g).add(Concern.class, co2WasteAvoidanceConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, co2WasteAvoidanceConcern)));
    }
}
