package dk.sdu.mmmi.gc.photosynthesismodel;

/**
 * Photosynthesis model.
 *
 * (* *) comments are from intelligrow 1.0
 * [J m^-2 s^-1] = [W m^-2] = [W/m^2]
 * Theta is variable
 * J_max and R_d varies with temperature, otherwise nothing.
 * For constant temperature J_max and R_d are constant no matter what.
 * P_n_CO2 varies with CO2 and temperature, otherwise nothing.
 * For constant CO2 and temperature P_n_CO2 is constant.
 * Light (in variable I_a) is ONLY used in calculate_P_g()
 */
public class LeafPhotosynthesisArtLight implements PhotosynthesisModel {

//    private static final String UNIT = "umol/m^2 pr second";
    //(*f(Tleaf) (temperature at leaf), temperature dependencies, Arrhenius function [F80, p.88] activation energies [J/mol]*)
    private final double E_J = 37000;           //(* Jmax maximum electron transport rate*)
    private final double E_C = 59356;           //(* KC Rubisco carboxilation*)
    private final double E_O = 35948;           //(* KO Rubisco oxygenation*)
    private final double E_Rd = 66405;          //(* RD dark respiration rate*)
    private final double E_VC = 58520;          //(* VCmax maximum carboxylation rate*)
    //(* [F80, p.88-89] constants for optimum curve temperature dependent electron transport rate*)
    private final double S = 710;               //(* [J/mol/K]*)
    private final double H = 220000;            //(* [J/mol]*)
//    private final double Q_10_Rd = 2.21;      //temperature coefficient
    private final double R = 8.314;             //(* gas constant [J/mol/K]*)
    private final double LGHTCON = 4.59 ;
    private final double V_c_max_25 = 97.875;   //Assumption: maximum carboxylation rate at 25 degrees, [umol m^-2 s^-1]
    private final double K_O_25 = 155;          //(*[G94, p.80] Michaelis Menten constant*)
    private final double K_C_25 = 310;          //(*[G94, p.80] Michaelis Menten constant*)
    private final double p_O2i = 210;           //O2i_ppm = 210;  (*[G94, p.80] O2 partial pressure*)
    private final double T_0 = 273.15;          //(* 273.15 K = 0 oC*)
    private final double T_25 = 298.15;         //(* 273.15+25 K = 25 oC*)
    private final double V_O_C = 0.21;          //(*[F80, p.81] VOC=VOmax/VCmax=ko/kc=constant*)
    private final double rho_CO2_T_0 = 1.98;    //(* density CO2 at T0 [kg/m3], [mg/ml]*)
    private final double M_CO2 = 44e-3;         //(* molar mass CO2 [kg/mol], [mg/umol]*)

    /*
     * CONSTRUCTORS
     */
    public LeafPhotosynthesisArtLight() {
    }

    /**
     * Calculates the photosynthesis rate dependent on temperature, CO2 and sun.
     * The method is implemented in order to reuse the existing model used in
     * Climate monitor but ensuring same input and outputs as in the
     * original IntelliGrow.
     *
     * @see dk.sdu.mmmi.predict.igrow.util.PhotosynthesisModel#calculate(double, double, double)
     */
    public double calculate(double temp, double co2, double par, PlantSpecificPhotoParams plantParams) {
        double T_l = temp + T_0;//T_1 is temperature in kelvin, this is leaf temperature
        double CO2 = co2;       //CO2 levels, very likely in [ppm]
        double I_a = par / 4.6; //divide by 4.6, to convert from [μmol m^-2 s^-1] to [J m^-2 s^-1]=[W/m^2], is the absorbed irradiance measured as photosynthetic active radiation (PAR).

        double P_g;             //Gross assimilation rate at present light level, [mg CO2 m^-2 s^-1]
        double P_g_max;         //(*assimilation rate at light saturation [mg CO2/m2 leaf/s]*), 2nd explaination: (* maximal rate of gross photosynthesis [mg CO2/m2 leaf/s]*), [mg m^-2 s^-1]
        double a_0;             //The light use efficiency in absence of oxygen, (0.017 mg CO2 J^-1)
        double a_l;             //The increase in light use efficiency at higher CO2 concentrations, [mg CO2 J^-1]
        double Gamma;           //(* [F80, p.85] CO2 compensation concentration*)(* in absence of dark respiration [ppm], [ubar]*)(* reduction of light use efficiency by photorespiration, affected by CO2 concentration*), [ppm]
        double P_n_max;         //maximal rate of net photosynthesis, [mg CO2 m^-2 s^-1]
        double P_n_CO2;         //From paper: CO2 limited net assimilation, [mg CO2 m^-2 s^-1]
        double J_max;           //Two explainations: (* [F80, p.84] maximum electron transport rate*)(* (on chlorofylbasis) [umol e-/m2/s]*) AND (* [??  (2.31 at 25 oC)] maximum endogenous*) (* photosynthetic capacity [mg CO2/m2/s]*), [umol m^-2 s^-1]
        double rho_CO2_T_l;     //(* [gaslaw] density CO2 at Tleaf [kg/m3], [mg/ml]*), [kg m^-3]
        double r_CO2;           //Assumption: another sort of resistance to CO2 diffusion [s/m], [s m^-1]
        double V_c_max;         //(* [F80, p.83] maximum carboxylation rate, [umol m^-2 s^-1]
        double r_c_CO2;         //(* carboxylation resistance [s/m]*), [bar s m^-1]
        double P_n;             //Photosynthesis, [mg m^-2 s^-1]
        double temp_diff;

        double R_d_25;          //(*[F80, p.89] dark respiration rate [umol CO2/m2/s]*), assumptiom: at 25 degrees
        double R_d;             //(*dark respiration rate [mg CO2/m2/s]*), [mg m^-2 s^-1]
        double r_b_H2O;         //Boundary layer resistance, [s/m]
        double Theta;           //This is convexity(theta), (* [G94, p.80] degree of curvature of CO2 response of light saturated net photosynthesis [-]*)
        double F;               //Fraction of PAR absorbered from the non photosynthetic plant material [%]

        double J_max_25;        //Assumption: maximum electron transport rate at 25 degrees, [umol e-/m2/s]
        double RHO_Chl;         //Chlorophyll density [g Chl m^-2]
        double r_s_H2O;         //Stomatal resistance, [s/m]

        temp_diff = calculate_temp_diff(T_l);        //[mol J^-1]


        //
        // Plant specific parameters
        //
        R_d_25 = plantParams.getRd25();

        R_d = calculate_R_d(T_l, temp_diff, R_d_25);

        r_b_H2O = plantParams.getRbH2O();
        Theta = plantParams.getTheta();
        F = plantParams.getF();
        RHO_Chl = plantParams.getRhoChl();

        r_s_H2O = plantParams.getRsH2O();

        Gamma = calculate_Gamma(temp_diff);
        rho_CO2_T_l = calculate_rho_CO2_T_l(T_l);
        J_max_25 = calculate_J_max_25(RHO_Chl);
        J_max = calculate_J_max(T_l, temp_diff, J_max_25);

        V_c_max = calculate_V_c_max(temp_diff);

        r_c_CO2 = calculate_r_c_CO2(temp_diff, rho_CO2_T_l, V_c_max);

        r_CO2 = calculate_r_CO2(r_s_H2O, r_b_H2O, r_c_CO2);
        P_n_CO2 = calculate_P_n_CO2(CO2, Gamma, rho_CO2_T_l, r_CO2);
        a_0 = calculate_a_0(F);
        a_l = calculate_a_l(CO2, Gamma, a_0);

        P_n_max = calculate_P_n_max(P_n_CO2, J_max, Theta);
        P_g_max = calculate_P_g_max(P_n_max, R_d);

        P_g = calculate_P_g(P_g_max, a_l, I_a);
        P_n = calculate_P_n(P_g, R_d);

        P_n *= 22.7222; // multiply by 22.7222 to convert from [mg m^-2 s^-1] to [μmol m^-2 s^-1]

        return P_n;     //Return value in [μmol m^-2 s^-1]
    }

    private double calculate_temp_diff(double T_l) { //  -- [mol J^-1]
        return (T_l - T_25) / (T_l * R * T_25);
    }

    private double calculate_P_n(double P_g, double R_d) { // -- [mg m^-2 s^-1]
        return P_g - R_d;
    }

    private double calculate_P_g_max(double P_n_max, double R_d) { // -- [mg m^-2 s^-1]
        return P_n_max + R_d;
    }

    private double calculate_P_g(double P_g_max, double a_l, double I_a) { // [1] -- [mg m^-2 s^-1]
        double part1 = 1 - Math.exp((-1 * a_l * I_a) / P_g_max);        //I_a is light in the greenhouse converted to [J m^-2 s^-1]
        return P_g_max * part1;
    }
    private double calculate_a_0(double F) { // [2] -- [mg J^-1]
        double alpha = (M_CO2 / 4) * ((1 - (F)) / 2); // electron yield of absorbed photons at low light intensities: 2 electrons per absorbed photon [umol e-/umol photons]
        return alpha * LGHTCON;
    }

    //Assumption: This formula is in case there is a higher level of CO2 concentration
    //Max here is Ci, which is the intercellular CO2 concentra [ppm].
    private double calculate_a_l(double CO2, double Gamma, double a_0) { // [2] -- [mg J^-1]
        double max = Math.max(CO2, Gamma);
        return a_0 * ((max - Gamma) / (max + 2 * Gamma));
    }

    private double calculate_Gamma(double temp_diff) { // [3] -- [?bar] = [μmol mol^-1] = [ppm]
        double part1 = (p_O2i * V_O_C) / 2;
        double part2_t = K_C_25 * Math.exp(E_C * temp_diff);
        double part2_n = K_O_25 * Math.exp(E_O * temp_diff);

        return part1 * (part2_t / part2_n);
    }

    private double calculate_P_n_max(double P_n_CO2, double J_max, double Theta) { // [4] -- [mg m^-2 s^-1]
        double part1 = Math.sqrt(Math.pow(P_n_CO2 + J_max, 2) - (4 * Theta * P_n_CO2 * J_max));
        return (P_n_CO2 + J_max - part1) / (2 * Theta);
    }

    private double calculate_P_n_CO2(double CO2, double Gamma, double rho_CO2_T_l, double r_CO2) { // [5] -- [mg m^-2 s^-1]
        return ((Math.max(CO2, Gamma) - Gamma) * rho_CO2_T_l) / r_CO2;
    }

    private double calculate_rho_CO2_T_l(double T_l) { // [6] -- [kg m^-3]
        return rho_CO2_T_0 * (T_0 / T_l);
    }

    private double calculate_r_c_CO2(double temp_diff, double rho_CO2_T_l, double V_c_max) { // [7] -- [bar s m^-1]
        double part1 = K_C_25 * Math.exp(E_C * temp_diff);
        double part2_n = K_O_25 * Math.exp(E_O * temp_diff);
        double part2 = 1 + (p_O2i / part2_n);

        return ((part1 * part2 * rho_CO2_T_l) / V_c_max) / M_CO2;
    }

    private double calculate_r_CO2(double r_s_H2O, double r_b_H2O, double r_c_CO2) { // [8] -- [s m^-1] -- TODO OBS: r_c_CO2 ^^
        return 1.6 * r_s_H2O + 1.37 * r_b_H2O + r_c_CO2;
    }

    private double calculate_J_max_25(double RHO_Chl) {
        return 467 * RHO_Chl;  // 467 is an emperical number for the max. posible transport of electrons
    }

    private double calculate_J_max(double T_l, double temp_diff, double J_max_25) { // [9] -- [μmol m^-2 s^-1]
        double part1 = Math.exp(E_J * temp_diff);
        double part2 = 1 + Math.exp((S - H / T_25) / R);
        double part3 = 1 + Math.exp((S - H / T_l) / R);

        return (M_CO2 / 4) * J_max_25 * part1 * (part2 / part3);
    }

    private double calculate_R_d(double T_l, double temp_diff, double R_d_25) { // [10] -- [mg m^-2 s^-1]
        //double Q_10_Rd = Math.exp(13.6e-6 * E_Rd);
        //return M_CO2 * R_d_25 * Math.pow(Q_10_Rd, (T_l - T_25) / 10);
        return M_CO2 * R_d_25 * Math.exp(E_Rd * temp_diff);

    }

    private double calculate_V_c_max(double temp_diff) { // [11] -- [μmol m^-2 s^-1]
        return V_c_max_25 * Math.exp(E_VC * temp_diff);
    }
}
