package dk.sdu.mmmi.gc.control.commons.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class CO2Series extends DoubleTimeSeries {

    private final ControlDomain g;

    public CO2Series(ControlDomain g) {
        this.g = g;
        setName("CO2");
        setUnit("[PPM]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<PPM>> raw = db.selectCO2(from, to);
            for (Sample<PPM> s : raw) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }
        return r;
    }
}
