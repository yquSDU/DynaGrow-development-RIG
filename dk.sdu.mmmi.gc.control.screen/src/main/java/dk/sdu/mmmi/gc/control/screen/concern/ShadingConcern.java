package dk.sdu.mmmi.gc.control.screen.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import dk.sdu.mmmi.gc.control.screen.input.OutdoorLightMaximumInput;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 * Protect plants of singeing when the outdoor light intensity is high (i.e
 * above max limit)
 *
 * @author jcs
 */
public class ShadingConcern extends AbstractConcern {

    public ShadingConcern(ControlDomain g) {
        super("Shading", g, 1);
    }

    @Override
    public double evaluate(Solution s) {

        double scrPosFactor = s.getValue(GeneralScreenPositionOutput.class).value();
        double fitness;

        // If out light is above max then best fitness is when screen is 90 % on
        if (isLightAboveMax(s)) {
            fitness = abs(90 - scrPosFactor);
        } else {
            fitness = 0;
        }
        return fitness;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        double scrPosPct = s.getValue(GeneralScreenPositionOutput.class).asPercent();

        if (isLightAboveMax(s)) {
            r.put(GeneralScreenPositionOutput.class, String.format("screen pos. %.1f - as high as possible", scrPosPct));
        }

        return r;
    }

    private boolean isLightAboveMax(Solution s) {
        double outLight = s.getValue(OutdoorLightInput.class).value();
        double outMaxLight = s.getValue(OutdoorLightMaximumInput.class).value();
        return outMaxLight < outLight;
    }
}
