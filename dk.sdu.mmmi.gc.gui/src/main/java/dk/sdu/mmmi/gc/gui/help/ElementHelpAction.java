package dk.sdu.mmmi.gc.gui.help;

import dk.sdu.mmmi.controleum.api.moea.Element;
import java.awt.event.ActionEvent;
import java.net.URL;
import javax.swing.AbstractAction;
import org.openide.windows.TopComponent;

/**
 * @author mrj
 */
public class ElementHelpAction extends AbstractAction {

    private final Element element;

    public ElementHelpAction(Element element) {
        super("Help");
        this.element = element;
    }

    @Override
    public boolean isEnabled() {
        return element.getHtmlHelp() != null;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        URL url = element.getHtmlHelp();
        if (url != null) {
            TopComponent win = new ElementHelpTopComponent(element);
            win.open();
            win.requestActive();
        }
    }
}
