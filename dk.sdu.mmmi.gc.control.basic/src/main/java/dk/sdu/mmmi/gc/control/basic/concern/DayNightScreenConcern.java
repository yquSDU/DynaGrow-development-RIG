package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.control.basic.input.DayScreenPositionInput;
import dk.sdu.mmmi.gc.control.basic.input.NightScreenPositionInput;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.NIGHT_LIMIT;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj
 */
public class DayNightScreenConcern extends AbstractConcern {

    public DayNightScreenConcern(ControlDomain g) {
        super("Day/night screen control", g, 1);
    }

    @Override
    public double evaluate(Solution s) {

        // Data.
        Percent t = s.getValue(GeneralScreenPositionOutput.class);

        // Fitness.
        if (isNight(s)) {
            Percent night = s.getValue(NightScreenPositionInput.class);
            return Math.abs(night.asPercent() - t.asPercent());
        } else {
            Percent day = s.getValue(DayScreenPositionInput.class);
            return Math.abs(day.asPercent() - t.asPercent());
        }

    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (isNight(s)) {
            Percent night = s.getValue(NightScreenPositionInput.class);
            r.put(GeneralScreenPositionOutput.class, String.format("prefer %.1f", night.asPercent()));
        } else {
            Percent day = s.getValue(DayScreenPositionInput.class);
            r.put(GeneralScreenPositionOutput.class, String.format("prefer %.1f", day.asPercent()));
        }

        return r;
    }

    private boolean isNight(Solution s) {
        WattSqrMeter outdoorLight = s.getValue(OutdoorLightInput.class);
        return outdoorLight.value() < NIGHT_LIMIT;
    }

}
