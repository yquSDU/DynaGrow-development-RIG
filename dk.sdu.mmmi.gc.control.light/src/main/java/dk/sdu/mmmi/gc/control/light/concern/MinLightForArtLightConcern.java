package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.commons.input.LightTransmissionFactorInput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.convertToIndoorPAR;
import dk.sdu.mmmi.gc.control.light.input.MinLightForArtLightInput;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.Date;
import java.util.List;

/**
 * Only lit art. light when the indoor light is below a certain level.
 *
 * @author jcs
 */
public class MinLightForArtLightConcern extends AbstractConcern {

    private double costValue;

    public MinLightForArtLightConcern(ControlDomain g) {
        super("Min. light for art. light", g, 5);
    }

    @Override
    public double evaluate(Solution option) {

        LightPlan lp = option.getValue(LightPlanTodayOutput.class);
        UMolSqrtMeterSecond minLevel = option.getValue(MinLightForArtLightInput.class);
        OutdoorLightForecast outLightForecast = option.getValue(OutdoorLightForecastInput.class);

        Percent greenTrans = option.getValue(LightTransmissionFactorInput.class);

        costValue = 0.0;
        for (Sample<Switch> lightStatus : lp.getList()) {

            if (lightStatus.getSample().isOn()) {
                Date ts = lightStatus.getTimestamp();
                List<Sample<UMolSqrtMeterSecond>> indoorForecast = convertToIndoorPAR(outLightForecast.getPeriod(ts, ts), greenTrans);

                if (indoorForecast.size() > 0) {
                    UMolSqrtMeterSecond forecastedIndoorLight = indoorForecast.get(0).getSample();
                    if (forecastedIndoorLight.value() > minLevel.value()) {
                        costValue += 1;
                    }
                }
            }
        }

        return costValue;
    }

}
