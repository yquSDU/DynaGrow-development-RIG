package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.gui.house.HouseTopComponent;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

/**
 * @author mrj
 */
public class ShowHouseAction extends SystemAction implements Presenter.Popup {

    @Override
    public String getName() {
        return "Show house";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public ControlDomain getGreenhouse() {
        ControlDomain g =
                Utilities.actionsGlobalContext().lookup(ControlDomain.class);
        return g;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ControlDomain g = getGreenhouse();
        if (g != null) {
            TopComponent win = new HouseTopComponent(g);
            win.open();
            win.requestActive();
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/house.png", false));
        return mi;
    }
}
