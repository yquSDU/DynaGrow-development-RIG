package dk.sdu.mmmi.gc.test;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import static dk.sdu.mmmi.controleum.impl.entities.units.Switch.OFF;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import net.jcip.annotations.NotThreadSafe;

/**
 * Integration tests
 *
 * @author jcs
 */
@NotThreadSafe
public class FeatureTest {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private GreenhouseTestFixture testFixture;

    @Before
    public void setUp() throws SQLException {
        //System.setProperty("org.openide.util.Lookup", TestLookup.class.getName());
    }

    @After
    public void tearDown() throws SQLException {
        testFixture.dispose();
    }

    @Test
    public void basicFI() throws Exception {
        // SETUP:
        System.out.println("######################");
        System.out.println(System.getProperty("java.library.path"));
        
        testFixture = new GreenhouseTestFixture("BasicFI", df.parse("2012-04-21 23:00")).
                // Security
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                avoidOverheatingFeature(30).
                // Basic control
                dayNightVentFeature(5, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                preferNoLightFeature(5).
                // Interaction
                isolationScreenFeature(5);

        // TEST:
        Date from = df.parse("2012-04-21 23:40");
        Date to = df.parse("2012-04-22 23:59");
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    /**
     * The security experiment demonstrates how the wasteful CO2 dosing
     * interaction, identified in the conflict experiment, can be avoid by
     * introducing a security requirement specification. That is, a constraint
     * specification (CO2WasteAvoidanceFeature) expressing that ventilation only
     * should be allowed when CO2 is not dosed.
     *
     * The experiment demonstrates a feature interaction between the SScrnThres
     * and SScrnIso features that emerges during night if the night threshold is
     * configured to take off the screens at the same time the isolation feature
     * wants to use screens for isolation. The purpose of the experiment is to
     * show how the interaction can be resolved by adjusting the priorities of
     * the conflicting \ScrnThres{F} and \ScrnIso{F} features such that
     * isolation is preferred over threshold control. We show that the resulting
     * screen control that embrace isolation during night but still incorporate
     * the threshold control when possible.
     */
    @Ignore
    @Test
    public void basicFIResolved() throws Exception {
        // SETUP:
 //       System.setProperty("java.library.path", "C:/MATLAB");
        testFixture = new GreenhouseTestFixture("BasicFIResolved", df.parse("2020-01-31 00:00")).
                // Environment
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                avoidOverheatingFeature(30).
                // Interactions resolved
                co2WasteAvoidanceFeature().
                // Basic
                dayNightVentFeature(5, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                preferNoLightFeature(5).
                isolationScreenFeature(4);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    //--------------------------------------------------------------------------
    @Ignore
    @Test
    public void infiniteLoopFI() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("LoopFI", df.parse("2020-01-31 00:00")).
                // Security
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                co2WasteAvoidanceFeature().
                avoidOverheatingFeature(30).
                // Basic control
                dayNightScreenFeature(5, 0, 0).
                dayNightCO2Feature(5, 750, 350).
                preferNoLightFeature(5).
                isolationScreenFeature(4).
                // Interaction
                dayNightVentFeature(5, 19, 18).
                dayNightTempFeature(5, 22, 20);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    @Ignore
    @Test
    public void infiniteLoopFIResolved() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("LoopFIResolved", df.parse("2020-01-31 00:00")).
                // Security
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                avoidOverheatingFeature(30).
                // Interaction resolved
                heatingWasteAvoidanceFeature().
                // Basic control
                dayNightScreenFeature(5, 0, 0).
                dayNightCO2Feature(5, 750, 350).
                preferNoLightFeature(5).
                co2WasteAvoidanceFeature().
                isolationScreenFeature(4).
                // Interaction
                dayNightVentFeature(5, 19, 18).
                dayNightTempFeature(5, 22, 20);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    //--------------------------------------------------------------------------
    @Ignore
    @Test
    public void basicExperiment() throws Exception {
        // SETUP: V1
        testFixture = new GreenhouseTestFixture("Basic", df.parse("2020-01-31 00:00")).
                // Environment
                avoidOverheatingFeature(30).
                heatingWasteAvoidanceFeature().
                co2WasteAvoidanceFeature().
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                // Basic
                preferNoLightFeature(5).
                dayNightVentFeature(5, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                isolationScreenFeature(3);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");

        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    @Ignore
    @Test
    public void photoExtExperiment() throws Exception {
        // SETUP: V1
        testFixture = new GreenhouseTestFixture("PhotoExt", df.parse("2020-01-31 00:00")).
                // Environment
                avoidOverheatingFeature(30).
                heatingWasteAvoidanceFeature().
                co2WasteAvoidanceFeature().
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                // Basic
                preferNoLightFeature(5).
                dayNightVentFeature(5, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                isolationScreenFeature(3).
                // Photo
                photoOptFeature(3);
        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");

        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    @Ignore
    @Test
    public void photoScreenExtExperiment() throws Exception {
        // SETUP: V1
        testFixture = new GreenhouseTestFixture("PhotoScreenExt", df.parse("2020-01-31 00:00")).
                // Environment
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                avoidOverheatingFeature(30).
                co2WasteAvoidanceFeature().
                heatingWasteAvoidanceFeature().
                // Basic
                dayNightVentFeature(5, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                preferNoLightFeature(5).
                isolationScreenFeature(4).
                // Photo ext
                photoOptFeature(3).
                // Photo screen ext
                photoScreenFeature(2, 10, 20);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");

        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    //--------------------------------------------------------------------------
    /**
     * The goal experiment is based on the interaction between SLightFixed and
     * SLightPAR described. The purpose of the experiment is to demonstrate that
     * we can resolve the interaction by changing the goal of the SLightFixed
     * specification PAR sum can be achieved.
     *
     * @throws java.lang.Exception
     */
    @Ignore
    @Test
    public void resolutionWithGoalExperiment() throws Exception {
        // SETUP:
        FixedDayLightPlan fixedLp = new FixedDayLightPlan().
                set(0, OFF).set(1, OFF).set(23, OFF);
        testFixture = new GreenhouseTestFixture("GoalExperimentFI", df.parse("2020-01-31 00:00")).
                achievePARSum(45).
                fixedLightPlanFeature(fixedLp).
                preferNoLightFeature(5);

        // TEST:
        Date from = df.parse("2020-01-31 00:00");
        Date to = df.parse("2020-01-31 23:59");
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // Solve interaction by adjusting sum goal
        testFixture = new GreenhouseTestFixture("GoalExperimentFISolved", df.parse("2020-01-31 00:00")).
                achievePARSum(40).
                fixedLightPlanFeature(fixedLp).
                preferNoLightFeature(5);

        // TEST:
        testFixture.startNegotiation(from, to);
        testFixture.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }
}
