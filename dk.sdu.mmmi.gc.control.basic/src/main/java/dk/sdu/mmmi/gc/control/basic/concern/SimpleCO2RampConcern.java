package dk.sdu.mmmi.gc.control.basic.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static java.lang.Math.abs;

/**
 * Simple CO2 Ramp
 *
 * @author jcs
 */
public class SimpleCO2RampConcern extends AbstractConcern {

    Date timeSp = new Date(0);
    private Sample<PPM> sp;
    private final double ramp = 50; // ramp rate 50 PPM/ctrl_interval
    private double diff;

    public SimpleCO2RampConcern(ControlDomain g) {
        super("Simple CO2 ramp", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        if (!timeSp.equals(s.getTime())) {
            readSp(s);
        }

        PPM proposedC02 = s.getValue(CO2GoalOutput.class);
        diff = abs(proposedC02.subtract(sp.getSample()).roundedValue());

        return (diff <= ramp) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        if (sp != null) {
            r.put(CO2GoalOutput.class,
                    String.format("diff is %.1f", diff));
        }
        return r;
    }

    private void readSp(Solution s) {
        // Read CO2 setpoint to find ramped set point
        timeSp = s.getTime();
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        sp = db.selectCO2Threshold(timeSp);
    }
}
