package dk.sdu.mmmi.gc.photosynthesismodel;

import org.openide.util.lookup.ServiceProvider;



/**
 * @author ao
 */
@ServiceProvider(service = PhotoModelFactory.class)
public class PhotoModelFactoryImpl implements PhotoModelFactory {

    public PhotoModelFactoryImpl() {
    }

    public PhotosynthesisCalculator getPhotosynthesisCalculator(PhotosynthesisModel model, double minTemp, double maxTemp, double minCO2, double maxCO2, boolean searchCO2First) {
        return new PhotosynthesisCalculator2(model, minTemp, maxTemp, minCO2, maxCO2, searchCO2First);
    }

    public PhotosynthesisCalculator getPhotosynthesisCalculator(double minTemp, double maxTemp, double minCO2, double maxCO2, boolean searchCO2First) {
        return new PhotosynthesisCalculator2(getPhotosynthesisModel(), minTemp, maxTemp, minCO2, maxCO2, searchCO2First);
    }

    public PhotosynthesisCalculator getPhotosynthesisCalculator() {
        return new PhotosynthesisCalculator2(getPhotosynthesisModel(), 14, 32, 350, 1450, true);
    }

    public PhotosynthesisModel getPhotosynthesisModel(){
        return new LeafPhotosynthesisArtLight();
    }

    public PlantSpecificPhotoParams getPlantSpecPhotoParams() {
        return new PlantSpecificPhotoParams();
    }

}
