package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.gui.charts.ChartListener;
import dk.sdu.mmmi.gc.gui.charts.ChartManager;
import java.util.Collections;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author jcs
 */
public class ChartsNodeChildren extends Children.Keys<ChartConfig> implements ChartListener {

    private List<ChartConfig> keys;
    private final ControlDomain g;
    private final ChartManager cm;
    private final DisposableList disposables = new DisposableList();

    public ChartsNodeChildren(ControlDomain g) {
        this.g = g;

        this.cm = context(g).one(ChartManager.class);
        disposables.add(context(cm).add(ChartListener.class, this));

        keys = cm.getCharts();
        setKeys(keys);
    }

    @Override
    public void onChartChanged(ChartConfig cfg) {
        keys.remove(cfg);
        keys.add(cfg);

        setKeys(keys);
    }

    @Override
    public void onChartDelete(ChartConfig cfg) {
        keys.remove(cfg);
        setKeys(keys);
    }

    @Override
    protected void removeNotify() {
        setKeys(Collections.EMPTY_SET);
        disposables.dispose();
    }

    @Override
    protected Node[] createNodes(ChartConfig chart) {

        ChartNode chartNode = new ChartNode(g, chart);
        return new Node[]{chartNode};
    }
}
