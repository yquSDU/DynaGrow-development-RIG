package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import static com.google.common.collect.Lists.newArrayList;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Value;
import dk.sdu.mmmi.controleum.api.moea.ValueListener;
import dk.sdu.mmmi.gc.gui.actions.DisableOutputAction;
import dk.sdu.mmmi.gc.gui.actions.EnableOutputAction;
import dk.sdu.mmmi.gc.gui.help.ElementHelpAction;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;

/**
 * @author mrj
 */
public class OutputNode extends BaseNode implements SearchObserver, ValueListener {

    private final Output output;
    private final Result<Concern> cR;
    private final List<Action> actions;
    private final Link<ValueListener> valueListener;
    private final Link<SearchObserver> searchListener;

    public OutputNode(ControlDomain g, Output output) {
        super(Lookups.singleton(output));
        this.output = output;

        this.actions = new ArrayList<>(context(output).all(Action.class));
        this.actions.addAll(newArrayList(new EnableOutputAction(), new DisableOutputAction(),
                new ElementHelpAction(output)));

        valueListener = context(output).add(ValueListener.class, this);
        searchListener = context(g).add(SearchObserver.class, this);

        Lookup sc = Utilities.actionsGlobalContext();
        cR = sc.lookupResult(Concern.class);
        cR.addLookupListener(selectedConcernListener);

        update();
    }

    @Override
    public void destroy() throws IOException {
        valueListener.dispose();
        searchListener.dispose();
        cR.removeLookupListener(selectedConcernListener);
    }

    @Override
    public final void onSearchCompleted(Solution s) {
        update();
    }

    private void update() {
        // Set icon.
        if (output.isEnabled()) {
            setIconBaseWithExtension("dk/sdu/mmmi/gc/impl/greenhouseicons/Output.png");
        } else {
            setIconBaseWithExtension("dk/sdu/mmmi/gc/impl/greenhouseicons/GrayOutput.png");
        }

        // Set name.
        Concern c = Utilities.actionsGlobalContext().lookup(Concern.class);
        String help = (c != null) ? c.getValueHelpOrNull(output.getClass()) : null;

        if (help != null) {
            // Set name + optimal value.
            setDisplayName(String.format("%s (<font color='3333FF'>%s</font>)", output.toString(), help));
        } else {
            // Set name.
            setDisplayName(output.toString());
        }
    }

    @Override
    public Action[] getActions(boolean context) {
        return actions.toArray(new Action[]{});
    }

    @Override
    public void onValueChanged(Value c) {
        update();
    }

    private final LookupListener selectedConcernListener = new LookupListener() {
        @Override
        public final void resultChanged(LookupEvent ev) {
            update();
        }
    };
}
