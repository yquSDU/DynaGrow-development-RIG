package dk.sdu.mmmi.gc.control.light.gui;

import dk.sdu.mmmi.gc.control.light.gui.LightForecastDBConfigPanel;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class ConfigureLightForecastDBAction extends AbstractAction implements Presenter.Popup {

    private final ControlDomain g;
    private final ClimateDataAccess db;

    public ConfigureLightForecastDBAction(ControlDomain g) {
        super("Configure Light Forecast Database");
        this.g = g;
        this.db = context(g).one(ClimateDataAccess.class);

    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        LightForecastConfig cfg = db.selectLightForecastConfig();

        // Panel.
        LightForecastDBConfigPanel pnl = new LightForecastDBConfigPanel(cfg);

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure Light Forecast Database");

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            updateConfig(pnl.get());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private void updateConfig(LightForecastConfig cfg) {
        db.updateLightForecastConfig(cfg);
    }
}
