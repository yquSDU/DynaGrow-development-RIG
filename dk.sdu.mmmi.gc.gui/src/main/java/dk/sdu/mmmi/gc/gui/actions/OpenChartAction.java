package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.gui.charts.ChartTopComponent;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author jcs
 */
public class OpenChartAction extends AbstractAction {

    private final ChartConfig chart;
    private final ControlDomain g;

    public OpenChartAction(ControlDomain g, ChartConfig chart) {
        super("Open Chart");
        this.g = g;
        this.chart = chart;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        ChartTopComponent win = new ChartTopComponent(g, chart);
        win.open();
        win.requestActive();
    }
}
