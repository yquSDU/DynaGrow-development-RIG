package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.moea.Output;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.util.HelpCtx;
import org.openide.util.Utilities;

/**
 * @author mrj
 */
public class DisableOutputAction extends AbstractAction implements HelpCtx.Provider {

    public DisableOutputAction() {
        super("Disable");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public Output getOutput() {
        return Utilities.actionsGlobalContext().lookup(Output.class);
    }

    @Override
    public boolean isEnabled() {
        return getOutput().isEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Output c = getOutput();
        if (c != null) {
            c.setEnabled(false);
        }
    }
}
