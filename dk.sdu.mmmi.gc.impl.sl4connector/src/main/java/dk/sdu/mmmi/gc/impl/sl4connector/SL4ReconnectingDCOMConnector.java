package dk.sdu.mmmi.gc.impl.sl4connector;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Wraps a SL4BasicDCOMConnector while making sure that a new instance is
 * created after each error. This approach is more reliable than just using a
 * SL4BasicDCOMConnector because the session sometimes keep failing after the
 * first error is encountered.
 *
 * @author mrj, ao
 */
public class SL4ReconnectingDCOMConnector implements SL4DCOMConnector {

    private final SL4Login account;
    private final AtomicReference<SL4DCOMConnector> delegate = new AtomicReference<SL4DCOMConnector>();

    public SL4ReconnectingDCOMConnector(SL4Login account)
            throws SL4DCOMException {

        this.account = account;
        reinit();
    }

    private void reinit() throws SL4DCOMException {
        close();
        delegate.set(new SL4BasicDCOMConnector(account));
    }

    @Override
    public void close() throws SL4DCOMException {
        try {
            SL4DCOMConnector d = delegate.get();
            if (d != null) {
                d.close();
            }
        } finally {
            delegate.set(null);
        }
    }

    @Override
    public double readValue(String projectID, String variableID) throws SL4DCOMException {
        try {
            return delegate.get().readValue(projectID, variableID);
        } catch (SL4DCOMException x) {
            reinit();
            throw x;
        }
    }

    @Override
    public void writeValue(String projectID, String variableID, double value) throws SL4DCOMException {
        try {
            delegate.get().writeValue(projectID, variableID, value);
        } catch (SL4DCOMException x) {
            reinit();
            throw x;
        }
    }
}
