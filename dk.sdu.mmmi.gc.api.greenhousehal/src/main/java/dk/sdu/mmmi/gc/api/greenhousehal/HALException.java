package dk.sdu.mmmi.gc.api.greenhousehal;

/**
 * @author mrj
 */
public class HALException extends Exception {

    public static final HALException NOT_CONNECTED = new HALException("Not connected.");

    public HALException(String msg) {
        super(msg);
    }

    public HALException(String msg, Exception cause) {
        super(msg, cause);
    }
}
