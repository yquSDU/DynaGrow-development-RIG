package dk.sdu.mmmi.gc.control.screen.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Allowed percentage of photo. loss without screen
 *
 * @author jcs
 */
public class ScreenPhotoLossRatioInput extends AbstractInput<Percent> {

    private final ControlDomain g;

    public ScreenPhotoLossRatioInput(ControlDomain g) {
        super("Screen Photo. ratio");
        this.g = g;
        context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        Percent v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Percent result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertScreenPhotoRatio(new Sample<>(t, result));
    }

    private Percent retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Percent> r = db.selectScreenPhotoRatio(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Percent oldValue = ScreenPhotoLossRatioInput.this.getValue();
            Percent newValue = DialogUtil.promtPercent(oldValue, "Screen closing pct.");
            if (newValue != null) {
                setValue(newValue);
                store(new Date(), newValue);
            }
        }
    }
}
