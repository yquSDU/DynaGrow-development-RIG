package dk.sdu.mmmi.gc.control.screen.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.control.commons.utils.EnergyUtil;
import java.sql.SQLException;
import java.util.Date;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class EnergyBalanceInput extends AbstractInput<Energy> {

    private final ControlDomain g;

    public EnergyBalanceInput(ControlDomain g) {
        super("Engergy Balance");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        try {
            Energy d = EnergyUtil.getEnergyBalance(g, t);
            if (d != null) {
                setValue(d);
                store(t, d);
            }
        } catch (HALException | SQLException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    protected void store(Date t, Energy result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertEnergyBalance(new Sample<>(t, result));
    }
}
