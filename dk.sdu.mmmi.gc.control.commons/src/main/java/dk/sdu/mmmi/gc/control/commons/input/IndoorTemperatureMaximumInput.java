package dk.sdu.mmmi.gc.control.commons.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author mrj
 */
public class IndoorTemperatureMaximumInput extends AbstractInput<Celcius> {

    private final ControlDomain g;
    private final Link<Action> action;

    public IndoorTemperatureMaximumInput(ControlDomain g) {
        super("Maximum indoor temperature");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        Celcius v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Celcius result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertMaxAirTemperature(new Sample<Celcius>(t, result));
    }

    private Celcius retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Celcius> r = db.selectMaxAirTemperature(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Celcius oldValue = IndoorTemperatureMaximumInput.this.getValue();
            Celcius newValue = DialogUtil.promtCelcius(oldValue, "Max temperature");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
