/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.control.commons.gui;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import static org.openide.util.ImageUtilities.loadImageIcon;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class ConfigureIndoorLightAction extends AbstractAction implements Presenter.Popup {

    private final ControlDomain g;

    public ConfigureIndoorLightAction(ControlDomain g) {
        super("Configure");
        this.g = g;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        IndoorLightConfig cfg = context(g).one(IndoorLightConfig.class);

        IndoorLightConfigPanel pnl = new IndoorLightConfigPanel(cfg);
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure PAR light input");
        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            updateConfig(pnl.get());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private void updateConfig(IndoorLightConfig cfg) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.updateIndoorLightConfig(cfg);
    }

}
