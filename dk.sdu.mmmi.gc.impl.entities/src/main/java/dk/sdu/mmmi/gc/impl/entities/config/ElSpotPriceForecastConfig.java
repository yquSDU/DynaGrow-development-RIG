package dk.sdu.mmmi.gc.impl.entities.config;

import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;

/**
 *
 * @author jcs
 */
public class ElSpotPriceForecastConfig {

    private boolean isActive = false;
    private int forecastPeriod = 1000 * 60 * 60 * 24 * 3;
    private EnergyPriceDatabaseArea area = EnergyPriceDatabaseArea.DK_WEST;
    private String currency = "DKK";


    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setElPriceArea(EnergyPriceDatabaseArea area) {
        this.area = area;
    }

    /**
     *
     * @param currency The ISO 4217 code of the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public EnergyPriceDatabaseArea getElPriceArea() {
        return area;
    }

    public String getCurrency() {
        return currency;
    }
}
