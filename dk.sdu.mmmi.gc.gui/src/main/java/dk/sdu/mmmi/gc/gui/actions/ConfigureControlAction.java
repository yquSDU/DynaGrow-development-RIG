package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * Action to configure control parameters.
 *
 * @author jcs
 */
public class ConfigureControlAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;
    private final ControlManager ctrlManager;

    public ConfigureControlAction(ControlDomain g) {
        super("Configure Control Interval");
        this.g = g;
        this.ctrlManager = context(g).one(ControlManager.class);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        // Dialog.
        ControlConfigPanel pnl = new ControlConfigPanel(ctrlManager, g);
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure Control Interval");

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {           
            ctrlManager.setDelayMS(pnl.getControlInterval().toMS());
        }
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    @Override
    public boolean isEnabled() {
        return g != null
                && context(g).one(HALConnector.class) != null
                && !ctrlManager.isSimulating()
                && !ctrlManager.isControlling()
                && !ctrlManager.isAutoRunning();
    }


    /**
     * Configuration panel.
     */
    private static class ControlConfigPanel extends JPanel {

        private final JTextField controlInterval = new JTextField();

        public ControlConfigPanel(ControlManager cm, ControlDomain g) {
            setLayout(new GridLayout(1, 2, 2, 2));
            add(new JLabel("Control interval (minutes):"));
            controlInterval.setText(String.format("%.0f", cm.getDelay().toMinutes()));
            add(controlInterval);
        }

        private Duration getControlInterval() {
            long minutes = Long.parseLong(controlInterval.getText());
            return new Duration(1000 * 60 * minutes);
        }
    }
}
