package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class PrioritizeConcernAction extends AbstractAction implements Presenter.Popup, HelpCtx.Provider {

    private final Concern concern;
    private final ControlDomain g;

    public PrioritizeConcernAction(ControlDomain g, Concern concern) {
        super("Set priority");
        this.concern = concern;
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/control_equalizer_blue.png", false));
        return mi;
    }

//    @Override
    public boolean isEnabled() {
        return concern.isEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
      
        ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);
        if (concern != null) {
            PriorityPanel pnl = new PriorityPanel();
            pnl.set(concern.getPriority());

            // Dialog.
            DialogDescriptor dcs = new DialogDescriptor(
                    pnl,
                    "Prioritize concern");

            Object result = DialogDisplayer.getDefault().notify(dcs);
            if (result == DialogDescriptor.OK_OPTION) {
                int priority = pnl.get();
                concern.setPriority(priority);
                cm.saveConcernConfig(g, concern);
            }

        }
    }

    private static class PriorityPanel extends JPanel {

        private JComboBox box = new JComboBox(new String[]{
            "1 (highest priority)",
            "2", "3", "4", "5", "6", "7", "8", "9",
            "10 (lowest priority)"
        });

        public PriorityPanel() {
            add(new JLabel("Set priority"));
            add(box);
        }

        public void set(int p) {
            box.setSelectedIndex(p - 1);
        }

        public int get() {
            return box.getSelectedIndex() + 1;
        }
    }
}
