package dk.sdu.mmmi.gc.control.light.output;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.BinaryOutput;
import dk.sdu.mmmi.controleum.control.commons.AbstractOutput;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.control.light.input.FrameEndInput;
import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Light plan for the rest of today. Note that then length of the plan depends
 * on the time of the day.
 *
 * @author mrj & jcs
 */
public class LightPlanTodayOutput extends AbstractOutput<LightPlan> implements BinaryOutput<LightPlan> {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public LightPlanTodayOutput(ControlDomain g) {
        super("Light plan", g);
    }

    @Override
    public LightPlan getRandomValue(Date now) {
        FrameEndInput end = context(this.domain).one(FrameEndInput.class);

        LightPlan r = new LightPlan(now, end.getValue().toDate(), getConfig().getTimeSlotDuration());

        for (int i = 0; i < r.size(); i++) {
            r.setElement(i, R.nextBoolean() ? Switch.ON
                    : Switch.OFF);
        }

        return r;
    }

    @Override
    public LightPlan getMutatedValue(LightPlan v) {
        LightPlan r = new LightPlan(v);

        int rIdx = R.nextInt(r.size());
        Switch oldS = r.getElement(rIdx);
        Switch newS = oldS.isOn() ? Switch.OFF : Switch.ON;
        r.setElement(rIdx, newS);

        return r;
    }

    @Override
    public LightPlan copy(Date now, LightPlan oldLightPlan) {
        Date endTime = context(this.domain).one(FrameEndInput.class).getValue().toDate();
        LightPlan newLightPlan = new LightPlan(now, endTime, getConfig().getTimeSlotDuration());

        for (int i = 0; i < newLightPlan.size(); i++) {

            Date tNew = newLightPlan.getElementStart(i);

            if (oldLightPlan.hasDate(tNew)) {
                newLightPlan.setElement(i, oldLightPlan.getElement(tNew));
            } else {
                newLightPlan.setElement(i,
                        R.nextBoolean() ? Switch.ON : Switch.OFF);
            }
        }
        return newLightPlan;
    }

    @Override
    public LightPlan crossover(LightPlan v1, LightPlan v2) {

        // Check args.
        if (v1.size() != v2.size()) {
            throw new IllegalStateException(
                    "Expected two lightplans of the same length.");
        }

        // Create copy from v1.
        LightPlan r = new LightPlan(v1);

        // Crossover by copying in stuff from v2.
        for (int idx = R.nextInt(r.size()); idx < r.size(); idx++) {
            r.setElement(idx, v2.getElement(idx));
        }

        return r;
    }

    @Override
    public void doCommitValue(Date t, LightPlan result) {

        if (isEnabled()) {
            HALConnector hal = HAL.get(domain);
            if (hal != null) {
                try {
                    hal.writeLightStatus(result.getStatusNow());
                } catch (HALException ex) {
                    Logger.getLogger(LightPlanTodayOutput.class.getName()).log(
                            Level.SEVERE, null, ex);
                }
            }
        }
        ClimateDataAccess db = getDb();
        db.insertLightStatus(new Sample<>(t, result.getStatusNow()));
        db.insertProposedLightPlan(result);
    }

    private ClimateDataAccess getDb() {
        return context(domain).one(ClimateDataAccess.class);
    }

    public synchronized void setLightPlanDuration(Duration duration) {
        getDb().insertLightPlanConfig(new LightPlanConfig.Builder().duration(duration.toMS()).build());
    }

    public LightPlanConfig getConfig() {
        return context(domain).one(ClimateDataAccess.class).selectLightPlanConfig();
    }

    @Override
    public BitSet asBitSet() {
        List<Sample<Switch>> lp = getValue().getList();
        BitSet binVar = new BitSet(lp.size());

        for (int i = 0; i < lp.size(); i++) {
            Switch sample = lp.get(i).getSample();
            binVar.set(i, sample.isOn());
        }
        return binVar;
    }

    @Override
    public void setBitSet(BitSet bitset) {
        LightPlan value = this.getValue();

        for (int i = 0; i < value.size(); i++) {
            boolean b = bitset.get(i);
            value.setElement(i, b ? Switch.ON : Switch.OFF);
        }
    }

    @Override
    public int bitSize() {
        return getValue().size();
    }
}
