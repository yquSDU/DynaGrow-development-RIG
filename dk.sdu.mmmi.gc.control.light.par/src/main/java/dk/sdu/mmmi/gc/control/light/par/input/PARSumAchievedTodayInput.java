package dk.sdu.mmmi.gc.control.light.par.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfDay;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.calcPARSum;
import dk.sdu.mmmi.gc.control.light.par.gui.ConfigurePARSumAction;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PARSumAchievedTodayInput extends AbstractInput<MolSqrMeter> {

    private final ControlDomain g;
    private static final Logger logger = Logger.getLogger(PARSumAchievedTodayInput.class.getName());
    private final Link<Action> action;

    public PARSumAchievedTodayInput(ControlDomain greenhouse) {
        super("PAR Day Light Integral achieved (today)");
        this.g = greenhouse;
        this.action = context(this).add(Action.class, new ConfigurePARSumAction(g, this));
    }

    @Override
    public void doUpdateValue(Date t) {
        MolSqrMeter sum = retrieve(t);
        if (sum != null) {
            setValue(sum);
            store(t, sum);
        }
    }

    private MolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        Duration interval = context(g).one(ControlManager.class).getDelay();

        List<Sample<UMolSqrtMeterSecond>> lightLevels = db.selectLightIntensity(startOfDay(t), t);

        PARSumAchievedConfig cfg = db.selectPARSumAchievedConfig();

        MolSqrMeter sum;

        if (cfg.isActive()) {
            sum = calcPARSum(lightLevels, interval);
        } else {
            sum = cfg.getPARSum();
        }

        return sum;
    }

    private void store(Date t, MolSqrMeter ms) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPARSumAchievedToday(new Sample<>(t, ms));
    }

}
