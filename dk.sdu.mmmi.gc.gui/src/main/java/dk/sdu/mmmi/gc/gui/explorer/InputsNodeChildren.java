package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.api.moea.Input;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 * @author mrj
 */
public class InputsNodeChildren extends Children.Keys<Input> implements ControleumListener {

    private final ControlDomain gh;
    private final DisposableList disposables = new DisposableList();

    public InputsNodeChildren(ControlDomain gh) {
        this.gh = gh;
        disposables.add(context(gh).add(ControleumListener.class, this));
    }

    private void setKeysOrdered(List<? extends Input> c) {
        Collections.sort(c, new Comparator<Input>() {
            @Override
            public int compare(Input t, Input t1) {
                return t.getName().compareTo(t1.getName());
            }
        });

        setKeys(c);
    }

    private void updateKeys() {
        List<Input> keys = new ArrayList<>(context(gh).all(Input.class));
        // No selected concerns - show all inputs.
        setKeysOrdered(keys);
    }

    @Override
    protected void addNotify() {
        updateKeys();
    }

    @Override
    protected void removeNotify() {
        setKeys(Collections.EMPTY_SET);
    }

    @Override
    public void onControlDomainAdded(ControleumEvent e) {
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
        updateKeys();
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        removeNotify();
        disposables.dispose();
    }

    @Override
    protected Node[] createNodes(Input input) {
        InputNode n = new InputNode(gh, input);
        return new Node[]{n};
    }
}
