package dk.sdu.mmmi.gc.control.light.gui;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class ConfigureElSpotPriceDBAction extends AbstractAction implements Presenter.Popup {

    private final ClimateDataAccess db;    

    public ConfigureElSpotPriceDBAction(ControlDomain g) {
        super("Configure El. Spot Price Database");
        db = context(g).one(ClimateDataAccess.class);        
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ElSpotPriceForecastConfig cfg = db.selectElSpotPriceForecastConfig();

        // Panel.
        ElSpotPriceDBConfigPanel pnl = new ElSpotPriceDBConfigPanel(cfg);

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure El. Spot Price Database");

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            updateConfig(pnl.get());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private void updateConfig(ElSpotPriceForecastConfig cfg) {
        db.updateElSpotPriceForecastConfig(cfg);
    }
}
