package dk.sdu.mmmi.gc.control.photo.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import java.util.Date;

/**
 * @author mrj
 */
public class OptimalPhotosynthesisInput extends AbstractInput<UMolSqrtMeterSecond> {

    private final ControlDomain g;

    public OptimalPhotosynthesisInput(ControlDomain g) {        
        super("Optimal photosynthesis");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        UMolSqrtMeterSecond light = db.selectLightIntensity(t).getSample();
        Sample<Percent> optPct = db.selectPhotoOptimizationGoal(t);

        PhotoUtil util = context(g).one(PhotoUtil.class);
        UMolSqrtMeterSecond photo = util.calcPhotoOptimalRate(light, optPct.getSample());

        // Store value
        db.insertOptimalPhoto(new Sample<>(t, photo));
        setValue(photo);

    }
}
