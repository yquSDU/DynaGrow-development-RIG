package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.basic.input.DayCO2Input;
import dk.sdu.mmmi.gc.control.basic.input.NightCO2Input;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.NIGHT_LIMIT;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj
 */
public class DayNightCO2Concern extends AbstractConcern {

    public DayNightCO2Concern(ControlDomain g) {
        super("Day/night CO2 control", g, 1);
    }

    @Override
    public double evaluate(Solution s) {
        // Data.
        PPM day = s.getValue(DayCO2Input.class);
        PPM night = s.getValue(NightCO2Input.class);
        PPM t = s.getValue(CO2GoalOutput.class);

        // Fitness.
        if (isNight(s)) {
            return Math.abs(night.subtract(t).roundedValue());
        } else {
            return Math.abs(day.subtract(t).roundedValue());
        }
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        PPM day = s.getValue(DayCO2Input.class);
        PPM night = s.getValue(NightCO2Input.class);

        if (isNight(s)) {
            r.put(CO2GoalOutput.class, String.format("prefer %.1f", night.value()));
        } else {
            r.put(CO2GoalOutput.class, String.format("prefer %.1f", day.value()));
        }

        return r;
    }

    private boolean isNight(Solution s) {
        UMolSqrtMeterSecond indoorLight = s.getValue(IndoorLightInput.class);
        return indoorLight.value() < NIGHT_LIMIT;
    }
}
