package dk.sdu.mmmi.gc.test;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.data.CSVExporter;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.impl.entities.config.ControlConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.results.NegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.Watt;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.HOUR_IN_MS;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.input.ElPriceMWhForecastInput;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PhotoSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import java.io.File;
import static java.lang.System.getProperty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author jcs
 */
public class GreenhouseTestFixture {

    private final String name;
    private Solver n;
    private final ClimateDataAccess db;
    private final ArrayList<Solution> solutionList;
    //
    // Default inputs
    //
    private final Date testTimestamp;
    private final ControlDomain g;
    private final ControlDomainManager gm;
    private Link<SearchConfiguration> searchCfg;
    private Solution selectedSolution;

    public GreenhouseTestFixture(String name, Date time) {
        this(name, time, 5, new SearchConfiguration.Builder().debuggingEnabled(true).build(),
                new LightPlanConfig.Builder().build()); // default is 5 min. control intervals
    }

    public GreenhouseTestFixture(String name, Date time, LightPlanConfig lpc) {
        this(name, time, 5, new SearchConfiguration.Builder().build(), lpc);
    }

    public GreenhouseTestFixture(String name, Date time, int ctrlIntervalMins,
            SearchConfiguration searchCfg) {
        this(name, time, ctrlIntervalMins, searchCfg, new LightPlanConfig.Builder().build());
    }

    public GreenhouseTestFixture(String name, Date time, int ctrlIntervalMins,
            SearchConfiguration searchCfg, LightPlanConfig lpc) {
        System.out.println("\n" + name);

        this.name = name;
        this.gm = Lookup.getDefault().lookup(ControlDomainManager.class);

        // DB context
        this.testTimestamp = time;
        this.g = gm.createControlDomain(name);
        this.db = context(g).one(ClimateDataAccess.class);

        // Config
        setControlInterval(ctrlIntervalMins);
        db.insertLightPlanConfig(lpc);

        // Negotiation callback listner
        solutionList = new ArrayList<>();

        setDefaultInputs();

        setDefaultConfigs(searchCfg);

        // Disable all concerns by default
        Collection<? extends Concern> allconcern = context(g).all(Concern.class);
        for (Concern c : context(g).all(Concern.class)) {
            c.setEnabled(false);
            
            /*if (c.getName() == "Achieve light-time sum"){
                c.setEnabled(true);
            }
            if (c.getName() == "Min. light swicthes"){
                c.setEnabled(true);
            }
            if (c.getName() == "Prefer cheap light"){
                c.setEnabled(true);
            }*/
        }

        // Always enable the outputs for testing
        for (Output o : context(g).all(Output.class)) {
            //o.setValue(o.getRandomValue(time));
            o.setEnabled(true);
        }

    }

    private void setDefaultConfigs(SearchConfiguration searchCfg1) {
        setControleumSolver();
        setForecastIntegrations();
        setCalculatedIndoorLight(true);
        n.setSearchConfiguration(searchCfg1);
    }

    private void setDefaultInputs() {
        db.insertScreenClosingPct(new Sample<>(testTimestamp, new Percent(97d)));
        db.insertLightTransmissionFactor(new Sample<>(testTimestamp, new Percent(60d)));
        db.insertPhotoOptimization(new Sample<>(testTimestamp, new Percent(80d)));
        db.insertLampIntensity(new Sample(testTimestamp, new UMolSqrtMeterSecond(52.0d)));
        db.insertMinAirTemperature(new Sample<>(testTimestamp, new Celcius(15d)));
        db.insertMaxAirTemperature(new Sample<>(testTimestamp, new Celcius(30d)));
        db.insertMaxHumidity(new Sample<>(testTimestamp, new Percent(90d)));
        db.insertDecreasingVentilationTemperaturePerMinute(new Sample<>(testTimestamp, new Celcius(0.2d)));
        db.insertPARSumDayGoal(new Sample<>(testTimestamp, new MolSqrMeter(1.0)));
    }

    public void dispose() {
        gm.deleteControlDomain(g);
        gm.dispose();
        solutionList.clear();
        selectedSolution = null;
    }

    public GreenhouseTestFixture updateInputs() {
        for (Input i : context(g).all(Input.class)) {
            i.doUpdateValue(testTimestamp);
        }

        return this;
    }

    public void startNegotiation(Date from, Date to) {

        SimulationContext context = context(g).one(SimulationContext.class);

        context.init(from, to);

        System.out.println("--------------------------------------------- "
                + "SIMULATION START ");


        while (context.hasNext()) {

           Date d = context.next();
             System.out.format("\n--------------------------------------------- "
                    + "STEP %d OF %d STARTED %s \n",
                    context.position(), context.size(), d);

            // Control.
            SearchStatistics ns = n.solve(d, null, true);

            // Get next state from simulator.            
/*            this.selectedSolution = n.getSolution();
            this.solutionList.addAll(n.getSolutionList());

            storeNegotiationStatistics(d, ns);
            storeConcernNegotiationResult(d);*/
        }

        System.out.println("\n--------------------------------------------- "
                + "SIMULATION END ");
    }

    public ControlDomain getContext() {
        return g;
    }

    //
    // Light features
    //
    public GreenhouseTestFixture lightIntervalFeature() {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.LightIntervalConcern", HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture fixedLightPlanFeature(FixedDayLightPlan lp) {
        db.insertFixedLightPlan(new Sample<>(testTimestamp, lp));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.FixedLightPlanConcern", HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture achieveLightTimeFeature(int priority, long minLightTimeSum) {
        db.insertLightSumHoursGoal(new Sample<>(testTimestamp, new Duration(minLightTimeSum * HOUR_IN_MS)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.AchieveLightTimeSumConcern", priority);
        return this;
    }

    public GreenhouseTestFixture preferNoLightFeature(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.PreferNoLightingConcern", priority);
        return this;
    }

    public GreenhouseTestFixture preferLightDuringNight(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.PreferLightDuringNightConcern", priority);
        return this;
    }

    public GreenhouseTestFixture preferCheapLightConcern(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.PreferCheapLightConcern", priority);
        return this;
    }

    public GreenhouseTestFixture minSwitchConcern(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.MinSwitchConcern", priority);
        return this;
    }

    //
    // Advanced Light features
    //
    public GreenhouseTestFixture lightPriceFeature(int priority, double greenhouseSize, double instLampEffect) {
        db.insertInstalledLampEffect(new Sample<>(testTimestamp, new WattSqrMeter(instLampEffect)));
        db.insertGreenhouseSize(new Sample<>(testTimestamp, new SqrMeter(greenhouseSize)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.concern.PreferCheapLightConcern", priority);
        return this;
    }

    public GreenhouseTestFixture achievePARSum(double parSum) {
        db.insertPARSumDayGoal(new Sample<>(testTimestamp, new MolSqrMeter(parSum)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.par.concern.AchievePARSumConcern", HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture achievePARSumBalance(int priority, double parSum) {
        db.insertPARSumDayGoal(new Sample<>(testTimestamp, new MolSqrMeter(parSum)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.par.concern.PARSumBalanceConcern", priority);
        return this;
    }

    public GreenhouseTestFixture achievePhotoSum(double sum) {
        db.insertPhotoSumDayGoal(new Sample<>(testTimestamp, new MMolSqrMeter(sum)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.photo.concern.AchievePhotoSumConcern", HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture achievePhotoSumBalance(int priority, double sum) {
        db.insertPhotoSumDayGoal(new Sample<>(testTimestamp, new MMolSqrMeter(sum)));
        setConcernProperties("dk.sdu.mmmi.gc.control.light.photo.concern.PhotoSumBalanceConcern", priority);
        return this;
    }

    public GreenhouseTestFixture preferPhotoGrowthFeature(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.light.photo.concern.PreferPhotoGrowthConcern", priority);
        return this;
    }

    //
    // Photo features
    //
    public GreenhouseTestFixture photoOptFeature(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.photo.concern.PhotoOptimalCO2Concern", priority);
        setConcernProperties("dk.sdu.mmmi.gc.control.photo.concern.PhotoOptimalTemperatureConcern", priority);
        return this;
    }

    public GreenhouseTestFixture photoOptCO2Feature(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.photo.concern.PhotoOptimalCO2Concern", priority);
        return this;
    }

    public GreenhouseTestFixture photoScreenFeature(int priority, double screenPhotoLossRatio, double screenTransFact) {
        db.insertScreenPhotoRatio(new Sample<>(testTimestamp, new Percent(screenPhotoLossRatio)));
        db.insertScreenTransmissionFactor(new Sample<>(testTimestamp, new Percent(screenTransFact)));
        setConcernProperties("dk.sdu.mmmi.gc.control.screen.concern.PhotoOptimalScreenConcern", priority);
        return this;
    }

    //
    // Temperature features
    //
    public GreenhouseTestFixture averageTempFeature(int priority, double avgTempGoal) {
        db.insertDailyAverageTemperatureGoal(new Sample<>(testTimestamp, new Celcius(avgTempGoal)));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.DailyAverageTempConcern", priority);
        return this;
    }

    public GreenhouseTestFixture dayNightTempFeature(int priority, double dayTemp, double nightTemp) {
        db.insertDailyAverageTemperature(new Sample<>(testTimestamp, new Celcius(dayTemp)));
        db.insertPreferredNightTemperature(new Sample<>(testTimestamp, new Celcius(nightTemp)));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.DayNightTemperatureConcern", priority);
        return this;
    }

    public GreenhouseTestFixture tempLimitsFeature(double minTempLimit, double maxTempLimit) {
        setMinIndoorTemp(minTempLimit);
        setMaxIndoorHumidity(maxTempLimit);
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.TemperatureLimitsConcern", Concern.HARD_PRIORITY);
        return this;
    }

    //
    // CO2 features
    //
    public GreenhouseTestFixture dayNightCO2Feature(int priority, double dayCO2, double nightCO2) {

        db.insertPreferredDayCO2(new Sample<>(testTimestamp, new PPM(dayCO2)));
        db.insertPreferredNightCO2(new Sample<>(testTimestamp, new PPM(nightCO2)));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.DayNightCO2Concern", priority);
        return this;
    }

    public GreenhouseTestFixture co2LimitsFeature(double minCO2Limit, double maxCO2Limit) {
        setMinCO2(minCO2Limit);
        setMaxCO2(maxCO2Limit);
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.CO2LimitsConcern", Concern.HARD_PRIORITY);
        return this;
    }

    //
    // Screen features
    //
    public GreenhouseTestFixture isolationScreenFeature(int priority) {
        setConcernProperties("dk.sdu.mmmi.gc.control.screen.concern.IsolationConcern", priority);
        return this;
    }

    public GreenhouseTestFixture shadingScreenFeature(int priority, double maxOutdoorLight) {
        db.insertMaxOutdoorLight(new Sample<>(testTimestamp, new WattSqrMeter(maxOutdoorLight)));
        setConcernProperties("dk.sdu.mmmi.gc.control.screen.concern.ShadingConcern", priority);
        return this;
    }

    public GreenhouseTestFixture dayNightScreenFeature(int priority, double dayScreenPos, double nightScreenPos) {
        db.insertPreferredDayScreenPosition(new Sample<>(testTimestamp, new Percent(dayScreenPos)));
        db.insertPreferredNightScreenPosition(new Sample<>(testTimestamp, new Percent(nightScreenPos)));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.DayNightScreenConcern", priority);

        return this;
    }

    //
    // Window features
    //
    public GreenhouseTestFixture dayNightVentFeature(int priority, double dayVentTemp, double nightVentTemp) {

        db.insertPreferredDayVentilationSP(new Sample<>(testTimestamp, new Celcius(dayVentTemp)));
        db.insertPreferredNightVentilationSP(new Sample<>(testTimestamp, new Celcius(nightVentTemp)));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.DayNightVentilationConcern", priority);

        return this;
    }

    public GreenhouseTestFixture avoidOverheatingFeature(double maxTemp) {
        setMaxIndoorTemp(maxTemp);
        setConcernProperties("dk.sdu.mmmi.gc.control.ventilation.concern.AllwaysVentilateAboveMaxConcern", Concern.HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture rampFeatures() {
        //addConcern(new VentilationRampConcern(this));
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.SimpleVentRampConcern", Concern.HARD_PRIORITY);
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.SimpleCO2RampConcern", Concern.HARD_PRIORITY);
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.SimpleScreenRampConcern", Concern.HARD_PRIORITY);
        setConcernProperties("dk.sdu.mmmi.gc.control.basic.concern.SimpleHeatingRampConcern", Concern.HARD_PRIORITY);

        return this;
    }

    //
    // Security features
    //
    public GreenhouseTestFixture co2WasteAvoidanceFeature() {
        setConcernProperties("dk.sdu.mmmi.gc.control.ventilation.concern.CO2WasteAvoidanceConcern", Concern.HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture heatingWasteAvoidanceFeature() {
        setConcernProperties("dk.sdu.mmmi.gc.control.ventilation.concern.HeatingWasteAvoidanceConcern", Concern.HARD_PRIORITY);
        return this;
    }

    public GreenhouseTestFixture avoidHighHumidityFeature() {
        setConcernProperties("dk.sdu.mmmi.gc.control.screen.concern.AvoidHighHumidityConcern", Concern.HARD_PRIORITY);
        return this;
    }

    //
    // Inputs
    //
    public GreenhouseTestFixture setPARSumDayGoal(double goal) {
        db.insertPARSumDayGoal(new Sample<>(testTimestamp, new MolSqrMeter(goal)));
        return this;
    }

    public GreenhouseTestFixture setInstalledLampEffect(double installedLampEffect) {
        db.insertInstalledLampEffect(new Sample<>(testTimestamp, new WattSqrMeter(installedLampEffect)));
        return this;
    }

    public GreenhouseTestFixture setLampIntensity(double lampIntensity) {
        db.insertLampIntensity(new Sample<>(testTimestamp, new UMolSqrtMeterSecond(lampIntensity)));
        return this;
    }

    public GreenhouseTestFixture setLightTransFactor(double lightTransFactor) {
        db.insertLightTransmissionFactor(new Sample<>(testTimestamp, new Percent(lightTransFactor)));
        return this;
    }

    public GreenhouseTestFixture setMaxIndoorHumidity(double maxIndoorHumidity) {
        db.insertMaxHumidity(new Sample<>(testTimestamp, new Percent(maxIndoorHumidity)));
        return this;
    }

    public GreenhouseTestFixture setMinCO2(double minCO2Limit) {
        db.insertMinCO2(new Sample<>(testTimestamp, new PPM(minCO2Limit)));
        return this;
    }

    public GreenhouseTestFixture setMaxCO2(double maxCO2Limit) {
        db.insertMaxCO2(new Sample<>(testTimestamp, new PPM(maxCO2Limit)));
        return this;
    }

    public GreenhouseTestFixture setMaxIndoorTemp(double maxIndoorTemp) {
        db.insertMaxAirTemperature(new Sample(testTimestamp, new Celcius(maxIndoorTemp)));
        return this;
    }

    public GreenhouseTestFixture setIndoorTempInput(double value) {
        db.insertAirTemperature(new Sample(testTimestamp, new Celcius(value)));
        return this;
    }

    public GreenhouseTestFixture setMinIndoorTemp(double minIndoorTemp) {
        db.insertMinAirTemperature(new Sample(testTimestamp, new Celcius(minIndoorTemp)));
        return this;
    }

    public GreenhouseTestFixture setPhotoOptPct(double photoOptPct) {
        db.insertPhotoOptimization(new Sample<>(testTimestamp, new Percent(photoOptPct)));
        return this;
    }

    public GreenhouseTestFixture setScrnClosingPct(double scrnClosingPct) {
        db.insertScreenClosingPct(new Sample<>(testTimestamp, new Percent(scrnClosingPct)));
        return this;
    }

    //
    // Configurations
    //
    public GreenhouseTestFixture setPARSumAchievedHistory(double parSum) {
        PARSumAchievedConfig cfg = db.selectPARSumAchievedConfig();
        cfg.setActive(false);
        cfg.setPARSum(parSum);
        db.updatePARSumAchievedConfig(cfg);
        return this;
    }

    public GreenhouseTestFixture setPhotoSumAchievedHistory(double parSum) {
        PhotoSumAchievedConfig cfg = db.selectPhotoSumAchievedConfig();
        cfg.setActive(false);
        cfg.setPhotoSum(parSum);
        db.updatePhotoSumAchievedConfig(cfg);
        return this;
    }

    public GreenhouseTestFixture setForecastIntegrations() {
        LightForecastConfig cfgLight = db.selectLightForecastConfig();
        cfgLight.setActive(true);
        db.updateLightForecastConfig(cfgLight);

        ElPriceForecastConfig cfgEl = db.selectElPriceForecastConfig();
        cfgEl.setActive(true);
        db.updateElPriceForecastConfig(cfgEl);

        ElSpotPriceForecastConfig cfgElSpot = db.selectElSpotPriceForecastConfig();
        cfgElSpot.setActive(true);
        db.updateElSpotPriceForecastConfig(cfgElSpot);

        return this;
    }

    public GreenhouseTestFixture setNSGA2Solver() {
        for (Solver solver : context(g).all(Solver.class)) {
            if (solver.getClass().getName().equals("dk.sdu.mmmi.controleum.impl.moea.pareto.MOEASolver")) {
                n = solver;
                return this;
            }
        }
        return this;
    }

    public GreenhouseTestFixture setControleumSolver() {
        for (Solver solver : context(g).all(Solver.class)) {
            if (solver.getClass().getName().equals("dk.sdu.mmmi.controleum.impl.moea.basic.Negotiator")) {
                n = solver;
                return this;
            }
        }
        return this;
    }

    public GreenhouseTestFixture setCalculatedIndoorLight(boolean isCalculated) {
        IndoorLightConfig cfg = context(g).one(IndoorLightConfig.class);
        cfg.setIsCalculated(isCalculated);
        db.updateIndoorLightConfig(cfg);
        return this;
    }

    //
    // Getters
    //
    public GreenhouseTestFixture enableLightplanOutput() {
        for (Output o : context(g).all(Output.class)) {
            if (o.getClass().getName().equals("dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput")) {
                o.setEnabled(true);
            }
        }
        return this;
    }

    public GreenhouseTestFixture setVentilationSp(double value) {
        db.insertVentilationThreshold(new Sample<>(testTimestamp, new Celcius(value)));
        return this;
    }

    public PPM getCO2Output() {
        return db.selectCO2Threshold(testTimestamp).getSample();
    }

    public MMolSqrMeter getPhotoBalanceYesterday(Date t) {
        return db.selectPhotoBalanceYesterday(t).getSample();
    }

    public ElPriceMWhForecast getElPriceMWhForecast(Date t) {
        context(g).one(ElPriceMWhForecastInput.class).doUpdateValue(t);
        return db.selectElPriceForecast(t, DateUtil.endOfDay(t));
    }

    public Watt getTotalLampLoad(Date time) {
        SqrMeter greenhouseSize = db.selectGreenhouseSize(time).getSample();
        WattSqrMeter lampEffect = db.selectInstalledLampEffect(time).getSample();

        return lampEffect.times(greenhouseSize);
    }


    //
    // Public helpers
    //
    public List<Solution> getSolutionList() {
        return this.solutionList;
    }

    public Solution getSelectedSolution() {
        return selectedSolution;
    }

    public void exportTestData(Date from, Date to) {
        try {
            String dir = getProperty("user.home") + "/TestSimulationData/";
            new File(dir).mkdir();
            File dataFile = new File(dir + name + ".csv");

            CSVExporter ex = new CSVExporter(g, from, to);
            ex.export(dataFile, null);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    //
    // Private helpers
    //
    private void setConcernProperties(String concernID, int priority) {

        for (Concern c : context(g).all(Concern.class)) {
            if (c.getID().equals(concernID)) {
                c.setEnabled(true);
                c.setPriority(priority);
                gm.saveConcernConfig(g, c);
            }
        }
    }

    private void storeNegotiationStatistics(Date t, SearchStatistics ns) {
        int gen = ns.getGenerationsEvaluated();
        long ms = ns.getTimeSpendMS();

        NegotiationResult nr = new NegotiationResult(gen, ms);
        db.insertNegotiationResult(new Sample<>(t, nr));
    }

    private void storeConcernNegotiationResult(Date t) {
        for (Concern c : context(g).all(Concern.class)) {
            if (c.isEnabled()) {

                db.insertConcernNegotiationResult(
                        new Sample<>(t,
                                new ConcernNegotiationResult(
                                        c.getID(), c.getEvaluationResult(),
                                        c.getValueHelp())));
            }
        }
    }

    private void setControlInterval(int ctrlIntervalMins) {
        // Set control config.
        long ctrlIntervalMilis = ctrlIntervalMins * 60 * 1000;
        ControlConfig ctrlCfg = new ControlConfig.Builder().delay(ctrlIntervalMilis).simulating(true).build();
        ContextDataAccess cda = context(g).one(ContextDataAccess.class);
        cda.setControlConfiguration(ctrlCfg);
        context(g).add(ControlConfig.class, ctrlCfg);
        context(g).one(ControlManager.class).setDelayMS(ctrlIntervalMilis);
    }

}
