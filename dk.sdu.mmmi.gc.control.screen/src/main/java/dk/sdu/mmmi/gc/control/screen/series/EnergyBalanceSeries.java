package dk.sdu.mmmi.gc.control.screen.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class EnergyBalanceSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public EnergyBalanceSeries(ControlDomain g) {
        this.g = g;
        setName("Energy Balance");
        setUnit("[Energy]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<Energy>> raw = db.selectEnergyBalance(from, to);
            for (Sample<Energy> s : raw) {
                Date t = s.getTimestamp();
                Double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
}
