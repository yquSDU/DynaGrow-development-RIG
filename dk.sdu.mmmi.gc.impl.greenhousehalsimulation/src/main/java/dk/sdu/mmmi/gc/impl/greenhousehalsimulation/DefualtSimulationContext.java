package dk.sdu.mmmi.gc.impl.greenhousehalsimulation;

import static com.decouplink.Utilities.context;
import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 *
 * @author jcs
 */
public class DefualtSimulationContext implements SimulationContext {

    private final DefaultHAL hal;
    private Date from;
    private Date to;
    private Date cursor;
    private final ClimateDataAccess db;
    private long dataIntervalMS;
    private final Random random;
    private final ControlDomain g;
    private long end;

    public DefualtSimulationContext(ControlDomain g) {
        this.g = g;
        this.hal = context(g).one(DefaultHAL.class);
        this.db = context(g).one(ClimateDataAccess.class);
        this.random = new Random();
    }

    @Override
    public void init(Date from, Date to) {
        this.dataIntervalMS = db.getControlConfiguration().getDelay().toMS();

        this.from = from;
        this.to = to;
        this.end = DateUtil.endOfDay(to).getTime();
        this.cursor = from;
        hal.setTime(cursor);

        activateSimForecast();
        //initLightForecast();
        //initElPriceForecast();
    }

    @Override
    public int size() {
        long l = to.getTime() - from.getTime();
        return (int) (l / (dataIntervalMS)) + 1;
    }

    @Override
    public int position() {
        long l = cursor.getTime() - from.getTime();
        return (int) (l / (dataIntervalMS));
    }

    private void activateSimForecast() {
        LightForecastConfig cfgLight = db.selectLightForecastConfig();
        cfgLight.setActive(true);

        ElPriceForecastConfig cfgEl = db.selectElPriceForecastConfig();
        cfgEl.setActive(true);

        ElSpotPriceForecastConfig elSpotPriceIntegration = db.selectElSpotPriceForecastConfig();
        elSpotPriceIntegration.setActive(true);
    }

    private void initLightForecast() {

        // Generate random test data
        List<Sample<WattSqrMeter>> samples = Lists.newArrayList();

        for (long t = from.getTime(); t <= end; t += dataIntervalMS) {
            // Interval [0;500] wm2
            double val = random.nextDouble() * (500 - 0) + 0;
            samples.add(new Sample<>(new Date(t), new WattSqrMeter(val)));
        }

        // Insert in DB
        OutdoorLightForecast forecast = new OutdoorLightForecast(samples);
        db.insertOutdoorLightForecast(new Sample<>(from, forecast));
    }

    private void initElPriceForecast() {

        List<Sample<MoneyMegaWattHour>> samples = Lists.newArrayList();

        for (long t = from.getTime(); t <= end; t += dataIntervalMS) {
            // interval = [10;90] euro/MWh
            double val = random.nextInt(80) + 10;
            samples.add(new Sample<>(new Date(t), new MoneyMegaWattHour(val, "EUR")));
        }

        // Create forecast with everything in it.
        ElPriceMWhForecast forecast = new ElPriceMWhForecast(samples, "dkwest", "EUR");

        // Insert it once to form combined forecast.
        db.insertElPriceForecast(new Sample<>(samples.get(0).getTimestamp(), forecast));
        
        //qqq Uncomment this line when need to add fake hybrid electricity prices 
        db.insertHybridElPriceForecast(new Sample<>(samples.get(0).getTimestamp(), forecast));

    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasNext() {
        return cursor.before(to) || cursor.equals(to);
    }

    @Override
    public Date next() {

        // Update inputs
        hal.readArtficialLightStatus("");
        hal.readCO2();
        hal.readHumidity();
        hal.readLightIntensity();
        hal.readOutdoorLightIntensity();
        hal.readOutdoorTemperature();
        hal.readTemperature();
        hal.readWindowOpening();

        // Update cursor
        this.cursor = new Date(cursor.getTime() + dataIntervalMS);
        hal.setTime(cursor);


        return cursor;
    }
}
