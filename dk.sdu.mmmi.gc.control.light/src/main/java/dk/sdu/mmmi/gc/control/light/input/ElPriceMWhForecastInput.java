package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.gui.ConfigureElPriceDBAction;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import javax.swing.Action;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

/**
 * Ex.: Energi DK prices
 *
 * @author jcs
 */
public class ElPriceMWhForecastInput extends AbstractInput<ElPriceMWhForecast> {

    private final ControlDomain g;
    private static final Logger logger = getLogger(ElPriceMWhForecastInput.class.getName());
    private final Link<Action> action;

    public ElPriceMWhForecastInput(ControlDomain g) {        
        super("El. price prognosis");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureElPriceDBAction(g));
    }

    //
    // Public
    //
    @Override
    public void doUpdateValue(Date from) {
        try {

            TimeStamp endTime = context(g).one(FrameEndInput.class).getValue();

            // Update Prices from el price integration.
            updateForecast(startOfHour(from), endTime.toDate());

        } catch (EnergyPriceDatabaseException ex) {

            logger.log(Level.WARNING, "No access to electricity price forecast.", ex);
            NotificationDisplayer.getDefault().
                    notify("No electricity price forecast!",
                            ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/warning.gif", true),
                            "Click to deactivate the Electricity price forecast input?",
                            context(this).one(Action.class));
        }

    }

    //
    // Private
    //
    private void updateForecast(Date from, Date to) throws EnergyPriceDatabaseException {

        
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        ElPriceForecastConfig elPriceIntegration = db.selectElPriceForecastConfig();

        if (elPriceIntegration.isActive()) {

            EnergyPriceService priceDB = Lookup.getDefault().lookup(EnergyPriceService.class);

            // Get prices from el price service
            EnergyPriceDatabaseArea area = elPriceIntegration.getElPriceArea();
            Map<Date, Double> prices = priceDB.getPrices(area, from, to);
            ElPriceMWhForecast f = new ElPriceMWhForecast(prices, area.toString(), elPriceIntegration.getCurrency());

            // Store prices in internal db
            Sample<ElPriceMWhForecast> s = new Sample<>(from, f);
            db.insertElPriceForecast(s);
        }

        ElPriceMWhForecast s = db.selectElPriceForecast(from, to);
        if (s != null) {
            setValue(s);
        }
    }
}
