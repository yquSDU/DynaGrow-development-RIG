package dk.sdu.mmmi.gc.control.light.gui;

import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.control.light.gui.LightStateComponent.State;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FixedLightPlanPanel extends JPanel {

    private final FixedDayLightPlan lightPlan;

    /**
     * Create the panel.
     */
    public FixedLightPlanPanel(FixedDayLightPlan lightPlan) {

        this.lightPlan = new FixedDayLightPlan(lightPlan);
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        setPreferredSize(new Dimension(1100, 80));

        // Init state of Buttons
        for (int i = 0; i < this.lightPlan.getHours().size(); i++) {

            LightStateComponent b = new LightStateComponent(Integer.toString(i + 1));
            b.addMouseListener(lightPlanChangedListener);

            Switch hour = lightPlan.getHour(i);
            if (hour.isOn()) {
                b.setState(LightStateComponent.State.ON);
            } else if (hour.isNotAvailable()) {
                b.setState(LightStateComponent.State.NEUTRAL);
            } else {
                b.setState(LightStateComponent.State.OFF);
            }
            b.updateUI();
            c.gridx = i;
            c.gridy = 0;
            c.weightx = 0.5;
            c.gridwidth = 1;
            add(b, c);
        }

        // Labels
        JLabel lbl = new JLabel("Click on hour number to change light status: ");
        lbl.setOpaque(true);
        c.gridwidth = 24;
        c.gridx = 0;
        c.gridy = 1;
        add(lbl, c);

        JLabel lblOff = new JLabel("Off");
        lblOff.setBackground(Color.LIGHT_GRAY);
        lblOff.setOpaque(true);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        add(lblOff, c);

        JLabel lblOn = new JLabel("On");
        lblOn.setBackground(Color.YELLOW);
        lblOn.setOpaque(true);
        c.gridx = 1;
        c.gridy = 2;
        add(lblOn, c);

        JLabel lblDontCare = new JLabel("N/A");
        lblDontCare.setBackground(Color.WHITE);
        lblDontCare.setOpaque(true);
        c.gridx = 2;
        c.gridy = 2;
        add(lblDontCare, c);

        JLabel space = new JLabel("");
        c.gridwidth = 20;
        c.gridx = 2;
        c.gridy = 2;
        add(space, c);

    }

    public FixedDayLightPlan getLightPlan() {
        return lightPlan;
    }
    private final MouseListener lightPlanChangedListener = new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent me) {
            LightStateComponent c = (LightStateComponent) me.getSource();
            State state = c.getState();
            int hourIdx = Integer.parseInt(c.getID()) - 1;

            if (state.equals(State.OFF)) {
                lightPlan.setHour(hourIdx, Switch.OFF);
            } else if (state.equals(State.ON)) {
                lightPlan.setHour(hourIdx, Switch.ON);
            } else {
                lightPlan.setHour(hourIdx, Switch.NA);
            }
        }
    };
}
