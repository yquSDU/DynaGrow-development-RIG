package dk.sdu.mmmi.gc.gui.charts;

import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import java.util.EventListener;

/**
 *
 * @author jcs
 */
public interface ChartListener extends EventListener {

    public void onChartChanged(ChartConfig cfg);

    public void onChartDelete(ChartConfig cfg);
}
