package dk.sdu.mmmi.gc.test;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static junit.framework.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author jcs
 */
@Ignore
public class SimilationTest {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private ControlDomain g;

    @Before
    public void setUp() throws SQLException {
    }

    @After
    public void tearDown() throws SQLException {
        g = null;
    }

    @Test
    public void testVentilation() throws Exception {
        // SETUP:
        g = new GreenhouseTestFixture("SimTest", df.parse("2020-02-01 23:00")).getContext();

        // TEST:
        Date from = df.parse("2020-02-01 00:00");
        Date to = df.parse("2020-02-01 23:59");

        HALConnector hal = context(g).one(HALConnector.class);

        SimulationContext sim = context(g).one(SimulationContext.class);
        sim.init(from, to);

        // Set default output
        hal.writeHeatingThreshold(new Celcius(20d));
        hal.writeVentilationThreshold(new Celcius(28d));
        hal.writeCO2Threshold(new PPM(2000d));

        // Run 100 steps
        for (int i = 1; i < 100; i++) {
            sim.next();
        }

        PPM t1CO2Level = hal.readCO2();
        Celcius t1AirTemp = hal.readTemperature();

        // Trigger Vent.
        hal.writeHeatingThreshold(new Celcius(15d));
        hal.writeVentilationThreshold(new Celcius(20d));
        //hal.writeCO2Threshold(new PPM(700));

        // Run next 5 step
        for (int i = 101; i < 110; i++) {
            sim.next();
        }
        PPM t2CO2Level = hal.readCO2();
        Celcius t2AirTemp = hal.readTemperature();

        // ASSERT:
        assertTrue("Assert that air temp. has decreased after ventilation.",
                t2AirTemp.value() < t1AirTemp.value());

        assertTrue("Assert that CO2 level has decreased after ventilation.",
                t2CO2Level.value() < t1CO2Level.value());

    }
}
