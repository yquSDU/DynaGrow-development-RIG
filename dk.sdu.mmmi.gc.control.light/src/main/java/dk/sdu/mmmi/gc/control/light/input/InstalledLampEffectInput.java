package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * The installed lamp effect in the greenhouse.
 *
 * E.g. if the installed lamp effect is 60 W/m2 in a greenhouse of size 100 m2
 * then the connected load is 60 W/m2 * 100 m2 = 6000 W
 *
 * @author jcs
 */
public class InstalledLampEffectInput extends AbstractInput<WattSqrMeter> {

    private final ControlDomain g;
    private final Link<Action> action;

    public InstalledLampEffectInput(ControlDomain g) {
        super("Installed lamp effect");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        WattSqrMeter v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, WattSqrMeter result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertInstalledLampEffect(new Sample<>(t, result));

    }

    private WattSqrMeter retrieve(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<WattSqrMeter> r = db.selectInstalledLampEffect(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            WattSqrMeter oldValue = InstalledLampEffectInput.this.getValue();
            WattSqrMeter newValue = DialogUtil.promtWattSqrMeter(oldValue, "Installed Lamp Effect");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
