package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.gc.control.basic.input.DailyAverageTempGoalInput;
import dk.sdu.mmmi.gc.control.basic.input.DailyAverageTempInput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import java.util.HashMap;
import java.util.Map;

/**
 * Ensures a daily average air temperature.
 *
 * @author jcs
 */
public class DailyAverageTempConcern extends AbstractConcern {

    public DailyAverageTempConcern(ControlDomain g) {
        super("Daily average temperature", g, HARD_PRIORITY);
    }

    /**
     * Ensure that the average temperature goal is either satisfied or getting
     * closer to being satisfied.
     */
    @Override
    public double evaluate(Solution s) {
        boolean aheadAvg = isAhead(s);
        boolean catchingUp = isCatchingUp(s);
        return (aheadAvg || catchingUp) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        Celcius avgTempGoal = s.getValue(DailyAverageTempGoalInput.class);

        if (!isAhead(s)) {
            r.put(HeatingSetpointOutput.class, String.format("above %.1f", avgTempGoal.value()));
        }

        return r;
    }

    private boolean isAhead(Solution s) {
        Celcius avgTempActual = s.getValue(DailyAverageTempInput.class);
        Celcius avgTempGoal = s.getValue(DailyAverageTempGoalInput.class);
        return avgTempActual.value() >= avgTempGoal.value();
    }

    private boolean isCatchingUp(Solution s) {
        double avgTempGoal = s.getValue(DailyAverageTempGoalInput.class).value();
        double heatingSp = s.getValue(HeatingSetpointOutput.class).value();
        return heatingSp > avgTempGoal;
    }
}
