package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.gui.charts.ChartManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

/**
 *
 * @author jcs
 */
public class RenameChartAction extends AbstractAction {

    private final ChartConfig chart;
    private final ControlDomain g;

    public RenameChartAction(ControlDomain g, ChartConfig chart) {
        super("Rename chart");
        this.g = g;
        this.chart = chart;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        // Ask for name
        NotifyDescriptor.InputLine d = new NotifyDescriptor.InputLine("Chart name", "Rename Chart");
        if (DialogDisplayer.getDefault().notify(d) == NotifyDescriptor.OK_OPTION) {
            chart.setName(d.getInputText());

            ChartManager cm = context(g).one(ChartManager.class);
            cm.update(chart);
        }
    }
}
