
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.control.commons.input.LightTransmissionFactorInput;
import dk.sdu.mmmi.gc.control.commons.input.PhotoOptimalPercentageInput;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.light.input.LampIntensityInput;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import dk.sdu.mmmi.gc.control.light.photo.concern.AchievePhotoSumConcern;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumDayGoalInput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author jcs
 */
public class AchievePhotoSumConcernTest {

    private final SimpleDateFormat df = new SimpleDateFormat("HH:mm");

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        context(g).add(PhotoUtil.class, new PhotoUtil());

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of fitness method, of class AchievePhotoSumConcern.
     */
    @Ignore
    @Test
    public void testFitness() throws ParseException {
        System.out.println("fitnessAchievePhotoSum");

        // SETUP:
        Solution s = mock(Solution.class);

        // Inputs
        when(s.getValue(PhotoSumDayGoalInput.class)).thenReturn(new MMolSqrMeter(400.0));
        when(s.getValue(PhotoOptimalPercentageInput.class)).thenReturn(new Percent(80.0));
        when(s.getValue(LampIntensityInput.class)).thenReturn(new UMolSqrtMeterSecond(100.0));
        when(s.getValue(LightTransmissionFactorInput.class)).thenReturn(new Percent(60d));
        when(s.getValue(PhotoSumAchievedPeriodInput.class)).thenReturn(new MMolSqrMeter(0d));

        // Weather forecast
        List<Sample<WattSqrMeter>> forecast = new ArrayList<>();
        forecast.add(new Sample<>(df.parse("07:00"), new WattSqrMeter(100d)));
        forecast.add(new Sample<>(df.parse("08:00"), new WattSqrMeter(100d)));
        forecast.add(new Sample<>(df.parse("09:00"), new WattSqrMeter(100d)));
        when(s.getValue(OutdoorLightForecastInput.class)).thenReturn(new OutdoorLightForecast(forecast));

        // Outputs
        //when(s.getSample(LightPlanTodayOutput.class)).thenReturn(new LightPlan(df.parse("07:00")));
        when(s.getTime()).thenReturn(df.parse("07:00"));

        // TEST:
        AchievePhotoSumConcern concern = new AchievePhotoSumConcern(g);
        double result = concern.evaluate(s);

        // ASSERTS:
        assertEquals(0.071, result, 0.001);
    }

    @Test
    public void testExspectedCombinedPhotoSumInput() {
        // SETUP:
        // TEST:
        // ASSERT:
    }
    private final ControlDomain g = new ControlDomain() {
        @Override
        public int getID() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setName(String name) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    };
}
