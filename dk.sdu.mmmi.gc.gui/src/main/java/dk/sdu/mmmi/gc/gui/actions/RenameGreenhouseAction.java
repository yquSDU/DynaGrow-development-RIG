package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;

/**
 *
 * @author jcs
 */
public class RenameGreenhouseAction extends AbstractAction {

    private final ControlDomain g;

    public RenameGreenhouseAction(ControlDomain g) {
        super("Rename greenhouse");
        this.g = g;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        NotifyDescriptor.InputLine d = new NotifyDescriptor.InputLine("Greenhouse name", "Rename greenhouse");
        if (DialogDisplayer.getDefault().notify(d) == NotifyDescriptor.OK_OPTION) {

            getGreenhouseService().renameControlDomain(g, d.getInputText());
        }
    }

    private ControlDomainManager getGreenhouseService() {
        return Lookup.getDefault().lookup(ControlDomainManager.class);
    }
}
