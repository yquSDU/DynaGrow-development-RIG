package dk.sdu.mmmi.gc.gui.explorer;

import dk.sdu.mmmi.gc.gui.actions.CreateGreenhouseAction;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;

/**
 * @author mrj
 */
public class GreenhousesNode extends AbstractNode {

    public GreenhousesNode(GreenhousesNodeChildren ghnc) {
        super(ghnc);
        setDisplayName("Greenhouses");
        setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/chart_organisation.png");
    }

    @Override
    public Action[] getActions(boolean context) {
        return new Action[]{new CreateGreenhouseAction()};
    }
}
