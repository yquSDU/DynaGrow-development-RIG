package dk.sdu.mmmi.gc.impl.energidkconnector;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author jemr, jcs
 */
@ServiceProvider(service = EnergyPriceService.class)
public class EnergiDkPriceDBConnector implements EnergyPriceService, Disposable {

    private final AnnotationConfigApplicationContext moduleContext;
    private final JdbcTemplate jdbcTemplate;

    public EnergiDkPriceDBConnector() {
        moduleContext = new AnnotationConfigApplicationContext(EnergiDkElPriceDBConfig.class);
        jdbcTemplate = moduleContext.getBean(JdbcTemplate.class);
    }

    @Override
    public Map<Date, Double> getPrices(EnergyPriceDatabaseArea area, final Date start, final Date end) throws EnergyPriceDatabaseException {

        // SYS is not support by EnergiDanmark
        String location = area == EnergyPriceDatabaseArea.SYS ? "DK1"
                : area == EnergyPriceDatabaseArea.DK_EAST ? "DK2" : "DK1";

        // Select the data from the most recent forecast
        TreeMap<Date, Double> result = jdbcTemplate.query(
                "SELECT * FROM energidanmark_el_forecast WHERE timest >= ? and timest <= ? and location = ? "
                + "and forecast_timest=(SELECT MAX(forecast_timest) FROM energidanmark_el_forecast)",
                new EnergiDkElPriceExtrator(),
                start.getTime(),
                end.getTime(),
                location);

        return result;
    }

    @Override
    public void dispose() {
        moduleContext.close();
    }
}
