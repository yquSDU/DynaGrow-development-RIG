package dk.sdu.mmmi.gc.impl.okosimulation;

import com.mathworks.toolbox.javabuilder.MWException;
import sdusimulator.API;

/**
 *
 * @author mrj
 */
class APISingleton {
    private static APISingleton instance = new APISingleton();
    
    private API api = null;
    
    private APISingleton() {
        }
    
    public static APISingleton getInstance() {    
        return instance;
    }
    
    public API getAPI() throws MWException {
        ensureInitialization();
        return api;
    }
        
    private void ensureInitialization() throws MWException {
        // Initialize if required.
        if(api == null) {
            System.out.println("MATLAB initialization.");
            api = new API();
        }                
    }
    
    public void dispose() {
        // Dispose if required.
        if(api != null) {
            System.out.println("MATLAB disposal.");
            api.dispose();
            api = null;
        }
    }           
}
