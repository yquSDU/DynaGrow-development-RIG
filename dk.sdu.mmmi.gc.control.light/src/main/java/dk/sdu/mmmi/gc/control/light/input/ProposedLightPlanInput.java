package dk.sdu.mmmi.gc.control.light.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.Date;

/**
 *
 * @author jcs
 */
public class ProposedLightPlanInput extends AbstractInput<LightPlan> {

    private final ControlDomain g;

    public ProposedLightPlanInput(ControlDomain g) {
        super("Proposed light plan");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        LightPlan value = retrieve(t);
        setValue(value);
    }

    private LightPlan retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        LightPlan r = db.selectProposedLightPlan(t, getLightPlanConfig().getTimeSlotDuration());

        return r;
    }

    private LightPlanConfig getLightPlanConfig() {
        return context(g).one(LightPlanTodayOutput.class).getConfig();
    }
}
