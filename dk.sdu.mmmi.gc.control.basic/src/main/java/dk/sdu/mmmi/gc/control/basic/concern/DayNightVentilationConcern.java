package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.basic.input.DayVentilationTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.input.NightVentilationTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.NIGHT_LIMIT;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj
 */
public class DayNightVentilationConcern extends AbstractConcern {

    public DayNightVentilationConcern(ControlDomain g) {
        super("Day/night ventilation control", g, 1);
    }

    @Override
    public double evaluate(Solution s) {
        // Data.
        Celcius t = s.getValue(VentilationTempOutput.class);

        // Fitness.
        if (isNight(s)) {
            Celcius night = s.getValue(NightVentilationTemperatureInput.class);
            return abs(night.subtract(t).roundedValue());
        } else {
            Celcius day = s.getValue(DayVentilationTemperatureInput.class);
            return abs(day.subtract(t).roundedValue());
        }
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (isNight(s)) {
            Celcius night = s.getValue(NightVentilationTemperatureInput.class);
            r.put(VentilationTempOutput.class, String.format("prefer %.1f", night.value()));
        } else {
            Celcius day = s.getValue(DayVentilationTemperatureInput.class);
            r.put(VentilationTempOutput.class, String.format("prefer %.1f", day.value()));
        }

        return r;
    }

    private boolean isNight(Solution s) {
        UMolSqrtMeterSecond indoorLight = s.getValue(IndoorLightInput.class);
        return indoorLight.value() < NIGHT_LIMIT;
    }

}
