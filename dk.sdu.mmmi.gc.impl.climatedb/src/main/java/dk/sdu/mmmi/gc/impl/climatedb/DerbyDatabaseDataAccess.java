package dk.sdu.mmmi.gc.impl.climatedb;

import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.db.DatabaseDataAccess;
import dk.sdu.mmmi.gc.impl.climatedb.schema.Schema;
import dk.sdu.mmmi.gc.impl.climatedb.schema.SchemaBuilder;
import dk.sdu.mmmi.gc.impl.climatedb.util.JDBC;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mrj
 */
public class DerbyDatabaseDataAccess implements DatabaseDataAccess {

    private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";

    static {
        JDBC.loadDriver(DRIVER);
    }
    private Connection con = null;

    /**
     * Initialize connection.
     */
    public void init() throws SQLException {

        assertConnectionState(false);

        // Connection properties.
        Properties props = new Properties();
        props.put("user", "user1");
        props.put("password", "user1");

        // Connect.
        String userDir = System.getProperty("user.home");
        String dbURL = String.format("jdbc:derby:%1$s/GreenhouseDB;create=true", userDir);

        con = DriverManager.getConnection(dbURL, null);

        // Update schema.
        SchemaBuilder sb = Schema.createSchemaBuilder(con);
        sb.update();
    }

    /**
     * Dispose connection.
     */
    public void dispose() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException x) {
            System.err.println(x.getMessage());
        } finally {
            con = null;
        }
    }

    /**
     * Test connection.
     */
    public boolean isConnected() {
        return con != null;
    }

    private void assertConnectionState(boolean connected) {
        if (connected != isConnected()) {
            throw new IllegalStateException();
        }
    }

    //
    // GreenhouseDatabase implementation.
    //
    @Override
    public ContextDataAccess createContext(String name) {
        assertConnectionState(true);

        DerbyContextDataAccess dba = null;

        try (PreparedStatement s = con.prepareStatement(
                "INSERT INTO context (name) VALUES (?)",
                Statement.RETURN_GENERATED_KEYS)) {

            s.setString(1, name);
            s.executeUpdate();
            con.commit();
            ResultSet rs = s.getGeneratedKeys();
            if (!rs.next()) {
                throw new SQLException("Unexpected error creating context.");
            }

            int id = rs.getInt(1);
            dba = new DerbyContextDataAccess(con, id, name);

        } catch (SQLException ex) {
            Logger.getLogger(DerbyDatabaseDataAccess.class.getName()).log(Level.SEVERE, "Could not select Contexts from DB.", ex);
        }

        return dba;
    }

    @Override
    public void updateContextName(int id, String name) {

        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE context "
                + "SET name = ? "
                + "WHERE id = ?")) {

            ps.setString(1, name);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, String.format("Could not update name of Context %s in DB.", id), ex);
        }
    }

    @Override
    public List<ContextDataAccess> selectContexts() {
        List<ContextDataAccess> r = new ArrayList<>();

        try (Statement s = con.createStatement()) {
            s.execute(
                    "SELECT id, name "
                    + "FROM context "
                    + "WHERE deleted = 0");

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                r.add(new DerbyContextDataAccess(con, id, name));
            }

        } catch (SQLException ex) {
            Logger.getLogger(DerbyDatabaseDataAccess.class.getName()).log(Level.SEVERE, "Could not select Contexts from DB.", ex);
        }
        return r;
    }

    @Override
    public void deleteContext(int ID) {

        try (PreparedStatement ps = con.prepareStatement(
                "UPDATE context "
                + "SET deleted = 1 "
                + "WHERE id = ?")) {
            ps.setInt(1, ID);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not delete Context from DB.", ex);
        }
    }
}
