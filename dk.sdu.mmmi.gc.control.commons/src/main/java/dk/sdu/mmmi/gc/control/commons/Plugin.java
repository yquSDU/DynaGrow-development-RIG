package dk.sdu.mmmi.gc.control.commons;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.input.CO2Input;
import dk.sdu.mmmi.gc.control.commons.input.CO2MaximumInput;
import dk.sdu.mmmi.gc.control.commons.input.CO2MinimumInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorHumidityInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMaximumInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMinimumInput;
import dk.sdu.mmmi.gc.control.commons.input.LightTransmissionFactorInput;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorLightInput;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.input.PhotoOptimalPercentageInput;
import dk.sdu.mmmi.gc.control.commons.input.WindowOpeningInput;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import dk.sdu.mmmi.gc.control.commons.series.CO2GoalSeries;
import dk.sdu.mmmi.gc.control.commons.series.CO2Series;
import dk.sdu.mmmi.gc.control.commons.series.GeneralScreenPositionSeries;
import dk.sdu.mmmi.gc.control.commons.series.HeatingSetpointSeries;
import dk.sdu.mmmi.gc.control.commons.series.IndoorHumiditySeries;
import dk.sdu.mmmi.gc.control.commons.series.IndoorLightSeries;
import dk.sdu.mmmi.gc.control.commons.series.IndoorTemperatureMaximumSeries;
import dk.sdu.mmmi.gc.control.commons.series.IndoorTemperatureMinimumSeries;
import dk.sdu.mmmi.gc.control.commons.series.IndoorTemperatureSeries;
import dk.sdu.mmmi.gc.control.commons.series.LightTransmissionFactorSeries;
import dk.sdu.mmmi.gc.control.commons.series.OutdoorLightSeries;
import dk.sdu.mmmi.gc.control.commons.series.OutdoorTemperatureSeries;
import dk.sdu.mmmi.gc.control.commons.series.PhotoOptimalPercentageSeries;
import dk.sdu.mmmi.gc.control.commons.series.VentilationTempSeries;
import dk.sdu.mmmi.gc.control.commons.series.WindowsOpeningSeries;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Utils with cache
        d.add(context(g).add(PhotoUtil.class, new PhotoUtil()));

        // Common inputs
        //createIndoorTemperatureMinimumInput(g, d);
        //createIndoorTemperatureMaximumInput(g, d);

        //createCO2MinimumInput(g, d);
        //createCO2MaximumInput(g, d);

        createLightTransmissionFactorInput(d, g);
        //createPhotoOptimalPercentageInput(d, g);
        createIndoorLightInput(d, g);
        createOutdoorLightInput(d, g);
        createCO2Input(g, d);
        createOutdoorTemperatureInput(d, g);
        createIndoorTemperatureInput(d, g);
        createIndoorHumidityInput(d, g);
        //createWindowOpeningInput(g, d);

        // Outputs
        //createCO2GoalOutput(g, d);
        //createHeatingSetpointOutput(g, d);
        //createGeneralScreenPositionOutput(g, d);
        //createVentilationTempOutput(g, d);

        // Configs
        loadIndoorLightConfig(g, d);

        return d;
    }

    private void createVentilationTempOutput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Output.class, new VentilationTempOutput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new VentilationTempSeries(g)));
    }

    private void createGeneralScreenPositionOutput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Output.class, new GeneralScreenPositionOutput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new GeneralScreenPositionSeries(g)));
    }

    private void createHeatingSetpointOutput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Output.class, new HeatingSetpointOutput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new HeatingSetpointSeries(g)));
    }

    private void createCO2GoalOutput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Output.class, new CO2GoalOutput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new CO2GoalSeries(g)));
    }

    private void createIndoorTemperatureMaximumInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new IndoorTemperatureMaximumInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorTemperatureMaximumSeries(g)));
    }

    private void createIndoorTemperatureMinimumInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new IndoorTemperatureMinimumInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorTemperatureMinimumSeries(g)));
    }

    private void createCO2MinimumInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new CO2MinimumInput(g)));
    }

    private void createCO2MaximumInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new CO2MaximumInput(g)));
    }

    private void createPhotoOptimalPercentageInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PhotoOptimalPercentageInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PhotoOptimalPercentageSeries(g)));
    }

    private void createLightTransmissionFactorInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new LightTransmissionFactorInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new LightTransmissionFactorSeries(g)));
    }

    private void createIndoorLightInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new IndoorLightInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorLightSeries(g)));
    }

    private void createOutdoorLightInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new OutdoorLightInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new OutdoorLightSeries(g)));
    }

    private void createCO2Input(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new CO2Input(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new CO2Series(g)));
    }

    private void createIndoorHumidityInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new IndoorHumidityInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorHumiditySeries(g)));
    }

    private void createIndoorTemperatureInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new IndoorTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorTemperatureSeries(g)));
    }

    private void createOutdoorTemperatureInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new OutdoorTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new OutdoorTemperatureSeries(g)));
    }

    private void createWindowOpeningInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new WindowOpeningInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new WindowsOpeningSeries(g)));
    }

    private void loadIndoorLightConfig(ControlDomain g, DisposableList d) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        d.add(context(g).add(IndoorLightConfig.class, db.selectIndoorLightConfig()));
    }

}
