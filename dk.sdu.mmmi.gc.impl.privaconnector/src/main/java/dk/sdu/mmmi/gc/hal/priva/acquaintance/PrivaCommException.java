/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva.acquaintance;

/**
 *
 * @author ancla
 */
public class PrivaCommException extends Exception {
    public PrivaCommException(String message)
    {
        super(message);
    }
    
    public PrivaCommException(String message, Throwable e)
    {
        super(message, e);
    }
}
