package dk.sdu.mmmi.gc.superlinkhal;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.impl.entities.config.SuperLinkConfig;
import dk.sdu.mmmi.gc.impl.sl4connector.SL4DCOMConnector;
import dk.sdu.mmmi.gc.impl.sl4connector.SL4DCOMException;
import dk.sdu.mmmi.gc.impl.sl4connector.SL4Login;
import dk.sdu.mmmi.gc.impl.sl4connector.SL4ReconnectingDCOMConnector;
import java.util.Date;
import java.util.List;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class SuperLinkHAL implements HALConnector {

    private static final long RECONNECT_DELAY_MS = 1000 * 60;
    private final ControlDomain g;
    private SL4DCOMConnector c = null;
    private Date lastConnect = new Date(0);
    private SuperLinkConfig cfg = null;
    private boolean co2OutputInitialized;
    private boolean lightInitialized;
    private boolean screensInitialized;
    private boolean tempInitialized;
    private boolean windowsInitialized;

    public SuperLinkHAL(ControlDomain g) {
        this.g = g;
    }

    private SL4DCOMConnector createConnector() throws HALException {

        // Respect reconnect delay.
        long diff = new Date().getTime() - lastConnect.getTime();
        if (diff < RECONNECT_DELAY_MS) {
            throw new HALException("Reconnect delay not elapsed. Try later.");
        }

        // Connect.
        try {
            lastConnect = new Date();
            SL4Login login = new SL4Login(
                    getConfiguration().getHostname(),
                    getConfiguration().getDomain(),
                    getConfiguration().getUsername(),
                    getConfiguration().getPassword());
            return new SL4ReconnectingDCOMConnector(login);
        } catch (SL4DCOMException ex) {
            throw new HALException("Connection failed.", ex);
        }
    }

    private synchronized SL4DCOMConnector getConnector() throws HALException {
        if (c == null) {
            // Create connector.
            c = createConnector();
        }

        return c;
    }

    private synchronized void write(String var, Double val) throws HALException {
        try {
            getConnector().writeValue(getConfiguration().getDepartment(), var, val);
        } catch (SL4DCOMException ex) {
            throw new HALException("Write failed.", ex);
        }
    }

    private synchronized Double read(String var) throws HALException {
        try {
            return getConnector().readValue(getConfiguration().getDepartment(), var);
        } catch (SL4DCOMException ex) {
            throw new HALException("Read failed.", ex);
        }
    }

    /*
     * Public.
     */
    public SuperLinkConfig getConfiguration() {
        // Get from db?
        if (cfg == null) {
            ClimateDataAccess d = context(g).one(ClimateDataAccess.class);
            cfg = d.getSuperLinkConfiguration();
        }

        return cfg;
    }

    /*
     * HAL implementation.
     */
    @Override
    public Status getStatus() {
        try {
            readTemperature();
            return Status.OPTIMAL_SERVICE;
        } catch (HALException ex) {
            return Status.NO_SERVICE;
        }
    }

    @Override
    public  Celcius readTemperature() throws HALException {
        return new Celcius(read("41001"));
    }

    @Override
    public  Celcius readOutdoorTemperature() throws HALException {
        return new Celcius(read("55"));
    }

    @Override
    public  PPM readCO2() throws HALException {
        return new PPM(read("41014"));
    }

    @Override
    public UMolSqrtMeterSecond readLightIntensity() throws HALException {
        //TODO: Support w and w/o sensor
        //Aarslev uden PAR sensor: "51017"
        //Taastrup med PAR sensor "41031"
        return new UMolSqrtMeterSecond(read(cfg.getIndoorLightCode()));

    }

    @Override
    public  Percent readHumidity() throws HALException {
        return new Percent(read("41015"));
    }

    @Override
    public  WattSqrMeter readOutdoorLightIntensity() throws HALException {
        return new WattSqrMeter(read("56"));
    }

    @Override
    public  Switch readArtficialLightStatus(String lightGroup) throws HALException {
        // Priva: "I206.0N1R18.1V1";
        return read(lightGroup) > 0 ? Switch.ON : Switch.OFF;
    }

    @Override
    public  Percent readWindowOpening() throws HALException {
        // TODO: Support multiple windows
        // 41006 (Top1), 41007 (Top2), 41008 (Top3), 41009 (Top4)
        return new Percent(read("41006"));
    }

    @Override
    public void writeHeatingThreshold(Celcius c) throws HALException {
        if (!tempInitialized) {
            initTemp();
        }
        write("41102", c.value());
    }

    @Override
    public void writeVentilationThreshold(Celcius c) throws HALException {
        if (!windowsInitialized) {
            initWindows();
        }
        //write("41302", c.value()); // threshold
        write("41303", c.value()); // absolute
//        write("51320", c.value()); // Zone 1
//        write("51350", c.value()); // Zone 2
    }

    @Override
    public  void writeCO2Threshold(PPM p) throws HALException {
        if (!co2OutputInitialized) {
            initCO2();
        }
        write("41610", p.value());
    }

    @Override
    public  void writeLightStatus(Switch s) throws HALException {
        if (!lightInitialized) {
            initLight();
        }
        for (String lg : getConfiguration().getLightGroups()) {
            write(lg, s.isOn() ? 3.0 : 0.0);
        }
    }

    @Override
    public  void writeGeneralScreenPosition(Percent p) throws HALException {
        if (!screensInitialized) {
            initScreens();
        }
        write("41535", p.asPercent());
    }

    /*
     * HAL initialization.
     */
    private void initCO2() throws HALException {
        try {
            // CO2
            write("41600", 1.0);

            write("41601", 0.0);
            write("41602", 0.0);
            write("41603", 0.0);
            write("41604", 0.0);
            write("41605", 0.0);
            write("41606", 0.0);

            write("41611", 0.0);
            write("41612", 0.0);
            write("41613", 0.0);
            write("41614", 0.0);
            write("41615", 0.0);
            write("41616", 0.0);

            write("41621", 0.0);
            write("41622", 2000.0);

            co2OutputInitialized = true;

        } catch (HALException ex) {
            throw new HALException("CO2 output Initialization failed.", ex);
        }
    }

    private void initLight() throws HALException {
        try {
            // Light
            // XXX How do we send these? To we need support for sending strings?
            //s.writeValue(dep, "41651", 0.0); // 00:01:00
            //s.writeValue(dep, "41652", 0.0); // 23:58:00
            write("51651", 50.0);

            lightInitialized = true;

        } catch (HALException ex) {
            throw new HALException("Light plan output Initialization failed.", ex);
        }
    }

    private void initScreens() throws HALException {
        try {
            // Screens
            write("41500", 4.0);
            write("41520", 1.0);
            write("41531", 100.0);
            write("41532", 100.0);
            write("41533", 100.0);

            screensInitialized = true;
        } catch (HALException ex) {
            throw new HALException("Screens output Initialization failed.", ex);
        }
    }

    private void initTemp() throws HALException {
        try {
            // Temperature
            write("37101", 0.0);
            write("41170", 0.0);
            write("41107", 0.0);

            write("41161", 0.0);
            write("41162", 0.0);
            write("41163", 0.0);
            write("41164", 0.0);
            write("41165", 0.0);
            write("41166", 0.0);

            write("41121", 0.0);
            write("41122", 0.0);
            write("41123", 0.0);
            write("41124", 0.0);
            write("41125", 0.0);
            write("41126", 0.0);

            write("41131", 0.0);
            write("41132", 0.0);
            write("41133", 0.0);
            write("41134", 0.0);
            write("41135", 0.0);
            write("41136", 0.0);

            write("41208", 0.0);
            write("41209", 0.0);

            write("41211", 0.0);
            write("41212", 0.0);

            write("41215", 0.0);
            write("41216", 0.0);
            write("41217", 0.0);
            write("41218", 0.0);

            write("41221", 0.0);
            write("41222", 0.0);

            write("41225", 0.0);
            write("41226", 0.0);
            write("41227", 0.0);
            write("41228", 0.0);

            tempInitialized = true;
        } catch (HALException ex) {
            throw new HALException("Temperature output Initialization failed.", ex);
        }
    }

    private void initWindows() throws HALException {
        try {
            // Windows
            write("41301", 0d); // absolute

            write("41407", 100.0);
            write("41412", 100.0);

            write("41311", 0.0);
            write("41312", 0.0);
            write("41313", 0.0);
            write("41314", 0.0);
            write("41315", 0.0);
            write("41316", 0.0);

            write("41331", 0.0);
            write("41332", 0.0);
            write("41333", 0.0);
            write("41334", 0.0);
            write("41335", 0.0);
            write("41336", 0.0);

            write("41341", 0.0);
            write("41342", 0.0);
            write("41343", 0.0);
            write("41344", 0.0);
            write("41345", 0.0);
            write("41346", 0.0);

            write("41304", 0.0);

            windowsInitialized = true;
        } catch (HALException ex) {
            throw new HALException("Windows output Initialization failed.", ex);
        }
    }

    @Override
    public void dispose() {
        try {
            this.c.close();
        } catch (SL4DCOMException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public List<String> readLightGroups() throws HALException {
        return this.cfg.getLightGroups();
    }
}
