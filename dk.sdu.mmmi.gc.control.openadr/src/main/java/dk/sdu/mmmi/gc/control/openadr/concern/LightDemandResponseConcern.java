/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.control.openadr.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.openadr.input.DemandResponseLightPlanInput;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;

/**
 *
 * @author ancla
 */
public class LightDemandResponseConcern extends AbstractConcern {

    private int numProblems;

    public LightDemandResponseConcern(ControlDomain g) {
        super("Demand response", g, HARD_PRIORITY);
    }

    /**
     * This method is implemented only to make the GA run faster. It helps
     * differentiate between solutions that are close to being acceptable and
     * solutions that are not.
     */
    @Override
    public double evaluate(Solution s) {

        numProblems = 0;

        FixedDayLightPlan fixed = s.getValue(DemandResponseLightPlanInput.class);
        LightPlan p = s.getValue(LightPlanTodayOutput.class);

        for (int i = 0; i < p.size(); i++) {

            Switch lightSwitch = p.getElement(i);

            int elementHour = DateUtil.hourOfDate(p.getElementStart(i));
            Switch req = fixed.getHour(elementHour);

            // Problem when req is not fulfilled
            if (req != null && !req.isNotAvailable()
                    && (req.isOn() == lightSwitch.isOff()
                    || req.isOff() == lightSwitch.isOn())) {
                numProblems++;
            }
        }

        //System.out.println("fit="+fit);
        return numProblems;
    }
}
