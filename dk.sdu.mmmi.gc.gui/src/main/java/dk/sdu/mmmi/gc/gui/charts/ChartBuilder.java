package dk.sdu.mmmi.gc.gui.charts;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.decouplink.Utilities.context;

/**
 * Builds combined chart of data series and fitness bar series
 *
 * @author jcs
 */
class ChartBuilder {

    private final ControlDomain g;
    private final SimpleDateFormat dfm = new SimpleDateFormat("d/M");
    // Builders
    private final JFreeChartBuilder xyChartBuilder;
    private final JFreeBarChartBuilder barChartBuilder;
    // Chart
    private final String title;
    private final Date to;
    private final Date from;

    ChartBuilder(ControlDomain g, Set<String> selectedSeries, String name, Date from, Date to) {
        this.g = g;

        this.from = from;
        this.to = to;

        this.title = name + " " + "(" + dfm.format(from) + "-" + dfm.format(to) + ")";

        List<DoubleTimeSeries> series = getSelectedTimeSeries(selectedSeries);
        xyChartBuilder = new JFreeChartBuilder(series, from, to);
        barChartBuilder = new JFreeBarChartBuilder(series, from, to);
    }

    ChartPanel createChart() {
        DateAxis axis = new DateAxis("Time");
        //axis.setRange(from, to);
        // Combined domain axis chart
        CombinedDomainXYPlot combinedDomainXYPlot = new CombinedDomainXYPlot(axis);

        // Build data series chart
        xyChartBuilder.loadSeries();

        XYPlot xyChartPlot = xyChartBuilder.createChart().getXYPlot();

        combinedDomainXYPlot.add(xyChartPlot, 3);
        combinedDomainXYPlot.setFixedLegendItems(xyChartPlot.getLegendItems());
        combinedDomainXYPlot.setBackgroundPaint(Color.white);

        // Build fitness series bar chart
        barChartBuilder.loadSeries();
        if (barChartBuilder.hasData()) {
            JFreeChart barChart = barChartBuilder.createChart();
            combinedDomainXYPlot.add(barChart.getXYPlot());
        }

        JFreeChart chart = new JFreeChart(title, combinedDomainXYPlot);
        chart.setBackgroundPaint(Color.white);

        ChartPanel chartPanel = new ChartPanel(chart);

        // Trace line
        chartPanel.setDismissDelay(1000 * 60);
        chartPanel.setHorizontalAxisTrace(true);
        chartPanel.addChartMouseListener(new ChartPanelMouseListener(g, chartPanel, from, to));

        return chartPanel;
    }

    //
    // Private methods
    //
    private List<DoubleTimeSeries> getSelectedTimeSeries(Set<String> selectedSeries) {

        List<DoubleTimeSeries> selectedDataSeries = new ArrayList<>();
        for (DoubleTimeSeries s : context(g).all(DoubleTimeSeries.class)) {
            if (selectedSeries.contains(s.getID())) {
                selectedDataSeries.add(s);
            }
        }
        return selectedDataSeries;
    }
}
