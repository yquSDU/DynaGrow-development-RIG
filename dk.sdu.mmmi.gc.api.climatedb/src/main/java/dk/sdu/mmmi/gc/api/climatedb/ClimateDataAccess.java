package dk.sdu.mmmi.gc.api.climatedb;

import dk.sdu.mmmi.controleum.impl.entities.config.ConcernConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.ControlConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.NegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PhotoSumAchievedConfig;
import dk.sdu.mmmi.gc.impl.entities.config.PrivaConfig;
import dk.sdu.mmmi.gc.impl.entities.config.SuperLinkConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public interface ClimateDataAccess {

    void cleanClimateData();

    //
    // Setter
    //
    void setSuperLinkConfiguration(SuperLinkConfig cfg);

    //
    // Getter
    //
    SuperLinkConfig getSuperLinkConfiguration();

    ChartConfig getChartConfig(String id);
    //
    // Updates
    //

    void updateLightForecastConfig(LightForecastConfig cfg);

    void updatePARSumAchievedConfig(PARSumAchievedConfig cfg);

    void updatePhotoSumAchievedConfig(PhotoSumAchievedConfig cfg);

    //
    // Deletes
    //
    void deleteChart(ChartConfig cfg);

    ConcernConfig getConcernConfiguration(String id);

    ControlConfig getControlConfiguration();

    List<Sample<UMolSqrtMeterSecond>> selectActualPhoto(Date fr, Date to);

    Sample<UMolSqrtMeterSecond> selectActualPhoto(Date t);

    List<Sample<Celcius>> selectAirTemperature(Date fr, Date to);

    Sample<Celcius> selectAirTemperature(Date t);

    List<Sample<PPM>> selectCO2(Date fr, Date to);

    Sample<PPM> selectCO2(Date t);

    List<Sample<PPM>> selectCO2Threshold(Date fr, Date to);

    Sample<PPM> selectCO2Threshold(Date t);

    List<ChartConfig> selectCharts();

    OutdoorLightForecast selectOutdoorLightForecast(Date fr, Date to);

    void updateChart(ChartConfig cfg);

    //
    // Inserts
    //
    void insertGreenhouseSize(Sample<SqrMeter> s);

    void insertLightForecastConfig(LightForecastConfig cfg);

    void insertPARSumAchievedConfig(PARSumAchievedConfig cfg);

    void insertActualPhoto(Sample<UMolSqrtMeterSecond> s);

    void insertAirTemperature(Sample<Celcius> s);

    void insertCO2(Sample<PPM> s);

    void insertCO2Threshold(Sample<PPM> s);

    ChartConfig insertChart(ChartConfig cfg);

    void insertOutdoorLightForecast(Sample<OutdoorLightForecast> s);

    void insertConcernNegotiationResult(Sample<ConcernNegotiationResult> s);

    void insertDailyAverageTemperature(Sample<Celcius> s);

    void insertDailyAverageTemperatureGoal(Sample<Celcius> s);

    void insertDecreasingVentilationTemperaturePerMinute(Sample<Celcius> s);

    void insertLightIntensity(Sample<UMolSqrtMeterSecond> s);

    void insertLightPlanConfig(LightPlanConfig cfg);

    void insertLightStatus(Sample<Switch> s);

    void insertLightSumHoursGoal(Sample<Duration> s);

    void insertLightTimeAchievedToday(Sample<Duration> s);

    void insertLightTransmissionFactor(Sample<Percent> s);

    void insertMaxAirTemperature(Sample<Celcius> s);

    void insertMaxHumidity(Sample<Percent> s);

    void insertMaxOutdoorLight(Sample<WattSqrMeter> s);

    void insertMinAirTemperature(Sample<Celcius> s);

    void insertNegotiationResult(Sample<NegotiationResult> s);

    void insertOptimalPhoto(Sample<UMolSqrtMeterSecond> s);

    void insertOutdoorLight(Sample<WattSqrMeter> s);

    void insertOutdoorTemperature(Sample<Celcius> s);

    void insertPARBalanceYesterday(Sample<MolSqrMeter> s);

    void insertPARSumAchievedToday(Sample<MolSqrMeter> s);

    void insertPARSumAchievedPeriod(Sample<MolSqrMeter> s);

    void insertPARSumDayGoal(Sample<MolSqrMeter> s);

    void insertPhotoBalanceYesterday(Sample<MMolSqrMeter> s);

    void insertPhotoOptimization(Sample<Percent> s);

    void insertPhotoSumAchievedToday(Sample<MMolSqrMeter> s);

    void insertPhotoSumDayGoal(Sample<MMolSqrMeter> s);

    void insertPhotoSumAchievedConfig(PhotoSumAchievedConfig cfg);

    void insertPreferredDayCO2(Sample<PPM> s);

    void insertPreferredDayScreenPosition(Sample<Percent> s);

    void insertPreferredDayTemperature(Sample<Celcius> s);

    void insertPreferredDayVentilationSP(Sample<Celcius> s);

    void insertPreferredNightCO2(Sample<PPM> s);

    void insertPreferredNightScreenPosition(Sample<Percent> s);

    void insertPreferredNightTemperature(Sample<Celcius> s);

    void insertPreferredNightVentilationSP(Sample<Celcius> s);

    void insertProposedLightPlan(LightPlan sample);

    void insertScreenClosingPct(Sample<Percent> sample);

    void insertScreenPhotoRatio(Sample<Percent> sample);

    void insertScreenTransmissionFactor(Sample<Percent> s);

    void insertVentilationThreshold(Sample<Celcius> s);

    void insertWindowsOpening(Sample<Percent> s);

    void insertHeatingThreshold(Sample<Celcius> s);

    void insertGeneralScreenPosition(Sample<Percent> s);

    void insertHumidity(Sample<Percent> s);

    void insertInstalledLampEffect(Sample<WattSqrMeter> s);

    void insertExpectedNaturalPARSumToday(Sample<MolSqrMeter> s);

    void insertExpectedNaturalPARSumPeriod(Sample<MolSqrMeter> sample);

    void insertExpectedNaturalPhotoSumToday(Sample<MMolSqrMeter> s);

    void insertExpectedNaturalPhotoSumPeriod(Sample<MMolSqrMeter> sample);

    void insertFixedLightPlan(Sample<FixedDayLightPlan> s);

    void insertLampIntensity(Sample<UMolSqrtMeterSecond> s);

    //
    // Selects
    //
    Sample<SqrMeter> selectGreenhouseSize(Date t);

    LightForecastConfig selectLightForecastConfig();

    Sample<FixedDayLightPlan> selectFixedLightPlan(Date t);

    Sample<UMolSqrtMeterSecond> selectLightIntensity(Date t);

    LightPlanConfig selectLightPlanConfig();

    List<Sample<Switch>> selectLightStatus(Date fr, Date to);

    Sample<Switch> selectLightStatus(Date t);

    List<Sample<Duration>> selectLightSumHoursGoal(Date fr, Date to);

    Sample<Duration> selectLightSumHoursGoal(Date t);

    List<Sample<Duration>> selectLightTimeAchievedToday(Date fr, Date to);

    Sample<Duration> selectLightTimeAchievedToday(Date t);

    List<Sample<Percent>> selectLightTransmissionFactor(Date fr, Date to);

    Sample<Percent> selectLightTransmissionFactor(Date t);

    List<Sample<Celcius>> selectMaxAirTemperature(Date fr, Date to);

    Sample<Celcius> selectMaxAirTemperature(Date t);

    List<Sample<Percent>> selectMaxHumidity(Date fr, Date to);

    Sample<Percent> selectMaxHumidity(Date t);

    List<Sample<WattSqrMeter>> selectMaxOutdoorLight(Date fr, Date to);

    Sample<WattSqrMeter> selectMaxOutdoorLight(Date t);

    List<Sample<Celcius>> selectMinAirTemperature(Date fr, Date to);

    Sample<Celcius> selectMinAirTemperature(Date t);

    List<Sample<NegotiationResult>> selectNegotiationResult(Date fr, Date to);

    Sample<NegotiationResult> selectNegotiationResult(Date t);

    List<Sample<UMolSqrtMeterSecond>> selectOptimalPhoto(Date fr, Date to);

    Sample<UMolSqrtMeterSecond> selectOptimalPhoto(Date t);

    List<Sample<WattSqrMeter>> selectOutdoorLight(Date fr, Date to);

    Sample<WattSqrMeter> selectOutdoorLight(Date t);

    List<Sample<Celcius>> selectOutdoorTemperature(Date fr, Date to);

    Sample<Celcius> selectOutdoorTemperature(Date t);

    Sample<MolSqrMeter> selectPARBalanceYesterday(Date t);

    List<Sample<MolSqrMeter>> selectPARSumAchievedToday(Date fr, Date to);

    Sample<MolSqrMeter> selectPARSumAchievedToday(Date t);

    Sample<MMolSqrMeter> selectPhotoSumAchievedToday(Date t);

    List<Sample<MMolSqrMeter>> selectPhotoSumAchievedToday(Date fr, Date to);

    List<Sample<MolSqrMeter>> selectPARSumAchievedPeriod(Date from, Date to);

    Sample<MolSqrMeter> selectPARSumAchievedPeriod(Date t);

    List<Sample<MolSqrMeter>> selectPARSumDayGoal(Date fr, Date to);

    Sample<MolSqrMeter> selectPARSumDayGoal(Date t);

    Sample<MMolSqrMeter> selectPhotoBalanceYesterday(Date t);

    List<Sample<Percent>> selectPhotoOptimizationGoal(Date fr, Date to);

    Sample<Percent> selectPhotoOptimizationGoal(Date t);

    List<Sample<MMolSqrMeter>> selectPhotoSumAchievedPeriod(Date fr, Date to);

    Sample<MMolSqrMeter> selectPhotoSumAchievedPeriod(Date t);

    List<Sample<MMolSqrMeter>> selectPhotoSumDayGoal(Date fr, Date to);

    Sample<MMolSqrMeter> selectPhotoSumDayGoal(Date t);

    List<Sample<PPM>> selectPreferredDayCO2(Date fr, Date to);

    Sample<PPM> selectPreferredDayCO2(Date t);

    List<Sample<Percent>> selectPreferredDayScreenPosition(Date fr, Date to);

    Sample<Percent> selectPreferredDayScreenPosition(Date t);

    List<Sample<Celcius>> selectPreferredDayTemperature(Date fr, Date to);

    Sample<Celcius> selectPreferredDayTemperature(Date t);

    List<Sample<Celcius>> selectPreferredDayVentilationSP(Date fr, Date to);

    Sample<Celcius> selectPreferredDayVentilationSP(Date t);

    List<Sample<PPM>> selectPreferredNightCO2(Date fr, Date to);

    Sample<PPM> selectPreferredNightCO2(Date t);

    List<Sample<Percent>> selectPreferredNightScreenPosition(Date fr, Date to);

    Sample<Percent> selectPreferredNightScreenPosition(Date t);

    List<Sample<Celcius>> selectPreferredNightTemperature(Date fr, Date to);

    Sample<Celcius> selectPreferredNightTemperature(Date t);

    List<Sample<Celcius>> selectPreferredNightVentilationSP(Date fr, Date to);

    Sample<Celcius> selectPreferredNightVentilationSP(Date t);

    LightPlan selectProposedLightPlan(Date from, Duration lightInterval);

    Sample<Percent> selectScreenClosingPct(Date t);

    Sample<Percent> selectScreenPhotoRatio(Date t);

    List<Sample<Percent>> selectScreenTransmissionFactor(Date fr, Date to);

    Sample<Percent> selectScreenTransmissionFactor(Date t);

    List<Sample<Celcius>> selectVentilationThreshold(Date fr, Date to);

    Sample<Celcius> selectVentilationThreshold(Date t);

    List<Sample<Percent>> selectWindowsOpening(Date fr, Date to);

    Sample<Percent> selectWindowsOpening(Date t);

    List<Sample<UMolSqrtMeterSecond>> selectLightIntensity(Date fr, Date to);

    Sample<Celcius> selectHeatingThreshold(Date t);

    Sample<WattSqrMeter> selectInstalledLampEffect(Date t);

    List<Sample<WattSqrMeter>> selectInstalledLampEffect(Date fr, Date to);

    Sample<MolSqrMeter> selectExpectedNaturalPARSumToday(Date t);

    List<Sample<MolSqrMeter>> selectExpectedNaturalPARSumToday(Date fr, Date to);

    Sample<MolSqrMeter> selectExpectedNaturalPARSumPeriod(Date t);

    List<Sample<MolSqrMeter>> selectExpectedNaturalPARSumPeriod(Date fr, Date to);

    Sample<MMolSqrMeter> selectExpectedNaturalPhotoSumToday(Date t);

    List<Sample<MMolSqrMeter>> selectExpectedNaturalPhotoSumToday(Date fr, Date to);

    Sample<UMolSqrtMeterSecond> selectLampIntensity(Date t);

    List<Sample<UMolSqrtMeterSecond>> selectLampIntensity(Date fr, Date to);

    void insertEnergyBalance(Sample<Energy> s);

    List<Sample<Celcius>> selectHeatingThreshold(Date fr, Date to);

    Sample<Percent> selectGeneralScreenPosition(Date t);

    List<Sample<Percent>> selectGeneralScreenPosition(Date fr, Date to);

    List<Sample<Celcius>> selectDailyAverageTemperatureGoal(Date fr, Date to);

    Sample<Celcius> selectDailyAverageTemperatureGoal(Date t);

    List<Sample<Celcius>> selectDecreasingVentilationTemperaturePerMinute(Date fr, Date to);

    Sample<Celcius> selectDecreasingVentilationTemperaturePerMinute(Date t);

    Sample<Percent> selectHumidity(Date t);

    List<Sample<Celcius>> selectDailyAverageTemperature(Date fr, Date to);

    List<Sample<Percent>> selectHumidity(Date fr, Date to);

    Sample<Energy> selectEnergyBalance(Date t);

    List<Sample<Energy>> selectEnergyBalance(Date fr, Date to);

    PARSumAchievedConfig selectPARSumAchievedConfig();

    PhotoSumAchievedConfig selectPhotoSumAchievedConfig();

    void insertPhotoSumAchievedPeriod(Sample<MMolSqrMeter> sample);

    void insertExpectedCombinedPhotoSumPeriod(Sample<MMolSqrMeter> sample);

    Sample<UMolSqrtMeterSecond> selectMinLightForArtLight(Date t);

    List<Sample<UMolSqrtMeterSecond>> selectMinLightForArtLight(Date fr, Date to);

    void insertMinLightForArtLight(Sample<UMolSqrtMeterSecond> sample);

    void updateIndoorLightConfig(IndoorLightConfig cfg);

    IndoorLightConfig selectIndoorLightConfig();

    void insertIndoorLightConfig(IndoorLightConfig cfg);

    ElSpotPriceForecastConfig selectElSpotPriceForecastConfig();

    void updateElSpotPriceForecastConfig(ElSpotPriceForecastConfig cfg);

    /**
     * @param s Sample of electricity spot prices (NordPool).
     */
    void insertElSpotPriceForecast(Sample<ElPriceMWhForecast> s);

    /**
     * @param from Start of data interval
     * @param to End of data interval
     * @return Forecast of electricity spot prices (NordPool) in the
     * time-interval from - to.
     */
    ElPriceMWhForecast selectElSpotPriceForecast(Date from, Date to);

    ElPriceForecastConfig selectElPriceForecastConfig();

    void updateElPriceForecastConfig(ElPriceForecastConfig c);

    void insertElPriceForecastConfig(ElPriceForecastConfig cfg);

    void insertElPriceForecast(Sample<ElPriceMWhForecast> s);

    ElPriceMWhForecast selectElPriceForecast(Date fr, Date to);

    /**
     * @param s Sample of combined electricity spot prices (NordPool)     * and
     * electricity price prognosis (Energi DK).
     */
    void insertHybridElPriceForecast(Sample<ElPriceMWhForecast> s);

    /**
     * @param fr Start of data interval
     * @param to End of data interval
     * @return Forecast of combined electricity spot prices (NordPool) and
     * electricity price prognosis (Energi DK) in the time-interval from - to.
     */
    ElPriceMWhForecast selectHybridElPriceForecast(Date fr, Date to);

    void insertMinCO2(Sample<PPM> sample);

    Sample<PPM> selectMinCO2(Date t);

    void insertMaxCO2(Sample<PPM> sample);

    Sample<PPM> selectMaxCO2(Date t);

    void insertLightTimeAchievedPeriod(Sample<Duration> sample);

    List<Sample<Duration>> selectLightTimeAchievedPeriod(Date from, Date to);

    Sample<TimeStamp> selectFrameStart(Date t);

    void insertFrameStart(Date t, TimeStamp newStartDate);

    Sample<TimeStamp> selectFrameEnd(Date t);

    void insertFrameEnd(Date t, TimeStamp newStartEnd);

    PrivaConfig getPrivaConfiguration();

    void setPrivaConfiguration(PrivaConfig get);

}
