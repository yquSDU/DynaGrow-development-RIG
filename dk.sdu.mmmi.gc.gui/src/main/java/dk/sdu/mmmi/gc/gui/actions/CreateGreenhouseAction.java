package dk.sdu.mmmi.gc.gui.actions;

import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.Presenter;

public final class CreateGreenhouseAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomainManager gs;

    public CreateGreenhouseAction() {
        super("Create greenhouse");
        this.gs = Lookup.getDefault().lookup(ControlDomainManager.class);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        NotifyDescriptor.InputLine dd;
        dd = new NotifyDescriptor.InputLine("Name", "Create Greenhouse");

        Object result = DialogDisplayer.getDefault().notify(dd);

        if (result == NotifyDescriptor.OK_OPTION) {
            createGreenhouse(dd.getInputText().trim());
        }
    }

    private void createGreenhouse(String name) {
        gs.createControlDomain(name);
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/house_add.png", false));
        return mi;
    }
}
