package dk.sdu.mmmi.gc.gui.house;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.awt.*;
import javax.swing.JComponent;

/**
 * @author mrj
 */
public class GreenhouseComponent extends JComponent {

    private static final int MIN_WIDTH = 500;
    private static final double RATIO = 0.75;
    private final ControlDomain g;
    private int xMargin, yMargin;
    private Dimension drawDimension;
    private GreenhouseState state = new GreenhouseState();

    public GreenhouseComponent(ControlDomain g) {
        this.g = g;

        // Configure sizes.
        Dimension dim = new Dimension(MIN_WIDTH, (int) (MIN_WIDTH * RATIO));
        setMinimumSize(dim);
        setPreferredSize(dim);
    }

    public void setState(GreenhouseState state) {
        this.state = state;
        repaint();
    }

    //
    // Positioning and scaling.
    //
    private void updateScale() {
        Dimension size = getSize();

        int w, h;

        if (size.getWidth() * RATIO > size.getHeight()) {
            // Too wide - calculate based on height.
            w = (int) (size.getHeight() / RATIO);
            h = (int) size.getHeight();
            xMargin = (int) ((size.getWidth() - w) / 2);
            yMargin = 0;
        } else {
            // Too high - calculate based on width.
            w = (int) size.getWidth();
            h = (int) (size.getWidth() * RATIO);
            xMargin = 0;
            yMargin = (int) ((size.getHeight() - h) / 2);
        }

        drawDimension = new Dimension(w, h);
    }

    private int xPos(int percent) {
        return xScale(percent) + xMargin;
    }

    private int yPos(int percent) {
        return yScale(percent) + yMargin;
    }

    private int xScale(int percent) {
        return (int) (percent * drawDimension.getWidth() / 100.0);
    }

    private int yScale(int percent) {
        return (int) (percent * drawDimension.getHeight() / 100.0);
    }

    //
    // Painting.
    //
    @Override
    protected void paintComponent(Graphics g) {

        // Get ready.
        super.paintComponent(g);
        Graphics2D gfx = (Graphics2D) g.create();
        gfx.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gfx.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        gfx.setFont(gfx.getFont().deriveFont(Font.BOLD));

        updateScale();

        // Paint.
        gfx.setColor(Color.BLACK);

        // Paint data.
        paintSun(gfx);
        paintArtificialLight(gfx);
        paintScreen(gfx);
        paintWindow(gfx);
        paintOutdoorTemperature(gfx);

        paintLineMeter(gfx, xPos(15), "T [C]", Color.ORANGE, 0.0, 50.0, asDouble(state.getIndoorTemperature()), asDouble(state.getIndoorTemperatureSP()));
        paintLineMeter(gfx, xPos(30), "CO2 [PPM]", Color.CYAN, 300.0, 2000.0, asDouble(state.getCo2()), asDouble(state.getCo2SP()));
        paintLineMeter(gfx, xPos(45), "P [%]", Color.GREEN, 0.0, 100.0, asDouble(state.getPhotosynthesisOptimization()), asDouble(state.getPhotosynthesisOptimizationGoal()));
        //paintLineMeter(gfx, xPos(60), "H [%]", Color.BLUE, 0.0, 100.0, asDouble(state.getHumidity()), null);

        paintNumber(gfx, 0, "I: %.0f [M/m2]", asDouble(state.getIndoorIrradiation()));
        paintNumber(gfx, 1, "H: %.0f [%%]", asDouble(state.getHumidity()));
        //paintNumber(gfx, 1, "more?");

        // Paint house.
        paintHouse(gfx);
        //paintDebug(gfx);
    }

    private void paintHouse(Graphics2D gfx) {
        gfx = (Graphics2D) gfx.create();

        Polygon p = new Polygon();
        p.addPoint(xPos(5), yPos(95));
        p.addPoint(xPos(5), yPos(55));
        p.addPoint(xPos(50), yPos(40));
        p.addPoint(xPos(95), yPos(55));
        p.addPoint(xPos(95), yPos(95));

        gfx.setColor(Color.BLACK);
        gfx.setStroke(new BasicStroke(3));
        gfx.drawPolygon(p);
    }

    private void paintSun(Graphics2D gfx) {
        gfx = (Graphics2D) gfx.create();

        int xC = xPos(5);
        int yC = yPos(5);
        int width = xScale(20);

        gfx.setColor(Color.ORANGE);
        gfx.fillArc(xC, yC, width, width, 0, 360);

        int xT = xC + width / 2;
        int yT = yC + width / 2;

        WattSqrMeter w = state.getOutdoorIrradiation();
        if (w != null) {
            gfx.setColor(Color.BLACK);
            paintHorizontallyCenteredString(
                    gfx, xT, yT + 7,
                    String.format("%.0f [W/m2]", w.value()));
        }
    }

    private void paintOutdoorTemperature(Graphics2D gfx) {
        gfx = (Graphics2D) gfx.create();

        int xC = xPos(5) + xScale(10);
        int yC = yPos(40);

        Celcius c = state.getOutdoorTemperature();
        if (c != null) {
            gfx.setColor(Color.BLACK);
            paintHorizontallyCenteredString(
                    gfx, xC, yC,
                    String.format("%.0f [C]", c.value()));
        }
    }

    private void paintWindow(Graphics2D gfx) {
        Boolean ventilating = state.isVentilating();
        if (ventilating != null) {
            gfx = (Graphics2D) gfx.create();

            double xC = xPos(50);
            double yC = yPos(40);
            double xT = xPos(95);
            double yT = yPos(55);

            double minAngle = Math.acos((yT - yC) / (xT - xC));
            double maxAngle = minAngle + Math.PI / 4;
            double angle = ventilating ? maxAngle : minAngle;

            double dia = xScale(10);

            gfx.setStroke(new BasicStroke(3));

            gfx.setColor(Color.BLACK);
            gfx.drawLine((int) xC, (int) yC, (int) (xC + Math.sin(angle) * dia), (int) (yC + Math.cos(angle) * dia));
            gfx.drawLine((int) xC, (int) yC, (int) (xC - Math.sin(angle) * dia), (int) (yC + Math.cos(angle) * dia));
        }
    }

    private void paintScreen(Graphics2D gfx) {
        Percent pos = state.getScreenPosition();
        if (pos != null) {
            gfx = (Graphics2D) gfx.create();

            int xMin = xPos(5);
            int xMax = xPos(95);
            int yPos = yPos(55);
            double sp = pos.asFactor();

            // Screen fill.
            gfx.setColor(Color.LIGHT_GRAY);
            gfx.fillRect(xMin, yPos, (int) ((xMax - xMin) * sp), 15);

            // Screen border.
            gfx.setColor(Color.BLACK);
            gfx.drawRect(xMin, yPos, xMax - xMin, 15);

            // Screen text.
            String str = String.format("%.0f %%", pos.asPercent());
            int strW = gfx.getFontMetrics().stringWidth(str);
            gfx.setColor(Color.BLACK);
            gfx.drawString(str, xPos(50) - (strW / 2), yPos(55) + 13);
        }
    }

    private void paintArtificialLight(Graphics2D gfx) {
        for (int pct = 20; pct <= 80; pct += 15) {
            paintArtificialLight(gfx, xPos(pct));
        }
    }

    private void paintArtificialLight(Graphics2D gfx, int xC) {
        gfx = (Graphics2D) gfx.create();

        Boolean on = state.isLightOn();

        int yC = yPos(55) + 15;

        if (on != null && on) {
            Stroke oldStroke = gfx.getStroke();
            gfx.setColor(Color.ORANGE);
            gfx.setStroke(new BasicStroke(3));
            for (int i = -20; i <= 20; i += 5) {
                gfx.drawLine(xC, yC + 2, xC + i, yC + 15);
            }
            gfx.setStroke(oldStroke);
        }

        gfx.setColor(Color.BLACK);
        Polygon p = new Polygon();
        p.addPoint(xC - 10, yC);
        p.addPoint(xC + 10, yC);
        p.addPoint(xC + 15, yC + 10);
        p.addPoint(xC - 15, yC + 10);
        gfx.fillPolygon(p);
    }

    private void paintCircleMeter(Graphics2D gfx, int x) {
        gfx = (Graphics2D) gfx.create();

        int width = xScale(10);
        int xC = x - width / 2;
        int yC = yPos(70);

        // Fills.
        gfx.setColor(Color.BLUE);
        gfx.fillArc(xC, yC, width, width, 0, 90);

        gfx.setColor(Color.RED);
        gfx.fillArc(xC, yC, width, width, 90, 90);

        // Arc.
        gfx.setColor(Color.BLACK);
        gfx.drawArc(xC, yC, width, width, 0, 360);
    }

    private void paintLineMeter(Graphics2D gfx, int xCenterPx, String text, Color color, double min, double max, Double value, Double sp) {
        gfx = (Graphics2D) gfx.create();

        Double valueFactor = value != null ? value / (max - min) : null;
        Double spFactor = sp != null ? sp / (max - min) : null;

        int x1 = xCenterPx - xScale(1);
        int x2 = xCenterPx + xScale(1);
        int xCenter = x1 + (x2 - x1) / 2;
        int y1 = yPos(70);
        int y2 = yPos(90);

        // Texts.
        paintHorizontallyCenteredString(gfx, xCenter, y1 - 3, text);

        // Data fill.
        if (valueFactor != null) {
            gfx.setColor(color);
            int yData = (int) (y2 - (y2 - y1) * valueFactor);
            gfx.fillRect(x1, yData, x2 - x1, y2 - yData);
            gfx.setColor(Color.BLACK);
            paintRightAlignedString(gfx, x1, yData, String.format("%.0f", value));
        }

        // Frame.
        gfx.setColor(Color.BLACK);
        gfx.drawRect(x1, y1, x2 - x1, y2 - y1);

        // Data line.
        if (valueFactor != null) {
            Stroke oldStroke = gfx.getStroke();
            gfx.setColor(Color.BLACK);
            gfx.setStroke(new BasicStroke(3));
            int yValue = (int) (y2 - (y2 - y1) * valueFactor);
            gfx.drawLine(x1 - 3, yValue, x2 + 3, yValue);
            gfx.setStroke(oldStroke);
        }

        // Setpoint line.
        if (spFactor != null) {
            Stroke oldStroke = gfx.getStroke();
            gfx.setColor(Color.RED);
            gfx.setStroke(new BasicStroke(3));
            int ySP = (int) (y2 - (y2 - y1) * spFactor);
            gfx.drawLine(x1 - 3, ySP, x2 + 3, ySP);
            paintLeftAlignedString(gfx, x2, ySP, String.format("%.0f", sp));
            gfx.setStroke(oldStroke);
        }
    }

    private void paintNumber(Graphics2D gfx, int line, String pattern, Double number) {
        gfx = (Graphics2D) gfx.create();

        if (number != null) {
            int x = xPos(70);
            int y = yPos(70) - 3 + (line * 15);
            gfx.drawString(String.format(pattern, number), x, y);
        }
    }

    private void paintHorizontallyCenteredString(Graphics2D gfx, int x, int y, String str) {
        int strWidth = gfx.getFontMetrics().stringWidth(str);
        int strXPos = x - strWidth / 2 + 5;
        gfx.drawString(str, strXPos, y);
    }

    private void paintRightAlignedString(Graphics2D gfx, int x, int y, String str) {
        int strWidth = gfx.getFontMetrics().stringWidth(str);
        int strXPos = x - strWidth - 5;
        int strYPos = y + 5;
        gfx.drawString(str, strXPos, strYPos);
    }

    private void paintLeftAlignedString(Graphics2D gfx, int x, int y, String str) {
        int strXPos = x + 5;
        int strYPos = y + 5;
        gfx.drawString(str, strXPos, strYPos);
    }

    private void paintDebug(Graphics gfx) {
        gfx = (Graphics2D) gfx.create();

        gfx.drawString("Size: " + getSize(), 10, 15);
        gfx.drawString("Draw: " + drawDimension, 10, 30);
        gfx.drawString("XMargin: " + xMargin, 10, 45);
        gfx.drawString("YMargin: " + xMargin, 10, 60);

        gfx.drawRect(xPos(0), yPos(0), xScale(100), yScale(100));
        //g.drawLine(xPos(50), yPos(0), xPos(50), (yPos(100)));
        //g.drawLine(xPos(0), yPos(50), xPos(100), (yPos(50)));
    }

    //
    // Utilities.
    //
    private Double asDouble(Celcius c) {
        return c == null ? null : c.value();
    }

    private Double asDouble(PPM p) {
        return p == null ? null : p.value();
    }

    private Double asDouble(Percent p) {
        return p == null ? null : p.asPercent();
    }

    private Double asDouble(MolSqrMeter m) {
        return m == null ? null : m.value();
    }

    private Double asDouble(UMolSqrtMeterSecond m) {
        return m == null ? null : m.value();
    }
}
