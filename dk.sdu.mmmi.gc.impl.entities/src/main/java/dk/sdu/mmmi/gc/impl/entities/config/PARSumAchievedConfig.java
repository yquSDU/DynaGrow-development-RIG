package dk.sdu.mmmi.gc.impl.entities.config;

import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;

/**
 *
 * @author jcs
 */
public class PARSumAchievedConfig {

    private boolean isActive = true;
    private MolSqrMeter parSum = new MolSqrMeter(0.0);

    public PARSumAchievedConfig() {
    }

    public PARSumAchievedConfig(PARSumAchievedConfig cfg) {
        this.isActive = cfg.isActive;
        this.parSum = cfg.getPARSum();
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setPARSum(double parSum) {
        this.parSum = new MolSqrMeter(parSum);
    }

    public boolean isActive() {
        return isActive;
    }

    public MolSqrMeter getPARSum() {
        return parSum;
    }
}
