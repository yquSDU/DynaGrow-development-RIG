package dk.sdu.mmmi.gc.control.basic.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author mrj
 */
public class DayTemperatureInput extends AbstractInput<Celcius> {

    private final ControlDomain g;
    private final Link<Action> action;

    public DayTemperatureInput(ControlDomain g) {
        super("Preferred day temperature");
        this.g = g;
        action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        Celcius v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Celcius result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPreferredDayTemperature(new Sample<>(t, result));
    }

    private Celcius retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Celcius> r = db.selectPreferredDayTemperature(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Celcius oldValue = DayTemperatureInput.this.getValue();
            Celcius newValue = DialogUtil.promtCelcius(oldValue, "Preferred day temperature");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
