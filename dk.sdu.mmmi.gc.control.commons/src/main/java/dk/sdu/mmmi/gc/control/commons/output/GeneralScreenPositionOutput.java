package dk.sdu.mmmi.gc.control.commons.output;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.BinaryOutput;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.bitSetToDouble;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.doubleToBitSet;
import dk.sdu.mmmi.controleum.control.commons.AbstractOutput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.BitSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author jcs
 */
public class GeneralScreenPositionOutput extends AbstractOutput<Percent> implements BinaryOutput<Percent>{

    private static final int DELTA = 2;
    private static final int MAX = 100;
    private static final int MIN = 0;
    private final int bitSize = 32;

    public GeneralScreenPositionOutput(ControlDomain g) {
        super("Hybrid screen setpoint", g);
    }

    @Override
    public Percent getRandomValue(Date t) {
        return new Percent(MAX * R.nextDouble());
    }

    @Override
    public Percent getMutatedValue(Percent v) {

        double mutated;

        boolean positive = R.nextBoolean();

        if (positive) {
            mutated = v.asPercent() + DELTA * R.nextDouble();
        } else {
            mutated = v.asPercent() - DELTA * R.nextDouble();
        }

        if (MAX < mutated) {
            mutated = MAX;
        } else if (mutated < MIN) {
            mutated = MIN;
        }

        return new Percent(mutated);
    }

    @Override
    public void doCommitValue(Date t, Percent result) {

        // HAL if enabled.
        if (isEnabled()) {
            HALConnector hal = HAL.get(domain);
            if (hal != null) {
                try {
                    hal.writeGeneralScreenPosition(result);
                } catch (HALException ex) {
                    Logger.getLogger(GeneralScreenPositionOutput.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        // DB.
        ClimateDataAccess db = context(domain).one(ClimateDataAccess.class);
        db.insertGeneralScreenPosition(new Sample<>(t, result));

    }

    @Override
    public BitSet asBitSet() {
        Double val = getValue().value();
        BitSet bs = new BitSet();
        doubleToBitSet(bs, 0, bitSize, MIN, MAX, val);
        return bs;

    }

    @Override
    public void setBitSet(BitSet bitset) {
        setValue(new Percent(bitSetToDouble(bitset, 0, bitSize, MIN, MAX)));
    }

    @Override
    public int bitSize() {
        return bitSize;
    }
}
