package dk.sdu.mmmi.gc.impl.okosimulation;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;
import com.mathworks.toolbox.javabuilder.MWStructArray;
import java.util.Calendar;
import java.util.Date;

/**
 * @author mrj
 */
public class Util {
    public static Double getStructDouble(MWStructArray a, String field) {
        // This is ugly, but i know of no better way.
        Double r = new Double(Double.POSITIVE_INFINITY);
        MWArray v = a.getField(a.fieldIndex(field) + 1);
        if(!v.isEmpty()){
            r= Double.parseDouble(a.getField(a.fieldIndex(field) + 1).toString());
        }
        return r;
    }

    public static Double getDouble(MWNumericArray a) {
        // This is ugly, but i know of no better way.
        return Double.parseDouble(a.toString());
    }

    public static double getDayno(Date time) {
        Calendar c = Calendar.getInstance();
        c.setTime(time);
        return c.get(Calendar.DAY_OF_YEAR); // first day is 1.
    }

    public static double getHour(Date time) {
        Calendar c = Calendar.getInstance();
        c.setTime(time);
        return c.get(Calendar.HOUR_OF_DAY) + 1;
    }

    public static double getFiveMinInterval(Date time) {
        Calendar c = Calendar.getInstance();
        c.setTime(time);
        return (c.get(Calendar.MINUTE) / 5) + 1;
    }
}
