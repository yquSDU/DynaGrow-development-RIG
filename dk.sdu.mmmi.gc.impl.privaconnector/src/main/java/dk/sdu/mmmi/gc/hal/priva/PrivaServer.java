package dk.sdu.mmmi.gc.hal.priva;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import dk.sdu.mmmi.gc.hal.priva.acquaintance.PrivaCommException;
import dk.sdu.mmmi.gc.hal.priva.acquaintance.PrivaServerException;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaConversionInfo;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaEntity;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaFormattedValue;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaListValue;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaValueList;
import java.util.ArrayList;

/**
 *
 * @author ancla
 */
public class PrivaServer {

    private final PosysCommunication comm;
    private boolean connected = false;
    private final ArrayList<String> scadList;

    private String[] mergedFormattedValues;
    private int[] rawValues;

    private final String pcuName;

    public PrivaServer(String pcuName) {
        connected = false;
        comm = PosysCommunication.getInstance();
        scadList = new ArrayList<>();
        this.pcuName = pcuName;
    }

    public synchronized void writeToggle(String scad, PrivaListValue toggleValue) throws PrivaServerException {
        int idx = getIndex(scad);

        String valToWrite = new String(toggleValue.getName()).trim();
        doWrite(valToWrite, idx);
    }

    public synchronized void writeRawValue(String scad, int value) throws PrivaServerException {
        int idx = getIndex(scad);

        checkHasAttribute(idx);
        String valToWrite = convertToFormatted(idx, value);
        doWrite(valToWrite, idx);
    }

    public synchronized void writeRawValueWithAttribute(String scad, int value, PrivaListValue attributeValue) throws PrivaServerException {
        int idx = getIndex(scad);

        String formattedValue = convertToFormatted(idx, value);
        String valToWrite = mergeValueAndAttribute(formattedValue, attributeValue);
        doWrite(valToWrite, idx);
    }

    public synchronized void writeFormattedValue(String scad, String value) throws PrivaServerException {
        int idx = getIndex(scad);

        checkHasAttribute(idx);
        doWrite(value, idx);
    }

    public synchronized void writeFormattedValueWithAttribute(String scad, String value, PrivaListValue attributeValue) throws PrivaServerException {
        int idx = getIndex(scad);

        String valueWithAttribute = mergeValueAndAttribute(value, attributeValue);
        doWrite(valueWithAttribute, idx);
    }

    public synchronized PrivaEntity readScad(String scad) throws PrivaServerException {
        int idx = getIndex(scad);

        PrivaValueList attributes = null;
        PrivaListValue currentAttribute = null;

        PrivaValueList toggleValues = null;
        PrivaListValue currentToggleValue = null;

        boolean isNumeric = isNumeric(idx);

        boolean hasAttrib = hasAttribute(idx);
        if (hasAttrib) {
            attributes = getAttributes(idx);
            currentAttribute = getAttribute(attributes, idx);
        }
        boolean isToggle = isToggleValue(idx);
        if (isToggle) {
            toggleValues = getToggleValues(idx);
            currentToggleValue = getToggleValue(toggleValues, idx);
        }

        return new PrivaEntity(scad, rawValues[idx], mergedFormattedValues[idx], isNumeric, isToggle, currentToggleValue, toggleValues, hasAttrib, currentAttribute, attributes);

    }

    private void checkHasAttribute(int idx) throws PrivaServerException {
        if (hasAttribute(idx)) {
            throw new PrivaServerException("No attribute passed, but SCAD has attribute.");
        }
    }

    private int getIndex(String scad) throws PrivaServerException {
        addScad(scad);
        readServerValues();
        int idx = scadList.indexOf(scad);
        return idx;
    }

    //Vi fjerner fra en liste baseret på string. Ikke sikker på jeg er pisse vild med det.
    private void addScad(String scad) throws PrivaServerException {
        if (!scadAdded(scad)) {
            if (isConnected()) {
                disconnect();
                scadList.add(scad);
                connect();
            } else {
                scadList.add(scad);
            }
        }
    }

    private void removeScad(String scad) throws PrivaServerException {
        if (scadAdded(scad)) {
            if (isConnected()) {
                disconnect();
                scadList.remove(scad);
                connect();
            } else {
                scadList.remove(scad);
            }
        }
    }

    private boolean scadAdded(String scad) {
        return scadList.contains(scad);
    }

    private void readServerValues() throws PrivaServerException {
        try {
            connect();
            rawValues = comm.readRawValues();
            mergedFormattedValues = comm.readValues();
            disconnect();
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to update values from Priva server.", ex);
        }
    }

    private void connect() throws PrivaServerException {
        if (!isConnected()) {
            try {
                comm.getAlloc(scadList.toArray(new String[scadList.size()]));
                comm.open(pcuName);
                connected = true;
            } catch (PrivaCommException ex) {
                throw new PrivaServerException("Unable to configure Priva server. " + pcuName, ex);
            }
        }
    }

    private void disconnect() throws PrivaServerException {
        try {
            if (isConnected()) {
                comm.close();
                connected = false;
            }
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to disconnect.", ex);
        }
    }

    private boolean isConnected() {
        return connected;
    }

    private String mergeValueAndAttribute(String value, PrivaListValue attributeValue) throws PrivaServerException {
        try {
            connect();
            String res = comm.attribMerge(value.getBytes(), attributeValue.getName());
            return res;
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to merge value and attribute.", ex);
        } finally {
            disconnect();
        }
    }

    private void doWrite(String value, int idx) throws PrivaServerException {
        String[] valuesToWrite = mergedFormattedValues.clone();
        valuesToWrite[idx] = value;
        try {
            connect();
            boolean writePermRequired = comm.isWritePermissionRequired(pcuName);
            if (writePermRequired) {
                comm.getWritePermissions(pcuName);
                comm.writeValues(valuesToWrite);
                comm.releaseWritePermissions(pcuName);
            } else {
                comm.writeValues(valuesToWrite);
            }
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Error while trying to write values to Priva server.", ex);
        } finally {
            disconnect();
        }
    }

    private boolean isNumeric(int idx) throws PrivaServerException {
        try {
            connect();
            boolean isNumeric = comm.isNumeric(idx);
            return isNumeric;
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to determine if SCAD is toggle value.", ex);
        } finally {
            disconnect();
        }
    }

    private boolean hasAttribute(int i) throws PrivaServerException {
        try {
            connect();
            boolean hasAttrib = comm.isAttrib(i);
            return hasAttrib;
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to determine if SCAD has attribute.", ex);
        } finally {
            disconnect();
        }
    }

    private boolean isToggleValue(int i) throws PrivaServerException {
        try {
            connect();
            boolean hasAttrib = comm.isToggle(i);
            return hasAttrib;
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to determine if SCAD is toggle value.", ex);
        } finally {
            disconnect();
        }
    }

    private PrivaListValue getAttribute(PrivaValueList attributeList, int idx) throws PrivaServerException {
        try {
            connect();
            PrivaFormattedValue pfv = new PrivaFormattedValue(comm.attribSplit(idx, mergedFormattedValues[idx]));
            return attributeList.getByName(pfv.getAttrib());
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to get attribute.", ex);
        } finally {
            disconnect();
        }
    }

    //Getting by index - should work, as long as we ensure that the toggleValue-list is ordered.
    private PrivaListValue getToggleValue(PrivaValueList toggleValues, int idx) {
        return toggleValues.get(rawValues[idx]);
    }

    private PrivaValueList getAttributes(int idx) throws PrivaServerException {
        try {
            connect();
            PrivaValueList attributeList = comm.createAttributeValueList(idx);
            attributeList = comm.configureAttributeValueList(idx, attributeList);
            return comm.fillValueList(attributeList);
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to get attributes.", ex);
        } finally {
            disconnect();
        }
    }

    private PrivaValueList getToggleValues(int idx) throws PrivaServerException {
        try {
            connect();
            PrivaValueList toggleValueList = comm.createToggleValueList(idx);
            toggleValueList = comm.configureToggleValueList(idx, toggleValueList);
            return comm.fillValueList(toggleValueList);
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Unable to get toggle values.", ex);
        } finally {
            disconnect();
        }
    }

    private String convertToFormatted(int idx, int value) throws PrivaServerException {
        try {
            connect();
            PrivaConversionInfo results = comm.getGetConversionInfo(idx);
            return comm.rawToFormatted(value, results);
        } catch (PrivaCommException ex) {
            throw new PrivaServerException("Error while converting raw value.", ex);
        } finally {
            disconnect();
        }
    }

}
