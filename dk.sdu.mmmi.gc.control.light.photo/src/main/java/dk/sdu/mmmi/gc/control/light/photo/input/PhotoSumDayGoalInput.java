package dk.sdu.mmmi.gc.control.light.photo.input;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PhotoSumDayGoalInput extends AbstractInput<MMolSqrMeter> {

    private final ControlDomain g;
    private DisposableList d = new DisposableList();

    public PhotoSumDayGoalInput(ControlDomain g) {
        super("Photo. sum day goal");
        this.g = g;
        d.add(context(this).add(Action.class, new ConfigureAction()));
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        MMolSqrMeter v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, MMolSqrMeter result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPhotoSumDayGoal(new Sample<>(t, result));
    }

    private MMolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<MMolSqrMeter> r = db.selectPhotoSumDayGoal(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            MMolSqrMeter oldValue = PhotoSumDayGoalInput.this.getValue();
            MMolSqrMeter newValue = DialogUtil.promtMMolSqrMeter(oldValue, "Photo sum goal");
            if (newValue != null) {
                setValue(newValue);
                store(new Date(), newValue);
            }
        }
    }
}
