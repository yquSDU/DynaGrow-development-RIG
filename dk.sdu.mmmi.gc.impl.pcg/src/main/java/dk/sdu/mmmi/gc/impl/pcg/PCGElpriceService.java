package dk.sdu.mmmi.gc.impl.pcg;

import com.csvreader.CsvReader;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergySpotPriceService;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static dk.sdu.mmmi.gc.impl.pcg.FileHelper.getLastestDataFile;

@ServiceProviders(value = {
        @ServiceProvider(service = EnergyPriceService.class)
        , @ServiceProvider(service = EnergySpotPriceService.class)}
)
public class PCGElpriceService implements EnergyPriceService, EnergySpotPriceService {

    public static final long HOUR_MILLIS = 1000 * 60 * 60;
    private SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
    private NumberFormat numberFormat = NumberFormat.getInstance(Locale.FRANCE);
    private final static String downloadDir = System.getProperty("user.home") + "/Downloads";
    private ClimateDataAccess cda;

    @Override
    public Map<Date, Double> getPrices(EnergyPriceDatabaseArea area, Date startDateTime, Date endDateTime)
            throws EnergyPriceDatabaseException {

        TreeMap<Date, Double> forecast = new TreeMap<>();

        File file = getLastestDataFile(downloadDir, "BelpexFilter*.csv");
        if (file != null) {
            forecast = parseDayAheadCSV(file);
            if (forecast.size() > 0) {
                fillFutureData(forecast);
            }
        }

        return intervalResult(forecast, startDateTime, endDateTime);
    }

    private void fillFutureData(TreeMap<Date, Double> forecast) {
        long latestTime = forecast.lastEntry().getKey().getTime();

        for (long d = latestTime; d <= latestTime + 4 * 24 * HOUR_MILLIS; d += HOUR_MILLIS) {
            forecast.put(new Date(d), forecast.get(new Date(d-24 * HOUR_MILLIS)));
        }
    }

    private TreeMap<Date, Double> intervalResult(TreeMap<Date, Double> forecast, Date startDateTime, Date endDateTime) {
        TreeMap<Date, Double> result = new TreeMap<>();
        for (Date d : forecast.keySet()) {
            if (startDateTime.getTime() <= d.getTime()
                    && d.getTime() <= endDateTime.getTime()) {
                result.put(d, forecast.get(d));
            }
        }
        return result;
    }

    private double averagePrice(TreeMap<Date, Double> forecast) {
        double sum = 0;
        for (Double value : forecast.values()) {
            sum += value;
        }

        return sum / forecast.size();
    }

    private TreeMap<Date, Double> parseDayAheadCSV(File file) {
        TreeMap<Date, Double> forecast = new TreeMap<>();

        try {
            CsvReader priceReader = new CsvReader(new FileReader(file), ';');
            priceReader.readHeaders();

            int hour = 23;
            while (priceReader.readRecord()) {
                String timeStr = priceReader.get(0);
                String value = priceReader.get(1);

                if (timeStr != "" && value != "") {
                    Date time = new Date(SDF.parse(timeStr).getTime() + hour * HOUR_MILLIS);
                    Double price = numberFormat.parse(value.substring(1).trim()).doubleValue();
                    forecast.put(time, price);
                }
                hour = hour > 0 ? hour - 1 : 23;
            }

        } catch (IOException | ParseException ex) {
            Exceptions.printStackTrace(ex);
        }

        return forecast;
    }

    public void set(ClimateDataAccess cda) {
        this.cda = cda;
    }
}
