package dk.sdu.mmmi.gc.control.light.photo.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfDay;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PhotoBalanceYesterdayInput extends AbstractInput<MMolSqrMeter> {

    private final ControlDomain g;
    private Date updateTime;

    public PhotoBalanceYesterdayInput(ControlDomain g) {
        super("Photo. balance yesterday");
        this.g = g;
        this.updateTime = new Date(endOfDay(new Date()).getTime() - 1000 * 60 * 5);
        context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {

        if (t.after(updateTime)) {
            MMolSqrMeter goalToDay = context(g).one(PhotoSumDayGoalInput.class).getValue();
            MMolSqrMeter photoToday = context(g).one(PhotoSumAchievedPeriodInput.class).getValue();
//qqq            MMolSqrMeter balance = this.getValue().add(photoToday).subtract(goalToDay);
            BigDecimal bigD = goalToDay.bigDecimal();
            MMolSqrMeter balance = goalToDay;

            store(t, balance);

            // Next update is 5 min before end of next day
            Date next = endOfDay(new Date(t.getTime() + 1000 * 60 * 60));
            updateTime = new Date(next.getTime() - 1000 * 60 * 5);
        }

        setValue(retrieve(t));
    }

    private MMolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<MMolSqrMeter> r = db.selectPhotoBalanceYesterday(t);
        return r.getSample();
    }

    private void store(Date t, MMolSqrMeter mol) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPhotoBalanceYesterday(new Sample<>(t, mol));
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            MMolSqrMeter oldValue = PhotoBalanceYesterdayInput.this.getValue();
            MMolSqrMeter newValue = DialogUtil.promtMMolSqrMeter(oldValue, "Photo. balance yesterday");
            if (newValue != null) {
                setValue(newValue);
                store(new Date(), newValue);
            }
        }
    }
}
