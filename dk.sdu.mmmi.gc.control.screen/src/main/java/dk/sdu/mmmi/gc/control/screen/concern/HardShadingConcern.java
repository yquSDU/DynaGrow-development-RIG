package dk.sdu.mmmi.gc.control.screen.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import dk.sdu.mmmi.gc.control.screen.input.OutdoorLightMaximumInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenClosingInput;

/**
 *
 * @author jcs
 */
public class HardShadingConcern extends AbstractConcern {

    public HardShadingConcern(ControlDomain d) {
        super("Hard Shading", d, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        double scrPos = s.getValue(GeneralScreenPositionOutput.class).asFactor();
        double scrClosingPct = s.getValue(ScreenClosingInput.class).asFactor();

        boolean accepted;
        // When Outdoor light is above max limit
        // Then screens should be at least [scrClosingPct] pct. closed
        if (isLightAboveMax(s)) {
            accepted = scrPos >= scrClosingPct;
        } else {
            accepted = true;
        }
        return accepted ? 0 : 1;
    }

    private boolean isLightAboveMax(Solution s) {
        double outLight = s.getValue(OutdoorLightInput.class).value();
        double outMaxLight = s.getValue(OutdoorLightMaximumInput.class).value();
        return outMaxLight < outLight;
    }
}
