package dk.sdu.mmmi.gc.test;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class v2Test {
    private final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static LightPlanConfig lpc;
    private static SearchConfiguration searchCfg;
    private GreenhouseTestFixture testFixture;

    private static Date from;
    private static Date to;

    @BeforeClass
    public static void setUpClass() throws ParseException {

        searchCfg = new SearchConfiguration.Builder().debuggingEnabled(true).build();
        lpc = new LightPlanConfig.Builder().duration(DateUtils.MILLIS_PER_HOUR).build();

        from = df.parse("2020-02-02 01:01");
        to = df.parse("2020-02-02 06:01");
    }

    @Test
    public void experimentA() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("TESTV2 "
                + df.format(new Date()), from, 60, searchCfg, lpc).
                setLampIntensity(100).
                setInstalledLampEffect(100).
                //preferLightDuringNight(3).   //qqq 2->3
                minSwitchConcern(3).
                preferCheapLightConcern(3).     //qqq add
                achievePARSum(10.0); //qqq achieveLightTimeFeature(3, 6)->achievePARSum(10.0)

                

        // TEST:
        testFixture.startNegotiation(from, to);
        //g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }
/*    @Test
    public void experimentB() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("ICSRExpB "
                + df.format(new Date()), from, 60, searchCfg, lpc).
                setLampIntensity(100).
                setInstalledLampEffect(100).
                achieveLightTimeFeature(1, 6). // 6 per day
                preferCheapLightConcern(2).
                minSwitchConcern(3);

        // TEST:
        testFixture.startNegotiation(from, to);
        //g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    @Test
    public void experimentC() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("ICSRExpC "
                + df.format(new Date()), from, 60, searchCfg, lpc).
                // Inputs
                setLampIntensity(100).
                setInstalledLampEffect(100).
                // Soft
                achieveLightTimeFeature(1, 6). // 6 per day
                preferPhotoGrowthFeature(2).
                minSwitchConcern(3);

        // TEST:
        testFixture.startNegotiation(from, to);
        //g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    @Test
    public void experimentD() throws Exception {
        // SETUP:
        testFixture = new GreenhouseTestFixture("ICSRExpD "
                + df.format(new Date()), from, 60, searchCfg, lpc).
                // Inputs
                setLampIntensity(100).
                setInstalledLampEffect(100).
                // Soft
                achieveLightTimeFeature(1, 6). // 6 per day
                preferPhotoGrowthFeature(2).
                preferCheapLightConcern(3).
                minSwitchConcern(4);

        // TEST:
        testFixture.startNegotiation(from, to);
        //g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : testFixture.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }*/

}
