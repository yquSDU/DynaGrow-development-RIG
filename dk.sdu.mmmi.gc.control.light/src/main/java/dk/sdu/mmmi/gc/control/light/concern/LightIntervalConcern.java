package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.control.light.input.ProposedLightPlanInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;

/**
 * Accept if the light status is not changed within a hour.
 *
 * @author jcs
 */
public class LightIntervalConcern extends AbstractConcern {

    public LightIntervalConcern(ControlDomain g) {
        super("Light interval", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        boolean accept = true;

        LightPlan newProposedPlan = s.getValue(LightPlanTodayOutput.class);
        LightPlan oldProposedPlan = s.getValue(ProposedLightPlanInput.class);

        if (newProposedPlan.size() == oldProposedPlan.size()) {

            Switch newElement = newProposedPlan.getElement(0);
            Switch oldElement = oldProposedPlan.getElement(0);

            if (!oldElement.isNotAvailable() && !newElement.equals(oldElement)) {
                accept = false;
            }
        }

        return accept ? 0 : 1;
    }
}
