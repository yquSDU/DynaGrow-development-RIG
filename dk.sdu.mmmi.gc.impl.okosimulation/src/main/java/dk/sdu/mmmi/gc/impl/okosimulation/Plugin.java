package dk.sdu.mmmi.gc.impl.okosimulation;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Setup simulated HAL context.
        d.add(context(g).add(HALConnector.class, new OKOSimulationHAL(), 1));
        d.add(context(g).add(SimulationContext.class, new OKOSimulationContext(g)));
        return d;
    }
}
