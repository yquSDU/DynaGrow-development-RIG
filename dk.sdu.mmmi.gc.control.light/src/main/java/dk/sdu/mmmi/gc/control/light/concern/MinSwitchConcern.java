package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;

/**
 * Requirement: Minimizes the number of on/off switches in the light-plan.
 *
 * @author jcs
 */
public class MinSwitchConcern extends AbstractConcern {

    private double cost;

    public MinSwitchConcern(ControlDomain g) {
        super("Min. light swicthes", g, 5);
    }

    @Override
    public double evaluate(Solution option) {

        LightPlan lightplan = option.getValue(LightPlanTodayOutput.class);

        cost = 0;

        for (int i = 0; i < lightplan.getList().size() - 1; i++) {
            Switch curSwitch = lightplan.getElement(i);
            Switch nextSwitch = lightplan.getElement(i + 1);

            boolean isSwitched = curSwitch.isOn() != nextSwitch.isOn();
            if (isSwitched) {
                cost++;
            }
        }

        return cost;
    }

}
