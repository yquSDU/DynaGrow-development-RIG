package dk.sdu.mmmi.gc.control.light.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.DAY_IN_MS;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.WINDOW_SIZE;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfXDaysBefore;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.Date;

/**
 A 3 days sliding window is defined by the FrameStartInput and FrameEndInput

 * @author corfixen
 */
public class FrameStartInput extends AbstractInput<TimeStamp> {


    private final ClimateDataAccess db;

    public FrameStartInput(ControlDomain g) {
        super("Frame Start");
        db = context(g).one(ClimateDataAccess.class);
    }

    @Override
    public void doUpdateValue(Date t) {

        long startMS = retrieve(t).toMS();
        
        // rollover after x days
        if (t.getTime() > (startMS + WINDOW_SIZE * DAY_IN_MS)) {
            startMS = startOfXDaysBefore(t, 1).getTime();
            setValue(new TimeStamp(startMS));
        }
        store(t, startMS);
    }

    private TimeStamp retrieve(Date t) {
        Sample<TimeStamp> v = db.selectFrameStart(t);
        setValue(v.getSample());
        return v.getSample();
    }

    private void store(Date t, long newStartDate) {
        db.insertFrameStart(t, new TimeStamp(newStartDate));
    }

}
