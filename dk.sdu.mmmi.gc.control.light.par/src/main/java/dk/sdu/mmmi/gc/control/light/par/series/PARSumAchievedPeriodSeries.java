package dk.sdu.mmmi.gc.control.light.par.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class PARSumAchievedPeriodSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public PARSumAchievedPeriodSeries(ControlDomain g) {
        this.g = g;
        setName("PAR sum achieved (today - 2 days)");
        setUnit("[mol m^-2]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<MolSqrMeter>> raw = db.selectPARSumAchievedPeriod(from, to);
            for (Sample<MolSqrMeter> s : raw) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }
        return r;
    }
}
