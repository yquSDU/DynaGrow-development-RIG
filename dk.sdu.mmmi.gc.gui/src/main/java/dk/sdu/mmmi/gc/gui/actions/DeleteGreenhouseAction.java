package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import static org.openide.util.Utilities.actionsGlobalContext;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class DeleteGreenhouseAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public DeleteGreenhouseAction(ControlDomain g) {
        super("Delete greenhouse");
        this.g = g;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (ControlDomain gh : actionsGlobalContext().lookupAll(ControlDomain.class)) {
            getControleumManager().deleteControlDomain(gh);
        }
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return !getControlManager().isAutoRunning()
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/house_delete.png", false));
        return mi;
    }

    public ControlDomain getGreenhouse() {
        return actionsGlobalContext().lookup(ControlDomain.class);
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }

    private ControlDomainManager getControleumManager() {
        return Lookup.getDefault().lookup(ControlDomainManager.class);
    }
}
