package dk.sdu.mmmi.gc.impl.entities.config;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ancla
 * TODO: Refactor the configuration out to support dynamic configuration.
 */
public class PrivaConfig {

    private final Gson gson = new Gson();

    // Measured art. light status = I206.0N1R18.1V1
    // TODO: art. intensity can be found at XXXX
    private String pcuName = "COMP1",
            outdoor_temp_scad_id = "M100.0N1R1.1V1",
            indoor_temp_scad_id = "M100.0N1R2.1V1",
            co2_scad_id = "M100.0N1R2.1V4",
            outdoor_light_intensity_scad_id = "M4.0N1R6.1V2",
            indoor_light_intensity_scad_id = "M100.0N1R3.1V6", //M100.0N1R3.1V7 = measured intensity
            humidity_scad_id = "M100.0N1R2.1V2",
            //co2_threshold_scad_id = "I170.0N1R39.1V1",
            heating_threshold_scad_id = "M110.0N1R4.1V1",
            ventilation_threshold_scad_id = "I120.0N1R5.1V1";
    private List<String> lightGroups,
            screens,
            window_sensors;

    public PrivaConfig() {
        lightGroups = new ArrayList<>();
        lightGroups.add("I206.0N1R15.1V1");
        lightGroups.add("I206.0N1R15.1V2");
        //lightGroups.add("I206.0N1R15.1V3");
        //lightGroups.add("I206.0N1R15.1V4");

        screens = new ArrayList<>();
        screens.add("I168.0N1R8.1V1");
        screens.add("I168.0N1R8.1V2");

        window_sensors = new ArrayList<>();
        window_sensors.add("M125.0N1R9.1V1");
        window_sensors.add("M125.0N1R18.1V1");

    }

    /**
     * @return the PcuName
     */
    public String getPcuName() {
        return pcuName;
    }

    /**
     * @param PcuName the PcuName to set
     */
    public void setPcuName(String PcuName) {
        this.pcuName = PcuName;
    }

    /**
     * @return the outdoor_temp_scad_id
     */
    public String getOutdoor_temp_scad_id() {
        return outdoor_temp_scad_id;
    }

    /**
     * @param outdoor_temp_scad_id the outdoor_temp_scad_id to set
     */
    public void setOutdoor_temp_scad_id(String outdoor_temp_scad_id) {
        this.outdoor_temp_scad_id = outdoor_temp_scad_id;
    }

    /**
     * @return the indoor_temp_scad_id
     */
    public String getIndoor_temp_scad_id() {
        return indoor_temp_scad_id;
    }

    /**
     * @param indoor_temp_scad_id the indoor_temp_scad_id to set
     */
    public void setIndoor_temp_scad_id(String indoor_temp_scad_id) {
        this.indoor_temp_scad_id = indoor_temp_scad_id;
    }

    /**
     * @return the co2_scad_id
     */
    public String getCo2_scad_id() {
        return co2_scad_id;
    }

    /**
     * @param co2_scad_id the co2_scad_id to set
     */
    public void setCo2_scad_id(String co2_scad_id) {
        this.co2_scad_id = co2_scad_id;
    }

    /**
     * @return the outdoor_light_intensity_scad_id
     */
    public String getOutdoor_light_intensity_scad_id() {
        return outdoor_light_intensity_scad_id;
    }

    /**
     * @param outdoor_light_intensity_scad_id the
     * outdoor_light_intensity_scad_id to set
     */
    public void setOutdoor_light_intensity_scad_id(String outdoor_light_intensity_scad_id) {
        this.outdoor_light_intensity_scad_id = outdoor_light_intensity_scad_id;
    }

    /**
     * @return the indoor_light_intensity_scad_id
     */
    public String getIndoor_light_intensity_scad_id() {
        return indoor_light_intensity_scad_id;
    }

    /**
     * @param indoor_light_intensity_scad_id the indoor_light_intensity_scad_id
     * to set
     */
    public void setIndoor_light_intensity_scad_id(String indoor_light_intensity_scad_id) {
        this.indoor_light_intensity_scad_id = indoor_light_intensity_scad_id;
    }

    /**
     * @return the humidity_scad_id
     */
    public String getHumidity_scad_id() {
        return humidity_scad_id;
    }

    /**
     * @param humidity_scad_id the humidity_scad_id to set
     */
    public void setHumidity_scad_id(String humidity_scad_id) {
        this.humidity_scad_id = humidity_scad_id;
    }

    /**
     * @return the heating_threshold_scad_id
     */
    public String getHeating_threshold_scad_id() {
        return heating_threshold_scad_id;
    }

    /**
     * @param heating_threshold_scad_id the heating_threshold_scad_id to set
     */
    public void setHeating_threshold_scad_id(String heating_threshold_scad_id) {
        this.heating_threshold_scad_id = heating_threshold_scad_id;
    }

    /**
     * @return the ventilation_threshold_scad_id
     */
    public String getVentilation_threshold_scad_id() {
        return ventilation_threshold_scad_id;
    }

    /**
     * @param ventilation_threshold_scad_id the ventilation_threshold_scad_id to
     * set
     */
    public void setVentilation_threshold_scad_id(String ventilation_threshold_scad_id) {
        this.ventilation_threshold_scad_id = ventilation_threshold_scad_id;
    }

    /**
     * @return the co2_threshold_scad_id
     *
     * public String getCo2_threshold_scad_id() { return co2_threshold_scad_id;
     * }
     *
     * /
     **
     * @param co2_threshold_scad_id the co2_threshold_scad_id to set
     *
     * public void setCo2_threshold_scad_id(String co2_threshold_scad_id) {
     * this.co2_threshold_scad_id = co2_threshold_scad_id; }
     */
    public String getLightGroupsAsString() {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return gson.toJson(lightGroups, type);
    }

    public List<String> getLightGroups() {
        return lightGroups;
    }

    public void setLightGroups(String lightGroups) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        this.lightGroups = gson.fromJson(lightGroups, type);
    }

    /**
     * @return the screens
     */
    public List<String> getScreens() {
        return screens;
    }

    /**
     * @param screens the screens to set
     */
    public void setScreens(List<String> screens) {
        this.screens = screens;
    }

    /**
     * @return the window_sensors
     */
    public List<String> getWindow_sensors() {
        return window_sensors;
    }

    /**
     * @param window_sensors the window_sensors to set
     */
    public void setWindow_sensors(List<String> window_sensors) {
        this.window_sensors = window_sensors;
    }
}
