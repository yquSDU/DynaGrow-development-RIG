package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.gui.actions.DeleteChartAction;
import dk.sdu.mmmi.gc.gui.actions.OpenChartAction;
import dk.sdu.mmmi.gc.gui.actions.RenameChartAction;
import dk.sdu.mmmi.gc.gui.charts.ChartListener;
import java.util.Collection;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author jcs
 */
public class ChartNode extends AbstractNode implements ChartListener {

    private final ChartConfig chart;
    private final Action prefferedAction;
    private final Collection<? extends Action> actions;
    private final DisposableList disposables = new DisposableList();

    public ChartNode(ControlDomain g, ChartConfig chart) {
        super(Children.LEAF);
        this.chart = chart;

        disposables.add(context(chart).add(ChartListener.class, this));

        this.prefferedAction = new OpenChartAction(g, chart);
        this.actions = Lists.newArrayList(prefferedAction,
                new RenameChartAction(g, chart), new DeleteChartAction(g, chart));

        // Add open action to context of chart to be used by other actions (e.g. CreateChartAction)
        setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/chart_curve_go.png");
        updateUI();
    }

    private void updateUI() {
        setDisplayName(chart.getName());
        setName(chart.getIDString());
    }

    @Override
    public void onChartChanged(ChartConfig cfg) {
        updateUI();
    }

    @Override
    public void onChartDelete(ChartConfig cfg) {
        disposables.dispose();
    }

    @Override
    public Action getPreferredAction() {
        return prefferedAction;
    }

    @Override
    public Action[] getActions(boolean context) {
        return actions.toArray(new Action[]{});
    }
}
