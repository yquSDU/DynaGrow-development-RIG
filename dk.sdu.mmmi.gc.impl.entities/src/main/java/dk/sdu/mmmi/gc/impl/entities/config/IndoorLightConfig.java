package dk.sdu.mmmi.gc.impl.entities.config;

/**
 * @author jcs
 */
public class IndoorLightConfig {

    private boolean isCalculated = false;
    private double conversionFactor = 1.0;

    /**
     * @return True if PAR light is calculated based on outdoor light. False if
     * indoor PAR light is measured.
     */
    public boolean isCalculated() {
        return this.isCalculated;
    }

    public void setIsCalculated(boolean isCalculated) {
        this.isCalculated = isCalculated;
    }

    /**
     * @return In case of isCalculated then the conversion factor can be used to
     * convert indoor light to a different unit.
     *
     * For example from klx to "umol m^-1 s^-1"
     */
    public double getLightConversionFactor() {
        return this.conversionFactor;
    }

    public void setConversionFactor(double conversionFactor) {
        this.conversionFactor = conversionFactor;
    }

}
