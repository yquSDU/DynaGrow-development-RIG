package dk.sdu.mmmi.gc.gui.help;

import dk.sdu.mmmi.controleum.api.moea.Element;
import java.awt.BorderLayout;
import java.io.IOException;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;

/**
 * Need help, see this: http://wiki.netbeans.org/DevFaqNonSingletonTopComponents
 *
 * @author mrj
 */
public class ElementHelpTopComponent extends TopComponent {

    public ElementHelpTopComponent(Element element) {
        setName(element.getName());
        JTextPane text = new JTextPane();
        try {
            text.setPage(element.getHtmlHelp());
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        JScrollPane scroll = new JScrollPane(text);
        setLayout(new BorderLayout());
        add(scroll, BorderLayout.CENTER);
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_NEVER;
    }
}
