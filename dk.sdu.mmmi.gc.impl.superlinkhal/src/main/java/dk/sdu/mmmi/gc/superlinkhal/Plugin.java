package dk.sdu.mmmi.gc.superlinkhal;

import com.decouplink.Disposable;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import javax.swing.Action;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain g) {
        SuperLinkHAL s = new SuperLinkHAL(g);
        context(g).add(Action.class, new ConfigureSuperLinkAction(g));
        return context(g).add(HALConnector.class, s);
    }
}
