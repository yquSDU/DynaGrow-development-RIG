package dk.sdu.mmmi.gc.control.advancedlight.input;

import static com.decouplink.Utilities.context;
import com.google.common.collect.Maps;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.input.ElPriceMWhForecastInput;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.Date;
import java.util.Map;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author jcs
 */
@Ignore
public class ElPriceMWhForecastInputTest {

    private ClimateDataAccess daMock;
    private EnergyPriceService energyDb;
    private final Map<Date, Double> dataPresentTime = Maps.newTreeMap();
    private final Map<Date, Double> dataPrevWeek = Maps.newTreeMap();
    private final Map<Date, Double> dataMerged = Maps.newTreeMap();
    private ElPriceForecastConfig cfg;

    public ElPriceMWhForecastInputTest() {
    }

    @Before
    public void setUp() {
        daMock = mock(ClimateDataAccess.class);
        energyDb = mock(EnergyPriceService.class);

        cfg = new ElPriceForecastConfig();
        cfg.setActive(true);

        // Setup test context
        context(test).add(ClimateDataAccess.class, daMock);
        context(test).add(EnergyPriceService.class, energyDb);
        context(test).add(ElPriceForecastConfig.class, cfg);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doUpdateValue method, of class ElPriceMWhForecastInput.
     */
    @Test
    public void testDoUpdateValue() throws EnergyPriceDatabaseException {

        System.out.println("doUpdateValue");

        // SETUP:
        DateTime from = new DateTime(2013, 8, 1, 00, 0, 0, 0);
        DateTime to = new DateTime(2013, 8, 2, 0, 0, 0, 0);
        DateTime noon = new DateTime(2013, 8, 1, 12, 0, 0, 0);

        DateTime fromPrevWeek = noon.minusWeeks(1);
        DateTime toPrevWeek = noon.minusWeeks(1).plusDays(1).withTimeAtStartOfDay();

        // Init data sets
        initData(from, noon, dataPresentTime);
        initData(fromPrevWeek, toPrevWeek, dataPrevWeek);
        initData(from, to, dataMerged);

        // Record Stub calls
        when(energyDb.getPrices(cfg.getElPriceArea(), from.toDate(), to.toDate())).thenReturn(dataPresentTime);
        when(energyDb.getPrices(cfg.getElPriceArea(), fromPrevWeek.toDate(), toPrevWeek.toDate())).thenReturn(dataPrevWeek);

        // TEST:
        ElPriceMWhForecastInput instance = new ElPriceMWhForecastInput(test);
        instance.doUpdateValue(from.toDate());

        // ASSERTS:
        // That forecast dataMerged is the merge of dataPresenTime and dataPrevWeek
        Sample<ElPriceMWhForecast> f = new Sample<>(from.toDate(), new ElPriceMWhForecast(dataMerged, cfg.getElPriceArea().name(), cfg.getCurrency()));
        verify(daMock).insertElPriceForecast(f);
    }

    private void initData(DateTime from, DateTime to, Map<Date, Double> data) {
        for (long i = from.getMillis(); i <= to.getMillis(); i += 1000 * 60 * 60) {
            data.put(new Date(i), 10.0);
        }
    }
    private final ControlDomain test = new ControlDomain() {
        @Override
        public int getID() {
            return -1;
        }

        @Override
        public void setName(String name) {
        }

        @Override
        public String getName() {
            return "Test Greenhouse";
        }
    };
}
