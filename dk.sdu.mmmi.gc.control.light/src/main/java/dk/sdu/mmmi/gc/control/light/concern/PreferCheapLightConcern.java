package dk.sdu.mmmi.gc.control.light.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Money;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Watt;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import static dk.sdu.mmmi.gc.control.commons.utils.EnergyUtil.calcPrice;
import dk.sdu.mmmi.gc.control.light.input.GreenhouseSizeInput;
import dk.sdu.mmmi.gc.control.light.input.HybridElPriceForecastInput;
import dk.sdu.mmmi.gc.control.light.input.InstalledLampEffectInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.HashMap;
import java.util.Map;

/**
 * Requirement: Minimize price of "on" hours in light plan.
 *
 * @author jcs
 */
public class PreferCheapLightConcern extends AbstractConcern {

    private Money energyCostSum;

    public PreferCheapLightConcern(ControlDomain g) {
        super("Prefer cheap light", g, 5);
    }

    /**
     * The cheaper the plan the better fitness
     *
     * consumedEnergy [MWh] = totalLampLoad [MW] * timeOfOperation [hour]
     * energyCost [EUR] = consumedEnergy [MWh] * unitCostOfEnergy [EUR/MWh]
     */
    @Override
    public double evaluate(Solution s) {
        // Inputs
        SqrMeter greenhouseSize = s.getValue(GreenhouseSizeInput.class);
        WattSqrMeter lampEffect = s.getValue(InstalledLampEffectInput.class);
        ElPriceMWhForecast unitCostList = s.getValue(HybridElPriceForecastInput.class);

        LightPlan plan = s.getValue(LightPlanTodayOutput.class);

        double fitness = 1.0;
        if (unitCostList != null) {

            Watt totalLampLoad = lampEffect.times(greenhouseSize);
            LightPlanConfig lpCfg = context(g).one(LightPlanTodayOutput.class).getConfig();

            energyCostSum = calcPrice(plan, unitCostList, totalLampLoad, lpCfg);

            if (energyCostSum != null) {
                // We have prices.
                fitness = energyCostSum.roundedValue();
            }
        }

        // Minimize price of "on" hours.
        return fitness;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (energyCostSum != null) {
            r.put(LightPlanTodayOutput.class, String.format("Cost remaining light plan "
                    + "%s %s", energyCostSum.roundedValue(), energyCostSum.getCurrency()));
        }
        return r;
    }
}
