package dk.sdu.mmmi.gc.control.photo;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.control.photo.concern.PhotoOptimalCO2Concern;
import dk.sdu.mmmi.gc.control.photo.concern.PhotoOptimalTemperatureConcern;
import dk.sdu.mmmi.gc.control.photo.input.ActualPhotosynthesisInput;
import dk.sdu.mmmi.gc.control.photo.input.OptimalPhotosynthesisInput;
import dk.sdu.mmmi.gc.control.photo.series.ActualPhotosynthesisSeries;
import dk.sdu.mmmi.gc.control.photo.series.OptimalPhotosynthesisSeries;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Concerns
        createPhotoOptimalTemperatureConcern(g, d);
        createPhotoOptimalCO2Concern(g, d);

        // Inputs
        createActualPhotosynthesisInput(d, g);
        createOptimalPhotosynthesisInput(d, g);

        return d;
    }

    private void createOptimalPhotosynthesisInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new OptimalPhotosynthesisInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new OptimalPhotosynthesisSeries(g)));
    }

    private void createActualPhotosynthesisInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ActualPhotosynthesisInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ActualPhotosynthesisSeries(g)));
    }

    private void createPhotoOptimalCO2Concern(ControlDomain g, DisposableList d) {
        PhotoOptimalCO2Concern photoOptimalCO2Concern = new PhotoOptimalCO2Concern(g);
        cm.loadConcernConfig(g, photoOptimalCO2Concern);
        d.add(context(g).add(Concern.class, photoOptimalCO2Concern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, photoOptimalCO2Concern)));
    }

    private void createPhotoOptimalTemperatureConcern(ControlDomain g, DisposableList d) {
        PhotoOptimalTemperatureConcern photoOptimalTemperatureConcern = new PhotoOptimalTemperatureConcern(g);
        cm.loadConcernConfig(g, photoOptimalTemperatureConcern);
        d.add(context(g).add(Concern.class, photoOptimalTemperatureConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, photoOptimalTemperatureConcern)));
    }
}
