package dk.sdu.mmmi.gc.gui.charts;

import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class JFreeChartBuilder {

    private final List<Series> data = new ArrayList<>();
    private final Map<String, Axis> axisMap = new HashMap<>();
    private final Date from, to;
    private final List<DoubleTimeSeries> series;

    public JFreeChartBuilder(List<DoubleTimeSeries> series, Date from, Date to) {
        this.from = from;
        this.to = to;
        this.series = series;
    }

    public JFreeChart createChart() {

        JFreeChart chart = ChartFactory.createTimeSeriesChart("", null, null, null, true, false, false);
        chart.setBackgroundPaint(Color.WHITE);
        chart.setNotify(false);

        XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.black);
        plot.setDomainGridlinePaint(Color.black);

        for (int i = 0; i < data.size(); i++) {
            Series si = data.get(i);

            plot.setRangeAxis(si.getAxis().getIdx(), si.getAxis().getAxis());
            plot.setDataset(i, si.getData());
            plot.mapDatasetToRangeAxis(i, si.getAxis().getIdx());

            XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, false);
            renderer.setSeriesStroke(0, new BasicStroke(1.5f));
            plot.setRenderer(i, renderer);
        }

        return chart;
    }

    /**
     * Load a context in which to find DoubleTimeSeries objects.
     */
    public void loadSeries() {
        StopWatch w1 = new StopWatch("build axis");
        // Build axis.
        int aIdx = 0;
        for (DoubleTimeSeries s : series) {
            if (axisMap.get(s.getUnit()) == null) {
                axisMap.put(s.getUnit(), new Axis(aIdx, new NumberAxis(s.getUnit())));
                aIdx++;
            }
        }
        w1.stop();

        StopWatch w2 = new StopWatch("build data");
        // Build dataseries.
        int sIdx = 0;
        for (DoubleTimeSeries s : series) {
            try {
                Axis a = axisMap.get(s.getUnit());
                XYDataset d = createDataset(s);
                if (d != null) {
                    data.add(new Series(sIdx++, a, d, d.getItemCount(0)));
                }
            } catch (Exception x) {
                Exceptions.printStackTrace(x);
            }
        }
        w2.stop();
    }

    private XYDataset createDataset(DoubleTimeSeries dts) throws Exception {

        StopWatch w1 = new StopWatch("Get from " + dts.getName() + " : " + dts.getClass().getSimpleName());

        // Get values.
            List<DoubleTimeValue> all = dts.getValues(from, to);

        if (all.isEmpty()) {
            return null;
        }
        List<DoubleTimeValue> values = new ArrayList<>(all.size() + 2);

        w1.stop();

        // Make sure there is a starting point.
        values.add(new DoubleTimeValue(from, null));

        values.addAll(DataUtil.addNoDataZones(all, dts.getMaxPeriodLenghtMS()));

        // Make sure there is an ending point.
        values.add(new DoubleTimeValue(to, null));

        // Put values in time series.
        String desc = dts.getName() + " " + dts.getUnit();
        TimeSeries s = new TimeSeries(desc);

        StopWatch w2 = new StopWatch("convert");

        for (DoubleTimeValue v : values) {
            s.addOrUpdate(new Millisecond(v.getTime()), v.getValue());
        }

        w2.stop();

        TimeSeriesCollection r = new TimeSeriesCollection();
        r.addSeries(s);

        return r;
    }

    //
    // Helper classes.
    //
    private static class Series {

        private final int idx;
        private final Axis axis;
        private final XYDataset data;
        private final int size;

        public Series(int idx, Axis axis, XYDataset data, int size) {
            this.idx = idx;
            this.axis = axis;
            this.data = data;
            this.size = size;
        }

        public int getIdx() {
            return idx;
        }

        public Axis getAxis() {
            return axis;
        }

        public XYDataset getData() {
            return data;
        }

        public int size() {
            return size;
        }
    }

    private static class Axis {

        private final int idx;
        private final NumberAxis axis;

        public Axis(int idx, NumberAxis axis) {
            this.idx = idx;
            this.axis = axis;
        }

        public int getIdx() {
            return idx;
        }

        public NumberAxis getAxis() {
            return axis;
        }
    }
}
