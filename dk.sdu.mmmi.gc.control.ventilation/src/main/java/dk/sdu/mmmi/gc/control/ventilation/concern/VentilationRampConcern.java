package dk.sdu.mmmi.gc.control.ventilation.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMaximumInput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import dk.sdu.mmmi.gc.control.ventilation.input.DecreasingVentilationTemperaturePerMinuteInput;
import dk.sdu.mmmi.gc.control.ventilation.input.LatestVentilationStartedTimeInput;
import static java.lang.Math.abs;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class VentilationRampConcern extends AbstractConcern {

    private static final int MINUTE_MS = 1000 * 60;

    public VentilationRampConcern(ControlDomain g) {
        super("Decreasing ventilation ramp", g, 5);
    }

    @Override
    public double evaluate(Solution s) {

        Celcius max = s.getValue(IndoorTemperatureMaximumInput.class);
        Celcius ventSP = s.getValue(VentilationTempOutput.class);
        Celcius ventTemp = getVentTemp(s);

        if (shouldVentilate(s)) {
            // Don't ventilate. Prefer ventilation temperature close to max temperature.
            return abs(max.subtract(ventSP).roundedValue());
        } else {
            // Do ventilate. Prefer ventilation temperature close to temperature ramp.
            return abs(ventTemp.subtract(ventSP).roundedValue());
        }
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (shouldVentilate(s)) {
            // Close to max.
            double max = s.getValue(IndoorTemperatureMaximumInput.class).value();
            r.put(VentilationTempOutput.class, String.format("prefer %.1f", max));
        } else {
            // Close to ramped vent t.
            r.put(VentilationTempOutput.class, String.format("prefer %.1f", getVentTemp(s)));
        }

        return r;
    }

    private boolean shouldVentilate(Solution s) {
        Celcius heatingSP = s.getValue(HeatingSetpointOutput.class);
        return getVentTemp(s).roundedValue() < heatingSP.roundedValue();
    }

    private Celcius getVentTemp(Solution s) {
        Date currentTime = s.getTime();
        double tempPerMin = s.getValue(DecreasingVentilationTemperaturePerMinuteInput.class).value();
        double max = s.getValue(IndoorTemperatureMaximumInput.class).value();
        Date latestVentStartTime = s.getValue(LatestVentilationStartedTimeInput.class);
        long ventDurationMS = currentTime.getTime() - latestVentStartTime.getTime();
        return new Celcius(max - (ventDurationMS / (MINUTE_MS)) * tempPerMin);
    }
}
