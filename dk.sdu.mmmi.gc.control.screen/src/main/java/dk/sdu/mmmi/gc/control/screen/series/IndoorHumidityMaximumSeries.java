package dk.sdu.mmmi.gc.control.screen.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class IndoorHumidityMaximumSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public IndoorHumidityMaximumSeries(ControlDomain g) {
        this.g = g;
        setName("Maximum humidity");
        setUnit("[%]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<DoubleTimeValue>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if(db != null) {
            List<Sample<Percent>> raw = db.selectMaxHumidity(from, to);
            for(Sample<Percent> s : raw) {
                Date t = s.getTimestamp();
                Double v = s.getSample().asFactor();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
}
