/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;

/**
 *
 * @author erso Created on 17-05-2010, 09:54:50
 */
public interface PosysLibrary extends Library {

    PosysLibrary INSTANCE = (PosysLibrary) Native.loadLibrary("posysx32.dll", PosysLibrary.class);

//POSYS functions from "POSYS Users guide v 1.01(2) 17th August 1999":
    //ps_get_alloc_size (int n, int *size);
    int ps_get_alloc_size(int n, IntByReference size);

    //ps_open (char *host_name, void *hdl, char **scad_list, int scad_list_len, int *ri);
    int ps_open(String host_name, Pointer hdl, String[] scad_list, int scad_list_len, IntByReference ri);
    //NB. Before calling to ps_open hdl should be initialized as:
    //  Pointer hdl = new Memory(size.getValue());
    // where size is obtained from ps_get_alloc_size();

    //ps_close (void);
    int ps_close();

    //ps_read_values (void *hdl, char **results, int *ri);
    int ps_read_values(Pointer hdl, String[] results, IntByReference ri);

    //ps_istoggle (void *handle, int idx, int *res);
    int ps_istoggle(Pointer handle, int idx, IntByReference res);

    //ps_isattrib (void *handle, int idx, int *res);
    int ps_isattrib(Pointer handle, int idx, IntByReference res);

    //ps_iscategory (void *handle, int idx, int *res);
    int ps_iscategory(Pointer handle, int idx, IntByReference res);

    //ps_get_description (void *handle, int idx, char *location, char *name);
    int ps_get_description(Pointer handle, int idx, byte[] location, byte[] name);

    //ps_get_unit_string (void *handle, int idx, char *unit_str);
    int ps_get_unit_string(Pointer handle, int idx, byte[] unit_str);

    //ps_get_limits (void *handle, int idx, char *lower, char *upper);
    int ps_get_limits(Pointer handle, int idx, byte[] lower, byte[] upper);

    //ps_get_toggle_size (void *handle, int idx, int *count, int *size);
    int ps_get_toggle_size(Pointer handle, int idx, IntByReference count, IntByReference size);

    //ps_get_toggle_list (void *handle, int idx, void *list);
    int ps_get_toggle_list(Pointer handle, int idx, Pointer list);

    //ps_get_attrib_toggle_size (void *handle, int idx, int *count, int *size);
    int ps_get_attrib_toggle_size(Pointer handle, int idx, IntByReference count, IntByReference size);

    //ps_get_attrib_toggle_list (void *handle, int idx, void *list);
    int ps_get_attrib_toggle_list(Pointer handle, int idx, Pointer list);

    //ps_get_toggle (void *list, int idx, char *toggle_name, long *val);
    int ps_get_toggle(Pointer list, int idx, byte[] toggle_name, byte[] val);

    //ps_attrib_split (void *handle, int idx, char *combined, char *val, char *attrib);
    int ps_attrib_split(Pointer handle, int idx, String combined, byte[] val, byte[] attrib);

    //ps_attrib_merge (char *combined, char *val, char *attrib);
    int ps_attrib_merge(byte[] combined, byte[] val, byte[] attrib);

    //ps_get_version_key (char *host_name, char *version_key);
    int ps_get_version_key(String host_name, byte[] version_key);

// POSYS write calls from "User guide extension v. 1.02(15), 1st April 2003:
    //ps_get_write_permission (char *host_name, short *write_id);
    int ps_get_write_permission(String host_name, ShortByReference write_id);

    //int ps_release_write_permission (char *host_name, short write_id);
    int ps_release_write_permission(String host_name, ShortByReference write_id);

    //int psw_write_values (void *hdl, char **values, int *ri, short write_id);
    int psw_write_values(Pointer hdl, String[] values, IntByReference ri, short write_id);

    //ps_isreadonly (void *handle, int idx, int *res);
    int ps_isreadonly(Pointer handle, int idx, IntByReference res);

    //int ps_is_write_permission_required (char *hostname);
    int ps_is_write_permission_required(String hostname);

    // POSYS extended calls from "User guide extension, 22nd June 2005"
    // Started implementation, not all methods implemented.
    // See Posysx.h or possy_x_v2.docx
    int psx_read_raw_values(Pointer handle, int[] results, IntByReference ri);

    int psx_get_conv_info(Pointer handle, int idx, byte[] format, byte[] formula);

    int psx_get_input_formula(Pointer handle, int idx, byte[] formula);

    int psx_isnumeric(Pointer handle, int idx, IntByReference res);

    int psx_get_pres_info(String host_name, short pres_hdl, byte[] format, byte[] input_formula, byte[] output_formula, byte[] unit_str);

    int psx_get_pres_hdl(Pointer handle, int idx, ShortByReference pres_handle);

    int psx_format(int raw, String format, String formula, byte[] res);

    int psx_scan(String value, String format, String formula, byte[] raw);

}
