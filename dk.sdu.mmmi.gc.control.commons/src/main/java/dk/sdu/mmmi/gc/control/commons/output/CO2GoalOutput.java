package dk.sdu.mmmi.gc.control.commons.output;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.BinaryOutput;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.bitSetToDouble;
import static dk.sdu.mmmi.controleum.api.moea.utils.BitSetUtil.doubleToBitSet;
import dk.sdu.mmmi.controleum.control.commons.AbstractOutput;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import static java.lang.Math.abs;
import java.util.BitSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mrj & jcs
 */
public class CO2GoalOutput extends AbstractOutput<PPM> implements BinaryOutput<PPM> {

    private static final int DELTA = 50;
    private static final int MAX = 2000;
    private static final int MIN = 350;
    private int bitSize = 32;

    public CO2GoalOutput(ControlDomain g) {
        super("CO2 setpoint", g);
    }

    @Override
    public PPM getRandomValue(Date t) {        
        return new PPM(350.0 + R.nextInt(650));
    }

    @Override
    public PPM getMutatedValue(PPM v) {
        double mutated;

        // mutate in interval [-50,50]
        if (R.nextBoolean()) {
            mutated = abs(v.value() + R.nextInt(DELTA));
        } else {
            mutated = abs(v.value() - R.nextInt(DELTA));
        }

        // Border case. CO2 level has to be in interval [300,2000]
        if (MAX < mutated) {
            mutated = MAX;
        } else if (mutated < MIN) {
            mutated = MIN;
        }

        return new PPM(mutated);
    }

    @Override
    public void doCommitValue(Date t, PPM result) {

        // HAL if enabled.
        if (isEnabled()) {

            HALConnector hal = HAL.get(domain);
            if (hal != null) {
                try {
                    hal.writeCO2Threshold(result);
                } catch (HALException ex) {
                    Logger.getLogger(CO2GoalOutput.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        // DB.
        ClimateDataAccess db = context(domain).one(ClimateDataAccess.class);
        db.insertCO2Threshold(new Sample<>(t, result));

    }

    @Override
    public BitSet asBitSet() {
        Double val = getValue().value();
        BitSet bs = new BitSet();
        doubleToBitSet(bs, 0, bitSize, MIN, MAX, val);
        return bs;
    }

    @Override
    public void setBitSet(BitSet bitset) {
        setValue(new PPM(bitSetToDouble(bitset, 0, bitSize, MIN, MAX)));
    }

    @Override
    public int bitSize() {
        return bitSize;
    }
}
