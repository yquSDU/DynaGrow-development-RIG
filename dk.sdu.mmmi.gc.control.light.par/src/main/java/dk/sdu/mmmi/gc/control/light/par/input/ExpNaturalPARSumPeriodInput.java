package dk.sdu.mmmi.gc.control.light.par.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.calcPARSum;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.convertToIndoorPAR;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public class ExpNaturalPARSumPeriodInput extends AbstractInput<MolSqrMeter> {

    private final ControlDomain g;

    public ExpNaturalPARSumPeriodInput(ControlDomain greenhouse) {
        super("Exp. natural light PAR sum (remaining window)");
        this.g = greenhouse;
    }

    @Override
    public void doUpdateValue(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        // Get Outdoor light forecast input
        OutdoorLightForecast remainingDayForecast = context(g).one(OutdoorLightForecastInput.class).getValue();

        // Convert to indoor PAR light
        Percent greenTrans = db.selectLightTransmissionFactor(t).getSample();
        Duration interval = context(g).one(ControlManager.class).getDelay();
        List<Sample<UMolSqrtMeterSecond>> indoorPARLevels = convertToIndoorPAR(remainingDayForecast, greenTrans);

        // Calc. PAR sum
        MolSqrMeter mol = calcPARSum(indoorPARLevels, interval);
        setValue(mol);
        store(t, mol);

    }

    private void store(Date t, MolSqrMeter mol) {

        // Store prices in internal db
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            db.insertExpectedNaturalPARSumPeriod(new Sample<>(t, mol));
        }
    }
}
