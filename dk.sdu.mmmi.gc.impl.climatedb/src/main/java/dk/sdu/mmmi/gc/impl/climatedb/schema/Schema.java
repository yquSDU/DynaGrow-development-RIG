package dk.sdu.mmmi.gc.impl.climatedb.schema;

import java.sql.Connection;

/**
 * @author mrj
 */
public class Schema {

    public static SchemaBuilder createSchemaBuilder(Connection con) {
        SchemaBuilder sb = new SchemaBuilder(con);

        /*
         * To maintain backwards compatibilty. - Only add new versions. - Make
         * sure to use increasing version numbers.
         */
        sb.addVersion(1,
                "CREATE TABLE context ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "name VARCHAR(128))");

        sb.addVersion(2,
                "CREATE TABLE air_temperature ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(3,
                "CREATE TABLE co2 ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(4,
                "CREATE TABLE light_intensity ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "umol DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(5,
                "CREATE TABLE heating_threshold ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(6,
                "CREATE TABLE ventilation_threshold ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(7,
                "CREATE TABLE co2_threshold ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(8,
                "CREATE TABLE light_status ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "switch SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(9,
                "ALTER TABLE context "
                + "ADD COLUMN deleted SMALLINT DEFAULT 0");

        sb.addVersion(10,
                "CREATE TABLE general_screen_position ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(11,
                "CREATE TABLE daily_avg_temp_goal ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(12,
                "CREATE TABLE daily_avg_temp ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(13,
                "CREATE TABLE decreasing_vent_temp_per_min ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(14,
                "CREATE TABLE light_sum_hours_goal ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "hours DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(15,
                "CREATE TABLE humidity ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(16,
                "CREATE TABLE max_humidity ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(17,
                "CREATE TABLE max_air_temperature ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(18,
                "CREATE TABLE min_air_temperature ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(19,
                "CREATE TABLE outdoor_light ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "wattm2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(20,
                "CREATE TABLE max_outdoor_light ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "wattm2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(21,
                "CREATE TABLE outdoor_temperature ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(22,
                "CREATE TABLE superlink_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "hostname VARCHAR(128), "
                + "domain VARCHAR(128), "
                + "username VARCHAR(128), "
                + "password VARCHAR(128), "
                + "department VARCHAR(128), "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(23,
                "CREATE TABLE control_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "delay_ms BIGINT, "
                + "is_running SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(24,
                "CREATE TABLE concern_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "concern_id VARCHAR(128), "
                + "is_enabled SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(25,
                "CREATE TABLE photo_opt ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(26,
                "CREATE TABLE output_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "output_id VARCHAR(128), "
                + "is_enabled SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(27,
                "CREATE TABLE chart_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "chart_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(28,
                "CREATE TABLE el_forecast ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "area VARCHAR(128), "
                + "currency VARCHAR(128), "
                + "forecast_csv CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(29,
                "CREATE TABLE outdoor_light_forecast ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "forecast_csv CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(30,
                "CREATE TABLE lamp_load ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "watt DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(31,
                "CREATE TABLE expected_natural_par_sum_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(32,
                "CREATE TABLE expected_natural_photo_sum_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(33,
                "CREATE TABLE lamp_intensity ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "umol_m2_s DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(34,
                "CREATE TABLE light_time_achieved_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ms BIGINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(35,
                "CREATE TABLE light_transmission_factor ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(36,
                "CREATE TABLE par_sum_achieved_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(37,
                "CREATE TABLE par_sum_day_goal ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(38,
                "CREATE TABLE photo_sum_achieved_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(39,
                "CREATE TABLE photo_sum_day_goal ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(40,
                "CREATE TABLE screen_transmission_factor ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(41,
                "CREATE TABLE fixed_day_light_plan ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "light_plan CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(42,
                "CREATE TABLE negotiation_statistics ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "generations INT, "
                + "duration_ms BIGINT, "
                + "solution_found SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(43,
                "CREATE TABLE concern_negotiation_result ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "result CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(44, "ALTER TABLE concern_configuration ADD COLUMN priority INT");

        sb.addVersion(45,
                "CREATE TABLE elprice_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "elprice_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(46,
                "CREATE TABLE light_forecast_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "light_forecast_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(47,
                "CREATE TABLE combined_el_prices ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "euro DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(48,
                "CREATE TABLE combined_light_forecast ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "wattm2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(49, "DROP TABLE lamp_load");

        sb.addVersion(50,
                "CREATE TABLE lamp_effect ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "wattm2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(51,
                "CREATE TABLE greenhouse_size ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "sqrmeter DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(52,
                "CREATE TABLE screen_closing_pct ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "pct DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(53,
                "CREATE TABLE screen_photo_loss_ratio ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT, "
                + "time TIMESTAMP, "
                + "pct DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(54, "DROP TABLE concern_negotiation_result");

        sb.addVersion(55,
                "CREATE TABLE concern_negotiation_result ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT, "
                + "concern_id VARCHAR(128), "
                + "time TIMESTAMP, "
                + "fitness DOUBLE, "
                + "accepted SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(56,
                "CREATE TABLE energy_balance ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "balance DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(57,
                "CREATE TABLE actual_photo ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2_s DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(58,
                "CREATE TABLE optimal_photo ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2_s DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(59,
                "CREATE TABLE preferred_day_temp ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(60,
                "CREATE TABLE preferred_night_temp ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(61,
                "CREATE TABLE preferred_day_co2 ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(62,
                "CREATE TABLE preferred_night_co2 ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(63,
                "CREATE TABLE preferred_day_screen_position ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(64,
                "CREATE TABLE preferred_night_screen_position ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(65,
                "CREATE TABLE preferred_day_ventilation_sp ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(66,
                "CREATE TABLE preferred_night_ventilation_sp ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "celcius DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(67, "ALTER TABLE concern_negotiation_result ADD COLUMN value_help CLOB");

        sb.addVersion(68,
                "DROP TABLE control_configuration");

        sb.addVersion(69,
                "CREATE TABLE control_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "delay_ms BIGINT, "
                + "is_controlling SMALLINT, "
                + "is_simulating SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(70, "ALTER TABLE control_configuration ADD COLUMN is_sim_csv_export SMALLINT");

        sb.addVersion(71,
                "CREATE TABLE windows_opening ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "factor DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(72, "ALTER TABLE superlink_configuration ADD COLUMN light_groups VARCHAR(128)");

        sb.addVersion(73,
                "CREATE TABLE proposed_light_plan ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "light_plan CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(74,
                "DROP TABLE control_configuration");

        sb.addVersion(75,
                "CREATE TABLE control_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(76,
                "CREATE TABLE expected_combined_photo_sum_today ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(77,
                "CREATE TABLE photo_balance_yesterday ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(78,
                "CREATE TABLE photo_sum_achieved_delta ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(79,
                "CREATE TABLE photo_sum_delta_goal ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(80,
                "CREATE TABLE par_balance_yesterday ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(81,
                "CREATE TABLE light_plan_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "light_plan_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(82, "ALTER TABLE concern_negotiation_result DROP COLUMN accepted");

        sb.addVersion(83, "ALTER TABLE negotiation_statistics DROP COLUMN solution_found");

        sb.addVersion(84,
                "CREATE TABLE par_sum_achieved_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "par_sum_achieved_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(85,
                "CREATE TABLE par_sum_achieved_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(86,
                "CREATE TABLE expected_natural_par_sum_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(87, "ALTER TABLE light_plan_configuration ADD COLUMN time TIMESTAMP");

        sb.addVersion(88,
                "CREATE TABLE photo_sum_achieved_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "photo_sum_achieved_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(89,
                "CREATE TABLE expected_natural_photo_sum_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(90,
                "CREATE TABLE expected_combined_photo_sum_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(91,
                "CREATE TABLE photo_sum_achieved_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "mmol_m2 DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(92,
                "CREATE TABLE min_light_art_light_intensity ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "umol_m2_s DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(93,
                "CREATE TABLE indoor_light_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "indoor_light_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(94,
                "CREATE TABLE el_spotprice_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "el_spotprice_config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(95,
                "CREATE TABLE combined_el_spotprices ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "price DOUBLE, "
                + "currency VARCHAR(128), "
                + "area VARCHAR(128), "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(96, "ALTER TABLE combined_el_prices ADD COLUMN currency VARCHAR(128)");

        sb.addVersion(97, "ALTER TABLE combined_el_prices ADD COLUMN area VARCHAR(128)");

        sb.addVersion(98, "RENAME COLUMN combined_el_prices.euro TO price");

        sb.addVersion(99,
                "CREATE TABLE hybrid_el_prices ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "price DOUBLE, "
                + "currency VARCHAR(128), "
                + "area VARCHAR(128), "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(100, "DROP TABLE el_forecast");

        sb.addVersion(101, "DROP TABLE outdoor_light_forecast");

        sb.addVersion(102, "DROP TABLE expected_combined_photo_sum_today");

        sb.addVersion(103,
                "CREATE TABLE max_co2 ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(104,
                "CREATE TABLE min_co2 ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ppm DOUBLE, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(105,
                "CREATE TABLE light_time_achieved_period ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "ms BIGINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(106,
                "CREATE TABLE frame_start ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "timest TIMESTAMP, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(107,
                "CREATE TABLE frame_end ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "timest TIMESTAMP, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(108,
                "CREATE TABLE priva_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "pcu VARCHAR(128), "
                + "indoorlight_scad VARCHAR(128), "
                + "co2_scad VARCHAR(128), "
                + "heating_scad VARCHAR(128), "
                + "humidity_scad VARCHAR(128), "
                + "light_groups VARCHAR(1024), "
                + "indoortemp_scad VARCHAR(128), "
                + "outdoorlight_scad VARCHAR(128), "
                + "outdoortemp_scad VARCHAR(128), "
                + "vent_scad VARCHAR(128), "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(109, "ALTER TABLE superlink_configuration ADD COLUMN indoorlight_code VARCHAR(128)");

        //sb.addVersion(x, "ALTER TABLE context ADD COLUMN name VARCHAR(128)");
        return sb;
    }
}
