package dk.sdu.mmmi.gc.impl.climatedb;

import com.decouplink.Link;
import dk.sdu.mmmi.controleum.api.db.DatabaseDataAccess;
import dk.sdu.mmmi.controleum.api.db.DatabaseProvider;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author mrj
 */
@ServiceProvider(service = DatabaseProvider.class)
public class DerbyDatabaseProvider implements DatabaseProvider {

    @Override
    public Link<DatabaseDataAccess> getDataAccess() {
        DerbyLink l = new DerbyLink();
        try {

            l.init();
        } catch (SQLException ex) {
            Logger.getLogger(DerbyDatabaseProvider.class.getName()).log(Level.SEVERE, "Could not initialize derby data access.", ex);
        }
        return l;
    }

    private static class DerbyLink implements Link<DatabaseDataAccess> {

        private DerbyDatabaseDataAccess da = null;

        public void init() throws SQLException {
            da = new DerbyDatabaseDataAccess();
            da.init();
        }

        @Override
        public void dispose() {
            da.dispose();
        }

        @Override
        public DatabaseDataAccess getDestination() {
            return da;
        }
    }
}
