package dk.sdu.mmmi.gc.commons.utils;

import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.api.data.TimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author jcs
 */
public class TestHelper {

    private final static SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

    /*
     * Helpers
     */
    public static Sample<Celcius> tempSample(String timest, double temp) throws ParseException {
        return new Sample<>(df.parse(timest), new Celcius(temp));
    }

    public static Sample<PPM> co2Sample(String timest, double co2) throws ParseException {
        return new Sample<>(df.parse(timest), new PPM(co2));
    }

    public static Sample<UMolSqrtMeterSecond> lightSample(String timest, double lightLevel) throws ParseException {
        return new Sample<>(df.parse(timest), new UMolSqrtMeterSecond(lightLevel));
    }

    public static TimeValue timeValue(String timest, double value) throws ParseException {
        return new DoubleTimeValue(df.parse(timest), value);
    }
}
