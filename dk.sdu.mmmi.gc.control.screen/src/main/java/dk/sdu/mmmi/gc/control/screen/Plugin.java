package dk.sdu.mmmi.gc.control.screen;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.control.screen.concern.AvoidHighHumidityConcern;
import dk.sdu.mmmi.gc.control.screen.concern.IsolationConcern;
import dk.sdu.mmmi.gc.control.screen.concern.PhotoOptimalScreenConcern;
import dk.sdu.mmmi.gc.control.screen.concern.ShadingConcern;
import dk.sdu.mmmi.gc.control.screen.input.EnergyBalanceInput;
import dk.sdu.mmmi.gc.control.screen.input.IndoorHumidityMaximumInput;
import dk.sdu.mmmi.gc.control.screen.input.OutdoorLightMaximumInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenClosingInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenPhotoLossRatioInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenTransmissionFactorInput;
import dk.sdu.mmmi.gc.control.screen.series.EnergyBalanceSeries;
import dk.sdu.mmmi.gc.control.screen.series.IndoorHumidityMaximumSeries;
import dk.sdu.mmmi.gc.control.screen.series.OutdoorLightMaximumSeries;
import dk.sdu.mmmi.gc.control.screen.series.ScreenTransmissionFactorSeries;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Concerns.
        createIsolationConcern(g, d);
        createShadingConcern(g, d);
        createPhotoOptimalScreenConcern(g, d);
        createAvoidHighHumidityConcern(g, d);

        // Inputs
        createScreenPhotoLossRatioInput(d, g);
        createScreenClosingInput(d, g);
        createScreenTransmissionFactorInput(d, g);
        createOutdoorLightMaximumInput(d, g);
        createIndoorHumidityMaximumInput(d, g);
        createEnergyBalanceInput(d, g);

        // TODO: Add ScreenClosingPctInput series
        return d;
    }

    private void createEnergyBalanceInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new EnergyBalanceInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new EnergyBalanceSeries(g)));
    }

    private void createIndoorHumidityMaximumInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new IndoorHumidityMaximumInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new IndoorHumidityMaximumSeries(g)));
    }

    private void createOutdoorLightMaximumInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new OutdoorLightMaximumInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new OutdoorLightMaximumSeries(g)));
    }

    private void createScreenTransmissionFactorInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ScreenTransmissionFactorInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ScreenTransmissionFactorSeries(g)));
    }

    private void createScreenClosingInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ScreenClosingInput(g)));
    }

    private void createScreenPhotoLossRatioInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ScreenPhotoLossRatioInput(g)));
    }

    private void createAvoidHighHumidityConcern(ControlDomain g, DisposableList d) {
        AvoidHighHumidityConcern avoidHighHumidityConcern = new AvoidHighHumidityConcern(g);
        cm.loadConcernConfig(g, avoidHighHumidityConcern);
        d.add(context(g).add(Concern.class, avoidHighHumidityConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, avoidHighHumidityConcern)));
    }

    private void createPhotoOptimalScreenConcern(ControlDomain g, DisposableList d) {
        PhotoOptimalScreenConcern photoOptimalScreenConcern = new PhotoOptimalScreenConcern(g);
        cm.loadConcernConfig(g, photoOptimalScreenConcern);
        d.add(context(g).add(Concern.class, photoOptimalScreenConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, photoOptimalScreenConcern)));
    }

    private ShadingConcern createShadingConcern(ControlDomain g, DisposableList d) {
        ShadingConcern shadingConcern = new ShadingConcern(g);
        cm.loadConcernConfig(g, shadingConcern);
        d.add(context(g).add(Concern.class, shadingConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, shadingConcern)));
        return shadingConcern;
    }

    private void createIsolationConcern(ControlDomain g, DisposableList d) {
        IsolationConcern isolationConcern = new IsolationConcern(g);
        cm.loadConcernConfig(g, isolationConcern);
        d.add(context(g).add(Concern.class, isolationConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, isolationConcern)));
    }
}
