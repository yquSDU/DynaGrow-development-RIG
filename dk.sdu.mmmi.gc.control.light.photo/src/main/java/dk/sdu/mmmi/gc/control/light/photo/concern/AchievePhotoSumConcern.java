package dk.sdu.mmmi.gc.control.light.photo.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumDayGoalInput;
import dk.sdu.mmmi.gc.control.light.photo.utils.AdvLightUtil;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj & jcs
 */
public class AchievePhotoSumConcern extends AbstractConcern {

    private MMolSqrMeter goalToDay;
    private MMolSqrMeter achievedPhotoSum;
    private MMolSqrMeter expCombinedToday;
    private MMolSqrMeter balance;

    public AchievePhotoSumConcern(ControlDomain g) {
        super("Achieve positive photo. sum", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {
        // Inputs
        // Goal window = DPI * (2 day past + today + 2 days future)
        goalToDay = s.getValue(PhotoSumDayGoalInput.class).multiply(3);
        achievedPhotoSum = s.getValue(PhotoSumAchievedPeriodInput.class);

        // Photo sum exspected from ligth plan and natural light
        LightPlan lightPlan = s.getValue(LightPlanTodayOutput.class);
        // TODO: Use photo. gain but not combined photo. sum. Ask Mærsk ;-)
        expCombinedToday = AdvLightUtil.calcExpectedCombinedPhotoSum(g, lightPlan);

        balance = achievedPhotoSum.add(expCombinedToday).subtract(goalToDay);
        return 0 <= balance.value() ? 0 : 1;
    }

//    @Override
//    public boolean accept(Solution s) {
//        // Positiv balance
//        balance = calculateBalance(s);
//        return 0.0d <= balance.value();
//    }
    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class,
                String.format(" Achieved(%.1f) + Expected(%.1f) +"
                        + " - Goal(%.1f) = Balance(%.1f)",
                        achievedPhotoSum.value(), expCombinedToday.value(),
                        goalToDay.value(), balance.value()));
        return r;
    }

}
