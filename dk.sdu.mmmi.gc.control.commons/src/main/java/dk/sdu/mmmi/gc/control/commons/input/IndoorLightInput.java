package dk.sdu.mmmi.gc.control.commons.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.control.commons.gui.ConfigureIndoorLightAction;
import dk.sdu.mmmi.gc.impl.entities.config.IndoorLightConfig;
import java.util.Date;
import javax.swing.Action;
import org.openide.util.Exceptions;

/**
 *
 * @author mrj and jcs
 */
public class IndoorLightInput extends AbstractInput<UMolSqrtMeterSecond> {

    private final ControlDomain g;
    private ClimateDataAccess db;
    private final Link<Action> action;

    public IndoorLightInput(ControlDomain g) {
        super("Indoor light intensity");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureIndoorLightAction(g));
    }

    @Override
    public void doUpdateValue(Date t) {
        try {
            HALConnector hal = HAL.get(g);

            IndoorLightConfig cfg = context(g).one(IndoorLightConfig.class);

            UMolSqrtMeterSecond v;

            if (cfg.isCalculated()) {
                v = new UMolSqrtMeterSecond(hal.readOutdoorLightIntensity().value() * cfg.getLightConversionFactor());
            } else {
                v = hal.readLightIntensity();
            }

            // Add supplementary light?
            // TODO: Read is supported for only one light group
            db = context(g).one(ClimateDataAccess.class);
            String lightGroup = hal.readLightGroups().get(0);

            if (cfg.isCalculated() && hal.readArtficialLightStatus(lightGroup).isOn()) {

                Sample<UMolSqrtMeterSecond> lamp = db.selectLampIntensity(t);
                UMolSqrtMeterSecond lampIntensity = lamp.getSample();
                if (lampIntensity != null) {
                    v = v.add(lampIntensity);
                }
            }
            setValue(v);
            store(t, v);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private void store(Date t, UMolSqrtMeterSecond result) {
        db.insertLightIntensity(new Sample<>(t, result));
    }
}
