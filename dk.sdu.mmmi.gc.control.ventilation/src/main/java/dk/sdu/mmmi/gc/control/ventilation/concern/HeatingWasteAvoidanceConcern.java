package dk.sdu.mmmi.gc.control.ventilation.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class HeatingWasteAvoidanceConcern extends AbstractConcern {

    public HeatingWasteAvoidanceConcern(ControlDomain g) {
        super("Avoid wasteful heating", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {
        double h = s.getValue(HeatingSetpointOutput.class).value();
        double v = s.getValue(VentilationTempOutput.class).value();

        double margin = 2.0;
        return (h < v - margin) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        double h = s.getValue(HeatingSetpointOutput.class).value();
        double v = s.getValue(VentilationTempOutput.class).value();
        r.put(HeatingSetpointOutput.class, String.format("below %.1f", v));
        r.put(VentilationTempOutput.class, String.format("above %.1f", h));
        return r;
    }
}
