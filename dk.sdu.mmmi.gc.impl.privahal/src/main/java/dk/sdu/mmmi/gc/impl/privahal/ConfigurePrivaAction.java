package dk.sdu.mmmi.gc.impl.privahal;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.PrivaConfig;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author ancla
 */
public class ConfigurePrivaAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain cd;

    public ConfigurePrivaAction(ControlDomain cd) {
        super("Configure Priva");
        this.cd = cd;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return cd != null
                && context(cd).one(PrivaConnectorImpl.class) != null
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling()
                && !getControlManager().isAutoRunning();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // Panel.
        PrivaConfigPanel pnl = new PrivaConfigPanel();

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(
                pnl,
                "Configure Priva");

        // Set defaults.
        ClimateDataAccess db = context(cd).one(ClimateDataAccess.class);
        pnl.set(db.getPrivaConfiguration());

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            db.setPrivaConfiguration(pnl.get());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private ControlManager getControlManager() {
        return context(cd).one(ControlManager.class);
    }

    /**
     * Configuration panel.
     */
    private static class PrivaConfigPanel extends JPanel {

        private JTextField pcuName = new JTextField(),
                lightGroups = new JTextField(),
                indoorLight = new JTextField();

        public PrivaConfigPanel() {
            setLayout(new GridLayout(6, 2, 2, 2));
            add(new JLabel("PCU:"));
            add(pcuName);
            add(new JLabel("Light groups:"));
            add(lightGroups);
            add(new JLabel("Indoor light:"));
            add(indoorLight);

        }

        public void set(PrivaConfig cfg) {
            pcuName.setText(cfg.getPcuName());
            lightGroups.setText(cfg.getLightGroupsAsString());
            indoorLight.setText(cfg.getIndoor_light_intensity_scad_id());
        }

        public PrivaConfig get() {
            PrivaConfig r = new PrivaConfig();
            r.setPcuName(pcuName.getText());
            r.setLightGroups(lightGroups.getText());
            r.setIndoor_light_intensity_scad_id(indoorLight.getText());

            return r;
        }
    }
}
