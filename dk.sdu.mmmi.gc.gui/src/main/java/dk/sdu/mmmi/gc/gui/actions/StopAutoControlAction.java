package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class StopAutoControlAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public StopAutoControlAction(ControlDomain g) {
        super("Stop automated control");
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return getControlManager().isAutoRunning();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        getControlManager().stopAutoControl();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/control_stop.png", false));
        return mi;
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }
}
