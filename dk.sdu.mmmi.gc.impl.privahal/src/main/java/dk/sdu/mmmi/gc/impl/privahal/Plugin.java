package dk.sdu.mmmi.gc.impl.privahal;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import javax.swing.Action;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author ancla
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain cd) {
        DisposableList d = new DisposableList();
        PrivaConnectorImpl s = new PrivaConnectorImpl(cd);
        d.add(context(cd).add(Action.class, new ConfigurePrivaAction(cd)));
        d.add(context(cd).add(HALConnector.class, s));
        return d;
    }
}
