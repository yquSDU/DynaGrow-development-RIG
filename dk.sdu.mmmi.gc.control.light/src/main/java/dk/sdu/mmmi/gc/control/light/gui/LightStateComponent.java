package dk.sdu.mmmi.gc.control.light.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author jcs
 */
public class LightStateComponent extends JPanel {

    private State state = State.NEUTRAL;
    private final String ID;
    JLabel text = new JLabel();

    public LightStateComponent(String id) {
        setLayout(new BorderLayout());
        this.ID = id;
        setOpaque(true);
        setPreferredSize(new Dimension(40, 40));
        addMouseListener(l);
        text.setText(id);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        add(text, BorderLayout.CENTER);
        updateGUI();
    }

    private void updateGUI() {
        setBackground(state.getColor());
        invalidate();
        repaint();
    }

    private void click() {
        if (state == State.NEUTRAL) {
            state = State.ON;
        } else if (state == State.ON) {
            state = State.OFF;
        } else if (state == State.OFF) {
            state = State.NEUTRAL;
        }
        updateGUI();
    }

    public void setState(State s) {
        state = s;
        updateGUI();
    }

    public State getState() {
        return this.state;
    }

    public String getID() {
        return this.ID;
    }
    private MouseListener l = new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent me) {
            click();
        }
    };

    public static enum State {

        ON(Color.YELLOW, "On"), OFF(Color.LIGHT_GRAY, "Off"), NEUTRAL(Color.WHITE, "?");

        private Color color;
        private String text;

        State(Color c, String txt) {
            this.color = c;
            this.text = txt;
        }

        public Color getColor() {
            return color;
        }

        public String getText() {
            return text;
        }
    }
}
