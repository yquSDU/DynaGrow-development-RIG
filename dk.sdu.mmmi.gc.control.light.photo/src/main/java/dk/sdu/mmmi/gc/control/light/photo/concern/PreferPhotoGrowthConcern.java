package dk.sdu.mmmi.gc.control.light.photo.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.photo.utils.AdvLightUtil;
import static java.lang.Math.abs;

/**
 * Requirement: Prefer to lit light on hours where most photo. is gained by
 * artificial light.
 *
 * That is: Maximize totalPhotoSum gain based on natural light forecast and
 * proposed light plan.
 *
 * @author jcs
 */
public class PreferPhotoGrowthConcern extends AbstractConcern {

    public PreferPhotoGrowthConcern(ControlDomain g) {
        super("Prefer light for photo. growth", g, 5);
    }

    /**
     * Better fitness, the more light hours with high photo. rate.
     *
     */
    @Override
    public double evaluate(Solution s) {

        LightPlan lightPlan = s.getValue(LightPlanTodayOutput.class);
        MMolSqrMeter expCombinedToday = AdvLightUtil.calcExpectedCombinedPhotoSum(g, lightPlan);

        return 1 - abs(expCombinedToday.roundedValue());
    }
}
