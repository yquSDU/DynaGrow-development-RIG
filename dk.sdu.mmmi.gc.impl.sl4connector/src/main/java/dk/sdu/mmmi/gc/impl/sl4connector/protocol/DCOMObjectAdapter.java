package dk.sdu.mmmi.gc.impl.sl4connector.protocol;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.IJIComObject;
import org.jinterop.dcom.core.JIVariant;
import org.jinterop.dcom.impls.JIObjectFactory;
import org.jinterop.dcom.impls.automation.IJIDispatch;

/**
 * Encapsulates stuff needed to invoke operations on a DCOM object. The 
 * implementation is far from complete. Add more stuff as you need it.
 * 
 * Note that you can optionally use this as a fluent interface, for example:
 * 
 * <pre>
 * return server
 *   .invoke("projects")
 *   .invoke("item", point.getCompartment())
 *   .invoke("variables")
 *   .invoke("item", point.getCode())
 *   .getDouble("value");
 * </pre>
 * 
 * @author mrj
 */
public class DCOMObjectAdapter {

    private final IJIDispatch dispatch;

    public DCOMObjectAdapter(IJIDispatch dispatch) {
        this.dispatch = dispatch;
    }

    public DCOMObjectAdapter invoke(String method) throws JIException {        
        JIVariant v = dispatch.callMethodA(method);
        IJIComObject o = v.getObjectAsComObject();
        IJIDispatch d = (IJIDispatch)JIObjectFactory.narrowObject(o.queryInterface(IJIDispatch.IID));
        return new DCOMObjectAdapter(d);
    }

    public DCOMObjectAdapter invoke(String method, String arg) throws JIException {
        JIVariant v = dispatch.callMethodA(method, new Object[] { new JIVariant(arg) } )[0];
        IJIComObject o = v.getObjectAsComObject();
        IJIDispatch d = (IJIDispatch)JIObjectFactory.narrowObject(o.queryInterface(IJIDispatch.IID));
        return new DCOMObjectAdapter(d);
    }
    
    public DCOMObjectAdapter invoke(String method, int arg) throws JIException {        
        JIVariant v = dispatch.callMethodA(method, new Object[] { new JIVariant(arg) } )[0];
        IJIComObject o = v.getObjectAsComObject();
        IJIDispatch d = (IJIDispatch)JIObjectFactory.narrowObject(o.queryInterface(IJIDispatch.IID));
        return new DCOMObjectAdapter(d);    
    }
    
    public Object getObject(String method) throws JIException {
        JIVariant valueVar = dispatch.callMethodA(method);
        if(valueVar.getType() == JIVariant.VT_ERROR) {
            return null;
        }
        return valueVar.getObject();
    }
    
    public String getString(String method) throws JIException {
        JIVariant valueVar = dispatch.callMethodA(method);
        if(valueVar.getType() == JIVariant.VT_ERROR) {
            return null;
        }
        return valueVar.getObjectAsString2();
       
    }


    public double getDouble(String method) throws JIException {
        JIVariant variant = dispatch.callMethodA(method);
        if(variant.getType() == JIVariant.VT_ERROR) {
            return 0;
        }
        return variant.getObjectAsDouble();
    }
    
    public boolean getBoolean(String method) throws JIException {
        JIVariant variant = dispatch.callMethodA(method);
        if(variant.getType() == JIVariant.VT_ERROR) {
            return false;
        }
        return variant.getObjectAsBoolean();
    }
    
    public int getInteger(String method) throws JIException {
        JIVariant variant = dispatch.callMethodA(method);
        if(variant.getType() == JIVariant.VT_ERROR) {
            return 0;
        }
        return variant.getObjectAsInt();
    }
    
    public void setDouble(String method, double value) throws JIException {
        dispatch.callMethod(method, new Object[]{
            new JIVariant(0),
            new JIVariant(value)});
    }
    
    /**
     * Get the dispatcher in order to perform any action not supported by the
     * DCOMObjectAdapter.
     */
    public IJIDispatch getDispatcher() {
        return dispatch;
    }

}
