package dk.sdu.mmmi.gc.gui.charts;

import com.toedter.calendar.JDateChooser;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.decouplink.Utilities.context;

/**
 * @author mrj
 */
public class NavigationPanel extends JPanel {

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private static final long DAY_MS = 1000 * 60 * 60 * 24;
    private Map<Long, Period> periodMap = new TreeMap<>();
    private JDateChooser dateChooser = new JDateChooser();
    private JComboBox<Object> dateBox;
    private SelectButton selectButton;
    private JButton toDayButton;
    private JToggleButton toggleButton = new JToggleButton("Autoupdate Window");
    private final ControlDomain g;
    private final ChartConfig chart;
    private ActionListener todayButtonListener = ae -> {
        Date toDay = DateUtil.endOfDay(new Date());
        dateChooser.setDate(toDay);
        updateGraph();
    };
    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            long chosenDate = DateUtil.startOfDay(dateChooser.getDate()).getTime();
            long windowPeriod = ((Period) Objects.requireNonNull(dateBox.getSelectedItem())).getMS();

            Date today = DateUtil.startOfDay(new Date());

            if ((today.getTime() - chosenDate) > windowPeriod) {
                dateChooser.setDate(today);
                updateGraph();
            }
        }
    };
    private ActionListener autoupdateToggleButtonListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (toggleButton.isSelected()) {
                chart.setAutoUpdate(true);
                executor = Executors.newScheduledThreadPool(1);
                executor.scheduleAtFixedRate(updateTask, 1, 1, TimeUnit.MINUTES);

            } else {
                chart.setAutoUpdate(false);
                executor.shutdown();
            }

            updateGraph();
        }
    };

    //
    // Events
    //
    private PropertyChangeListener propListener = e -> {
        if ("date".equals(e.getPropertyName())) {
            updateGraph();
        }
    };
    private ActionListener dateBoxListener = ae -> updateGraph();
    //
    // Public
    //
    NavigationPanel(ControlDomain g, ChartConfig chart) {

        this.g = g;
        this.chart = chart;

        setBackground(Color.WHITE);
        setLayout(new FlowLayout());

        // DateChooser
        dateChooser.setBackground(Color.WHITE);

        Date to = DateUtil.endOfDay(chart.getToDate());
        this.dateChooser.setDate(to);

        // Datebox
        periodMap.put(DAY_MS, new Period("One day", DAY_MS));
        periodMap.put(DAY_MS * 2, new Period("Two days", DAY_MS * 2));
        periodMap.put(DAY_MS * 3, new Period("Three days", DAY_MS * 3));
        periodMap.put(DAY_MS * 7, new Period("One week", DAY_MS * 7));
        periodMap.put(DAY_MS * 7 * 3, new Period("Three weeks", DAY_MS * 7 * 3));
        periodMap.put(DAY_MS * 30, new Period("One month", DAY_MS * 30));
        periodMap.put(DAY_MS * 30 * 3, new Period("Three months", DAY_MS * 30 * 3));
        periodMap.put(DAY_MS * 30 * 6, new Period("Six months", DAY_MS * 30 * 6));
        dateBox = new JComboBox<>(periodMap.values().toArray());
        this.dateBox.setSelectedItem(periodMap.get(chart.getPeriod()));

        // Today button;
        this.toDayButton = new JButton("Today");

        // Select series button
        selectButton = new SelectButton();

        toggleButton.setSelected(chart.isAutoUpdate());

        // Layout.
        add(dateBox);
        add(dateChooser);
        add(selectButton);
        add(toDayButton);
        add(toggleButton);

        // Listeners.
        dateChooser.addPropertyChangeListener(propListener);
        dateBox.addActionListener(dateBoxListener);
        toDayButton.addActionListener(todayButtonListener);

        toggleButton.addActionListener(autoupdateToggleButtonListener);
        autoupdateToggleButtonListener.actionPerformed(null);
    }

    public void dispose() {
        periodMap.clear();
        periodMap = null;

        dateBox.removeActionListener(dateBoxListener);
        dateBox = null;
        dateBoxListener = null;

        dateChooser.removePropertyChangeListener(propListener);
        dateChooser = null;
        propListener = null;

        selectButton.removeActionListener(selectButton);
        selectButton = null;

        toDayButton.removeActionListener(todayButtonListener);
        toDayButton = null;

        toggleButton.removeActionListener(autoupdateToggleButtonListener);
        toggleButton = null;
        removeAll();
    }

    //
    // Private
    //
    private void updateGraph() {
        Period t = (Period) dateBox.getSelectedItem();
        long l = Objects.requireNonNull(t).getMS();
        chart.setPeriod(l);
        chart.setToDate(dateChooser.getDate());
        chart.setAutoUpdate(toggleButton.isSelected());

        context(g).one(ChartManager.class).update(chart);
    }

    //
    // Types
    //
    private class SelectButton extends JButton implements ActionListener {

        SelectButton() {
            super("select series");
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            // Notify listeners

            // TODO: Change Dialog so it doesnt have a cancel button
            ChartConfigPanel pnl = new ChartConfigPanel(g, chart);
            DialogDescriptor dcs = new DialogDescriptor(pnl, "Select series");

            // Set defaults.
            DialogDisplayer.getDefault().notify(dcs);
        }
    }

    private class Period {

        private String str;
        private long ms;

        Period(String str, long ms) {
            this.str = str;
            this.ms = ms;
        }

        long getMS() {
            return ms;
        }

        @Override
        public String toString() {
            return str;
        }
    }
}
