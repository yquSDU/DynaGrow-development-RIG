package dk.sdu.mmmi.gc.impl.sl4connector;

/**
 * @author mrj
 */
public interface SL4DCOMConnector {

    public void close() throws SL4DCOMException;
    
    public double readValue(String projectID, String variableID) throws SL4DCOMException;
    
    public void writeValue(String projectID, String variableID, double value) throws SL4DCOMException;
}
