package dk.sdu.mmmi.gc.gui.explorer;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import org.openide.nodes.AbstractNode;

/**
 * @author mrj
 */
public class InputsNode extends AbstractNode {

    public InputsNode(ControlDomain g) {
        super(new InputsNodeChildren(g));
        setDisplayName("Inputs");

        setIconBaseWithExtension("dk/sdu/mmmi/gc/impl/greenhouseicons/Input.png");
    }
}
