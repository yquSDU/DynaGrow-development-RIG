package dk.sdu.mmmi.gc.impl.okosimulation;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import java.util.Date;
import java.util.List;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class OKOSimulationContext implements SimulationContext {

    private static final long HOUR = 60 * 60 * 1000;
    private Date from, to;
    private ControlDomain g;
    private OKOSimulationHAL hal;
    private Link<?> dispose = null;
    private Simulation simulation = null;
    private long interval;

    public OKOSimulationContext(ControlDomain g) {
        this.g = g;
        this.hal = context(g).one(OKOSimulationHAL.class);
    }

    @Override
    public void init(Date from, Date to) {
        this.from = from;
        this.to = to;

        // Clean contents of greenhouse database.
        cleanClimateDataWhileKeepingConfiguration();

        // Setup environment simualtion.
        simulation = new Simulation(from, to, interval);

        // Init light.
        //activateSimForecast();

        //initLightForecast();

        // Init el.
        //initElPriceForecast();

    }

    @Override
    public void dispose() {

        if (simulation != null) {
            simulation.dispose();
        }

        if (dispose != null) {
            dispose.dispose();
        }
    }

    @Override
    public int size() {
        return simulation.getTotalSimulationSteps();
    }

    @Override
    public int position() {
        return simulation.getCurrentSimulationStep();
    }

    @Override
    public boolean hasNext() {
        return !simulation.isSimulationCompleted();
    }

    @Override
    public Date next() {
        Date time = simulation.getCurrentSimulationTime();

        try {
            // Get setpoints.
            Inputs i = hal.getInputs();

            // Set configuration parameters not covered by HAL.
            ClimateDataAccess db = getDB();
            if (db != null) {
                Sample<WattSqrMeter> effect = db.selectInstalledLampEffect(time);
                if (effect != null) {
                    i.setLampCapacityInst(effect.getSample().value());
                }

                Sample<Percent> trans = db.selectScreenTransmissionFactor(time);
                if (trans != null) {
                    i.setTrScreen(trans.getSample().asFactor());
                }
            }

            // Run simulation using those setpoints.
            Outputs o = simulation.control(i);

            // Set those outputs.
            hal.setOutputs(o);

        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }

        return time;
    }

    //
    // Private helper stuff.
    //
    private void initLightForecast() {
        // Fetch light forecast.
        try {
            System.out.println("LIGHT FORECAST");

            ClimateDataAccess db = getDB();
            if (db != null) {

                // Create forecast with everything in it.
                List<Sample<WattSqrMeter>> list = simulation.getLightForecast();
                OutdoorLightForecast forecast = new OutdoorLightForecast(list);

                // Insert forecast once in forecast table and once in combined table.
                if (!list.isEmpty()) {
                    db.insertOutdoorLightForecast(
                            new Sample<>(
                                    list.get(0).getTimestamp(), forecast));
                }
            }
            System.out.println("DONE");
        } catch (Exception x) {
            Exceptions.printStackTrace(x);
        }
    }

    private void initElPriceForecast() {
        System.out.println("EL FORECAST");

        try {
            ClimateDataAccess db = getDB();
            if (db != null) {

                // Create forecast with everything in it.
                List<Sample<MoneyMegaWattHour>> list = simulation.getElPriceForecast();
                ElPriceMWhForecast forecast = new ElPriceMWhForecast(list, "dkwest", "EUR");

                // Insert it once to form combined forecast.
                if (!list.isEmpty()) {
                    db.insertElPriceForecast(
                            new Sample<>(
                                    list.get(0).getTimestamp(), forecast));
                }
            }
        } catch (Exception x) {
            Exceptions.printStackTrace(x);
        }

        System.out.println("DONE");
    }

    private void activateSimForecast() {

        ClimateDataAccess db = getDB();

        LightForecastConfig cfgLight = new LightForecastConfig();
        cfgLight.setActive(false);
        db.insertLightForecastConfig(cfgLight);

        ElPriceForecastConfig cfgEl = new ElPriceForecastConfig();
        cfgEl.setActive(false);
        db.insertElPriceForecastConfig(cfgEl);
    }

    private void cleanClimateDataWhileKeepingConfiguration() {
        ClimateDataAccess db = getDB();

        Date now = new Date();
        this.interval = db.getControlConfiguration().getDelay().toMS();

        // Get configuration.
        Sample<WattSqrMeter> connectedLampLoad = db.selectInstalledLampEffect(now);
        Sample<Celcius> dailyAvgTempGoal = db.selectDailyAverageTemperatureGoal(now);
        Sample<Celcius> descVentTemp = db.selectDecreasingVentilationTemperaturePerMinute(now);
        Sample<FixedDayLightPlan> fixedPlan = db.selectFixedLightPlan(now);
        Sample<UMolSqrtMeterSecond> lampInt = db.selectLampIntensity(now);
        Sample<Duration> lightSumHourGoal = db.selectLightSumHoursGoal(now);
        Sample<Percent> lightTransmissionFactor = db.selectLightTransmissionFactor(now);
        Sample<Celcius> maxAirTemp = db.selectMaxAirTemperature(now);
        Sample<Percent> maxHumidity = db.selectMaxHumidity(now);
        Sample<WattSqrMeter> maxOutLight = db.selectMaxOutdoorLight(now);
        Sample<Celcius> minAirTemp = db.selectMinAirTemperature(now);
        Sample<MolSqrMeter> parSumDayGoal = db.selectPARSumDayGoal(now);
        Sample<MMolSqrMeter> photoSumDayGoal = db.selectPhotoSumDayGoal(now);
        Sample<Percent> screenTransFactor = db.selectScreenTransmissionFactor(now);
        Sample<Percent> screenClosingPct = db.selectScreenClosingPct(now);
        Sample<Percent> optPhotoGoal = db.selectPhotoOptimizationGoal(now);

        Sample<PPM> dayCO2 = db.selectPreferredDayCO2(now);
        Sample<PPM> nightCO2 = db.selectPreferredNightCO2(now);

        Sample<Percent> dayScreen = db.selectPreferredDayScreenPosition(now);
        Sample<Percent> nightScreen = db.selectPreferredNightScreenPosition(now);

        Sample<Celcius> dayTemp = db.selectPreferredDayTemperature(now);
        Sample<Celcius> nightTemp = db.selectPreferredNightTemperature(now);

        Sample<Celcius> dayVentTemp = db.selectPreferredDayVentilationSP(now);
        Sample<Celcius> nightVentTemp = db.selectPreferredNightVentilationSP(now);

        // Clean all climate data.
        db.cleanClimateData();

        // Set configuration.
        if (connectedLampLoad != null) {
            db.insertInstalledLampEffect(new Sample<>(from, connectedLampLoad.getSample()));
        }
        if (dailyAvgTempGoal != null) {
            db.insertDailyAverageTemperatureGoal(new Sample<>(from, dailyAvgTempGoal.getSample()));
        }
        if (descVentTemp != null) {
            db.insertDecreasingVentilationTemperaturePerMinute(new Sample<Celcius>(from, descVentTemp.getSample()));
        }
        if (fixedPlan != null) {
            db.insertFixedLightPlan(new Sample<>(from, fixedPlan.getSample()));
        }
        if (lampInt != null) {
            db.insertLampIntensity(new Sample<>(from, lampInt.getSample()));
        }
        if (lightSumHourGoal != null) {
            db.insertLightSumHoursGoal(new Sample<>(from, lightSumHourGoal.getSample()));
        }
        if (lightTransmissionFactor != null) {
            db.insertLightTransmissionFactor(new Sample<>(from, lightTransmissionFactor.getSample()));
        }
        if (maxAirTemp != null) {
            db.insertMaxAirTemperature(new Sample<>(from, maxAirTemp.getSample()));
        }
        if (maxHumidity != null) {
            db.insertMaxHumidity(new Sample<>(from, maxHumidity.getSample()));
        }
        if (maxOutLight != null) {
            db.insertMaxOutdoorLight(new Sample<>(from, maxOutLight.getSample()));
        }
        if (minAirTemp != null) {
            db.insertMinAirTemperature(new Sample<>(from, minAirTemp.getSample()));
        }
        if (parSumDayGoal != null) {
            db.insertPARSumDayGoal(new Sample<>(from, parSumDayGoal.getSample()));
        }
        if (photoSumDayGoal != null) {
            db.insertPhotoSumDayGoal(new Sample<>(from, photoSumDayGoal.getSample()));
        }
        if (screenTransFactor != null) {
            db.insertScreenTransmissionFactor(new Sample<>(from, screenTransFactor.getSample()));
        }
        if (screenClosingPct != null) {
            db.insertScreenClosingPct(new Sample<>(from, screenClosingPct.getSample()));
        }
        if (optPhotoGoal != null) {
            db.insertPhotoOptimization(new Sample<>(from, optPhotoGoal.getSample()));
        }
        if (dayCO2 != null && nightCO2 != null) {
            db.insertPreferredDayCO2(new Sample<>(from, dayCO2.getSample()));
            db.insertPreferredNightCO2(new Sample<>(from, nightCO2.getSample()));
        }
        if (dayTemp != null && nightTemp != null) {
            db.insertPreferredDayTemperature(new Sample<>(from, dayTemp.getSample()));
            db.insertPreferredNightTemperature(new Sample<>(from, nightTemp.getSample()));
        }
        if (dayScreen != null && nightScreen != null) {
            db.insertPreferredDayScreenPosition(new Sample<>(from, dayScreen.getSample()));
            db.insertPreferredNightScreenPosition(new Sample<>(from, nightScreen.getSample()));
        }
        if (dayVentTemp != null && nightVentTemp != null) {
            db.insertPreferredDayVentilationSP(new Sample<>(from, dayVentTemp.getSample()));
            db.insertPreferredNightVentilationSP(new Sample<>(from, nightVentTemp.getSample()));
        }

    }

    private ClimateDataAccess getDB() {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        return db;
    }

    public boolean isSimulationEnvironmentInstalled() {
        return Simulation.isSimulationEnvironmentInstalled();
    }
}
