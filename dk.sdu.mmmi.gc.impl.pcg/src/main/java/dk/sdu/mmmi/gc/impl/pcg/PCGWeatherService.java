package dk.sdu.mmmi.gc.impl.pcg;

import com.csvreader.CsvReader;
import dk.sdu.mmmi.controleum.api.weather.IWeatherServiceDriver;
import dk.sdu.mmmi.controleum.api.weather.WeatherCommunicationException;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import static dk.sdu.mmmi.gc.impl.pcg.FileHelper.getLastestDataFile;
import java.io.File;
import java.io.FileReader;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = IWeatherServiceDriver.class)
public class PCGWeatherService  implements IWeatherServiceDriver{

    private SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private NumberFormat numberFormat = NumberFormat.getInstance(Locale.FRANCE);
    private final static String downloadDir = "c:\\ProgramData\\Priva\\Priva Office\\Export\\";

    @Override
    public Sample<OutdoorLightForecast> getOutdoorLightForecast(LightForecastConfig cfg, Date from, Date to) throws WeatherCommunicationException {

        List<Sample<WattSqrMeter>> forecast = new ArrayList<>();
        try {
            File file = getLastestDataFile(downloadDir, "Gebruikt verwachte weer [COMP1# 1]*.csv");

            if (file != null) {

                CsvReader weatherReader = new CsvReader(new FileReader(file), ';');
                weatherReader.readHeaders();

                while (weatherReader.readRecord()) {

                    String timeStr = weatherReader.get(0);
                    String value = weatherReader.get("Verwachte straling buiten");

                    if (timeStr != "" && value != "") {

                        Date time = SDF.parse(timeStr);
                        Double light = numberFormat.parse(value).doubleValue();
                        forecast.add(new Sample<>(time, new WattSqrMeter(light)));
                    }
                }
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }


        return new Sample<>(from, new OutdoorLightForecast(intervalResult(forecast, from, to)));
    }


    private List<Sample<WattSqrMeter>> intervalResult(List<Sample<WattSqrMeter>> forecast, Date from, Date to) {
        List<Sample<WattSqrMeter>> result = new ArrayList<>();
        for (Sample<WattSqrMeter> sample : forecast) {
            if(from.getTime() <= sample.getTimestamp().getTime()
                    && sample.getTimestamp().getTime() <= to.getTime()){
                result.add(sample);
            }
        }
        return result;
    }

}
