package dk.sdu.mmmi.gc.hal.priva.entities;

/**
 * Entiy holding value of SCAD from Priva computer. TODO Ensure that exception
 * is thrown, if trying to get values associated with attribute or toggle if it
 * does not have attribute or is toggle. This is to ensure that we don't get
 * null-pointer exception when using these entities.
 *
 * @author ancla
 */
public class PrivaEntity {

    private final boolean isNumeric;

    private final boolean isToggle;
    private final PrivaListValue currentToggleValue;
    private final PrivaValueList possibleToggleValues;

    private final boolean hasAttribute;
    private final PrivaListValue currentAttribute;
    private final PrivaValueList possibleAttributes;

    private final Integer rawValue;
    private final String formattedValue;

    private final String scad;

    public PrivaEntity(String scad, Integer rawValue, String formattedValue, boolean isNumeric, boolean isToggle, PrivaListValue currentToggleValue, PrivaValueList possibleToggleValues, boolean hasAttribute, PrivaListValue currentAttribute, PrivaValueList possibleAttributes) {
        this.scad = scad;
        this.rawValue = rawValue;
        this.formattedValue = formattedValue;
        this.hasAttribute = hasAttribute;
        this.currentAttribute = currentAttribute;
        this.possibleAttributes = possibleAttributes;
        this.isNumeric = isNumeric;
        this.isToggle = isToggle;
        this.currentToggleValue = currentToggleValue;
        this.possibleToggleValues = possibleToggleValues;
    }

    @Override
    public String toString() {
        synchronized (this) {
            return getScad() + "\tRaw value:[" + getRawValue() + "]\tFormatted value:[" + getFormattedValue() + "]\tAttribute:[" + getAttrib() + "]";
        }
    }

    /**
     * @return the raw value of the SCAD, if possible.
     */
    public Integer getRawValue() {
        return rawValue;
    }

    /**
     * @return the formatted value of the SCAD.
     */
    public String getFormattedValue() {
        return formattedValue;
    }

    /**
     * @return
     */
    public PrivaListValue getAttrib() {
        return getCurrentAttribute();
    }

    /**
     * @return the name of the SCAD
     */
    public String getScad() {
        return scad;
    }

    /**
     * @return the isToggle
     */
    public boolean isToggle() {
        return isToggle;
    }

    /**
     * @return the currentToggleValue
     */
    public PrivaListValue getCurrentToggleValue() {
        return currentToggleValue;
    }

    /**
     * @return the possibleToggleValues
     */
    public PrivaValueList getPossibleToggleValues() {
        return possibleToggleValues;
    }

    /**
     * @return the hasAttribute
     */
    public boolean hasAttribute() {
        return hasAttribute;
    }

    /**
     * @return the currentAttribute
     */
    public PrivaListValue getCurrentAttribute() {
        return currentAttribute;
    }

    /**
     * @return the possibleAttributes
     */
    public PrivaValueList getPossibleAttributes() {
        return possibleAttributes;
    }

}
