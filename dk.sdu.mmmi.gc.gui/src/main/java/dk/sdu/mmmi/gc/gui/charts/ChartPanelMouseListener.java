package dk.sdu.mmmi.gc.gui.charts;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Element;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.impl.entities.results.ConcernNegotiationResult;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CombinedDomainXYPlot;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.decouplink.Utilities.context;

/**
 * @author jcs
 */
public class ChartPanelMouseListener implements ChartMouseListener {

    private final Map<Date, String> tooltips;
    private final ChartPanel chartPanel;
    private final ControlDomain g;
    //
    // Events
    //
    private final Comparator<Date> DESCENDING_DATE_COMPERATOR = (t0, t1) -> {
        int r = 0;
        if (t0.after(t1)) {
            r = -1;
        } else if (t0.before(t1)) {
            r = 1;
        }
        return r;
    };

    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
    }

    ChartPanelMouseListener(ControlDomain g, ChartPanel chartPanel, Date from, Date to) {
        this.g = g;
        this.chartPanel = chartPanel;
        this.tooltips = generateTooltipMap(from, to);
    }

    private String findTooltip(Date date, JFreeChart chart) {

        String tooltip = date.toString();

        for (Date tooltipDate : tooltips.keySet()) {
            if (tooltipDate.before(date) || tooltipDate.equals(date)) {
                tooltip = tooltips.get(tooltipDate);
                break;
            }
        }
        return tooltip;
    }

    private Date getDate(ChartMouseEvent event) {

        Point2D p = event.getTrigger().getPoint();
        Rectangle2D plotArea = chartPanel.getScreenDataArea();
        CombinedDomainXYPlot plot = (CombinedDomainXYPlot) event.getChart().getXYPlot();
        long dateLong = (long) plot.getDomainAxis().java2DToValue(p.getX(), plotArea, plot.getDomainAxisEdge());

        return new Date(dateLong);
    }

    @Override
    public synchronized void chartMouseMoved(ChartMouseEvent event) {
        JFreeChart chart = event.getChart();
        Date date = getDate(event);
        String tooltip = findTooltip(date, chart);
        event.getEntity().setToolTipText(tooltip);
    }

    private Map<Date, String> generateTooltipMap(Date from, Date to) {

        Map<Date, String> tooltipMultiMap = new TreeMap<>(DESCENDING_DATE_COMPERATOR);

        ContextDataAccess db = context(g).one(ContextDataAccess.class);

        Map<Date, List<ConcernNegotiationResult>> concernResultMap = db.selectConcernsNegotiationResult(from, to);

        for (Date date : concernResultMap.keySet()) {
            tooltipMultiMap.put(date, generateTooltip(date, concernResultMap.get(date)));
        }

        return tooltipMultiMap;
    }

    private String generateTooltip(Date date, List<ConcernNegotiationResult> concernResultList) {

        // Create output tooltip map for each concern
        Map<String, List<String>> toolTipMap = new TreeMap<>();

        for (ConcernNegotiationResult c : concernResultList) {

            Map<String, String> concernResult = c.getValueHelp();

            for (String outputID : concernResult.keySet()) {

                String help = concernResult.get(outputID) + " <i>[" + getConcernName(c.getConcernID()) + "]</i>";
                if (toolTipMap.containsKey(outputID)) {
                    toolTipMap.get(outputID).add(help);
                } else {
                    List<String> l = new ArrayList<>();
                    l.add(help);
                    toolTipMap.put(outputID, l);
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        SimpleDateFormat fm = new SimpleDateFormat("HH:mm");

        sb.append("<html>");
        sb.append("<head>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<b>");
        sb.append("Time=");
        sb.append("</b>");
        sb.append(fm.format(date));
        sb.append("<br>");

        // Create the html based on tooltip map
        for (String outputID : toolTipMap.keySet()) {
            String outputName = getOutputName(outputID);
            sb.append("<b>");
            sb.append(outputName);
            sb.append("</b>");
            sb.append("<br>");
            for (String help : toolTipMap.get(outputID)) {
                sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
                sb.append(help);
                sb.append("<br>");
            }
        }
        sb.append("</body>");
        sb.append("</html>");
        return sb.toString();
    }

    private String getOutputName(String outputID) {
        String outputName = null;
        // Find the output name
        for (Output o : context(g).all(Output.class)) {
            if (o.getID().equals(outputID)) {
                outputName = o.getName();
            }
        }
        return outputName;
    }

    private String getConcernName(String concernID) {
        String concernName = null;
        // Find the output name
        for (Element o : context(g).all(Concern.class)) {
            if (o.getID().equals(concernID)) {
                concernName = o.getName();
            }
        }
        return concernName;
    }
}
