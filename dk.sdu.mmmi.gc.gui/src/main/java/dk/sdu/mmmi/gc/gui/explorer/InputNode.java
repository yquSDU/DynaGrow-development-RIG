package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Value;
import dk.sdu.mmmi.controleum.api.moea.ValueListener;
import dk.sdu.mmmi.gc.gui.help.ElementHelpAction;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;

/**
 * @author mrj
 */
public class InputNode extends BaseNode implements SearchObserver, ValueListener {

    private final Input input;
    private final Result<Concern> cR;
    private final ElementHelpAction helpAction;
    private final ArrayList<Object> actions;
    private final Link<ValueListener> valueListener;
    private final Link<SearchObserver> searchObj;

    public InputNode(ControlDomain g, Input input) {
        super(Lookups.singleton(input));

        this.input = input;
        setIconBaseWithExtension("dk/sdu/mmmi/gc/impl/greenhouseicons/Input.png");
        update();
        valueListener = context(input).add(ValueListener.class, this);

        // Listeners
        searchObj = context(g).add(SearchObserver.class, this);

        // Actions
        this.helpAction = new ElementHelpAction(input);
        actions = new ArrayList<>();
        actions.add(helpAction);
        actions.addAll(context(input).all(Action.class));

        Lookup sc = Utilities.actionsGlobalContext();
        cR = sc.lookupResult(Concern.class);
        cR.addLookupListener(selectedConcernListener);
    }

    @Override
    public void destroy() throws IOException {
        valueListener.dispose();
        searchObj.dispose();
        cR.removeLookupListener(selectedConcernListener);
    }

    @Override
    public final void onSearchCompleted(Solution s) {
        update();
    }

    @Override
    public Action[] getActions(boolean context) {
        return actions.toArray(new Action[0]);
    }

    private void update() {
        // Set name.
        Concern c = Utilities.actionsGlobalContext().lookup(Concern.class);
        String help = (c != null) ? c.getValueHelpOrNull(input.getClass()) : null;

        if (help != null) {
            // Set name + optimal value.
            setDisplayName(String.format("%s (<font color='3333FF'>%s</font>)", input.toString(), help));
        } else {
            // Set name.
            setDisplayName(input.toString());
        }
    }

    @Override
    public void onValueChanged(Value v) {
        update();
    }
    
    private final LookupListener selectedConcernListener = new LookupListener() {
        @Override
        public final void resultChanged(LookupEvent ev) {
            update();
        }
    };
}
