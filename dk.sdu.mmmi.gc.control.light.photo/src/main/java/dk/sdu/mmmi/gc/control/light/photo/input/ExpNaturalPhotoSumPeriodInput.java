package dk.sdu.mmmi.gc.control.light.photo.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.convertToIndoorPAR;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.light.input.OutdoorLightForecastInput;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public class ExpNaturalPhotoSumPeriodInput extends AbstractInput<MMolSqrMeter> {

    private final ControlDomain g;

    public ExpNaturalPhotoSumPeriodInput(ControlDomain g) {
        super("Exp. natural light photo. sum  (remaining day + 2 days)");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        // Get forecast for remaining day
        Percent greenTrans = db.selectLightTransmissionFactor(t).getSample();
        Percent photoOptPct = db.selectPhotoOptimizationGoal(t).getSample();

        // Weather forecast
        OutdoorLightForecast remainingDayForecast = context(g).one(OutdoorLightForecastInput.class).getValue();

        // Weather forecast converted to indoor light
        List<Sample<UMolSqrtMeterSecond>> indoorPARLevels = convertToIndoorPAR(remainingDayForecast, greenTrans);

        Duration ctrlDelay = context(g).one(ControlManager.class).getDelay();
        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);

        // Calc. photo. sum.
        MMolSqrMeter p = photoUtil.calcPhotoSum(indoorPARLevels, photoOptPct, ctrlDelay);
        setValue(p);
        store(t, p);
    }

    private void store(Date t, MMolSqrMeter mol) {
        // Store prices in internal db
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertExpectedNaturalPhotoSumPeriod(new Sample<>(t, mol));
    }
}
