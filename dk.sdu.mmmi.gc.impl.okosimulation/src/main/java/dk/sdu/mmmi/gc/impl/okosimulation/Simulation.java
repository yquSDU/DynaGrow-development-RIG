package dk.sdu.mmmi.gc.impl.okosimulation;

import com.mathworks.toolbox.javabuilder.MWClassID;
import com.mathworks.toolbox.javabuilder.MWNumericArray;
import com.mathworks.toolbox.javabuilder.MWStructArray;
import com.mathworks.toolbox.javabuilder.internal.MCRConfiguration;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import static dk.sdu.mmmi.gc.impl.okosimulation.Util.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * For all this to work, first install MATLAB Compiler Runtime. Find
 * installation files and documentation in
 * $svnroot/install/matlab_compiler_runtime.
 *
 * MatLab API:
 *
 * [output,input]=SimStart(delta,init,input,output,SimDuration)
 *
 * Delta=minutter tidsinterval, 5 min = 300 Init=0 (nyt), 1 (kørende)
 * output,input (struct som sendt tidligere); første gang du kalder, sendt
 * output med ønskede inputs SimDuration=Simulation time i timer!
 *
 * input.spAirHeat=18; input.spAirVent=20; input.spCO2=1000; input.spRHgh=85;
 * input.spScreenPos=0; input.LampCapacityInst=400; input.NoLampsSqm=0.15;
 * input.ArtficialLightStatus=1; input.dayno=30; %starter med Day 1
 * input.hour=10; %fra 1 to 24 output.Tair=20; output.RHair=80;
 * output.CO2air=400; SimDuration=100;
 *
 * [output,input]=SimStart(300,0,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 * [output,input]=SimStart(300,1,input,output,SimDuration)
 *
 * @author mrj
 */
public class Simulation {

    public static final int DAY_MILLIS = 1000 * 60 * 60 * 24;
    private int intervalSeconds;
    private final Date from, to;
    private Outputs lastOutputs = null;
    private Date cursor;

    public Simulation(Date from, Date to, long intervalMS) {
        this.from = from;
        this.to = to;
        this.cursor = from;
        this.intervalSeconds = (int) (intervalMS / 1000);
    }

    public void dispose() {
    }

    public int getTotalSimulationSteps() {
        long l = to.getTime() - from.getTime();
        return (int) (l / (intervalSeconds * 1000)) + 1;
    }

    public int getCurrentSimulationStep() {
        long l = cursor.getTime() - from.getTime();
        return (int) (l / (intervalSeconds * 1000));
    }

    public Date getCurrentSimulationTime() {
        return cursor;
    }

    public boolean isSimulationInProgress() {
        return !from.equals(cursor);
    }

    public boolean isSimulationCompleted() {
        return cursor.after(to) || cursor.equals(to);
    }

    private double getSimulationDurationDays() {
        double ms = to.getTime() - from.getTime();
        double days = ms / (24 * 60 * 60 * 1000);
        return Math.ceil(days);
    }

    public synchronized Outputs control(Inputs inputs) throws Exception {
        if (isSimulationCompleted()) {
            throw new IllegalStateException("Simulation completed!");
        }

        try {
            // On first run. Use desired inputs and previous outputs.
            if (!isSimulationInProgress()) {
                lastOutputs = new Outputs();
                lastOutputs.setTair(inputs.getSpAirHeat());
                lastOutputs.setCO2air(inputs.getSpCO2());
                lastOutputs.setRHair(inputs.getSpRHgh());
//                lastOutputs.setTpipe(20); //lastOutputs.setTpipe(TODO);
//                lastOutputs.setAlpha(1); //lastOutputs.setAlpha(TODO);
            }

            // Set time of inputs.
            Date d = getCurrentSimulationTime();
            inputs.setTime(d);
            inputs.setAlpha(lastOutputs.getAlpha());
            inputs.setTpipe(lastOutputs.getTpipe());

            // Arguments.
            MWNumericArray intervalArg = new MWNumericArray(intervalSeconds, MWClassID.DOUBLE); // Seconds
            MWNumericArray firstRunArg = new MWNumericArray(isSimulationInProgress() ? 1 : 0, MWClassID.DOUBLE); // First run (0), later runs (1).
            MWStructArray inputsArg = MWStructArray.fromMap(inputs.toMap());
            MWStructArray outputsArg = MWStructArray.fromMap(lastOutputs.toMap());
            MWNumericArray totalDaysArg = new MWNumericArray(getSimulationDurationDays(), MWClassID.DOUBLE);
            MWNumericArray day = new MWNumericArray(getDayno(d), MWClassID.DOUBLE);
            MWNumericArray hour = new MWNumericArray(getHour(d), MWClassID.DOUBLE);
            MWNumericArray fiveMinInt = new MWNumericArray(getFiveMinInterval(d), MWClassID.DOUBLE);

            // Invoke SimStart.
            Object[] result = APISingleton.getInstance().getAPI().SimStart(2, intervalArg, firstRunArg, inputsArg, outputsArg, totalDaysArg);
            MWStructArray r1 = (MWStructArray) result[0];
            MWStructArray r2 = (MWStructArray) result[1];

            // Get it.
            Object[] rv = APISingleton.getInstance().getAPI().GetLight(1, day, hour, fiveMinInt);
            Double outLightW = Util.getDouble((MWNumericArray) rv[0]);

            // Debug.
            System.out.format(
                    "SimStart(%s, %s, %s, %s, %s) -> {%s, %s}\n",
                    f(intervalArg), f(firstRunArg), f(inputsArg), f(outputsArg), f(totalDaysArg),
                    f(r1), f(r2));

            // Results.
            Outputs outputResult = new Outputs(r1, outLightW);
            //Inputs inputResult = new Inputs(r2);

            // Dispose.
            intervalArg.dispose();
            firstRunArg.dispose();
            inputsArg.dispose();
            outputsArg.dispose();
            totalDaysArg.dispose();
            hour.dispose();
            day.dispose();
            r1.dispose();
            r2.dispose();

            // Remember outputs - they will be needed on next run.
            lastOutputs = outputResult;

            return outputResult;
        } finally {
            cursor = new Date(cursor.getTime() + intervalSeconds * 1000);
        }
    }

    /**
     * [ outdoorlight ] = GetLight( dayno, hour )
     *
     * @return
     */
    public synchronized List<Sample<WattSqrMeter>> getLightForecast() throws Exception {
        List<Sample<WattSqrMeter>> r = new ArrayList<>();

        Date d = from;
        Date t = new Date(to.getTime() + DAY_MILLIS);

        while (!d.after(t)) {

            // Initialize matlab args.
            MWNumericArray day = new MWNumericArray(getDayno(d), MWClassID.DOUBLE);
            MWNumericArray hour = new MWNumericArray(getHour(d), MWClassID.DOUBLE);
            MWNumericArray fiveMinInt = new MWNumericArray(getFiveMinInterval(d), MWClassID.DOUBLE);

            // Get it.
            Object[] rv = APISingleton.getInstance().getAPI().GetLight(1, day, hour, fiveMinInt);
            Double rd = Util.getDouble((MWNumericArray) rv[0]);

            //System.out.format("Forecast: %s = %f\n", d, rd);

            // Add item to result.
            Sample<WattSqrMeter> item = new Sample<>(d, new WattSqrMeter(rd));
            r.add(item);

            // Dispose matlab args.
            day.dispose();
            hour.dispose();

            // Move d.
            d = new Date(d.getTime() + intervalSeconds * 1000);
        }

        return r;
    }

    public List<Sample<MoneyMegaWattHour>> getElPriceForecast() {
        List<Sample<MoneyMegaWattHour>> r = new ArrayList<>();

        Date d = from;
        Date t = new Date(to.getTime() + DAY_MILLIS);

        while (!d.after(t)) {
            MoneyMegaWattHour money = ElPrices.getInstance().getPrice(d);
            Sample<MoneyMegaWattHour> item = new Sample<>(d, money);
            r.add(item);

            // Move d. one hour
            d = new Date(d.getTime() + 60 * 60 * 1000);
        }

        return r;
    }

    private static String f(Object o) {
        String s = o.toString().trim();
        s = s.replaceAll("\n", " ");
        s = s.replaceAll("\t", " ");
        s = s.replaceAll(" +", " ");
        return "[" + s + "]";
    }
    private static Boolean isInstalled = null;

    public static synchronized boolean isSimulationEnvironmentInstalled() {
        if (isInstalled == null) {
            isInstalled = MCRConfiguration.isInstalledMCR();
        }

        return isInstalled;
    }
}
