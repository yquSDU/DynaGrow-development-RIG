package dk.sdu.mmmi.gc.impl.climatedb.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author jcs, mrj
 */
public class JDBC {

    public static boolean loadDriver(String driver) {
        try {
            Class.forName(driver).newInstance();
            return true;
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Unable to load the JDBC driver " + driver);
            cnfe.printStackTrace(System.err);
        } catch (InstantiationException ie) {
            System.err.println(
                    "Unable to instantiate the JDBC driver " + driver);
            ie.printStackTrace(System.err);
        } catch (IllegalAccessException iae) {
            System.err.println(
                    "Not allowed to access the JDBC driver " + driver);
            iae.printStackTrace(System.err);
        }
        return false;
    }

    public static void loadSchema(Connection c, URL resource)
            throws SQLException {

        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = resource.openStream();
            BufferedReader bisr = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = bisr.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException x) {
            System.err.println(x.getMessage());
        }

        execute(c, sb.toString());
    }

    /**
     * Execute a single statement, commit, and dispose statement. Don't use this
     * when building a composite transaction!
     */
    public static void execute(final Connection c, final String sql)
            throws SQLException {

        Statement s = null;
        try {
            s = c.createStatement();
            s.execute(sql);
            c.commit();
        } catch (SQLException x) {
            c.rollback();
        } finally {
            if (s != null) {
                s.close();
                s = null;
            }
        }
    }

    /**
     * Build a SQL TimeStamp from a Java Date.
     */
    public static Timestamp toTime(Date d) {
        return new Timestamp(d.getTime());
    }

    /**
     * Build a Java Date from a SQL TimeStamp.
     */
    public static Date fromTime(Timestamp t) {
        return new Date(t.getTime());
    }
}
