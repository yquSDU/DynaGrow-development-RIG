package dk.sdu.mmmi.gc.impl.privahal;

import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector.Status;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.impl.entities.config.PrivaConfig;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ancla
 */
public class PrivaConnectorImplTest {

    private PrivaConnectorImpl instance;

    public PrivaConnectorImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.instance = new PrivaConnectorImpl(null);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getStatus method, of class PrivaConnectorImpl.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");

        HALConnector.Status expResult = Status.OPTIMAL_SERVICE;
        HALConnector.Status result = instance.getStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of readTemperature method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadTemperature() {
        System.out.println("readTemperature");

        Celcius result = null;
        try {
            result = instance.readTemperature();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readOutdoorTemperature method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadOutdoorTemperature() {
        System.out.println("readOutdoorTemperature");

        Celcius result = null;
        try {
            result = instance.readOutdoorTemperature();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readCO2 method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadCO2() {
        System.out.println("readCO2");

        PPM result = null;
        try {
            result = instance.readCO2();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readLightIntensity method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadLightIntensity() {
        System.out.println("readLightIntensity");

        UMolSqrtMeterSecond result = null;
        try {
            result = instance.readLightIntensity();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readHumidity method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadHumidity() {
        System.out.println("readHumidity");

        Percent result = null;
        try {
            result = instance.readHumidity();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readOutdoorLightIntensity method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadOutdoorLightIntensity() {
        System.out.println("readOutdoorLightIntensity");

        WattSqrMeter result = null;
        try {
            result = instance.readOutdoorLightIntensity();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readArtficialLightStatus method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadArtficialLightStatus() {
        System.out.println("readArtficialLightStatus");
        String lightGroup = "I206.0N1R15.1V1";

        Switch result = null;
        try {
            result = instance.readArtficialLightStatus(lightGroup);
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of readWindowOpening method, of class PrivaConnectorImpl.
     */
    @Test
    public void testReadWindowOpening() {
        System.out.println("readWindowOpening");

        Percent result = null;
        try {
            result = instance.readWindowOpening();
            System.out.println(result);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
        assertNotNull(result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of writeHeatingThreshold method, of class PrivaConnectorImpl.
     */
    @Test
    public void testWriteHeatingThreshold() {
        System.out.println("writeHeatingThreshold");
        Celcius c = new Celcius(80d);

        try {
            instance.writeHeatingThreshold(c);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }

    }

    /**
     * Test of writeVentilationThreshold method, of class PrivaConnectorImpl.
     */
    @Test
    public void testWriteVentilationThreshold() {
        System.out.println("writeVentilationThreshold");
        Celcius c = new Celcius(80d);

        try {
            instance.writeVentilationThreshold(c);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }

    }

    /**
     * Test of getConfiguration method, of class PrivaConnectorImpl.
     */
    @Test
    public void testGetConfiguration() {
        System.out.println("getConfiguration");

        PrivaConfig result = instance.getConfiguration();
        assertNotNull(result);
    }

    /**
     * Test of writeCO2Threshold method, of class PrivaConnectorImpl.
     */
    @Test
    public void testWriteCO2Threshold() {
        System.out.println("writeCO2Threshold");
        PPM p = new PPM(800d);

        try {
            instance.writeCO2Threshold(p);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }
    }

    /**
     * Test of writeLightStatus method, of class PrivaConnectorImpl.
     */
    @Test
    public void testWriteLightStatus() {
        System.out.println("writeLightStatus");
        Switch s = Switch.OFF;

        try {
            instance.writeLightStatus(s);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }

    }

    /**
     * Test of writeGeneralScreenPosition method, of class PrivaConnectorImpl.
     */
    @Test
    public void testWriteGeneralScreenPosition() {
        System.out.println("writeGeneralScreenPosition");
        Percent p = new Percent(50d);

        try {
            instance.writeGeneralScreenPosition(p);
        } catch (HALException ex) {
            fail("Test failed due to exception: " + ex.getLocalizedMessage());
        }

    }

    /**
     * Test of dispose method, of class PrivaConnectorImpl.
     */
    @Test
    public void testDispose() {
        System.out.println("dispose");

        instance.dispose();
    }

}
