package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.api.simulation.SimulationManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class StopSimulationAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public StopSimulationAction(ControlDomain g) {
        super("Stop simulation control");
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        boolean hasSimulator = context(g).one(SimulationContext.class) != null;
        return hasSimulator && getControlManager().isSimulating();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        context(g).one(SimulationManager.class).stopSimulation();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/control_stop.png", false));
        return mi;
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }
}
