package dk.sdu.mmmi.gc.control.screen.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class OutdoorLightMaximumInput extends AbstractInput<WattSqrMeter> {

    private final ControlDomain g;
    private final Link<Action> action;

    public OutdoorLightMaximumInput(ControlDomain g) {
        super("Outdoor light shading limit");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        WattSqrMeter v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, WattSqrMeter result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertMaxOutdoorLight(new Sample<>(t, result));
    }

    private WattSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<WattSqrMeter> r = db.selectMaxOutdoorLight(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            WattSqrMeter oldValue = OutdoorLightMaximumInput.this.getValue();
            WattSqrMeter newValue = DialogUtil.promtWattSqrMeter(oldValue, "Max outdoor light");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
