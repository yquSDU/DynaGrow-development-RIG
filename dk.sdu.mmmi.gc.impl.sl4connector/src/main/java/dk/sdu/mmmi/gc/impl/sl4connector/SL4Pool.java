package dk.sdu.mmmi.gc.impl.sl4connector;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Ensure that only one SL4DCOMConnector is created for each SL4Login.
 *
 * @author mrj, ao
 */
public enum SL4Pool {

    INSTANCE;
//    private final static Logger LOG = Logger.getLogger(SL4Pool.class.getName());
//    private static final int KEEP_CONNECTION_ALIVE_MS = 1000 * 60 * 15;
    private final Map<SL4Login, SL4DCOMConnector> pool = new ConcurrentHashMap<SL4Login, SL4DCOMConnector>();

    /**
     * The caller does not need to close the turned connection. The pool
     * automatically does that when it think it is needed.
     */
    public synchronized SL4DCOMConnector getConnector(SL4Login login) throws SL4DCOMException {
        SL4DCOMConnector r = pool.get(login);
        if (r == null) {
            r = new SL4ReconnectingDCOMConnector(login);
            pool.put(login, r);
        }
        return r;
    }
    /**
     * Wrapper for connection that makes sure it is closed after an appropriate
     * timeout.
     */
//    private class KeepAliveWrapper implements SL4DCOMConnector {
//
//        private Timer timer;
//        private final SL4DCOMConnector delegate;
//
//        public KeepAliveWrapper(SL4DCOMConnector delegate) {
//            this.delegate = delegate;
//            resetKeepAliveTimeout();
//        }
//
//        public double readValue(String projectID, String variableID) throws SL4DCOMException {
//            resetKeepAliveTimeout();
//            return delegate.readValue(projectID, variableID);
//        }
//
//        public void writeValue(String projectID, String variableID, double value) throws SL4DCOMException {
//            resetKeepAliveTimeout();
//            delegate.writeValue(projectID, variableID, value);
//        }
//
//        public void close() throws SL4DCOMException {
//            // Do nothing. Keep it in the pool.
//        }
//
//        private void resetKeepAliveTimeout() {
//            if (timer != null) {
//                timer.cancel();
//            }
//            timer = new Timer();
//
//            final TimerTask task = new TimerTask() {
//                @Override
//                public void run() {
//                    // Remove from pool.
//                    pool.values().remove(KeepAliveWrapper.this);
//
//                    try {
//                        // Close connection
//                        delegate.close();
//                    } catch (Throwable t) {
//                        // Ignore. We are not expecting to use this anymore.
//                        LOG.log(Level.WARNING, null, t);
//                    }
//                }
//            };
//
//            timer.schedule(task, KEEP_CONNECTION_ALIVE_MS);
//        }
//    }
}
