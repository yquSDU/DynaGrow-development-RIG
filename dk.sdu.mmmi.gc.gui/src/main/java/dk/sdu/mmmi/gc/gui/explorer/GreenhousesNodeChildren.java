package dk.sdu.mmmi.gc.gui.explorer;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 * @author mrj
 */
public class GreenhousesNodeChildren extends Children.Keys<ControlDomain> implements ControleumListener {

    private List<ControlDomain> keys;

    public GreenhousesNodeChildren() {
        this.keys = new ArrayList<>();
    }

    //
    // Modifiers.
    //
    @Override
    public void onControlDomainAdded(ControleumEvent e) {
        keys.add(e.getControlDomain());
        setKeys(keys);
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        keys.remove(e.getControlDomain());
        setKeys(keys);
    }

    @Override
    protected Node[] createNodes(ControlDomain g) {
        GreenhouseNode gn = new GreenhouseNode(g);
        return new Node[]{gn};
    }
}
