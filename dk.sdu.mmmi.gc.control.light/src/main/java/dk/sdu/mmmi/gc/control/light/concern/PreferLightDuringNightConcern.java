package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Prefer to have lights on when it is dark outside. This is a simple way to use
 * lights when the benefit in terms of photosynthesis is the highest.
 *
 * @author mrj
 */
public class PreferLightDuringNightConcern extends AbstractConcern {

    private int objectiveCost;

    public PreferLightDuringNightConcern(ControlDomain g) {
        super("Prefer light during night", g, 5);
    }

    @Override
    public double evaluate(Solution s) {

        LightPlan plan = s.getValue(LightPlanTodayOutput.class);

        objectiveCost = 0;
        for (int i = 0; i < plan.size(); i++) {
            Date hourDate = plan.getElementStart(i);
            Switch sw = plan.getElement(hourDate);

            // Add cost when light is lit.
            // Cost is lowest around midnight and highest around noon.
            Calendar cal = Calendar.getInstance();
            cal.setTime(hourDate);
            int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);

            if (sw.isOn()) {
                if (hourOfDay < 12) {
                    objectiveCost += hourOfDay;
                } else {
                    objectiveCost += (24 - hourOfDay);
                }
            }
        }

        return objectiveCost;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class, String.format("Cost %d", objectiveCost));
        return r;
    }

}
