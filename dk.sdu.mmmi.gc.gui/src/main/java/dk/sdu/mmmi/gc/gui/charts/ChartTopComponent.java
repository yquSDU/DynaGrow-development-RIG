package dk.sdu.mmmi.gc.gui.charts;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import java.awt.BorderLayout;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.swing.SwingUtilities;
import org.jfree.chart.ChartPanel;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

/**
 * Need help, see this: http://wiki.netbeans.org/DevFaqNonSingletonTopComponents
 *
 * TODO: Automatic update of Charts to always show present time period
 *
 * @author mrj & jcs
 */
@ConvertAsProperties(dtd = "-//dk.sdu.mmmi.gc.gui.chart//Chart//EN", autostore = false)
@TopComponent.Description(preferredID = "ChartTopComponent" /*, iconBase="SET/PATH/TO/ICON/HERE"*/, persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED)
@TopComponent.Registration(mode = "editor", openAtStartup = false)
public final class ChartTopComponent extends TopComponent implements ChartListener, ControleumListener {

    private NavigationPanel navPanel;
    private final DisposableList disposables = new DisposableList();
    private ChartConfig chart;
    private ControlDomain g;
    private ChartManager cm;
    private String chartID;
    private int greenhouseID;
    private ChartPanel chartPanel;
    private Executor exe = Executors.newFixedThreadPool(4);

    /**
     * Default constructor called on restore.
     */
    public ChartTopComponent() {
    }

    public ChartTopComponent(ControlDomain g, ChartConfig chart) {
        this.g = g;
        this.chart = chart;
        this.cm = context(g).one(ChartManager.class);
        this.greenhouseID = g.getID();
        this.chartID = chart.getIDString();

        init();
    }

    private void init() {
        this.navPanel = new NavigationPanel(g, chart);

        // Layout.
        setLayout(new BorderLayout());
        add(navPanel, BorderLayout.NORTH);

        // Listeners
        disposables.add(context(g).add(ControleumListener.class, this));
        disposables.add(context(chart).add(ChartListener.class, this));

        // Update
        updateGUI();
    }

    private void updateGUI() {

        setName(String.valueOf(g.getID()));
        setDisplayName(chart.getName());
        
        exe.execute(new Runnable() {
            @Override
            public void run() {
                Date to = DateUtil.endOfDay(chart.getToDate());
                Date from = new Date(to.getTime() - chart.getPeriod());

                ChartBuilder builder = new ChartBuilder(g, chart.getSeries(), chart.getName(), from, to);
                final ChartPanel newChartPanel = builder.createChart();

                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (chartPanel != null) {
                            remove(chartPanel);
                        }
                        add(newChartPanel, BorderLayout.CENTER);
                        chartPanel = newChartPanel;
                        revalidate();
                    }
                });
            }
        });
    }

    void readProperties(java.util.Properties p) {
        // do the read
        this.greenhouseID = Integer.valueOf(p.getProperty("greenhouseID"));
        this.chartID = p.getProperty("chartID");

        this.g = getControleumManager().getControlDomain(greenhouseID);

        this.cm = context(g).one(ChartManager.class);
        this.chart = cm.getChartConfig(chartID);

        init();
    }

    void writeProperties(java.util.Properties p) {
        // handle the store
        p.put("greenhouseID", String.valueOf(greenhouseID));
        p.put("chartID", chartID);
    }
    //
    // Events
    //

    @Override
    protected void componentClosed() {
        super.componentClosed();
        dispose();
    }

    @Override
    public void onChartChanged(ChartConfig chart) {
        if (this.chart == chart) {
            this.chart = chart;
            updateGUI();
        }
    }

    @Override
    public void onChartDelete(ChartConfig chart) {
        if (this.chart == chart) {
            close();
        }
    }

    @Override
    public void onControlDomainAdded(ControleumEvent e) {
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        cm.delete(chart);
        this.componentClosed();
    }

    private ControlDomainManager getControleumManager() {
        return Lookup.getDefault().lookup(ControlDomainManager.class);
    }

    private void dispose() {
        disposables.dispose();
        navPanel.dispose();
        this.removeAll();
    }
}
