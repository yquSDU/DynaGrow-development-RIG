package dk.sdu.mmmi.gc.impl.okosimulation;

import static dk.sdu.mmmi.gc.impl.okosimulation.Util.*;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author mrj
 */
public class Inputs {

    private double spAirHeat=18;
    private double spAirVent=20;
    private double spCO2=500;
    private double spRHgh=85;
    private double spScreenPos=0;
    private double LampCapacityInst=400; // TODO combine these to connected lamp load
    private double NoLampsSqm=0.15; // TODO
    private double ArtficialLightStatus=0;
    private double Tpipe;
    private double alpha;
    private double TrScreen = 0.20; //input.TrScreen=0.75;
    private double CO2maxSupply = 5.0 * Math.pow(10, -4);

    private Date time = new Date();

    public Inputs() {
    }

    public HashMap<String, Object> toMap() {
        HashMap<String,Object> r = new HashMap<String, Object>();
        r.put("spAirHeat", spAirHeat);
        r.put("spAirVent", spAirVent);
        r.put("spCO2", spCO2);
        r.put("spRHgh", spRHgh);
        r.put("spScreenPos", spScreenPos);
        r.put("LampCapacityInst", LampCapacityInst);
        r.put("NoLampsSqm", NoLampsSqm);
        r.put("ArtficialLightStatus", ArtficialLightStatus);
        r.put("dayno", getDayno(time)); // %starter med Day 1
        r.put("hour", getHour(time)); // %fra 1 to 24
        r.put("min5int", getFiveMinInterval(time)); // %fra 1 to 12
        r.put("Tpipe", getTpipe());
        r.put("alpha", getAlpha());
        r.put("TrScreen", getTrScreen());
        r.put("CO2maxSupply", CO2maxSupply);
        return r;
    }

    public void setTrScreen(double TrScreen) {
        this.TrScreen = TrScreen;
    }

    public double getTrScreen() {
        return TrScreen;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setTpipe(double Tpipe) {
        this.Tpipe = Tpipe;
    }

    public double getTpipe() {
        return Tpipe;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getArtficialLightStatus() {
        return ArtficialLightStatus;
    }

    public void setArtficialLightStatus(double ArtficialLightStatus) {
        this.ArtficialLightStatus = ArtficialLightStatus;
    }

    public double getLampCapacityInst() {
        return LampCapacityInst;
    }

    public void setLampCapacityInst(double LampCapacityInst) {
        this.LampCapacityInst = LampCapacityInst;
    }

    public double getNoLampsSqm() {
        return NoLampsSqm;
    }

    public void setNoLampsSqm(double NoLampsSqm) {
        this.NoLampsSqm = NoLampsSqm;
    }

    public double getSpAirHeat() {
        return spAirHeat;
    }

    public void setSpAirHeat(double spAirHeat) {
        this.spAirHeat = spAirHeat;
    }

    public double getSpAirVent() {
        return spAirVent;
    }

    public void setSpAirVent(double spAirVent) {
        this.spAirVent = spAirVent;
    }

    public double getSpCO2() {
        return spCO2;
    }

    public void setSpCO2(double spCO2) {
        this.spCO2 = spCO2;
    }

    public double getSpRHgh() {
        return spRHgh;
    }

    public void setSpRHgh(double spRHgh) {
        this.spRHgh = spRHgh;
    }

    public double getSpScreenPos() {
        return spScreenPos;
    }

    public void setSpScreenPos(double spScreenPos) {
        this.spScreenPos = spScreenPos;
    }
}
