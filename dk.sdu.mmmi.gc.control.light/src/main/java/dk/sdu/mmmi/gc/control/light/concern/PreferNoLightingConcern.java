package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.util.HashMap;
import java.util.Map;

/**
 * Minimize the amount of time spend with lights on. The advantage of this
 * concern is that it minimizes cost without depending on electricity prices.
 *
 * @author mrj, jcs
 */
public class PreferNoLightingConcern extends AbstractConcern {

    public PreferNoLightingConcern(ControlDomain g) {
        this(g, 5);
    }

    public PreferNoLightingConcern(ControlDomain g, int priority) {
        super("Prefer no lighting", g, 5);
    }

    @Override
    public double evaluate(Solution s) {

        // Inputs
        LightPlan plan = s.getValue(LightPlanTodayOutput.class);

        double minutesOn = plan.getTotalLightOnDuration().toMinutes();
        double totalLength = plan.getPlanDuration().toMinutes();

        return Math.abs(0 - (minutesOn / totalLength));
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Duration totalLight = s.getValue(LightPlanTodayOutput.class).getTotalLightOnDuration();
        Map<Class, String> help = new HashMap<>();
        help.put(LightPlanTodayOutput.class,
                String.format("total light is %.1f [hours]", totalLight.toHours()));
        return help;
    }
}
