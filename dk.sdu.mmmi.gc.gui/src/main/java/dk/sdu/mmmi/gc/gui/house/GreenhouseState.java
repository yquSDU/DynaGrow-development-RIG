package dk.sdu.mmmi.gc.gui.house;

import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;

/**
 * @author mrj
 */
public class GreenhouseState {

    private Percent screenPosition;
    private Boolean ventilating;
    private Celcius indoorTemperature, indoorTemperatureSP;
    private PPM co2, co2SP;
    private Percent humidity;
    private Percent photosynthesisOptimization, photosynthesisOptimizationGoal;
    private WattSqrMeter outdoorIrradiation;
    private Celcius outdoorTemperature;
    private UMolSqrtMeterSecond indoorIrradiation;
    private Boolean lightOn;

    public Boolean isLightOn() {
        return lightOn;
    }

    public void setLightOn(Boolean lightOn) {
        this.lightOn = lightOn;
    }

    public UMolSqrtMeterSecond getIndoorIrradiation() {
        return indoorIrradiation;
    }

    public void setIndoorIrradiation(UMolSqrtMeterSecond indoorIrradiation) {
        this.indoorIrradiation = indoorIrradiation;
    }

    public Celcius getOutdoorTemperature() {
        return outdoorTemperature;
    }

    public void setOutdoorTemperature(Celcius outdoorTemperature) {
        this.outdoorTemperature = outdoorTemperature;
    }

    public WattSqrMeter getOutdoorIrradiation() {
        return outdoorIrradiation;
    }

    public void setOutdoorIrradiation(WattSqrMeter outdoorIrradiation) {
        this.outdoorIrradiation = outdoorIrradiation;
    }

    public Percent getScreenPosition() {
        return screenPosition;
    }

    public void setScreenPosition(Percent screenPosition) {
        this.screenPosition = screenPosition;
    }

    public Boolean isVentilating() {
        return ventilating;
    }

    public void setVentilating(Boolean ventilating) {
        this.ventilating = ventilating;
    }

    public PPM getCo2() {
        return co2;
    }

    public void setCo2(PPM co2) {
        this.co2 = co2;
    }

    public PPM getCo2SP() {
        return co2SP;
    }

    public void setCo2SP(PPM co2SP) {
        this.co2SP = co2SP;
    }

    public Percent getHumidity() {
        return humidity;
    }

    public void setHumidity(Percent humidity) {
        this.humidity = humidity;
    }

    public Celcius getIndoorTemperature() {
        return indoorTemperature;
    }

    public void setIndoorTemperature(Celcius indoorTemperature) {
        this.indoorTemperature = indoorTemperature;
    }

    public Celcius getIndoorTemperatureSP() {
        return indoorTemperatureSP;
    }

    public void setIndoorTemperatureSP(Celcius indoorTemperatureSP) {
        this.indoorTemperatureSP = indoorTemperatureSP;
    }

    public Percent getPhotosynthesisOptimization() {
        return photosynthesisOptimization;
    }

    public void setPhotosynthesisOptimization(Percent photosynthesisOptimization) {
        this.photosynthesisOptimization = photosynthesisOptimization;
    }

    public Percent getPhotosynthesisOptimizationGoal() {
        return photosynthesisOptimizationGoal;
    }

    public void setPhotosynthesisOptimizationGoal(Percent photosynthesisOptimizationGoal) {
        this.photosynthesisOptimizationGoal = photosynthesisOptimizationGoal;
    }
}
