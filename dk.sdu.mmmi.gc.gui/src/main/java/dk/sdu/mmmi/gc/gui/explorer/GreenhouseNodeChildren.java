package dk.sdu.mmmi.gc.gui.explorer;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import java.util.Collections;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author mrj
 */
public class GreenhouseNodeChildren extends Children.Keys<ControlDomain> {

    GreenhouseNodeChildren(ControlDomain g) {
        setKeys(Collections.singleton(g));
    }

    @Override
    protected Node[] createNodes(ControlDomain g) {
        Node[] add = {
            new ConcernsNode(g),
            new OutputsNode(g),
            new InputsNode(g),
            new ChartsNode(g)};
        return add;
    }
}
