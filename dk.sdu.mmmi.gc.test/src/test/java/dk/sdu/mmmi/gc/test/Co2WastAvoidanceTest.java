package dk.sdu.mmmi.gc.test;

import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class Co2WastAvoidanceTest {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private GreenhouseTestFixture testFixture;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        testFixture.dispose();
    }

    @Test
    public void co2WasteAvoidance() throws ParseException {

        // Setup:
        SearchConfiguration sc = new SearchConfiguration.Builder()
                .maxGenerations(30)
                .debuggingEnabled(true)
                .build();
        testFixture = new GreenhouseTestFixture("CO2WasteAvoidance", df.parse("2012-04-21 13:00"), 5, sc)
                .co2WasteAvoidanceFeature()
                .dayNightVentFeature(1, 27, 27)
                .dayNightCO2Feature(1, 800, 350)
                .achievePARSumBalance(2, 20)
                .preferCheapLightConcern(3)
                .minSwitchConcern(4)
                .setVentilationSp(27)
                .setIndoorTempInput(28);

        // Test:
        testFixture.startNegotiation(df.parse("2012-04-21 13:00"),
                df.parse("2012-04-21 13:00"));

        // Asserts:
        PPM co2Sp = testFixture.getSelectedSolution().getValue(CO2GoalOutput.class);
        assertEquals(350.0, co2Sp.value(), 0.1);
    }
}
