package dk.sdu.mmmi.gc.control.light.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.gc.control.light.input.LightTimeAchievedPeriod;
import dk.sdu.mmmi.gc.control.light.input.MinimumLightTimeSumInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class AchieveLightTimeSumConcern extends AbstractConcern {

    private Duration lightHoursGoal;
    private Duration totalHistory;
    private Duration totalInPlan;
    private double balance;

    public AchieveLightTimeSumConcern(ControlDomain g) {
        super("Achieve light-time sum", g, 1);
    }

    @Override
    public double evaluate(Solution s) {

        lightHoursGoal = s.getValue(MinimumLightTimeSumInput.class).multiply(3);
        totalHistory = s.getValue(LightTimeAchievedPeriod.class);

        LightPlan p = s.getValue(LightPlanTodayOutput.class);

        totalInPlan = p.getTotalLightOnDuration();

        balance = abs(lightHoursGoal.subtract(totalHistory.add(totalInPlan)).toHours());
        return balance;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class,
                String.format("LightHoursGoal(%.1f) - (TotalHistory(%.1f) + TotalInPlan(%.1f)) = Balance(%.1f)",
                        lightHoursGoal.toHours(), totalHistory.toHours(), totalInPlan.toHours(), balance));
        return r;
    }
}
