package dk.sdu.mmmi.gc.control.commons.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrj
 */
public class CO2Input extends AbstractInput<PPM> {

    private final ControlDomain g;

    public CO2Input(ControlDomain g) {
        super("CO₂");
        assert (g != null);
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        HALConnector hal = HAL.get(g);
        if (hal != null) {
            try {
                PPM newValue = hal.readCO2();
                setValue(newValue);
                store(t, newValue);
            } catch (HALException ex) {
                Logger.getLogger(CO2Input.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void store(Date t, PPM result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertCO2(new Sample<>(t, result));
    }
}
