package dk.sdu.mmmi.gc.impl.energidkconnector;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author jcs
 */
@Configuration
@PropertySource("classpath:dk/sdu/mmmi/gc/impl/energidkconnector/db.properties")
public class EnergiDkElPriceDBConfig {

    @Autowired
    private Environment env;

    public EnergiDkElPriceDBConfig() {
    }

    @Bean
    public DataSource dataSource() {

        final BasicDataSource ds = new org.apache.commons.dbcp.BasicDataSource();
        ds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        ds.setUrl(env.getProperty("db.url"));
        ds.setUsername(env.getProperty("db.user"));
        ds.setPassword(env.getProperty("db.password"));

        ds.setTestWhileIdle(true);
        ds.setTestOnBorrow(true);
        ds.setTestOnReturn(false);
        ds.setValidationQuery("SELECT 1");
        ds.setValidationQueryTimeout(3000);
        ds.setTimeBetweenEvictionRunsMillis(5000);
        ds.setMaxActive(10);
        ds.setMinIdle(4);
        ds.setMaxWait(10000);
        ds.setInitialSize(4);
        ds.setRemoveAbandonedTimeout(60);
        ds.setRemoveAbandoned(true);
        ds.setLogAbandoned(true);
        ds.setMinEvictableIdleTimeMillis(3000);
        return ds;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }
}
