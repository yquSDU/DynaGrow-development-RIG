package dk.sdu.mmmi.gc.commons.utils;

import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.gc.commons.utils.TestHelper.lightSample;
import dk.sdu.mmmi.gc.control.commons.utils.MathUtil;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class MathUtilTest {

    public MathUtilTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testInterpolationOfData() throws ParseException {
        System.out.println("testInterpolationOfData");

        // SETUP
        List<Sample<UMolSqrtMeterSecond>> samples = Lists.newArrayList(lightSample("00:00:00", 600.0), lightSample("00:05:00", 700.0), lightSample("00:15:00", 800.0));

        // TEST
        Map<Long, Double> result = MathUtil.interpolate(samples, new Duration(300 * 1000L));

        // ASSERTS
        assertEquals(4, result.size());
    }
}
