package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj
 */
public class CO2LimitsConcern extends AbstractConcern {

    private final double min, max;

    public CO2LimitsConcern(ControlDomain g, int min, int max) {
        super("CO₂ limits", g, Concern.HARD_PRIORITY);
        this.min = min;
        this.max = max;
    }

    @Override
    public double evaluate(Solution s) {
        PPM co2 = s.getValue(CO2GoalOutput.class);
        return (co2.value() >= min && co2.value() < max) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(CO2GoalOutput.class,
                String.format("between %.1f and %.1f", min, max));
        return r;
    }
}
