package dk.sdu.mmmi.gc.control.light.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj 
 */
public class CombinedElPriceForecastSeries extends DoubleTimeSeries {

    private final ClimateDataAccess db;

    public CombinedElPriceForecastSeries(ControlDomain g) {
        setName("El. prognosis prices");
        
        this.db = context(g).one(ClimateDataAccess.class);

        ElPriceForecastConfig elPriceIntegration = db.selectElPriceForecastConfig();
        setUnit(String.format("[%s/MWh]", elPriceIntegration.getCurrency()));
    }

    @Override
    public long getMaxPeriodLenghtMS() {
        return 1000 * 60 * 90;
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {
        List<DoubleTimeValue> r = new ArrayList<>();
       
        List<Sample<MoneyMegaWattHour>> raw = db.selectElPriceForecast(from, to).asListSampleMoneyMegaWattHour();

            for (Sample<MoneyMegaWattHour> s : raw) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }

        return r;
    }
}
