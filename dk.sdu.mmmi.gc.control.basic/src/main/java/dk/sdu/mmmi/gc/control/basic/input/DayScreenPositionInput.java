package dk.sdu.mmmi.gc.control.basic.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author mrj
 */
public class DayScreenPositionInput extends AbstractInput<Percent> {

    private final ControlDomain g;
    private final Link<Action> action;

    public DayScreenPositionInput(ControlDomain g) {
        super("Preferred day screen position");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        Percent v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Percent result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPreferredDayScreenPosition(new Sample<Percent>(t, result));
    }

    private Percent retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Percent> r = db.selectPreferredDayScreenPosition(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Percent oldValue = DayScreenPositionInput.this.getValue();
            Percent newValue = DialogUtil.promtPercent(oldValue, "Preferred day screen position");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
