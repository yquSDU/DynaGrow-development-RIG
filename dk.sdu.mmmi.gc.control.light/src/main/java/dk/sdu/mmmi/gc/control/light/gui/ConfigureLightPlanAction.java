package dk.sdu.mmmi.gc.control.light.gui;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class ConfigureLightPlanAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public ConfigureLightPlanAction(ControlDomain g) {
        super("Configure Light Plan Interval");
        this.g = g;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        // Panel.
        LightPlanConfigPanel pnl = new LightPlanConfigPanel();

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure Light Plan Interval");

        // Set defaults.
        LightPlanTodayOutput lp = context(g).one(LightPlanTodayOutput.class);
        pnl.set(lp.getConfig());

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            lp.setLightPlanDuration(pnl.getDuration());
        }
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    @Override
    public boolean isEnabled() {
        return g != null
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling()
                && !getControlManager().isAutoRunning();
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }

    /**
     * Light Plan panel.
     */
    private static class LightPlanConfigPanel extends JPanel {

        private static final long serialVersionUID = 1L;
        private final JTextField lightInterval = new JTextField();

        public LightPlanConfigPanel() {
            setLayout(new GridLayout(1, 2, 2, 2));
            add(new JLabel("Light plan interval (minutes):"));
            add(lightInterval);
        }

        public void set(LightPlanConfig cfg) {
            lightInterval.setText(String.format("%.0f", cfg.getTimeSlotDuration().toMinutes()));
        }

        public Duration getDuration() {
            return new Duration(Long.parseLong(lightInterval.getText()) * 60 * 1000);
        }
    }
}
