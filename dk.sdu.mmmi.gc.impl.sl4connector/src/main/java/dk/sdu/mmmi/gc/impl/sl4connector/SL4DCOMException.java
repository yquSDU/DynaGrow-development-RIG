package dk.sdu.mmmi.gc.impl.sl4connector;

/**
 * @author mrj
 */
public class SL4DCOMException extends Exception {

    public SL4DCOMException(String msg) {
        super(msg);
    }
    
    public SL4DCOMException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
