package dk.sdu.mmmi.gc.control.light.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class CombinedOutdoorLightForecastSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public CombinedOutdoorLightForecastSeries(ControlDomain g) {
        this.g = g;
        setName("Historical outdoor light forecast");
        setUnit("[W m^-2]");
    }

    @Override
    public long getMaxPeriodLenghtMS() {
        return 1000 * 60 * 90;
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {
        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<WattSqrMeter>> raw = db.selectOutdoorLightForecast(from, to).get();
            for (Sample<WattSqrMeter> s : raw) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
    /*
     * @Override public List<DoubleTimeValue> getValues(Date from, Date to)
     * throws Exception {
     *
     * List<DoubleTimeValue> r = new ArrayList<DoubleTimeValue>();
     *
     * ClimateDataAccess db = context(g).one(ClimateDataAccess.class); if(db !=
     * null) { // Select most recent forecast.
     * List<Sample<OutdoorLightForecast>> forecasts =
     * db.selectOutdoorLightForecast(from, to);
     *
     * // Newest first. Collections.reverse(forecasts);
     *
     * // Combine data. Date head = to; for(Sample<OutdoorLightForecast> f :
     * forecasts) { List<Sample<WattSqrMeter>> list =
     * f.getSample().getOutdoorLightForecast(); Collections.reverse(list);
     * for(Sample<WattSqrMeter> s : list) { Date t = s.getTimestamp();
     * if(t.before(head)) { if(!t.before(from)) { Double v =
     * s.getSample().doubleValue(); r.add(new DoubleTimeValue(t, v)); } head = t;
     * } } } }
     *
     * Collections.sort(r, DoubleTimeValue.ASCENDING_ORDER);
     *
     * return r; }
     */
}
