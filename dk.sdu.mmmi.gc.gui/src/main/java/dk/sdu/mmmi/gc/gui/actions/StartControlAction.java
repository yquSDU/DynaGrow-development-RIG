package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class StartControlAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private ControlManager ctrlManager;
    private final ControlDomain g;

    public StartControlAction(ControlDomain g) {
        super("Control once");
        this.g = g;
        init();
    }

    private void init() {
        this.ctrlManager = context(g).one(ControlManager.class);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ctrlManager.controlOnce();
    }

    @Override
    public boolean isEnabled() {
        return !ctrlManager.isAutoRunning()
                && !ctrlManager.isSimulating()
                && !ctrlManager.isControlling();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/control_play.png", false));
        return mi;
    }
}
