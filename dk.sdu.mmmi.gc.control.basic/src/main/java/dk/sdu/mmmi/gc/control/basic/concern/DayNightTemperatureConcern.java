package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.basic.input.DayTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.input.NightTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.NIGHT_LIMIT;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj
 */
public class DayNightTemperatureConcern extends AbstractConcern {

    public DayNightTemperatureConcern(ControlDomain g, int priority) {
        super("Day/night temperature control", g, priority);
    }

    @Override
    public double evaluate(Solution s) {
        // Data.
        Celcius t = s.getValue(HeatingSetpointOutput.class);

        // Fitness.
        if (isNight(s)) {
            Celcius night = s.getValue(NightTemperatureInput.class);
            return Math.abs(night.subtract(t).roundedValue());
        } else {
            Celcius day = s.getValue(DayTemperatureInput.class);
            return Math.abs(day.subtract(t).roundedValue());
        }
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (isNight(s)) {
            Celcius night = s.getValue(NightTemperatureInput.class);
            r.put(HeatingSetpointOutput.class, String.format("prefer %.1f", night.value()));
        } else {
            Celcius day = s.getValue(DayTemperatureInput.class);
            r.put(HeatingSetpointOutput.class, String.format("prefer %.1f", day.value()));
        }

        return r;
    }

    private boolean isNight(Solution s) {
        UMolSqrtMeterSecond indoorLight = s.getValue(IndoorLightInput.class);
        return indoorLight.value() < NIGHT_LIMIT;
    }

}
