package dk.sdu.mmmi.gc.gui.charts;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import java.awt.Color;
import java.awt.Paint;
import java.util.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.openide.util.Exceptions;

/**
 *
 * @author jcs
 */
public class JFreeBarChartBuilder {

    private static final double BAR_WIDTH = 0.2D;
    private static final int MIN_DATA_POINTS = 6;
    private final List<DoubleTimeSeries> serieList;
    private final Date from;
    private final Date to;
    private final List<String> symbolAxisLabels = new ArrayList<>();
    private final XYIntervalSeriesCollection data = new XYIntervalSeriesCollection();
    private final Table<Integer, Integer, Double> valueTable = TreeBasedTable.create();

    public JFreeBarChartBuilder(List<DoubleTimeSeries> series, Date from, Date to) {
        this.serieList = series;
        this.from = from;
        this.to = to;
    }

    /**
     * Load a context in which to find DoubleTimeSeries fitness objects.
     */
    public void loadSeries() {
        try {
            int serieIdx = 0;

            for (DoubleTimeSeries serie : serieList) {

                // Serie is a fitness serie
                if (serie.getUnit().equals("[Satisfaction]")) {

                    List<DoubleTimeValue> allValues = serie.getValues(from, to);

                    List<DoubleTimeValue> valueList = DataUtil.addNoDataZones(allValues, serie.getMaxPeriodLenghtMS());

                    // Include only intervals when there is data for more than halv an hour 6*5 min intervals).
                    // This check is done because the fitness data is normalized and only makes sense the present when enough data exists.
                    if (allValues.size() > MIN_DATA_POINTS) {
                        // Add axis labels
                        symbolAxisLabels.add(serie.getName());

                        data.addSeries(createDataset(serieIdx, serie, valueList));
                        serieIdx++;
                    }
                }
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private XYIntervalSeries createDataset(int serieIdx, DoubleTimeSeries fitnessSerie, List<DoubleTimeValue> valueList) {

        // New series for concern
        XYIntervalSeries intervalSeries = new XYIntervalSeries(fitnessSerie.getName());

        int itemIdx = 0;

        // Add items to series
        for (int valIdx = 0; valIdx < valueList.size() - 1; valIdx++) {

            DoubleTimeValue val = valueList.get(valIdx);
            DoubleTimeValue nxtVal = valueList.get(valIdx + 1);

            // If interval has start and end
            if (val.getValue() != null && nxtVal.getValue() != null) {

                double start = Long.valueOf(val.getTime().getTime()).doubleValue();
                double end = Long.valueOf(nxtVal.getTime().getTime()).doubleValue();

                // Save series value in map
                intervalSeries.add(start, start, end, serieIdx, serieIdx - BAR_WIDTH, serieIdx + BAR_WIDTH);
                valueTable.put(serieIdx, itemIdx, val.getValue());
                itemIdx++;
            }
        }

        return intervalSeries;
    }

    public JFreeChart createChart() {

        JFreeChart jfreechart = ChartFactory.createXYBarChart(null, null, true, null, data, PlotOrientation.VERTICAL, false, false, false);
        jfreechart.setBackgroundPaint(Color.WHITE);
        jfreechart.setNotify(false);

        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        xyPlot.setBackgroundPaint(Color.WHITE);
        xyPlot.setRangeGridlinePaint(Color.black);
        xyPlot.setDomainGridlinePaint(Color.black);

        xyPlot.setDomainAxis(new DateAxis("Time"));
        SymbolAxis symbolAxis = new SymbolAxis("Concerns", symbolAxisLabels.toArray(new String[symbolAxisLabels.size()]));
        xyPlot.setRangeAxis(symbolAxis);

        XYBarRenderer xyBarRenderer = new FitnessXYBarChartRenderer(valueTable);
        xyBarRenderer.setUseYInterval(true);
        xyBarRenderer.setShadowVisible(false);
        xyBarRenderer.setBarPainter(new StandardXYBarPainter());

        xyPlot.setRenderer(xyBarRenderer);

        return jfreechart;
    }

    public boolean hasData() {
        return data.getSeriesCount() > 0;
    }

    /**
     * XYBarRenderer implementation that draws different colors for each data
     * item dependent on fitness values. Fitness value 1 = Red, Fitness value
     * ]0;1[ = Yellow and Fitness value 0 = Green
     */
    class FitnessXYBarChartRenderer extends XYBarRenderer {

        private final Table<Integer, Integer, Double> valueTable;

        private FitnessXYBarChartRenderer(Table<Integer, Integer, Double> valueTable) {
            this.valueTable = valueTable;
        }

        @Override
        public Paint getItemPaint(int seriesIdx, int itemIdx) {

            // Get fitness value
            double fitness = valueTable.get(seriesIdx, itemIdx);

            // is the value from accepted concern
            boolean isAccepted = fitness <= 0;

            // Get value for data item in series
            int p = 100;
            if (isAccepted) {
                p = (int) Math.round(fitness * 100);
            }

            // Draw a gradient bases on normalized fitness value
            int R = (255 * p) / 100;

            // if not acceptable then red color
            int G = !isAccepted ? 0 : 255;
            int B = 0;

            return new Color(R, G, B);
        }
    }
}
