package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import com.toedter.calendar.JDateChooser;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.api.simulation.SimulationManager;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class DoSimulateControlAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public DoSimulateControlAction(ControlDomain g) {
        super("Simulate");
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {

        // Build panel.
        SimCfgPnl pnl = new SimCfgPnl();

        // Build descriptor.
        DialogDescriptor desc = new DialogDescriptor(pnl, "Simulate.");

        // Show dialog.
        Object r = DialogDisplayer.getDefault().notify(desc);

        // Do simulate.
        Date from = pnl.getStart();
        Date to = pnl.getEnd();

        if (r.equals(DialogDescriptor.OK_OPTION)) {
            context(g).one(SimulationManager.class).startSimulation(from, to);
        }
    }

    @Override
    public boolean isEnabled() {
        boolean hasSimulator = context(g).one(SimulationContext.class) != null;
        return hasSimulator
                && !getControlManager().isAutoRunning()
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling();
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/calculator.png", false));
        return mi;
    }

    //
    // Private
    //
    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }

    /**
     *
     */
    private static class SimCfgPnl extends JPanel {

        private static final long DAY_MS = 1000 * 60 * 60 * 24;
        private final JDateChooser startChooser = new JDateChooser();
        private final JComboBox durChooser = new JComboBox(new Period[]{
            //            new Period("6 hours", DAY_MS / 4),
            //            new Period("12 hours", DAY_MS / 2),
            new Period("1 day", DAY_MS),
            new Period("3 days", DAY_MS * 3),
            new Period("1 week", DAY_MS * 7),
            new Period("2 weeks", DAY_MS * 7),
            new Period("1 month", DAY_MS * 30),});

        public SimCfgPnl() {

            startChooser.setDate(new Date());

            // Center.
            JPanel pnl = new JPanel(new GridLayout(2, 2));
            pnl.add(new JLabel("Starting date:"));
            pnl.add(startChooser);
            pnl.add(new JLabel("Duration:"));
            pnl.add(durChooser);

            // South.
            JLabel msg = new JLabel(
                    "All data in the greenhouse will be deleted before "
                    + "starting simulation. Do you want to continue?");

            //setLayout(new BorderLayout());
            setLayout(new GridBagLayout());

            //add(pnl, BorderLayout.CENTER);
            //add(msg, BorderLayout.SOUTH);
            add(pnl, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
            add(msg, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
        }

        public Date getStart() {
            return startChooser.getDate();
        }

        public Date getEnd() {
            Period p = (Period) durChooser.getSelectedItem();
            return new Date(getStart().getTime() + p.getMS());
        }
    }

    /**
     *
     */
    private static class Period {

        private final String str;
        private final long ms;

        public Period(String str, long ms) {
            this.str = str;
            this.ms = ms;
        }

        public long getMS() {
            return ms;
        }

        @Override
        public String toString() {
            return str;
        }
    }
}
