package dk.sdu.mmmi.gc.impl.pcg;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class FileHelper {

    public static void setUpClass(File dir, String filename) throws IOException {
        File file = new File(PCGWeatherService.class.getResource(".").getPath() + filename);
        FileUtils.copyFileToDirectory(file, dir);
    }

    public static void tearDownClass(File dir, String filename) throws IOException {
        FileUtils.deleteQuietly(new File(dir.getAbsolutePath() + "//" + filename));
    }

    public static File getLastestDataFile(String dirPath, String filter) {

        File dir = new File(dirPath);
        FileFilter fileFilter = new WildcardFileFilter(filter);

        File[] files = dir.listFiles(fileFilter);
        File file = null;
        long latestTime = 0;

        for (int i = 0; i < files.length; i++) {
            if (latestTime < files[i].lastModified()) {
                file = files[i];
                latestTime = files[i].lastModified();
            }
        }

        return file;
    }


}
