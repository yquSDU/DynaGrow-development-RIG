package dk.sdu.mmmi.gc.impl.privahal;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.hal.priva.PrivaServer;
import dk.sdu.mmmi.gc.hal.priva.acquaintance.PrivaServerException;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaEntity;
import dk.sdu.mmmi.gc.hal.priva.entities.PrivaListValue;
import dk.sdu.mmmi.gc.impl.entities.config.PrivaConfig;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class PrivaConnectorImpl implements HALConnector {

    private static final long RECONNECT_DELAY_MS = 1000 * 2;
    private final ControlDomain g;
    private PrivaServer readServer = null;
    private Date lastConnect = new Date(0);
    private PrivaConfig cfg;
    private final Map<String, PrivaEntity> entities;
    private PrivaServer writeServer;

    public PrivaConnectorImpl(ControlDomain g) {
        this.g = g;
        this.entities = new HashMap<>();
        createConnectors();
    }

    @Override
    public Status getStatus() {
        try {
            readTemperature();
            return Status.OPTIMAL_SERVICE;
        } catch (HALException ex) {
            return Status.NO_SERVICE;
        }
    }

    @Override
    public Celcius readTemperature() throws HALException {
        try {
            PrivaEntity res = this.readScad(getConfiguration().getIndoor_temp_scad_id());
            int rawValue = res.getRawValue();
            return new Celcius(convertFromPrivaRawToDouble(rawValue));
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read indoor temperature.", ex);
        }
    }

    @Override
    public Celcius readOutdoorTemperature() throws HALException {
        try {
            PrivaEntity res = this.readScad(getConfiguration().getOutdoor_temp_scad_id());
            int rawValue = res.getRawValue();
            return new Celcius(convertFromPrivaRawToDouble(rawValue));
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read indoor temperature.", ex);
        }
    }

    @Override
    public PPM readCO2() throws HALException {
        try {
            PrivaEntity res = this.readScad(getConfiguration().getCo2_scad_id());
            double rawValue = res.getRawValue();
            return new PPM(rawValue);
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read indoor temperature.", ex);
        }
    }

    @Override
    public UMolSqrtMeterSecond readLightIntensity() throws HALException {
        try {
            //TODO: Not the correct light intensity. It is W/m^2 atm.
            //TODO: Get the param from paramCode.
            PrivaEntity res = this.readScad(getConfiguration().getIndoor_light_intensity_scad_id());
            double rawValue = res.getRawValue();
            return new UMolSqrtMeterSecond(rawValue);
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read light intensity.", ex);
        }
    }

    @Override
    public Percent readHumidity() throws HALException {
        try {
            PrivaEntity res = this.readScad(getConfiguration().getHumidity_scad_id());
            int rawValue = res.getRawValue();
            return new Percent(convertFromPrivaRawToDouble(rawValue));
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read humidity.", ex);
        }
    }

    @Override
    public WattSqrMeter readOutdoorLightIntensity() throws HALException {
        try {
            PrivaEntity res = this.readScad(getConfiguration().getOutdoor_light_intensity_scad_id());
            double rawValue = res.getRawValue();
            return new WattSqrMeter(rawValue);
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read outdoor light.", ex);
        }
    }

    @Override
    public Switch readArtficialLightStatus(String lightGroup) throws HALException {
        try {
            PrivaEntity res = this.readScad(lightGroup);
            return res.getCurrentToggleValue().getValueAsInt() == 2 ? Switch.ON : Switch.OFF;
        } catch (PrivaServerException ex) {
            throw new HALException("Unable to read artificial light status.", ex);
        }
    }

    @Override
    public Percent readWindowOpening() throws HALException {
        PrivaEntity e = null;
        for (String lg : getConfiguration().getWindow_sensors()) {
            try {
                e = this.readScad(lg);
            } catch (PrivaServerException ex) {
                throw new HALException("Unable to write screen position.", ex);
            }
        }
        return new Percent(this.convertFromPrivaRawToDouble(e.getRawValue()));
    }

    @Override
    public void writeHeatingThreshold(Celcius c) throws HALException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //TODO Don't have the address for setting/overwriting this point yet.
        /*
         try {
         this.writeScadRaw(getConfiguration().getHeating_threshold_scad_id(), convertFromDoubleToPrivaRaw(readServer.value()));
         } catch (PrivaServerException ex) {
         throw new HALException("Unable to write heating threshold.", ex);
         }*/
    }

    @Override
    public void writeVentilationThreshold(Celcius c) throws HALException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //TODO Don't have the address for setting/overwriting this point yet.
        /*try {
         this.writeScadRaw(getConfiguration().getVentilation_threshold_scad_id(), convertFromDoubleToPrivaRaw(readServer.value()));
         } catch (PrivaServerException ex) {
         throw new HALException("Unable to write ventilation threshold.", ex);
         }*/
    }

    public PrivaConfig getConfiguration() {
        // Get from db?
        if (cfg == null) {
            cfg = context(g).one(ClimateDataAccess.class).getPrivaConfiguration();
        }

        return cfg;
    }

    @Override
    public void writeCO2Threshold(PPM p) throws HALException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //TODO Don't have address for this SCAD yet.
        /*try {
         this.writeScadRaw(getConfiguration().getCo2_threshold_scad_id(), convertFromDoubleToPrivaRaw(p.value()));
         } catch (PrivaServerException ex) {
         throw new HALException("Unable to write heating threshold.", ex);
         }
         */
    }

    @Override
    public void writeLightStatus(Switch s) throws HALException {
        int toggleValue = s.isOn() ? 2 : 1;
        for (String lg : getConfiguration().getLightGroups()) {
            try {
                //Check if light has been read yet.
                if (entities.get(lg) == null) {
                    //If not, read light to get possible toggle values.
                    readArtficialLightStatus(lg);
                }
                PrivaEntity e = entities.get(lg);
                //Get toggle value from list of possible toggle values in the entity.
                //Alternatively, a new instance of PrivaListValue could be made, but current approach ensures that we write a valid value.
                writeScadFormatted(lg, new String(e.getPossibleToggleValues().get(toggleValue).getName()).trim());
               
            } catch (PrivaServerException ex) {
                throw new HALException("Unable to write light status.", ex);
            }
        }
    }

    @Override
    public void writeGeneralScreenPosition(Percent p) throws HALException {
        //TODO Make sure that static screen position is active.
        for (String lg : getConfiguration().getScreens()) {
            try {
                this.writeScadRaw(lg, this.convertFromDoubleToPrivaRaw(p.asPercent()));
            } catch (PrivaServerException ex) {
                throw new HALException("Unable to write screen position.", ex);
            }
        }
    }

    @Override
    public void dispose() {
        //TODO Make sure to do proper dispossal when needed.
    }

    private Double convertFromPrivaRawToDouble(int rawValue) {
        //Remove trailing zero
        int convertedRaw = rawValue / 10;
        //Use last number in the int as fraction
        return (double) convertedRaw / 10.0;
    }

    private int convertFromDoubleToPrivaRaw(Double value) {
        BigDecimal numToConvert = new BigDecimal(value);
        //Round to one decimal - round 0.5 up.
        numToConvert = numToConvert.setScale(1, RoundingMode.HALF_UP);
        //Multiply by 100 to get first fraction converted to int.
        numToConvert = numToConvert.multiply(new BigDecimal(100));

        return numToConvert.intValue();
    }

    private PrivaEntity readScad(String scad) throws PrivaServerException {
        PrivaEntity res = readServer.readScad(scad);
        this.entities.put(scad, res);
        return res;
    }

    private void writeToggle(String scad, PrivaListValue toggle) throws PrivaServerException {
        writeServer.writeToggle(scad, toggle);
    }

    private void writeScadRaw(String scad, int value) throws PrivaServerException {
        writeServer.writeRawValue(scad, value);
    }

    private void writeScadFormatted(String scad, String value) throws PrivaServerException {
        writeServer.writeFormattedValue(scad, value);
    }

    private void writeScadRaw(String scad, int value, PrivaListValue attrib) throws PrivaServerException {
        writeServer.writeRawValueWithAttribute(scad, value, attrib);
    }

    private void writeScadFormatted(String scad, String value, PrivaListValue attrib) throws PrivaServerException {
        writeServer.writeFormattedValueWithAttribute(scad, value, attrib);
    }

    private void createConnectors()  {
        readServer = new PrivaServer(getConfiguration().getPcuName());
        writeServer = new PrivaServer(getConfiguration().getPcuName());
    }

    @Override
    public List<String> readLightGroups() throws HALException {
        return this.cfg.getLightGroups();
    }
}
