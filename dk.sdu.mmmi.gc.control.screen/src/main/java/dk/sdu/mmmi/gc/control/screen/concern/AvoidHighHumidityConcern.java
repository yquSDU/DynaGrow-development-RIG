package dk.sdu.mmmi.gc.control.screen.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.input.IndoorHumidityInput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import dk.sdu.mmmi.gc.control.screen.input.IndoorHumidityMaximumInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenClosingInput;
import java.util.HashMap;
import java.util.Map;

/**
 * Avoid excessive humidity that can cause plant disease
 *
 * @author jcs
 */
public class AvoidHighHumidityConcern extends AbstractConcern {

    public AvoidHighHumidityConcern(ControlDomain g) {
        super("Avoid excessive humidity", g, HARD_PRIORITY);
    }

    /**
     */
    @Override
    public double evaluate(Solution s) {
        double scrPos = s.getValue(GeneralScreenPositionOutput.class).asFactor();
        double scrClosingPct = s.getValue(ScreenClosingInput.class).asFactor();

        if (isHumidityHigh(s)) {
            return (scrPos <= scrClosingPct) ? 0 : 1;
        }

        return true ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        double scrClosingPct = s.getValue(ScreenClosingInput.class).asPercent();

        if (isHumidityHigh(s)) {
            r.put(GeneralScreenPositionOutput.class, String.format("ensure %.1f or less", scrClosingPct));
        }

        return r;
    }

//    /**
//     * @param s Solution
//     * @return If humidity is above max limit then a solution has better fitness
//     * the closer the screen position is to 90%. I.e. using the screens to
//     * protect against high humidity.
//     */
//    @Override
//    protected double fitness(Solution s) {
//
//        double scrPos = s.getValue(GeneralScreenPositionOutput.class).asFactor();
//        double scrClosingPct = s.getValue(ScreenClosingInput.class).asFactor();
//        double fitness = 0;
//
//        if (isHumidityHigh(s)) {
//            fitness = MathUtil.absDifference(scrPos, scrClosingPct);
//        }
//
//        return fitness;
//    }
//
//    @Override
//    protected Map<Class, String> getValueHelp(Solution s) {
//        Map<Class,String> r = new HashMap<Class, String>();
//
//        double scrClosingPct = s.getValue(ScreenClosingInput.class).asPercent();
//
//        if(isHumidityHigh(s)) {
//            r.put(GeneralScreenPositionOutput.class, String.format("close to %.1f", scrClosingPct));
//        }
//
//        return r;
//    }
    private boolean isHumidityHigh(Solution s) {
        double hMax = s.getValue(IndoorHumidityMaximumInput.class).asFactor();
        double h = s.getValue(IndoorHumidityInput.class).asFactor();
        return h > hMax;
    }
}
