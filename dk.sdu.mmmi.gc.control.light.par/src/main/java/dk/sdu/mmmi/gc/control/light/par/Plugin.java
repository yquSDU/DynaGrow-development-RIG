package dk.sdu.mmmi.gc.control.light.par;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.par.concern.AchievePARSumConcern;
import dk.sdu.mmmi.gc.control.light.par.concern.PARSumBalanceConcern;
import dk.sdu.mmmi.gc.control.light.par.input.ExpNaturalPARSumPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.ExpNaturalPARSumTodayInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARBalanceYesterdayInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumAchievedTodayInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumDayGoalInput;
import dk.sdu.mmmi.gc.control.light.par.series.ExpNaturalPARSumPeriodSeries;
import dk.sdu.mmmi.gc.control.light.par.series.ExpNaturalPARSumTodaySeries;
import dk.sdu.mmmi.gc.control.light.par.series.PARSumAchievedPeriodSeries;
import dk.sdu.mmmi.gc.control.light.par.series.PARSumAchievedTodaySeries;
import dk.sdu.mmmi.gc.control.light.par.series.PARSumDayGoalSeries;
import dk.sdu.mmmi.gc.impl.entities.config.PARSumAchievedConfig;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {

        DisposableList d = new DisposableList();

        // Concerns
        createAchievePARSumConcern(d, g);
        createPARSumBalanceConcern(d, g);

        // Inputs
        createPARSumDayGoalInput(d, g);

        createPARSumAchievedPeriodInput(d, g);
        createExpNaturalPARSumPeriodInput(d, g);

        createPARSumAchievedTodayInput(d, g);
        createExpNaturalPARSumTodayInput(d, g);
        //createPARBalanceYesterdayInput(d, g);

        retrievePARSumAchievedConfig(d, g);
        // Data.
        //d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, preferPhotoGrowthConcern)));
        return d;
    }

    //
    // Configs
    //
    private PARSumAchievedConfig retrievePARSumAchievedConfig(DisposableList d, ControlDomain g) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        PARSumAchievedConfig cfg = db.selectPARSumAchievedConfig();
        d.add(context(g).add(PARSumAchievedConfig.class, cfg));
        return cfg;
    }

    //
    // Inputs
    //
    private void createPARBalanceYesterdayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PARBalanceYesterdayInput(g)));
    }

    private void createExpNaturalPARSumPeriodInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ExpNaturalPARSumPeriodInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ExpNaturalPARSumPeriodSeries(g)));
    }

    private void createPARSumAchievedPeriodInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PARSumAchievedPeriodInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PARSumAchievedPeriodSeries(g)));
    }

    private void createPARSumAchievedTodayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PARSumAchievedTodayInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PARSumAchievedTodaySeries(g)));
    }

    private void createExpNaturalPARSumTodayInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new ExpNaturalPARSumTodayInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new ExpNaturalPARSumTodaySeries(g)));
    }

    private void createPARSumDayGoalInput(DisposableList d, ControlDomain g) {
        d.add(context(g).add(Input.class, new PARSumDayGoalInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new PARSumDayGoalSeries(g)));
    }

    //
    // Concerns
    //
    private PARSumBalanceConcern createPARSumBalanceConcern(DisposableList d, ControlDomain g) {
        PARSumBalanceConcern parSumBalanceConcern = new PARSumBalanceConcern(g);
        cm.loadConcernConfig(g, parSumBalanceConcern);
        d.add(context(g).add(Concern.class, parSumBalanceConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, parSumBalanceConcern)));
        return parSumBalanceConcern;
    }

    private AchievePARSumConcern createAchievePARSumConcern(DisposableList d, ControlDomain g) {
        AchievePARSumConcern achievePARSumConcern = new AchievePARSumConcern(g);
        cm.loadConcernConfig(g, achievePARSumConcern);
        d.add(context(g).add(Concern.class, achievePARSumConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, achievePARSumConcern)));
        return achievePARSumConcern;
    }
}
