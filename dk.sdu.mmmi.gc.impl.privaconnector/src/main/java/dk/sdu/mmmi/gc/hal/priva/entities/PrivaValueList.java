/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.gc.hal.priva.entities;


import com.sun.jna.Memory;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ancla
 */
public class PrivaValueList extends ArrayList<PrivaListValue> {
    private final int count;
    private final Memory size;
    
    public PrivaValueList(int count, Memory size)
    {
        this.count = count;
        this.size = size;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @return the size
     */
    public Memory getSize() {
        return size;
    }
    
    
    public PrivaListValue getByName(byte[] name)
    {
        for(PrivaListValue v : this)
        {
            if(Arrays.equals(v.getName(), name))
                return v;
        }
        return null;
    }
    
}