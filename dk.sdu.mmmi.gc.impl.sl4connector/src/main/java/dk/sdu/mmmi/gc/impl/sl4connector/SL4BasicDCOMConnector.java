package dk.sdu.mmmi.gc.impl.sl4connector;

import dk.sdu.mmmi.gc.impl.sl4connector.protocol.DCOMObjectAdapter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.common.JISystem;
import org.jinterop.dcom.core.IJIComObject;
import org.jinterop.dcom.core.JIComServer;
import org.jinterop.dcom.core.JIProgId;
import org.jinterop.dcom.core.JISession;
import org.jinterop.dcom.impls.JIObjectFactory;
import org.jinterop.dcom.impls.automation.IJIDispatch;

/**
 * @author mrj
 */
public final class SL4BasicDCOMConnector implements SL4DCOMConnector {
//    static {
//        // Get rid of:
//        // java.lang.IllegalStateException: You are trying to access file:
//        // progIdVsClsidDB.properties from the default package.
//        //
//        // Please notify us if you find a more beautiful solution to this
//        // problem ... /mrj, ao
//        Logger log = Logger.getLogger("org.netbeans.ProxyClassLoader");
//        Level old = log.getLevel();
//        log.setLevel(Level.SEVERE);
//        JIProgId.valueOf(APP_OBJECT);
//        log.setLevel(old);
//    }

    private static final Logger LOG = Logger.getLogger(SL4BasicDCOMConnector.class.getName());
    private final static int SOCKET_TIMEOUT_MS = 500;
    private final static String APP_OBJECT = "zenOn.Application";
    /**
     * We have experienced problems with dirty reads just after writes. We see
     * no good solution to this problem. The therefore sleep a short while to
     * minimize the risk. This seams to work with the systems we tested it on.
     * However, there is no guarantees!
     */
    private final static long POST_WRITE_DELAY = 200;
    private final static long PRE_CONNECT_DELAY = 200;
    private final DCOMObjectAdapter server;
    private final JISession session;

    /**
     * Creates connection to SL4 using DCOM.
     *
     * @param account
     * @throws dk.sdu.mmmi.gc.impl.sl4connector.SL4DCOMException
     */
    public SL4BasicDCOMConnector(SL4Login account)
            throws SL4DCOMException {

        LOG.log(Level.INFO, "Connecting to {0}", account.toString());

        // XXX We have experienced problems when creating connections often.
        // Sleeping a bit seems to avoid to problem. But it's ugly.
//        try {
//            Thread.sleep(PRE_CONNECT_DELAY);
//        } catch (Exception x) {
//        }
        try {
            // Disable logging to stdout.
            JISystem.setInBuiltLogHandler(false);
            JISystem.getLogger().setLevel(Level.SEVERE);

            // Create COM session
            session = JISession.createSession(account.getDomain(), account.getUsername(), account.getPassword());
            //session.setGlobalSocketTimeout(SOCKET_TIMEOUT_MS);
            session.useSessionSecurity(true);

            // Create COM server
            JIComServer comServer = new JIComServer(JIProgId.valueOf(APP_OBJECT), account.getHostname(), session);
            IJIComObject comObj = comServer.createInstance();

            IJIDispatch serverDispatch = (IJIDispatch) JIObjectFactory.narrowObject(comObj.queryInterface(IJIDispatch.IID));
            server = new DCOMObjectAdapter(serverDispatch);
        } catch (SecurityException | IOException | JIException x) {
            close();
            throw new SL4DCOMException("Opening SL connection failed.", x);
        }
    }

    /**
     * Close connection.
     */
    @Override
    public void close() throws SL4DCOMException {

        LOG.log(Level.INFO, "closing connection");

        try {
            JISession.destroySession(session);
        } catch (JIException x) {
            throw new SL4DCOMException("Closing SL connection failed.", x);
        }
    }

    /**
     * Reads a value from the SL4 server.
     */
    @Override
    public double readValue(String projectID, String variableID) throws SL4DCOMException {

        try {
            return server
                    .invoke("Projects")
                    .invoke("Item", projectID)
                    .invoke("Variables")
                    .invoke("Item", variableID)
                    .getDouble("Value");

        } catch (Throwable x) {
            throw new SL4DCOMException("Reading value from SL failed.", x);
        }
    }

    /**
     * Writes a value to the SL4 server.
     */
    @Override
    public void writeValue(String projectID, String variableID, double value)
            throws SL4DCOMException {

        try {
                DCOMObjectAdapter variable = server.invoke("Projects")
                        .invoke("Item", projectID)
                        .invoke("Variables")
                        .invoke("Item", variableID);

                variable.setDouble("Value", value);

            // XXX Sleep due to problems with dirty reads just after write.
            Thread.sleep(POST_WRITE_DELAY);

        } catch (JIException | InterruptedException x) {
            throw new SL4DCOMException("Writing value to SL failed.", x);
        }
    }
}
