package dk.sdu.mmmi.gc.control.photo.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class ActualPhotosynthesisSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public ActualPhotosynthesisSeries(ControlDomain g) {
        this.g = g;
        setName("Actual photosynthesis");
        setUnit("[umol m^-2 s^-1]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<UMolSqrtMeterSecond>> raw = db.selectActualPhoto(from, to);
            for (Sample<UMolSqrtMeterSecond> s : raw) {
                Date t = s.getTimestamp();
                Double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
}
