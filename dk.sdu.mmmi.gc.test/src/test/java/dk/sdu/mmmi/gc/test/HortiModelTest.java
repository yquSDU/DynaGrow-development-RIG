package dk.sdu.mmmi.gc.test;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class HortiModelTest {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private GreenhouseTestFixture g;

    @Before
    public void setUp() throws SQLException {
    }

    @After
    public void tearDown() throws SQLException {
        //g.dispose();
        g = null;
    }

    /**
     * V1: The extensibility experiment evaluates the extensibility of our
     * control system. First, we describe a basic version of our system that is
     * capable of controlling a greenhouse. The system is built from 6 hard
     * specifications (HeatOverheat, CO2Waste, WinWaste, Temp. Limits, CO2
     * limits, ScreenHumidity) which must always be satisfied and 6 soft
     * specifications (SrnIso, SrnShade and day/night thresholds) that the user
     * may prioritize.
     */
    @Ignore
    @Test
    public void hortiModelExperimentV1() throws Exception {
        // SETUP: V1
        g = new GreenhouseTestFixture("HortiModelExpV1", df.parse("2012-04-21 23:00")).
                // Environment
                avoidHighHumidityFeature().
                avoidOverheatingFeature(30).
                heatingWasteAvoidanceFeature().
                co2WasteAvoidanceFeature().
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                // V1
                preferNoLightFeature(5).
                dayNightVentFeature(1, 22, 20).
                dayNightTempFeature(5, 19, 18).
                dayNightScreenFeature(5, 0, 0).
                dayNightCO2Feature(5, 750, 350).
                isolationScreenFeature(3).
                shadingScreenFeature(3, 1200);

        // TEST:
        Date from = df.parse("2012-04-21 23:40");
        Date to = df.parse("2012-04-22 23:59");

        g.startNegotiation(from, to);
        g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : g.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }

    /**
     * V2: Second, we present a version of our system extended with
     * photosynthesis optimizing temperature and CO2 control. The new features
     * are implemented as two soft requirements (HeatPhoto and CO2Photo).
     */
    //@Ignore
    @Test
    public void hortiModelExperimentV2() throws Exception {
        // SETUP: V2
        g = new GreenhouseTestFixture("HortiModelExpV2", df.parse("2012-04-21 23:00")).
                // Environment
                //avoidHighHumidityFeature().
                avoidOverheatingFeature(30).
                heatingWasteAvoidanceFeature().
                co2WasteAvoidanceFeature().
                rampFeatures().
                tempLimitsFeature(5, 30).
                co2LimitsFeature(300, 1500).
                // V1
                preferNoLightFeature(5).
                dayNightVentFeature(1, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                isolationScreenFeature(3).
                shadingScreenFeature(3, 1200).
                // V2
                photoOptFeature(3);
        // TEST:
        Date from = df.parse("2012-04-21 23:40");
        Date to = df.parse("2012-04-22 23:59");
        g.startNegotiation(from, to);
        g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : g.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }

    }

    /**
     * V3: Finally, we present an extension of our system with a new screen
     * control concern that also promotes photosynthesis (ScrnPhoto).
     */
    //  @Ignore
    @Test
    public void hortiModelExperimentV3() throws Exception {
        // SETUP: V3
        g = new GreenhouseTestFixture("HortiModelExpV3", df.parse("2012-04-21 23:00")).
                // Environment
                avoidHighHumidityFeature().
                avoidOverheatingFeature(30).
                heatingWasteAvoidanceFeature().
                co2WasteAvoidanceFeature().
                rampFeatures().
                tempLimitsFeature(5, 50).
                co2LimitsFeature(300, 1500).
                // V1
                preferNoLightFeature(5).
                dayNightVentFeature(1, 22, 20).
                dayNightScreenFeature(5, 0, 0).
                dayNightTempFeature(5, 19, 18).
                dayNightCO2Feature(5, 750, 350).
                isolationScreenFeature(3).
                shadingScreenFeature(3, 1200).
                // V2
                photoOptFeature(3).
                // V3
                photoScreenFeature(2, 10, 20);

        // TEST:
        Date from = df.parse("2012-04-21 23:40");
        Date to = df.parse("2012-04-22 23:59");
        g.startNegotiation(from, to);
        g.exportTestData(from, to);

        // ASSERT:
        for (Solution solution : g.getSolutionList()) {
            assertTrue(solution.getEvaluationResult(Concern.HARD_PRIORITY) == 0);
        }
    }
}
