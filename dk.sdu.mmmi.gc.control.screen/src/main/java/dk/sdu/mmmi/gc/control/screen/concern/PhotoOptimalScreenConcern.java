package dk.sdu.mmmi.gc.control.screen.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.control.commons.input.CO2Input;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureInput;
import dk.sdu.mmmi.gc.control.commons.input.LightTransmissionFactorInput;
import dk.sdu.mmmi.gc.control.commons.input.OutdoorLightInput;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.fromSunToPAR;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.screen.input.ScreenPhotoLossRatioInput;
import dk.sdu.mmmi.gc.control.screen.input.ScreenTransmissionFactorInput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class PhotoOptimalScreenConcern extends AbstractConcern {

    public PhotoOptimalScreenConcern(ControlDomain g) {
        super("Photosynthesis-optimal screen", g, 2);
    }

    @Override
    public double evaluate(Solution s) {
        double scrPos = s.getValue(GeneralScreenPositionOutput.class).asFactor();

        double fitness;
        if (isMinimiumPhotosynthesisAchieved(s) && !isPhotoNotLostWithScreens(s)) {
            // There is a loss with screens. Prefer no screens.
            fitness = scrPos;
        } else {
            // No loss. Don't care about screen position.
            fitness = Math.abs(1 - scrPos);
        }
        return fitness;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (isMinimiumPhotosynthesisAchieved(s) && !isPhotoNotLostWithScreens(s)) {
            r.put(GeneralScreenPositionOutput.class, "prefer low coverage");
        } else {
            //r.put(GeneralScreenPositionOutput.class, "prefer high coverage");
        }

        return r;
    }

    private boolean isMinimiumPhotosynthesisAchieved(Solution s) {
        //Inputs
        Percent greenTrans = s.getValue(LightTransmissionFactorInput.class);
        WattSqrMeter outSunLight = s.getValue(OutdoorLightInput.class);
        UMolSqrtMeterSecond parWithoutScreens = fromSunToPAR(outSunLight.times(greenTrans));
        Celcius temp = s.getValue(IndoorTemperatureInput.class);
        PPM co2 = s.getValue(CO2Input.class);

        // Calculated input
        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
        UMolSqrtMeterSecond photoWithoutScreens = photoUtil.calcPhoto(temp, co2, parWithoutScreens);

        return photoWithoutScreens.value() > 1; // TODO: Externalize this minimum value.
    }

    private boolean isPhotoNotLostWithScreens(Solution s) {
        //Inputs
        Percent greenTrans = s.getValue(LightTransmissionFactorInput.class);
        Percent screenTrans = s.getValue(ScreenTransmissionFactorInput.class);
        WattSqrMeter outSunLight = s.getValue(OutdoorLightInput.class);
        UMolSqrtMeterSecond parWithoutScreens = fromSunToPAR(outSunLight.times(greenTrans));
        UMolSqrtMeterSecond parWithScreens = parWithoutScreens.times(screenTrans);
        Celcius temp = s.getValue(IndoorTemperatureInput.class);
        PPM co2 = s.getValue(CO2Input.class);
        double photoLossWoScreen = s.getValue(ScreenPhotoLossRatioInput.class).asPercent();

        // Calculated input
        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);

        // TODO: Implement photoWithScreens and photoWithoutScreens as inputs instead of cached photoUtil.
        UMolSqrtMeterSecond photoWithScreens = photoUtil.calcPhoto(temp, co2, parWithScreens);
        UMolSqrtMeterSecond photoWithoutScreens = photoUtil.calcPhoto(temp, co2, parWithoutScreens);

        UMolSqrtMeterSecond preservedPhotoPct = photoWithScreens.divide(photoWithoutScreens);

        // if photo is not harmed by screen then try to put screen on
        boolean photoNotLost = preservedPhotoPct.value() > photoLossWoScreen;
        return photoNotLost;
    }
}
