package dk.sdu.mmmi.gc.control.basic;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.control.commons.series.ConcernFitnessTimeSeries;
import dk.sdu.mmmi.gc.control.basic.concern.CO2LimitsConcern;
import dk.sdu.mmmi.gc.control.basic.concern.DailyAverageTempConcern;
import dk.sdu.mmmi.gc.control.basic.concern.DayNightCO2Concern;
import dk.sdu.mmmi.gc.control.basic.concern.DayNightScreenConcern;
import dk.sdu.mmmi.gc.control.basic.concern.DayNightTemperatureConcern;
import dk.sdu.mmmi.gc.control.basic.concern.DayNightVentilationConcern;
import dk.sdu.mmmi.gc.control.basic.concern.SimpleCO2RampConcern;
import dk.sdu.mmmi.gc.control.basic.concern.SimpleHeatingRampConcern;
import dk.sdu.mmmi.gc.control.basic.concern.SimpleScreenRampConcern;
import dk.sdu.mmmi.gc.control.basic.concern.SimpleVentRampConcern;
import dk.sdu.mmmi.gc.control.basic.concern.TemperatureLimitsConcern;
import dk.sdu.mmmi.gc.control.basic.input.DailyAverageTempGoalInput;
import dk.sdu.mmmi.gc.control.basic.input.DailyAverageTempInput;
import dk.sdu.mmmi.gc.control.basic.input.DayCO2Input;
import dk.sdu.mmmi.gc.control.basic.input.DayScreenPositionInput;
import dk.sdu.mmmi.gc.control.basic.input.DayTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.input.DayVentilationTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.input.NightCO2Input;
import dk.sdu.mmmi.gc.control.basic.input.NightScreenPositionInput;
import dk.sdu.mmmi.gc.control.basic.input.NightTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.input.NightVentilationTemperatureInput;
import dk.sdu.mmmi.gc.control.basic.series.DailyAverageTempGoalSeries;
import dk.sdu.mmmi.gc.control.basic.series.DailyAverageTempSeries;
import dk.sdu.mmmi.gc.control.basic.series.DayCO2Series;
import dk.sdu.mmmi.gc.control.basic.series.DayScreenPositionSeries;
import dk.sdu.mmmi.gc.control.basic.series.DayTemperatureSeries;
import dk.sdu.mmmi.gc.control.basic.series.DayVentilationTemperatureSeries;
import dk.sdu.mmmi.gc.control.basic.series.NegotiationGenerationsSeries;
import dk.sdu.mmmi.gc.control.basic.series.NegotiationTimeSeries;
import dk.sdu.mmmi.gc.control.basic.series.NightCO2Series;
import dk.sdu.mmmi.gc.control.basic.series.NightScreenPositionSeries;
import dk.sdu.mmmi.gc.control.basic.series.NightTemperatureSeries;
import dk.sdu.mmmi.gc.control.basic.series.NightVentilationTemperatureSeries;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ControlDomainManager cm = Lookup.getDefault().lookup(ControlDomainManager.class);

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        // Inputs
        createDailyAverageTempInput(g, d);
        createDayTemperatureInput(g, d);
        createNightTemperatureInput(g, d);
        createDayCO2Input(g, d);
        createNightCO2Input(g, d);
        createDayScreenPositionInput(g, d);
        createNightScreenPositionInput(g, d);
        createDayVentilationTemperatureInput(g, d);
        createNightVentilationTemperatureInput(g, d);

        // Concerns.
        createTemperatureLimitsConcern(g, d);
        createCO2LimitsConcern(g, d);
        createDailyAverageTempConcern(g, d);
        createSimpleCO2RampConcern(g, d);
        createSimpleHeatingRampConcern(g, d);
        createSimpleScreenRampConcern(g, d);
        createSimpleVentRampConcern(g, d);
        createDayNightTemperatureConcern(g, d);
        createDayNightCO2Concern(g, d);
        createDayNightScreenConcern(g, d);
        createDayNightVentilationConcern(g, d);

        // Dataseries.
        d.add(context(g).add(DoubleTimeSeries.class, new NegotiationGenerationsSeries(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new NegotiationTimeSeries(g)));

        return d;
    }

    private void createNightVentilationTemperatureInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new NightVentilationTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new NightVentilationTemperatureSeries(g)));
    }

    private void createDayVentilationTemperatureInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new DayVentilationTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DayVentilationTemperatureSeries(g)));
    }

    private void createNightScreenPositionInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new NightScreenPositionInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new NightScreenPositionSeries(g)));
    }

    private void createDayScreenPositionInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new DayScreenPositionInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DayScreenPositionSeries(g)));
    }

    private void createNightCO2Input(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new NightCO2Input(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new NightCO2Series(g)));
    }

    private void createDayCO2Input(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new DayCO2Input(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DayCO2Series(g)));
    }

    private void createNightTemperatureInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new NightTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new NightTemperatureSeries(g)));
    }

    private void createDayTemperatureInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new DayTemperatureInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DayTemperatureSeries(g)));
    }

    private void createDailyAverageTempInput(ControlDomain g, DisposableList d) {
        d.add(context(g).add(Input.class, new DailyAverageTempGoalInput(g)));
        d.add(context(g).add(Input.class, new DailyAverageTempInput(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DailyAverageTempGoalSeries(g)));
        d.add(context(g).add(DoubleTimeSeries.class, new DailyAverageTempSeries(g)));
    }

    private DayNightVentilationConcern createDayNightVentilationConcern(ControlDomain g, DisposableList d) {
        DayNightVentilationConcern dayNightVent = new DayNightVentilationConcern(g);
        cm.loadConcernConfig(g, dayNightVent);
        d.add(context(g).add(Concern.class, dayNightVent));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, dayNightVent)));
        return dayNightVent;
    }

    private DayNightScreenConcern createDayNightScreenConcern(ControlDomain g, DisposableList d) {
        DayNightScreenConcern dayNightScreen = new DayNightScreenConcern(g);
        cm.loadConcernConfig(g, dayNightScreen);
        d.add(context(g).add(Concern.class, dayNightScreen));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, dayNightScreen)));
        return dayNightScreen;
    }

    private DayNightCO2Concern createDayNightCO2Concern(ControlDomain g, DisposableList d) {
        DayNightCO2Concern dayNightCO2 = new DayNightCO2Concern(g);
        cm.loadConcernConfig(g, dayNightCO2);
        d.add(context(g).add(Concern.class, dayNightCO2));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, dayNightCO2)));
        return dayNightCO2;
    }

    private DayNightTemperatureConcern createDayNightTemperatureConcern(ControlDomain g, DisposableList d) {
        DayNightTemperatureConcern dayNightTemp = new DayNightTemperatureConcern(g, 5);
        cm.loadConcernConfig(g, dayNightTemp);
        d.add(context(g).add(Concern.class, dayNightTemp));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, dayNightTemp)));
        return dayNightTemp;
    }

    private SimpleVentRampConcern createSimpleVentRampConcern(ControlDomain g, DisposableList d) {
        SimpleVentRampConcern simpleVentRampConcern = new SimpleVentRampConcern(g);
        cm.loadConcernConfig(g, simpleVentRampConcern);
        d.add(context(g).add(Concern.class, simpleVentRampConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, simpleVentRampConcern)));
        return simpleVentRampConcern;
    }

    private SimpleScreenRampConcern createSimpleScreenRampConcern(ControlDomain g, DisposableList d) {
        SimpleScreenRampConcern simpleScreenRampConcern = new SimpleScreenRampConcern(g);
        cm.loadConcernConfig(g, simpleScreenRampConcern);
        d.add(context(g).add(Concern.class, simpleScreenRampConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, simpleScreenRampConcern)));
        return simpleScreenRampConcern;
    }

    private SimpleHeatingRampConcern createSimpleHeatingRampConcern(ControlDomain g, DisposableList d) {
        SimpleHeatingRampConcern simpleHeatingRampConcern = new SimpleHeatingRampConcern(g);
        cm.loadConcernConfig(g, simpleHeatingRampConcern);
        d.add(context(g).add(Concern.class, simpleHeatingRampConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, simpleHeatingRampConcern)));
        return simpleHeatingRampConcern;
    }

    private SimpleCO2RampConcern createSimpleCO2RampConcern(ControlDomain g, DisposableList d) {
        SimpleCO2RampConcern simpleCO2RampConcern = new SimpleCO2RampConcern(g);
        cm.loadConcernConfig(g, simpleCO2RampConcern);
        d.add(context(g).add(Concern.class, simpleCO2RampConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, simpleCO2RampConcern)));
        return simpleCO2RampConcern;
    }

    private DailyAverageTempConcern createDailyAverageTempConcern(ControlDomain g, DisposableList d) {
        DailyAverageTempConcern dailyAverageTempConcern = new DailyAverageTempConcern(g);
        cm.loadConcernConfig(g, dailyAverageTempConcern);
        d.add(context(g).add(Concern.class, dailyAverageTempConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, dailyAverageTempConcern)));
        return dailyAverageTempConcern;
    }

    private CO2LimitsConcern createCO2LimitsConcern(ControlDomain g, DisposableList d) {
        CO2LimitsConcern co2BoundariesConcern = new CO2LimitsConcern(g, 350, 1500);
        cm.loadConcernConfig(g, co2BoundariesConcern);
        d.add(context(g).add(Concern.class, co2BoundariesConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, co2BoundariesConcern)));
        return co2BoundariesConcern;
    }

    private TemperatureLimitsConcern createTemperatureLimitsConcern(ControlDomain g, DisposableList d) {
        TemperatureLimitsConcern frostProtectionConcern = new TemperatureLimitsConcern(g);
        cm.loadConcernConfig(g, frostProtectionConcern);
        d.add(context(g).add(Concern.class, frostProtectionConcern));
        d.add(context(g).add(DoubleTimeSeries.class, new ConcernFitnessTimeSeries(g, frostProtectionConcern)));
        return frostProtectionConcern;
    }
}
