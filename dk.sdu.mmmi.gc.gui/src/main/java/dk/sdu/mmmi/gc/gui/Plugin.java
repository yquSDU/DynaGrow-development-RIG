package dk.sdu.mmmi.gc.gui;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.gc.gui.charts.ChartManager;
import dk.sdu.mmmi.gc.gui.explorer.GreenhousesNode;
import dk.sdu.mmmi.gc.gui.explorer.GreenhousesNodeChildren;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final static GreenhousesNodeChildren ghnc = new GreenhousesNodeChildren();
    private final static GreenhousesNode ghsn = new GreenhousesNode(ghnc);

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();

        ChartManager cm = new ChartManager(g);
        cm.loadChartsFromDB();

        d.add(context(g).add(ChartManager.class, cm));
        d.add(context(g).add(ControleumListener.class, ghnc));

        return d;
    }

    public static GreenhousesNode getGreenhousesNode() {
        return ghsn;
    }
}
