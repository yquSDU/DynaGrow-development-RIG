package dk.sdu.mmmi.gc.impl.entities.config;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mrj
 */
public class SuperLinkConfig {

    private final Gson gson = new Gson();
    private String hostname = "hostname",
            domain = "domain",
            username = "username",
            password = "password",
            department = "DEP01";
    private List<String> lightGroups;
    private String indoorLightCode = "51017";

    public SuperLinkConfig() {
        lightGroups = new ArrayList<>();
        lightGroups.add("41650");
        lightGroups.add("41660");
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLightGroupsAsString() {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return gson.toJson(lightGroups, type);
    }

    public List<String> getLightGroups() {
        return lightGroups;
    }

    public void setLightGroups(String lightGroups) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        this.lightGroups = gson.fromJson(lightGroups, type);
    }

    public String getIndoorLightCode() {
        return this.indoorLightCode;
    }

    public void setIndoorLightCode(String code) {
        this.indoorLightCode = code;
    }
}
