package dk.sdu.mmmi.gc.control.screen.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Energy;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import dk.sdu.mmmi.gc.control.screen.input.EnergyBalanceInput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class IsolationConcern extends AbstractConcern {

    public IsolationConcern(ControlDomain g) {
        super("Isolation", g, 3);
    }

    @Override
    public double evaluate(Solution s) {

        // Output
        Percent scrPos = s.getValue(GeneralScreenPositionOutput.class);

        // Find fitness
        double fitness;

        // if pos. energy balance then happy
        if (isEnergyBalancePositive(g, s)) {
            fitness = 0;
        } // if neg. energy balance then happier
        // the more scr. pos is close to 100 pct.
        else {
            fitness = scrPos.asPercent();
        }

        return fitness;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();

        if (!isEnergyBalancePositive(g, s)) {
            r.put(GeneralScreenPositionOutput.class, "prefer high coverage");
        }

        return r;
    }

    private static boolean isEnergyBalancePositive(ControlDomain g, Solution s) {
        Energy sml = s.getValue(EnergyBalanceInput.class);
        assert sml != null;
        Double energyBalance = sml.value();
        return 0 <= energyBalance;
    }
}
