package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class GreenhouseSizeInput extends AbstractInput<SqrMeter> {

    private final ControlDomain g;
    private final Link<Action> action;

    public GreenhouseSizeInput(ControlDomain g) {
        super("Size of Greenhouse");
        this.g = g;
        this.action = context(this).add(Action.class, new GreenhouseSizeInput.ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        // get value from db
        SqrMeter v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private SqrMeter retrieve(Date t) {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<SqrMeter> r = db.selectGreenhouseSize(t);

        return r.getSample();
    }

    private void store(Date t, SqrMeter v) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertGreenhouseSize(new Sample<>(t, v));
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            SqrMeter oldValue = GreenhouseSizeInput.this.getValue();
            SqrMeter newValue = DialogUtil.promtSqrMeter(oldValue, "Size of Greenhouse");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
