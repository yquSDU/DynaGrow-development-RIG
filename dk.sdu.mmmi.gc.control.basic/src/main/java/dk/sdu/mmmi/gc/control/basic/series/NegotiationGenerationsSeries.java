package dk.sdu.mmmi.gc.control.basic.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.results.NegotiationResult;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class NegotiationGenerationsSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public NegotiationGenerationsSeries(ControlDomain g) {
        this.g = g;
        setName("Negotiation cost");
        setUnit("[generations]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<NegotiationResult>> raw = db.selectNegotiationResult(from, to);
            for (Sample<NegotiationResult> s : raw) {
                Date t = s.getTimestamp();
                NegotiationResult v = s.getSample();
                r.add(new DoubleTimeValue(t, v.getGenerationsExecuted().doubleValue()));
            }
        }

        return r;
    }
}
