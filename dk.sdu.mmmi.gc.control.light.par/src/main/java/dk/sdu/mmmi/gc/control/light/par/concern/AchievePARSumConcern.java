package dk.sdu.mmmi.gc.control.light.par.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.gc.control.commons.utils.LightUtil.calcLightplanPARSum;
import dk.sdu.mmmi.gc.control.light.input.LampIntensityInput;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.par.input.ExpNaturalPARSumPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.par.input.PARSumDayGoalInput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj & jcs
 */
public class AchievePARSumConcern extends AbstractConcern {

    private MolSqrMeter totalInPlan;
    private MolSqrMeter balance;
    private MolSqrMeter expNatPARSum;
    private MolSqrMeter goalSum;
    private MolSqrMeter achievedPARSum;

    public AchievePARSumConcern(ControlDomain g) {
        super("Achieve positive PAR sum", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        // Inputs
        // Goal window = DLI for period
        goalSum = s.getValue(PARSumDayGoalInput.class).multiply(3);
        achievedPARSum = s.getValue(PARSumAchievedPeriodInput.class);
        expNatPARSum = s.getValue(ExpNaturalPARSumPeriodInput.class);

        UMolSqrtMeterSecond lampIntensity = s.getValue(LampIntensityInput.class);

        LightPlan p = s.getValue(LightPlanTodayOutput.class);
        totalInPlan = calcLightplanPARSum(p, lampIntensity);

        // PAR sum balance today
        balance = achievedPARSum.add(expNatPARSum).add(totalInPlan).subtract(goalSum);

        return 0.0 <= balance.value() ? 0.0 : 1.0;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class,
                String.format("Balance(%.1f) (Acceptable = %s)",
                        balance.value(), 0 <= balance.value()));

        return r;
    }
}
