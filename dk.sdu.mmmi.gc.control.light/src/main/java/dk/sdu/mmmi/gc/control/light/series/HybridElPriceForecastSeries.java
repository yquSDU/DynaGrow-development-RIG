package dk.sdu.mmmi.gc.control.light.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class HybridElPriceForecastSeries extends DoubleTimeSeries {

    private final ClimateDataAccess db;

    public HybridElPriceForecastSeries(ControlDomain g) {
        setName("El. spot + prognosis prices");
        
        this.db = context(g).one(ClimateDataAccess.class);

        ElSpotPriceForecastConfig elPriceIntegration = db.selectElSpotPriceForecastConfig();
        setUnit(String.format("[%s/MWh]", elPriceIntegration.getCurrency()));
    }

    @Override
    public long getMaxPeriodLenghtMS() {
        return 1000 * 60 * 90;
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {
        List<DoubleTimeValue> r = new ArrayList<>();
       
        for (Sample<MoneyMegaWattHour> s : db.selectHybridElPriceForecast(from, to).asListSampleMoneyMegaWattHour()) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }

        return r;
    }
}
