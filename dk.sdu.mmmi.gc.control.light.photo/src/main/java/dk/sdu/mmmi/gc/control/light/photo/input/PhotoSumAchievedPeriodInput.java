package dk.sdu.mmmi.gc.control.light.photo.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import dk.sdu.mmmi.gc.control.light.input.FrameStartInput;
import dk.sdu.mmmi.gc.control.light.photo.gui.ConfigurePhotoSumAction;
import dk.sdu.mmmi.gc.impl.entities.config.PhotoSumAchievedConfig;
import java.util.Date;
import java.util.List;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PhotoSumAchievedPeriodInput extends AbstractInput<MMolSqrMeter> {

    private final ControlDomain g;
    private final Link<Action> action;

    public PhotoSumAchievedPeriodInput(ControlDomain g) {
        super("Photo. sum archived (window)");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigurePhotoSumAction(g, this));
    }

    @Override
    public void doUpdateValue(Date t) {
        MMolSqrMeter sum = retrieve(t);
        if (sum != null) {
            setValue(sum);
            store(t, sum);
        }
    }

    private MMolSqrMeter retrieve(Date t) {
        // Calculate photo. sum archieved witin period
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        Date startTime = context(g).one(FrameStartInput.class).getValue().toDate();

        List<Sample<UMolSqrtMeterSecond>> indoorLightHistory = db.selectLightIntensity(startTime, t);
        List<Sample<Celcius>> tempHistory = db.selectAirTemperature(startTime, t);
        List<Sample<PPM>> co2History = db.selectCO2(startTime, t);

        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
        Duration controlInterval = context(g).one(ControlManager.class).getDelay();

        PhotoSumAchievedConfig cfg = db.selectPhotoSumAchievedConfig();
        MMolSqrMeter sum;

        if (cfg.isActive()) {
            sum = photoUtil.calcAchievedPhotoSum(tempHistory, co2History, indoorLightHistory, controlInterval);
        } else {
            sum = cfg.getPhotoSum();
        }
        return sum;
    }

    private void store(Date t, MMolSqrMeter mol) {
        // Store prices in internal db
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPhotoSumAchievedPeriod(new Sample<>(t, mol));
    }
}
