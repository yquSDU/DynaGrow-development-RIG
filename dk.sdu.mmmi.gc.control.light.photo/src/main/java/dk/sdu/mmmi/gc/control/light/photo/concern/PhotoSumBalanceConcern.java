package dk.sdu.mmmi.gc.control.light.photo.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumAchievedPeriodInput;
import dk.sdu.mmmi.gc.control.light.photo.input.PhotoSumDayGoalInput;
import dk.sdu.mmmi.gc.control.light.photo.utils.AdvLightUtil;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class PhotoSumBalanceConcern extends AbstractConcern {

    private MMolSqrMeter goal;
    private MMolSqrMeter achievedPhoto;
    private MMolSqrMeter expCombinedPhotosum;
    private MMolSqrMeter balance;

    public PhotoSumBalanceConcern(ControlDomain g) {
        super("Achieve Photo. sum balance", g, 5);
    }

    @Override
    public double evaluate(Solution s) {
        // Inputs
        // Goal window = DPI * (2 day past + today + 2 days future)
        goal = s.getValue(PhotoSumDayGoalInput.class).multiply(3);
        achievedPhoto = s.getValue(PhotoSumAchievedPeriodInput.class);

        // Photo sum exspected from ligth plan and natural light
        LightPlan lightPlan = s.getValue(LightPlanTodayOutput.class);

        // TODO: Use photo. gain but not combined photo. sum. Ask Maersk ;-)
        expCombinedPhotosum = AdvLightUtil.calcExpectedCombinedPhotoSum(g, lightPlan);

        balance = achievedPhoto.add(expCombinedPhotosum).subtract(goal);

        return abs(balance.roundedValue());
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        r.put(LightPlanTodayOutput.class,
                String.format("ExpectedNatArt(%.1f) + Achieved(%.1f)"
                        + " - Goal(%.1f) = Balance(%.1f)",
                        expCombinedPhotosum.value(), achievedPhoto.value(),
                        goal.value(), balance.value()));
        return r;
    }
}
