package dk.sdu.mmmi.gc.control.ventilation.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMaximumInput;
import dk.sdu.mmmi.gc.control.commons.output.VentilationTempOutput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class AllwaysVentilateAboveMaxConcern extends AbstractConcern {

    public AllwaysVentilateAboveMaxConcern(ControlDomain g) {
        super("Avoid overheating", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        Double max = s.getValue(IndoorTemperatureMaximumInput.class).value();
        Double v = s.getValue(VentilationTempOutput.class).value();

        // Always ventilate above maximum temperature.
        return (v <= max) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        double max = s.getValue(IndoorTemperatureMaximumInput.class).value();
        r.put(VentilationTempOutput.class, String.format("below %.1f", max));
        return r;
    }
}
