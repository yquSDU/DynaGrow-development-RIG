package dk.sdu.mmmi.gc.control.basic.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public class DailyAverageTempInput extends AbstractInput<Celcius> {

    private final ControlDomain g;

    public DailyAverageTempInput(ControlDomain g) {
        super("Average temperature");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        Celcius v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Celcius result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertDailyAverageTemperature(new Sample<Celcius>(t, result));
    }

    private Celcius retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Date start = DateUtil.startOfDay(t);
        List<Sample<Celcius>> list = db.selectAirTemperature(start, t);
        Celcius value = avg(list, t);
        return value;
    }

    private Celcius avg(List<Sample<Celcius>> list, Date now) {

        // Check for empty list.
        if (list.isEmpty()) {
            return new Celcius(0d);
        }

        // Add "now"-sample to make avg. calculation easier.
        Sample<Celcius> last = list.get(list.size() - 1);
        list.add(new Sample<>(now, last.getSample()));

        // Calculate avg.
        double sum = 0;
        long ms = 0;
        Sample<Celcius> prev = null;
        for (Sample<Celcius> i : list) {
            if (prev != null) {
                // Add time.
                long dur = i.getTimestamp().getTime() - prev.getTimestamp().getTime();
                ms += dur;

                // Add temperature.
                sum += dur * prev.getSample().value();
            }
            prev = i;
        }

        return new Celcius(sum / ms);
    }
}
