package dk.sdu.mmmi.gc.control.light.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergySpotPriceService;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ElPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import javax.swing.Action;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

/**
 * Combined Electricity prices from NordPool and Energi DK
 *
 * @author jcs
 */
public class HybridElPriceForecastInput extends AbstractInput<ElPriceMWhForecast> {

    private final ControlDomain g;
    private static final Logger logger = getLogger(HybridElPriceForecastInput.class.getName());

    public HybridElPriceForecastInput(ControlDomain g) {        
        super("El. spot & prognosis prices");
        this.g = g;
    }

    //
    // Public
    //
    @Override
    public void doUpdateValue(Date from) {
        try {

            TimeStamp endTime = context(g).one(FrameEndInput.class).getValue();

            // Update Prices from el price integration.
            updateForecast(startOfHour(from), endTime.toDate());

        } catch (EnergyPriceDatabaseException ex) {

            logger.log(Level.WARNING, "No access to electricity price forecast.", ex);
            NotificationDisplayer.getDefault().
                    notify("No electricity price forecast!",
                            ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/warning.gif", true),
                            "Click to deactivate the Electricity price forecast input?",
                            context(this).one(Action.class));
        }

    }

    //
    // Private
    //
    private void updateForecast(Date from, Date to) throws EnergyPriceDatabaseException {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        ElPriceForecastConfig elPriceIntegration = db.selectElPriceForecastConfig();
        ElSpotPriceForecastConfig elSpotPriceIntegration = db.selectElSpotPriceForecastConfig();


        if (elPriceIntegration.isActive() && elSpotPriceIntegration.isActive()) {

            EnergyPriceService prognosisDB = Lookup.getDefault().lookup(EnergyPriceService.class);
            EnergySpotPriceService spotDB = Lookup.getDefault().lookup(EnergySpotPriceService.class);

            // Get prices from el price service
            EnergyPriceDatabaseArea area = elPriceIntegration.getElPriceArea();
            String currency = elPriceIntegration.getCurrency();

            //TODO: Deal with currency conversion. For now everything is in DKK
            Map<Date, Double> pronosisPrices = prognosisDB.getPrices(area, from, to);
            Map<Date, Double> spotPrices = spotDB.getPrices(area, from, to);
            pronosisPrices.putAll(spotPrices);

            ElPriceMWhForecast prognosis = new ElPriceMWhForecast(pronosisPrices, area.toString(), currency);

            // Store prices in internal db
            Sample<ElPriceMWhForecast> s = new Sample<>(from, prognosis);            
            db.insertHybridElPriceForecast(s);
        }

        ElPriceMWhForecast s = db.selectHybridElPriceForecast(from, to);
        if (s != null) {
            setValue(s);
        }
    }
}
