package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergySpotPriceService;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.TimeStamp;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.gui.ConfigureElSpotPriceDBAction;
import dk.sdu.mmmi.gc.impl.entities.config.ElSpotPriceForecastConfig;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import javax.swing.Action;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

/** 
 * Ex.: NordPool spot prices.
 *
 * @author jcs
 */
public class ElSpotPriceMWhForecastInput extends AbstractInput<ElPriceMWhForecast> {

    private final ControlDomain g;
    private static final Logger logger = getLogger(ElSpotPriceMWhForecastInput.class.getName());
    private final Link<Action> action;

    public ElSpotPriceMWhForecastInput(ControlDomain g) {        
        super("El. spot prices");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureElSpotPriceDBAction(g));
    }

    //
    // Public
    //
    @Override
    public void doUpdateValue(Date from) {
        try {
            // Update Prices from el price integration.

            TimeStamp endTime = context(g).one(FrameEndInput.class).getValue();

            updateForecast(startOfHour(from), endTime.toDate());

        } catch (EnergyPriceDatabaseException ex) {

            logger.log(Level.WARNING, "No access to electricity price forecast.", ex);
            NotificationDisplayer.getDefault().
                    notify("No electricity price forecast!",
                            ImageUtilities.loadImageIcon("org/netbeans/modules/dialogs/warning.gif", true),
                            "Click to deactivate the Electricity price forecast input?",
                            context(this).one(Action.class));
        }

    }

    //
    // Private
    //
    private void updateForecast(Date from, Date to) throws EnergyPriceDatabaseException {

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        ElSpotPriceForecastConfig elPriceIntegration = db.selectElSpotPriceForecastConfig();

        if (elPriceIntegration.isActive()) {

            EnergySpotPriceService priceDB = Lookup.getDefault().lookup(EnergySpotPriceService.class);

            // Get prices from el price service
            EnergyPriceDatabaseArea area = elPriceIntegration.getElPriceArea();
            Map<Date, Double> prices = priceDB.getPrices(area, from, to);

            // Store prices in internal db
            ElPriceMWhForecast f = new ElPriceMWhForecast(prices, area.toString(), elPriceIntegration.getCurrency());
            Sample<ElPriceMWhForecast> s = new Sample<>(from, f);

            db.insertElSpotPriceForecast(s);
        }

        
        ElPriceMWhForecast s = db.selectElSpotPriceForecast(from, to);
        if (s != null) {
            setValue(s);
        }
    }
}
