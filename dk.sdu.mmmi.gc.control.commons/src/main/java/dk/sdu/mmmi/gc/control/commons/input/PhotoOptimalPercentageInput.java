package dk.sdu.mmmi.gc.control.commons.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PhotoOptimalPercentageInput extends AbstractInput<Percent> {

    private final ControlDomain g;
    private final Link<Action> action;

    public PhotoOptimalPercentageInput(ControlDomain g) {
        super("Photosynthesis optimization");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        Percent v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    protected void store(Date t, Percent result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPhotoOptimization(new Sample<>(t, result));
    }

    private Percent retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Percent> r = db.selectPhotoOptimizationGoal(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Percent oldValue = PhotoOptimalPercentageInput.this.getValue();
            Percent newValue = DialogUtil.promtPercent(oldValue, "Photo-optimization");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
