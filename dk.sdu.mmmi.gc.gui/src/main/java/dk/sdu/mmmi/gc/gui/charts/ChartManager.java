package dk.sdu.mmmi.gc.gui.charts;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 *
 * @author jcs
 */
public class ChartManager implements SearchObserver {

    private final ClimateDataAccess da;
    private final List<ChartConfig> charts = new ArrayList<>();
    private final ControlDomain domain;

    public ChartManager(ControlDomain d) {
        this.domain = d;
        this.da = context(d).one(ClimateDataAccess.class);
        context(d).add(SearchObserver.class, this);
    }

    public void delete(ChartConfig chart) {
        charts.remove(chart);
        da.deleteChart(chart);
        for (ChartListener l : context(this).all(ChartListener.class)) {
            l.onChartDelete(chart);
        }
    }

    public void update(ChartConfig cfg) {
        // Save in db
        da.updateChart(cfg);
        charts.remove(cfg);
        charts.add(cfg);
        fireChartConfigChangeEvent(cfg);
    }

    public void save(ChartConfig chart) {
        da.updateChart(chart);
    }

    public void createChart(String name) {
        ChartConfig cfg = new ChartConfig();
        cfg.setName(name);
        da.insertChart(cfg);
        da.updateChart(cfg);
        charts.add(cfg);
        fireChartConfigChangeEvent(cfg);
    }

    public ChartConfig getChartConfig(String id) {
        for (ChartConfig chartConfig : charts) {
            if (id.equals(chartConfig.getIDString())) {
                return chartConfig;
            }
        }
        return null;
    }

    public List<ChartConfig> getCharts() {
        return charts;
    }

    public void loadChartsFromDB() {
        charts.addAll(da.selectCharts());
    }

    /**
     * Set the MaxPeriodLenghtMS for time series that determines acceptable gaps
     * in data for display.
     */
    private void setTimeSeriesMaxPeriod() {
        for (DoubleTimeSeries s : context(domain).all(DoubleTimeSeries.class)) {

            long searchTime = context(domain).one(SearchConfiguration.class).getMaxNegotiationTimeMS() + 60 * 1000;
            ControlManager ctrlManager = context(domain).one(ControlManager.class);
            s.setMaxPeriodLenghtMS(ctrlManager.getDelay().toMS() + searchTime);
        }    
    }

    //
    // Other
    //
    private void fireChartConfigChangeEvent(ChartConfig chart) {

        setTimeSeriesMaxPeriod();

        // Chart Global listeners
        for (ChartListener l : context(this).all(ChartListener.class)) {
            l.onChartChanged(chart);
        }

        // Chart specific listeners
        for (ChartListener l : context(chart).all(ChartListener.class)) {
            l.onChartChanged(chart);
        }

    }

    @Override
    public void onSearchCompleted(Solution s) {
        List<ChartConfig> l = new CopyOnWriteArrayList(charts);
        for (ChartConfig chart : l) {
            fireChartConfigChangeEvent(chart);
        }
    }
}
