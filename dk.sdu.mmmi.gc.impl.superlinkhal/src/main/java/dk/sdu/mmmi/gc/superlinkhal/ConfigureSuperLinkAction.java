package dk.sdu.mmmi.gc.superlinkhal;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.impl.entities.config.SuperLinkConfig;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 * @author mrj
 */
public class ConfigureSuperLinkAction extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControlDomain g;

    public ConfigureSuperLinkAction(ControlDomain g) {
        super("Configure SuperLink");
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return g != null
                && context(g).one(HALConnector.class) != null
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling()
                && !getControlManager().isAutoRunning();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // Panel.
        SLConfigPanel pnl = new SLConfigPanel();

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(
                pnl,
                "Configure SuperLink");

        // Set defaults.
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

        pnl.set(db.getSuperLinkConfiguration());

        Object result = DialogDisplayer.getDefault().notify(dcs);
        if (result == DialogDescriptor.OK_OPTION) {
            db.setSuperLinkConfiguration(pnl.get());
        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }

    /**
     * Configuration panel.
     */
    private static class SLConfigPanel extends JPanel {

        private JTextField hostname = new JTextField(),
                domain = new JTextField(),
                username = new JTextField(),
                password = new JTextField(),
                department = new JTextField(),
                lightGroups = new JTextField(),
                indoorLight = new JTextField();

        public SLConfigPanel() {
            setLayout(new GridLayout(7, 2, 2, 2));
            add(new JLabel("Hostname:"));
            add(hostname);
            add(new JLabel("Domain:"));
            add(domain);
            add(new JLabel("Username:"));
            add(username);
            add(new JLabel("Password:"));
            add(password);
            add(new JLabel("Department:"));
            add(department);
            add(new JLabel("Light groups:"));
            add(lightGroups);
            add(new JLabel("Indoor Light:"));
            add(indoorLight);

        }

        public void set(SuperLinkConfig cfg) {
            hostname.setText(cfg.getHostname());
            domain.setText(cfg.getDomain());
            username.setText(cfg.getUsername());
            password.setText(cfg.getPassword());
            department.setText(cfg.getDepartment());
            lightGroups.setText(cfg.getLightGroupsAsString());
            indoorLight.setText(cfg.getIndoorLightCode());
        }

        public SuperLinkConfig get() {
            SuperLinkConfig r = new SuperLinkConfig();
            r.setHostname(hostname.getText());
            r.setDomain(domain.getText());
            r.setUsername(username.getText());
            r.setPassword(password.getText());
            r.setDepartment(department.getText());
            r.setLightGroups(lightGroups.getText());
            r.setIndoorLightCode(indoorLight.getText());
            return r;
        }
    }
}
