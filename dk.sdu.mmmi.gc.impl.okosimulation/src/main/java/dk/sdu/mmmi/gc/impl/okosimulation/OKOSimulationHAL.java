package dk.sdu.mmmi.gc.impl.okosimulation;

import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mrj
 */
public class OKOSimulationHAL implements HALConnector {

    private Inputs inputs = new Inputs();
    private Outputs outputs = new Outputs();

    public Inputs getInputs() {
        return inputs;
    }

    public void setOutputs(Outputs outputs) {
        this.outputs = outputs;
    }

    //
    // HALConnector
    //
    @Override
    public Status getStatus() {
        return Status.OPTIMAL_SERVICE;
    }

    @Override
    public synchronized Celcius readTemperature() {
        return new Celcius(outputs.getTair());
    }

    @Override
    public synchronized Celcius readOutdoorTemperature() {
        return new Celcius(outputs.getTout());
    }

    @Override
    public synchronized PPM readCO2() {
        return new PPM(outputs.getCO2air());
    }

    @Override
    public synchronized UMolSqrtMeterSecond readLightIntensity() {
        return new WattSqrMeter(outputs.getRadiIn()).toUMolPAR();
    }

    @Override
    public synchronized Percent readHumidity() {
        return new Percent(outputs.getRHair());
    }

    @Override
    public synchronized WattSqrMeter readOutdoorLightIntensity() {
        return new WattSqrMeter(outputs.getOutLightW());
    }

    @Override
    public synchronized Switch readArtficialLightStatus(String lightGroup) {
        return inputs.getArtficialLightStatus() > 0 ? Switch.ON : Switch.OFF;
    }

    @Override
    public synchronized Percent readWindowOpening() throws HALException {
        // Alpha is how many percent the sreen is open.
        return new Percent(outputs.getAlpha());
    }

    @Override
    public synchronized void writeHeatingThreshold(Celcius c) {
        inputs.setSpAirHeat(c.value());
    }

    @Override
    public synchronized void writeVentilationThreshold(Celcius c) {
        inputs.setSpAirVent(c.value());
    }

    @Override
    public synchronized void writeCO2Threshold(PPM p) {
        inputs.setSpCO2(p.value());
    }

    @Override
    public synchronized void writeLightStatus(Switch s) {
        inputs.setArtficialLightStatus(s.isOn() ? 1.0 : 0.0);
    }

    @Override
    public synchronized void writeGeneralScreenPosition(Percent p) {
        inputs.setSpScreenPos(p.asFactor());
    }

    @Override
    public void dispose() {
    }

    @Override
    public List<String> readLightGroups() throws HALException {
        //qqq return new ArrayList();
        List<String> l = new ArrayList();
        l.add("On");
        return l;
    }
}
