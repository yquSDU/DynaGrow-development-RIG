package dk.sdu.mmmi.gc.control.photo.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.control.commons.input.IndoorLightInput;
import dk.sdu.mmmi.gc.control.commons.input.PhotoOptimalPercentageInput;
import dk.sdu.mmmi.gc.control.commons.output.CO2GoalOutput;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import static java.lang.Math.abs;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrj & jcs
 */
public class PhotoOptimalCO2Concern extends AbstractConcern {

    public PhotoOptimalCO2Concern(ControlDomain g) {
        super("Photo. optimal CO₂", g, 3);
    }

    /**
     * @param s The solution to be evaluated
     * @return Fitness [0;1] of solution where 0 is best and one is worse. The
     * fitness is better the closer CO2 is to photo. opt. CO2
     */
    @Override
    public double evaluate(Solution s) {
        PPM co2SP = s.getValue(CO2GoalOutput.class);
        PPM photoOptCO2 = getOptPhoto(s);

        // Happy if CO2SP and CO2 is close to optimal photo. CO2
        return abs(photoOptCO2.subtract(co2SP).roundedValue());
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        PPM photoOptCO2 = getOptPhoto(s);
        if (photoOptCO2 != null) {
            r.put(CO2GoalOutput.class, String.format("prefer %s", photoOptCO2));
        }
        return r;
    }

    private PPM getOptPhoto(Solution s) {
        UMolSqrtMeterSecond light = s.getValue(IndoorLightInput.class);
        Percent p = s.getValue(PhotoOptimalPercentageInput.class);

        if (light == null || p == null) {
            return null;
        }

        // TODO Consider calculating optimal CO2 based on current temperature
        // - not any assumptions about the heating setpoint.
        PhotoUtil photoUtil = context(g).one(PhotoUtil.class);
        PPM photoOptCO2 = photoUtil.calcPhotoOptimalCO2(light, p);
        return photoOptCO2;
    }
}
