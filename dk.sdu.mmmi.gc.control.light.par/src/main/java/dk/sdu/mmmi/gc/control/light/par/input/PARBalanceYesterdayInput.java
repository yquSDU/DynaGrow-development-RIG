package dk.sdu.mmmi.gc.control.light.par.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfDay;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import static dk.sdu.mmmi.gc.control.commons.utils.DialogUtil.promtMolSqrMeter;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author jcs
 */
public class PARBalanceYesterdayInput extends AbstractInput<MolSqrMeter> {

    private final ControlDomain g;
    private Date updateTime;
    private final Link<Action> action;

    public PARBalanceYesterdayInput(ControlDomain g) {
        super("PAR balance yesterday");
        this.g = g;
        this.updateTime = new Date(endOfDay(new Date()).getTime() - 1000 * 60 * 5);
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {

        if (t.after(updateTime)) {

            MolSqrMeter goalSum = context(g).one(PARSumDayGoalInput.class).getValue();
            MolSqrMeter parSumAchieved = context(g).one(PARSumAchievedPeriodInput.class).getValue();

            MolSqrMeter balance = this.getValue().add(parSumAchieved).subtract(goalSum);

            store(t, balance);

            // Next update is 5 min before end of next day
            Date next = endOfDay(new Date(t.getTime() + 1000 * 60 * 60));
            updateTime = new Date(next.getTime() - 1000 * 60 * 5);
        }

        setValue(retrieve(t));
    }

    private MolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<MolSqrMeter> r = db.selectPARBalanceYesterday(t);
        return r.getSample();
    }

    private void store(Date t, MolSqrMeter mol) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPARBalanceYesterday(new Sample<>(t, mol));
    }

    private class ConfigureAction extends AbstractAction implements Presenter.Popup {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            MolSqrMeter oldValue = PARBalanceYesterdayInput.this.getValue();
            MolSqrMeter newValue = promtMolSqrMeter(oldValue, "PAR balance yesterday");
            if (newValue != null) {
                setValue(newValue);
                store(new Date(), newValue);
            }
        }

        @Override
        public JMenuItem getPopupPresenter() {
            JMenuItem mi = new JMenuItem(this);
            mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
            return mi;
        }
    }
}
