package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author mrj
 */
public class MinimumLightTimeSumInput extends AbstractInput<Duration> {

    private final ControlDomain g;
    private final Link<Action> action;

    public MinimumLightTimeSumInput(ControlDomain g) {
        super("Light-time sum (day)");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        Duration v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    protected void store(Date t, Duration result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertLightSumHoursGoal(new Sample<>(t, result));
    }

    private Duration retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        return db.selectLightSumHoursGoal(t).getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Duration oldValue = MinimumLightTimeSumInput.this.getValue();
            Duration newValue = DialogUtil.promtHour(oldValue, "Min light time per day");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
