package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.CSVExporter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.concurrent.Executor;
import javax.swing.JMenuItem;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;

/**
 * @author mrj
 */
public class ExportGreenhouseAction extends SystemAction implements Presenter.Popup {

    public ControlDomain getGreenhouse() {
        return Utilities.actionsGlobalContext().lookup(ControlDomain.class);
    }

    @Override
    public String getName() {
        return "Export to CSV";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ControlDomain g = getGreenhouse();
        if (g != null) {
            showExportDialog(g);
        }
    }

    private void showExportDialog(ControlDomain g) {

        String dir = System.getProperty("user.home");
        FileChooserBuilder b = new FileChooserBuilder(dir);

        b.setTitle("Export to CSV");
        b.setDefaultWorkingDirectory(new File(dir));
        b.setApproveText("Export");

        File r = b.showSaveDialog();

        if (r != null) {

            // Ensure .csv extension
            if (!r.getName().endsWith(".csv")) {
                r = new File(r.getAbsolutePath() + ".csv");
            }

            // Export
            System.out.println("Export to " + r);
            export(g, r);
        }
    }

    private void export(final ControlDomain g, final File dest) {
        Executor e = context(getGreenhouse()).one(Executor.class);
        Runnable t = new Runnable() {
            @Override
            public void run() {
                CSVExporter x = new CSVExporter(g);
                try {
                    ProgressHandle h = ProgressHandleFactory.createHandle(
                            "Exporting to " + dest.getAbsolutePath());
                    x.export(dest, h);
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        };
        e.execute(t);
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/table_save.png", false));
        return mi;
    }
}
