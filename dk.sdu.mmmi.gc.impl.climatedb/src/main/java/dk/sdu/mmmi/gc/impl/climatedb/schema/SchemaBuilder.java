package dk.sdu.mmmi.gc.impl.climatedb.schema;

import dk.sdu.mmmi.gc.impl.climatedb.util.JDBC;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mrj
 */
public class SchemaBuilder {

    private final Connection con;
    private final List<Version> versions = new ArrayList<Version>();

    public SchemaBuilder(Connection con) {
        this.con = con;
    }

    public int getVersion() throws SQLException {
        // Create schema_version if not exists.
        try {
            JDBC.execute(con, "CREATE TABLE schema_version (version INT)");
        } catch (SQLException e) {
            if (!e.getSQLState().equals("X0Y32")) {
                throw e;
            }
        }

        // Get version.
        PreparedStatement s = null;
        try {
            s = con.prepareStatement(
                    "SELECT MAX(version) FROM schema_version");
            ResultSet rs = rs = s.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }

    public void addVersion(int version, String sql) {
        versions.add(new Version(version, sql));
    }

    public void update() throws SQLException {
        int currentVersion = getVersion();

        boolean ac = con.getAutoCommit();
        List<Statement> cleanup = new ArrayList<>();

        try {
            // Deploy all undeployed versions.
            con.setAutoCommit(false);

            for (Version v : versions) {
                if (v.getVersion() > currentVersion) {
                    // Add to schema.
                    Statement s1 = con.createStatement();
                    s1.execute(v.getSql());
                    cleanup.add(s1);

                    // Maintain version.
                    PreparedStatement s2 = con.prepareStatement(
                            "INSERT INTO schema_version VALUES(?)");
                    s2.setInt(1, v.getVersion());
                    s2.executeUpdate();
                    cleanup.add(s2);
                }
            }

            // Commit.
            con.commit();
        } catch (SQLException x) {
            // On exception, rollback.
            con.rollback();
            throw x;
        } finally {
            // Use old auto commit setting.
            con.setAutoCommit(ac);
        }
    }

    private static class Version {

        private final int version;
        private final String sql;

        public Version(int version, String sql) {
            this.version = version;
            this.sql = sql;
        }

        public String getSql() {
            return sql;
        }

        public int getVersion() {
            return version;
        }
    }
}
