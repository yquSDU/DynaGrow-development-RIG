package dk.sdu.mmmi.gc.gui.explorer;

import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.api.moea.Output;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 * @author mrj
 */
public class OutputsNodeChildren extends Children.Keys<Output> implements ControleumListener {

    private final ControlDomain gh;
    private final DisposableList disposables = new DisposableList();

    public OutputsNodeChildren(ControlDomain gh) {
        this.gh = gh;
        disposables.add(context(gh).add(ControleumListener.class, this));
    }

    private void setKeysOrdered(List<? extends Output> c) {
        Collections.sort(c, new Comparator<Output>() {
            @Override
            public int compare(Output t, Output t1) {
                return t.getName().compareTo(t1.getName());
            }
        });

        setKeys(c);
    }

    private void updateKeys() {
        // No selected concerns - show all outputs.
        setKeysOrdered(new ArrayList<>(context(gh).all(Output.class)));
    }

    @Override
    protected void addNotify() {
        updateKeys();
    }

    @Override
    protected void removeNotify() {
        setKeys(Collections.EMPTY_SET);
    }

    @Override
    public void onControlDomainAdded(ControleumEvent e) {
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
        updateKeys();
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        removeNotify();
        disposables.dispose();
    }

    @Override
    protected Node[] createNodes(Output output) {

        OutputNode n = new OutputNode(gh, output);
        return new Node[]{n};
    }
}
