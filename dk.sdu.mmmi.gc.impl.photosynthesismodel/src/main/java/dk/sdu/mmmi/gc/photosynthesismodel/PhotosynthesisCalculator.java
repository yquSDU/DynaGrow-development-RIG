package dk.sdu.mmmi.gc.photosynthesismodel;

/**
 * 
 * @author ao
 */
public interface PhotosynthesisCalculator {

    /**
     * Calculates the optimized climate given a mathematical photosynthesis model
     *
     * @param par Photosynthetic Absorbed Radiation in [µmol m^-2 s^-1]
     * @param pctFnMax Percentage of photosynthesis optimization wanted [%]
     * @param plantParams Plant specific parameters
     */
    public void calculateOptimalClimate(double par, double pctFnMax, PlantSpecificPhotoParams plantParams);

    /**
     * @return Greenhouse CO2 concentration [PPM (parts per million)]
     */
    double getOptimalCO2();

    /**
     * @return Photosynthetic Active Radiation in [μmol m^-2 s^-1]
     */
    double getMaxPhotosynthesis();

    /**
     * @return Canopy/leaf temperature [°C]
     */
    double getOptimalTemperature();
}
