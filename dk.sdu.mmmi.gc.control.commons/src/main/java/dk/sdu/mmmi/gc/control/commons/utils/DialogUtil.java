package dk.sdu.mmmi.gc.control.commons.utils;

import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.SqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.Watt;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public final class DialogUtil {

    private static final NumberFormat FORMAT = NumberFormat.getInstance();

    public static UMolSqrtMeterSecond promtUMol(UMolSqrtMeterSecond currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[UMOL]");
        return r != null ? new UMolSqrtMeterSecond(r) : null;
    }

    public static Celcius promtCelcius(Celcius currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[C]");
        return r != null ? new Celcius(r) : null;
    }

    public static PPM promtPPM(PPM currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[PPM]");
        return r != null ? new PPM(r) : null;
    }

    public static Watt promtWatt(Watt currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[W]");
        return r != null ? new Watt(r) : null;
    }

    public static Percent promtPercent(Percent currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.asPercent() : null, msg, "[%]");
        return r != null ? new Percent(r) : null;
    }

    public static Duration promtHour(Duration currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? BigDecimal.valueOf(currentValue.toHours()).doubleValue() : null, msg, "[hour]");
        return r != null ? new Duration(BigDecimal.valueOf(r).longValue() * 60 * 60 * 1000) : null;
    }

    public static WattSqrMeter promtWattSqrMeter(WattSqrMeter currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[watt m^-2]");
        return r != null ? new WattSqrMeter(r) : null;
    }

    public static MolSqrMeter promtMolSqrMeter(MolSqrMeter currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[mol m^-2]");
        return r != null ? new MolSqrMeter(r) : null;
    }

    public static MMolSqrMeter promtMMolSqrMeter(MMolSqrMeter currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[mol m^-2 day^-1]");
        return r != null ? new MMolSqrMeter(r) : null;
    }

    public static Double promtDouble(Double currentValue, String msg, String unit) {
        // Notify descriptor.
        NotifyDescriptor.InputLine l;
        l = new NotifyDescriptor.InputLine(msg + " " + unit, msg);
        if (currentValue != null) {
            l.setInputText(FORMAT.format(currentValue));
        }

        // Notify.
        Object answer = DialogDisplayer.getDefault().notify(l);

        // Parse result.
        if (answer == DialogDescriptor.OK_OPTION) {
            try {
                return FORMAT.parse(l.getInputText()).doubleValue();
            } catch (ParseException ex) {
                // Notify.
                Exceptions.printStackTrace(ex);
                return null;
            }
        }

        return null;
    }

    public static Integer promtInteger(Integer currentValue, String msg, String unit) {
        // Notify descriptor.
        NotifyDescriptor.InputLine l;
        l = new NotifyDescriptor.InputLine(msg + " " + unit, msg);
        if (currentValue != null) {
            l.setInputText(FORMAT.format(currentValue));
        }

        // Notify.
        Object answer = DialogDisplayer.getDefault().notify(l);

        // Parse result.
        if (answer == DialogDescriptor.OK_OPTION) {
            try {
                return FORMAT.parse(l.getInputText()).intValue();
            } catch (ParseException ex) {
                // Notify.
                Exceptions.printStackTrace(ex);
                return null;
            }
        }

        return null;
    }

    public static Double promtDoubles(Double currentValue, String msg, String unit) {
        // Notify descriptor.
        NotifyDescriptor.InputLine l;
        l = new NotifyDescriptor.InputLine(msg + " " + unit, msg);
        if (currentValue != null) {
            l.setInputText(FORMAT.format(currentValue));
        }

        // Notify.
        Object answer = DialogDisplayer.getDefault().notify(l);

        // Parse result.
        if (answer == DialogDescriptor.OK_OPTION) {
            try {
                return FORMAT.parse(l.getInputText()).doubleValue();
            } catch (ParseException ex) {
                // Notify.
                Exceptions.printStackTrace(ex);
                return null;
            }
        }

        return null;
    }

    public static SqrMeter promtSqrMeter(SqrMeter currentValue, String msg) {
        Double r = promtDouble(currentValue != null ? currentValue.value() : null, msg, "[m^-2]");
        return r != null ? new SqrMeter(r) : null;
    }
}
