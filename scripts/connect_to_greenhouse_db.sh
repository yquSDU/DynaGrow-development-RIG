#!/bin/sh

# Start ij using Derby jars and connect to GreenhouseDB.
java \
	-cp "derbytools-10.9.1.0.jar:derby-10.9.1.0.jar" \
	-Dij.protocol=jdbc:derby: \
	-Dij.database=$HOME/GreenhouseDB \
	org.apache.derby.tools.ij
