package dk.sdu.mmmi.gc.control.light.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class MinimumLightTimeSumSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public MinimumLightTimeSumSeries(ControlDomain g) {
        this.g = g;
        setName("Minimum light-time sum");
        setUnit("[hour]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<Duration>> raw = db.selectLightSumHoursGoal(from, to);
            for (Sample<Duration> s : raw) {
                Date t = s.getTimestamp();
                Double v = s.getSample().toHours();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
}
