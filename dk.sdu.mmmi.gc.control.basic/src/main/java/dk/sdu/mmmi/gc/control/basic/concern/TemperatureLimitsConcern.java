package dk.sdu.mmmi.gc.control.basic.concern;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMaximumInput;
import dk.sdu.mmmi.gc.control.commons.input.IndoorTemperatureMinimumInput;
import dk.sdu.mmmi.gc.control.commons.output.HeatingSetpointOutput;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrj
 */
public class TemperatureLimitsConcern extends AbstractConcern {

    public TemperatureLimitsConcern(ControlDomain g) {
        super("Temperature limits", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {
        double h = s.getValue(HeatingSetpointOutput.class).value();
        double min = s.getValue(IndoorTemperatureMinimumInput.class).value();
        double max = s.getValue(IndoorTemperatureMaximumInput.class).value();
        return (h >= min && h <= max) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        double min = s.getValue(IndoorTemperatureMinimumInput.class).value();
        double max = s.getValue(IndoorTemperatureMaximumInput.class).value();

        Map<Class, String> r = new HashMap<>();
        r.put(HeatingSetpointOutput.class,
                String.format("between %.1f and %.1f", min, max));
        return r;
    }
}
