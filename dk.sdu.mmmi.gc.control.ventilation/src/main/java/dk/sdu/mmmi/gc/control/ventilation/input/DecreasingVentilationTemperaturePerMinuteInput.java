package dk.sdu.mmmi.gc.control.ventilation.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class DecreasingVentilationTemperaturePerMinuteInput extends AbstractInput<Celcius> {

    private final ControlDomain g;

    public DecreasingVentilationTemperaturePerMinuteInput(ControlDomain g) {
        super("Maximum temperature-decrease per minute");
        this.g = g;
        context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        Celcius v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Celcius result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertDecreasingVentilationTemperaturePerMinute(new Sample<Celcius>(t, result));
    }

    private Celcius retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Celcius> r = db.selectDecreasingVentilationTemperaturePerMinute(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Celcius oldValue = DecreasingVentilationTemperaturePerMinuteInput.this.getValue();
            Celcius newValue = DialogUtil.promtCelcius(oldValue, "The maximum decrease in ventilation-temperature per minute");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
