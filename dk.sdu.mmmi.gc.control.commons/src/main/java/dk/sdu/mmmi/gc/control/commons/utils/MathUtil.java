package dk.sdu.mmmi.gc.control.commons.utils;

import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Unit;
import flanagan.interpolation.LinearInterpolation;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author mrj & jcs
 */
public class MathUtil {

    //
    // Public Math functions
    //
    public static <V extends Unit> Map<Long, Double> interpolate(List<Sample<V>> samples, Duration interpolationInterval) {

        double[] x = new double[samples.size()];
        double[] y = new double[samples.size()];

        for (int i = 0; i < samples.size(); i++) {
            Sample<V> s = samples.get(i);
            x[i] = Long.valueOf(s.getTimestamp().getTime()).doubleValue();
            y[i] = s.getSample().value();
        }

        Map<Long, Double> result = new TreeMap<>();

        // Interpolate if enough data points
        if (x.length > 4 && y.length > 4) {

            LinearInterpolation interpolator = new LinearInterpolation(x, y);

            for (double i = x[0]; i <= x[x.length - 1]; i += interpolationInterval.toMS()) {
                result.put((long) i, interpolator.interpolate(i));
            }
        }
        return result;
    }
}
