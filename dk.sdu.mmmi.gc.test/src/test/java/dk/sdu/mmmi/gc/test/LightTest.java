package dk.sdu.mmmi.gc.test;

import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.impl.entities.config.LightPlanConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Money;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.Watt;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.FIVE_MINS_IN_MS;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.HOUR_IN_MS;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.dateOfIndex;
import dk.sdu.mmmi.gc.control.commons.utils.EnergyUtil;
import dk.sdu.mmmi.gc.control.commons.utils.LightUtil;
import dk.sdu.mmmi.gc.control.light.concern.FixedLightPlanConcern;
import dk.sdu.mmmi.gc.control.light.concern.LightIntervalConcern;
import dk.sdu.mmmi.gc.control.light.concern.PreferCheapLightConcern;
import dk.sdu.mmmi.gc.control.light.input.LightTimeAchievedToday;
import dk.sdu.mmmi.gc.control.light.output.LightPlanTodayOutput;
import dk.sdu.mmmi.gc.control.light.par.concern.AchievePARSumConcern;
import dk.sdu.mmmi.gc.control.light.par.concern.PARSumBalanceConcern;
import dk.sdu.mmmi.gc.control.light.photo.concern.PhotoSumBalanceConcern;
import dk.sdu.mmmi.gc.impl.entities.results.ElPriceMWhForecast;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import static java.lang.String.format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class LightTest {

    private static int ctrlInterval;
    private static SearchConfiguration cfg;
    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private GreenhouseTestFixture testFixture;

    @BeforeClass
    public static void setUpClass() {
        ctrlInterval = 5;
        cfg = new SearchConfiguration.Builder().
                maxGenerations(1500).
                maxNegotiationTimeMS(60 * 1000).
                debuggingEnabled(true).
                build();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        //System.setProperty("org.openide.util.Lookup", TestLookup.class.getName());
    }

    @After
    public void tearDown() {
        if (testFixture != null) {
            testFixture.dispose();
        }
    }

    @Test
    public void basicLightConcerns() throws Exception {

        // SETUP:
        FixedDayLightPlan fixedLigtPlan = new FixedDayLightPlan().
                set(0, Switch.OFF).set(22, Switch.OFF).set(23, Switch.OFF);
        Duration expLightPlanDuration = new Duration(1000 * 60 * 60 * 6l); // 6 hours

        Date from = df.parse("2012-01-01 00:00");
        Date to = df.parse("2012-01-01 00:05");

        testFixture = new GreenhouseTestFixture("BasicLightConcerns", from, ctrlInterval, cfg)
                .lightIntervalFeature()
                .fixedLightPlanFeature(fixedLigtPlan)
                .achieveLightTimeFeature(1, 6)
                .preferNoLightFeature(3)
                .preferLightDuringNight(2);

        // TEST: One day in January
        testFixture.startNegotiation(from, to);
        Solution solution = testFixture.getSelectedSolution();

        // ASSERT: Light interval concern
        assertTrue(format("Solution for LightIntervalConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(LightIntervalConcern.class) == 0);

        // ASSERT: Fixed plan feature
        assertTrue(format("Solution for FixedLightPlanConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(FixedLightPlanConcern.class) == 0);

        // ASSERT: Light time sum feature
        LightPlan lp = solution.getValue(LightPlanTodayOutput.class);
        Duration lta = solution.getValue(LightTimeAchievedToday.class);
        Duration ltp = lp.getTotalLightOnDuration();
        assertEquals("Assert total light time.", expLightPlanDuration.toHours(), lta.add(ltp).toHours(), 1.0);
    }

    @Test
    public void advancedPhotoLightConcerns() throws Exception {
        // SETUP:
        FixedDayLightPlan flp = new FixedDayLightPlan().set(0, Switch.OFF).set(22, Switch.OFF).set(23, Switch.OFF);

        Date from = df.parse("2013-09-30 00:00");
        Date to = df.parse("2013-09-30 00:10"); // two runs

        // last two hour switched off, photo sum. = 600, hour intervals.
        testFixture = new GreenhouseTestFixture("AdvancedLightConcerns", from, ctrlInterval, cfg)
                .fixedLightPlanFeature(flp)
                .lightIntervalFeature()
                .lightPriceFeature(1, 38, 80)
                .achievePhotoSumBalance(2, 400)
                .achievePhotoSum(400)
                .setLampIntensity(100);

        // TEST:
        testFixture.startNegotiation(from, to);
        Solution solution = testFixture.getSelectedSolution();

        // ASSERTS:
        assertTrue(format("Solution for FixedLightPlanConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(FixedLightPlanConcern.class) == 0);

        assertTrue(format("Solution for LightIntervalConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(LightIntervalConcern.class) == 0);

        assertEquals(format("Solution for AchievePhotoSumConcern was not acceptable: %s", solution),
                0.0, solution.getEvaluationValue(PhotoSumBalanceConcern.class), 10.0);

        assertEquals(format("Solution for PreferCheapLightConcern was not acceptable: %s", solution),
                16.0, solution.getEvaluationValue(PreferCheapLightConcern.class), 1.0);

    }

    @Test
    public void achievePARSumConcernHourInteval() throws Exception {
        // SETUP:
        Date from = df.parse("2013-01-01 00:00");
        Date to = df.parse("2013-01-01 00:05");

        testFixture = new GreenhouseTestFixture("AchievePARSumConcernHourInterval", from, ctrlInterval, cfg)
                .lightIntervalFeature()
                .achievePARSum(3.0)
                .achievePARSumBalance(2, 3.0)
                .lightPriceFeature(1, 38, 80)
                .setLampIntensity(100);

        // TEST:
        testFixture.startNegotiation(from, to);
        Solution solution = testFixture.getSelectedSolution();

        // ASSERTS:
        assertEquals(format("Solution for PreferCheapLightConcern was not acceptable: %s", solution),
                8.0, solution.getEvaluationValue(PreferCheapLightConcern.class), 1.0);

        assertEquals(format("Solution for AchievePARSumConcern was not acceptable: %s", solution),
                0.0, solution.getEvaluationValue(AchievePARSumConcern.class), 0.0);

        assertEquals(format("Solution for PARSumBalanceConcern was not acceptable: %s", solution),
                0.1, solution.getEvaluationValue(PARSumBalanceConcern.class), 0.1);

        assertTrue(format("Solution for LightIntervalConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(LightIntervalConcern.class) == 0);
    }

    @Test
    public void achievePARSumConcern5MinInteval() throws Exception {
        // SETUP:
        // 5 mins light plans
        LightPlanConfig lpCfg = new LightPlanConfig.Builder().duration(FIVE_MINS_IN_MS).build();
        Date from = df.parse("2013-01-01 00:00");
        Date to = df.parse("2013-01-01 00:05");

        testFixture = new GreenhouseTestFixture("AchievePARSumConcern5MinIntervals",
                from, ctrlInterval, cfg, lpCfg)
                .lightIntervalFeature()
                .achievePARSum(3.0)
                .achievePARSumBalance(2, 3.0)
                .lightPriceFeature(3, 38, 80)
                .setLampIntensity(100);

        // TEST:
        testFixture.startNegotiation(from, to);
        Solution solution = testFixture.getSelectedSolution();

        // ASSERT: Solutions are all acceptable
        assertEquals(format("Solution for PreferCheapLightConcern was not acceptable: %s", solution),
                12.5, solution.getEvaluationValue(PreferCheapLightConcern.class), 1.);

        assertEquals(format("Solution for AchievePARSumConcern was not acceptable: %s", solution),
                0.0, solution.getEvaluationValue(AchievePARSumConcern.class), 0.0);

        assertEquals(format("Solution for PARSumBalanceConcern was not acceptable: %s", solution),
                0.1, solution.getEvaluationValue(PARSumBalanceConcern.class), 0.1);

        assertTrue(format("Solution for LightIntervalConcern was not acceptable: %s", solution),
                solution.getEvaluationValue(LightIntervalConcern.class) == 0);

    }

    @Test
    public void lightUtil() {
        // SETUP:
        Duration oneHour = new Duration(HOUR_IN_MS);
        LightPlan hourPlan = new LightPlan(dateOfIndex(0), dateOfIndex(0), oneHour);
        hourPlan.setElement(0, Switch.ON);

        Duration fiveMin = new Duration(FIVE_MINS_IN_MS);
        LightPlan fiveMinPlan = new LightPlan(dateOfIndex(0), dateOfIndex(0), fiveMin);
        for (int i = 0; i < fiveMinPlan.size(); i++) {
            fiveMinPlan.setElement(i, Switch.ON);
        }

        UMolSqrtMeterSecond intensity = new UMolSqrtMeterSecond(100.0);

        // TESTS:
        MolSqrMeter resultHourPlan = LightUtil.calcLightplanPARSum(hourPlan, intensity);
        MolSqrMeter resultFiveMinPlan = LightUtil.calcLightplanPARSum(fiveMinPlan, intensity);

        // ASSERTS:
        MolSqrMeter expResult = intensity.times(oneHour).toMol();
        assertEquals("Hourplan PAR sum calculation failed.", expResult.value(), resultHourPlan.value(), 0.1);
        assertEquals("Five mins plan PAR sum calculation failed.", expResult.value(), resultFiveMinPlan.value(), 0.1);
    }

    @Test
    public void calcLightplanPrice() throws ParseException {
        // SETUP:
        Date from = df.parse("2012-01-01 00:00");
        Date to = df.parse("2012-01-01 00:05");

        testFixture = new GreenhouseTestFixture("CalculateLightplanPrice", from, ctrlInterval, cfg);

        ElPriceMWhForecast priceForecast = testFixture.getElPriceMWhForecast(from);
        Watt totalLampLoad = testFixture.getTotalLampLoad(from);

        // Hourly plan
        LightPlanConfig hourLightplanCfg = new LightPlanConfig.Builder().build();
        Duration oneHour = new Duration(HOUR_IN_MS);
        LightPlan hourPlan = new LightPlan(from, to, oneHour);
        hourPlan.setElement(0, Switch.ON);

        // Five min. plan
        LightPlanConfig fiveMinLightplanCfg = new LightPlanConfig.Builder().duration(FIVE_MINS_IN_MS).build();
        Duration fiveMin = new Duration(FIVE_MINS_IN_MS);
        LightPlan fiveMinPlan = new LightPlan(from, to, fiveMin);
        for (int i = 0; i < fiveMinPlan.size(); i++) {
            fiveMinPlan.setElement(i, Switch.ON);
        }

        // TESTS:
        Money hourPlanPrice = EnergyUtil.calcPrice(hourPlan, priceForecast, totalLampLoad, hourLightplanCfg);
        Money fiveMinPlanPrice = EnergyUtil.calcPrice(fiveMinPlan, priceForecast, totalLampLoad, fiveMinLightplanCfg);

        // ASSERTS:
        assertEquals(fiveMinPlanPrice.value(), hourPlanPrice.value(), 0.1);
    }
}
