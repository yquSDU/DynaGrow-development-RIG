package dk.sdu.mmmi.gc.impl.sl4connector;

import java.io.Serializable;

/**
 * A SL4Login provides the information needed to connect to a superlink 4 system
 * using DCOM.
 *
 * @author mrj, ao
 */
public class SL4Login implements Serializable {

    private final String hostname;

    private final String domain;

    private final String username;

    private final String password;

    public SL4Login(String hostname, String domain, String username, String password) {
        this.hostname = hostname;
        this.domain = domain;
        this.username = username;
        this.password = password;
    }

    public final String getHostname() {
        return hostname;
    }

    public final String getDomain() {
        return domain;
    }

    public final String getUsername() {
        return username;
    }

    public final String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format(
                "SL4Login(host=%s, domain=%s, user=%s)",
                hostname, domain, username);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SL4Login other = (SL4Login) obj;
        if ((this.hostname == null) ? (other.hostname != null) : !this.hostname.equals(other.hostname)) {
            return false;
        }
        if ((this.domain == null) ? (other.domain != null) : !this.domain.equals(other.domain)) {
            return false;
        }
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.hostname != null ? this.hostname.hashCode() : 0);
        hash = 97 * hash + (this.domain != null ? this.domain.hashCode() : 0);
        hash = 97 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 97 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }
}
