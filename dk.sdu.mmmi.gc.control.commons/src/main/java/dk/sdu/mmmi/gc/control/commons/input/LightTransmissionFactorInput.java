package dk.sdu.mmmi.gc.control.commons.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class LightTransmissionFactorInput extends AbstractInput<Percent> {

    private final ControlDomain g;
    private final Link<Action> action;

    public LightTransmissionFactorInput(ControlDomain g) {
        super("Greenhouse Light Transmission factor");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        Percent v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, Percent result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertLightTransmissionFactor(new Sample<>(t, result));
    }

    private Percent retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<Percent> r = db.selectLightTransmissionFactor(t);

        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Percent oldValue = LightTransmissionFactorInput.this.getValue();
            Percent newValue = DialogUtil.promtPercent(oldValue, "Light Transmission Factor");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
