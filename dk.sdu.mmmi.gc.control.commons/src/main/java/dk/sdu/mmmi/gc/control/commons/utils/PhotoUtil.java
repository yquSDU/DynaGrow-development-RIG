package dk.sdu.mmmi.gc.control.commons.utils;

import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.HOUR_IN_MS;
import static dk.sdu.mmmi.gc.control.commons.utils.MathUtil.interpolate;
import dk.sdu.mmmi.gc.photosynthesismodel.PhotoModelFactory;
import dk.sdu.mmmi.gc.photosynthesismodel.PhotosynthesisCalculator;
import dk.sdu.mmmi.gc.photosynthesismodel.PhotosynthesisModel;
import static java.util.Collections.synchronizedMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.openide.util.Lookup;

/**
 * A cached implementation of a photosynthesis calculator. The cached result is
 * needed due to performance issue as a consequence of using a GA search
 * algorithm.
 *
 * @author jcs
 */
public class PhotoUtil {
    //
    // Helpers
    //

    private final PhotoModelFactory f = Lookup.getDefault().lookup(PhotoModelFactory.class);
    private final PhotosynthesisCalculator calculator = f.getPhotosynthesisCalculator();
    private final PhotosynthesisModel model = f.getPhotosynthesisModel();
    /**
     * Cached photo. results
     */
    private final Map<PhotoParams, PhotoOptResult> photoOptResults = synchronizedMap(new WeakHashMap<PhotoParams, PhotoOptResult>());

    //
    // Public methods
    //
    public UMolSqrtMeterSecond calcPhoto(Celcius temp, PPM co2, UMolSqrtMeterSecond par) {
        double photoRate = model.calculate(temp.value(), co2.value(), par.value(), f.getPlantSpecPhotoParams());
        return new UMolSqrtMeterSecond(photoRate);
    }

    public PPM calcPhotoOptimalCO2(UMolSqrtMeterSecond parLight, Percent photoOptPct) {
        return calcOptimalClimate(parLight, photoOptPct).optimalCO2;
    }

    public Celcius calcPhotoOptimalTemp(UMolSqrtMeterSecond parLight, Percent photoOptPct) {
        return calcOptimalClimate(parLight, photoOptPct).optimalTemp;
    }

    public UMolSqrtMeterSecond calcPhotoOptimalRate(UMolSqrtMeterSecond parLight, Percent photoOptPct) {
        return calcOptimalClimate(parLight, photoOptPct).optimalPhoto;
    }

    public MMolSqrMeter calcPhotoSum(LightPlan plan, UMolSqrtMeterSecond lampIntensity, Percent photoPct) {

        // Check Cache
        UMolSqrtMeterSecond photoRate = calcOptimalClimate(lampIntensity, photoPct).optimalPhoto;

        MMolSqrMeter artPhotoSum = new MMolSqrMeter(0d);

        for (int i = 0; i < plan.size(); i++) {
            Switch s = plan.getElement(i);
            if (s.isOn()) {
                MMolSqrMeter photoSumHour = photoRate.times(new Duration(HOUR_IN_MS)).toMMol();
                artPhotoSum = artPhotoSum.add(photoSumHour);
            }
        }

        return artPhotoSum;
    }

    public MMolSqrMeter calcPhotoSum(List<Sample<UMolSqrtMeterSecond>> lightLevels, Percent photoOptPct, Duration interpolationInterval) {

        Map<Long, Double> lightData = interpolate(lightLevels, interpolationInterval);

        MMolSqrMeter sum = new MMolSqrMeter(0d);

        for (Map.Entry<Long, Double> s : lightData.entrySet()) {
            // Calc. sum
            UMolSqrtMeterSecond photoRate = calcOptimalClimate(new UMolSqrtMeterSecond(s.getValue()), photoOptPct).optimalPhoto;
            Duration interval = interpolationInterval;
            MMolSqrMeter subElem = photoRate.times(interval).toMMol();
            sum = sum.add(subElem);
        }

        return sum;
    }

    public MMolSqrMeter calcAchievedPhotoSum(List<Sample<Celcius>> tempHistory, List<Sample<PPM>> co2History, List<Sample<UMolSqrtMeterSecond>> lightLevels, Duration interpolationInterval) {

        MMolSqrMeter sum = new MMolSqrMeter(0d);

        if (tempHistory.size() > 2 && co2History.size() > 2 && lightLevels.size() > 2) {

            Map<Long, Double> tempData = interpolate(tempHistory, interpolationInterval);
            Map<Long, Double> co2Data = interpolate(co2History, interpolationInterval);
            Map<Long, Double> lightData = interpolate(lightLevels, interpolationInterval);

            if (tempData.size() == co2Data.size() && co2Data.size() == lightData.size()) {

                for (Map.Entry<Long, Double> tempSample : tempData.entrySet()) {

                    long t = tempSample.getKey();
                    // Calc. sum
                    Celcius temp = new Celcius(tempSample.getValue());
                    PPM co2 = new PPM(co2Data.get(t));
                    UMolSqrtMeterSecond light = new UMolSqrtMeterSecond(lightData.get(t));

                    UMolSqrtMeterSecond photoRate = calcPhoto(temp, co2, light);

                    MMolSqrMeter subElem = photoRate.times(interpolationInterval).toMMol();

                    // Only sum positive photo. values
                    sum = subElem.value() > 0 ? sum.add(subElem) : sum;
                }
            }
        }
        return sum;
    }

    //
    // Private methods
    //
    private PhotoOptResult calcOptimalClimate(UMolSqrtMeterSecond lightLevel, Percent photoOptPct) {

        if (lightLevel == null) {
            throw new IllegalArgumentException("Light level null.");
        }

        if (photoOptPct == null) {
            throw new IllegalArgumentException("Photo opt. pct. null.");
        }

        PhotoParams p = new PhotoParams(lightLevel, photoOptPct);

        PhotoOptResult r = photoOptResults.get(p);
        if (r == null) {
            calculator.calculateOptimalClimate(p.parLight.value(), p.photoOptPct.asPercent(), f.getPlantSpecPhotoParams());

            PPM optCO2 = new PPM(calculator.getOptimalCO2());
            Celcius optTemp = new Celcius(calculator.getOptimalTemperature());

            double photoRate = calculator.getMaxPhotosynthesis() < 0 ? 0 : calculator.getMaxPhotosynthesis();
            UMolSqrtMeterSecond optPhoto = new UMolSqrtMeterSecond(photoRate);

            r = new PhotoOptResult(optCO2, optTemp, optPhoto);
            photoOptResults.put(p, r);
        }
        return r;
    }

    /**
     * Photo parameters for calculation that results in same result. I.e.
     * parameters for which a cached result is eligible.
     */
    class PhotoParams {

        private final UMolSqrtMeterSecond parLight;
        private final Percent photoOptPct;

        /**
         * @param parLight PAR light [umol m^-2 s^-1]
         * @param photoOptPercentage Photosynthesis optimization percentage [%]
         */
        public PhotoParams(UMolSqrtMeterSecond parLight, Percent photoOptPercentage) {

            this.parLight = parLight;
            this.photoOptPct = photoOptPercentage;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PhotoParams other = (PhotoParams) obj;
            if (this.parLight != other.parLight && (this.parLight == null || !this.parLight.equals(other.parLight))) {
                return false;
            }
            if (this.photoOptPct != other.photoOptPct && (this.photoOptPct == null || !this.photoOptPct.equals(other.photoOptPct))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + (this.parLight != null ? this.parLight.hashCode() : 0);
            hash = 97 * hash + (this.photoOptPct != null ? this.photoOptPct.hashCode() : 0);
            return hash;
        }
    }

    /**
     * Cached result for given PhotoParams`
     */
    class PhotoOptResult {

        private final PPM optimalCO2;
        private final Celcius optimalTemp;
        private final UMolSqrtMeterSecond optimalPhoto;

        public PhotoOptResult(PPM optimalCO2, Celcius optimalTemp, UMolSqrtMeterSecond optimalPhoto) {
            this.optimalCO2 = optimalCO2;
            this.optimalTemp = optimalTemp;
            this.optimalPhoto = optimalPhoto;
        }
    }
}
