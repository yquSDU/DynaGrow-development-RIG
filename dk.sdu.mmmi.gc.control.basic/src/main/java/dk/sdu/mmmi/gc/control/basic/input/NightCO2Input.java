package dk.sdu.mmmi.gc.control.basic.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author mrj
 */
public class NightCO2Input extends AbstractInput<PPM> {

    private final ControlDomain g;
    private final Link<Action> action;

    public NightCO2Input(ControlDomain g) {
        super("Preferred night CO2");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        PPM v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, PPM result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPreferredNightCO2(new Sample<PPM>(t, result));
    }

    private PPM retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<PPM> r = db.selectPreferredNightCO2(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            PPM oldValue = NightCO2Input.this.getValue();
            PPM newValue = DialogUtil.promtPPM(oldValue, "Preferred night CO2");
            if (newValue != null) {
                store(new Date(), newValue);
                setValue(newValue);
            }
        }
    }
}
