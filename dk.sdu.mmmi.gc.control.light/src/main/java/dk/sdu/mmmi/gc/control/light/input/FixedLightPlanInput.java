package dk.sdu.mmmi.gc.control.light.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.light.gui.FixedLightPlanPanel;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;

/**
 *
 * @author jcs
 */
public class FixedLightPlanInput extends AbstractInput<FixedDayLightPlan> {

    private final ControlDomain greenhouse;
    private final Link<Action> action;

    public FixedLightPlanInput(ControlDomain g) {
        super("Fixed light plan");
        this.greenhouse = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public void doUpdateValue(Date t) {
        setValue(retrieve(t));
    }

    // TODO: Refactor database methods into generic ones that can handle values.
    private void store(Date t, Sample<FixedDayLightPlan> result) {
        ClimateDataAccess db = context(greenhouse).one(ClimateDataAccess.class);
        db.insertFixedLightPlan(result);
    }

    private FixedDayLightPlan retrieve(Date t) {
        ClimateDataAccess db = context(greenhouse).one(ClimateDataAccess.class);
        Sample<FixedDayLightPlan> r = db.selectFixedLightPlan(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            FixedLightPlanPanel pnl = new FixedLightPlanPanel(retrieve(new Date()));
            DialogDescriptor dcs = new DialogDescriptor(pnl, "Configure fixed Lightplan");
            Object result = DialogDisplayer.getDefault().notify(dcs);

            if (result == DialogDescriptor.OK_OPTION) {
                Date now = new Date();
                FixedDayLightPlan newPlan = pnl.getLightPlan();
                store(now, new Sample<>(now, newPlan));
                setValue(newPlan);
            }
        }
    }
}
