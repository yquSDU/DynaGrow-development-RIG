package dk.sdu.mmmi.gc.control.photo.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.PPM;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.api.greenhousehal.HAL;
import dk.sdu.mmmi.gc.api.greenhousehal.HALConnector;
import dk.sdu.mmmi.gc.api.greenhousehal.HALException;
import dk.sdu.mmmi.gc.control.commons.utils.PhotoUtil;
import java.util.Date;
import org.openide.util.Exceptions;

/**
 * @author mrj
 */
public class ActualPhotosynthesisInput extends AbstractInput<UMolSqrtMeterSecond> {

    private final ControlDomain g;

    public ActualPhotosynthesisInput(ControlDomain g) {
        super("Actual photosynthesis");
        this.g = g;        
    }

    @Override
    public void doUpdateValue(Date t) {
        try {
            ClimateDataAccess db = context(g).one(ClimateDataAccess.class);

            HALConnector hal = HAL.get(g);

            PPM ppm = hal.readCO2();
            Celcius temp = hal.readTemperature();

            PhotoUtil util = context(g).one(PhotoUtil.class);

            UMolSqrtMeterSecond light = db.selectLightIntensity(t).getSample();

            UMolSqrtMeterSecond photo = util.calcPhoto(temp, ppm, light);
            db.insertActualPhoto(new Sample<>(t, photo));
            setValue(photo);
        } catch (HALException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
