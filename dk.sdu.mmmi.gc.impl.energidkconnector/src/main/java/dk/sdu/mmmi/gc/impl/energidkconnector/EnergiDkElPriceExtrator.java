package dk.sdu.mmmi.gc.impl.energidkconnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 *
 * @author jcs
 */
class EnergiDkElPriceExtrator implements ResultSetExtractor<TreeMap<Date, Double>> {


    public EnergiDkElPriceExtrator() {
    }

    @Override
    public TreeMap<Date, Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
        TreeMap<Date, Double> result = new TreeMap();

        while (rs.next()) {
            Long sqldate = rs.getLong("timest");

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(sqldate);

            Date date = new Date(cal.getTimeInMillis());
            //get the price of the hour
            Double hourlyprice = rs.getDouble("price");

            //put into list
            result.put(date, hourlyprice);
        }
        return result;
    }
}
