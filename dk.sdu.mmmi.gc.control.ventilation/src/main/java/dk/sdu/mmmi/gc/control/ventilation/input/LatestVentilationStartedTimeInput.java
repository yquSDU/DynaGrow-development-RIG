package dk.sdu.mmmi.gc.control.ventilation.input;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author jcs
 */
public class LatestVentilationStartedTimeInput extends AbstractInput<Date> {

    private static final long DAY_MS = 1000 * 60 * 60 * 24;
    private final ControlDomain g;

    public LatestVentilationStartedTimeInput(ControlDomain g) {
        super("Start of last ventilationperiod");
        this.g = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        setValue(calculate(t));
    }

    private Date calculate(Date now) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Date from = new Date(now.getTime() - DAY_MS);

        // Input data set. Most recent data first in lists.
        List<Sample<Celcius>> vList = db.selectVentilationThreshold(from, now);
        List<Sample<Celcius>> tList = db.selectAirTemperature(from, now);
        return searchForLastVentilationStart(tList, vList, now);
    }

    public static Date searchForLastVentilationStart(
            List<Sample<Celcius>> tList,
            List<Sample<Celcius>> vList,
            Date now) {

        // Prepare for reverce iteration.
        Collections.reverse(vList);
        Collections.reverse(tList);
        Iterator<Sample<Celcius>> tItr = tList.iterator();
        Iterator<Sample<Celcius>> vItr = vList.iterator();
        Sample<Celcius> v = vItr.hasNext() ? vItr.next() : null;
        Date tPrevDate = now;

        // Assigned by the algorithm below.
        Date ventEnd = null;
        Date ventStart = null;

        // Find ventEnd : the most lately point in time where ventilation stopped.
        while (ventEnd == null && tItr.hasNext()) {
            Sample<Celcius> t = tItr.next();

            while (ventEnd == null && v != null && v.getTimestamp().after(t.getTimestamp())) {
                // Compare v and t.
                if (t.getSample().value() > v.getSample().value()) {
                    ventEnd = t.getTimestamp(); // assignment makes both loops terminate.
                    break;
                }

                // Move.
                v = vItr.hasNext() ? vItr.next() : null;
            }

            tPrevDate = t.getTimestamp();
        }

        // Find ventStart : the most lately point in time where ventilation started.
        while (ventStart == null && tItr.hasNext()) {
            Sample<Celcius> t = tItr.next();

            while (ventStart == null && v != null && v.getTimestamp().after(t.getTimestamp())) {
                // Compare v and t.
                if (t.getSample().value() < v.getSample().value()) {
                    ventStart = tPrevDate; // assignment makes both loops terminate.
                    break;
                }

                // Move.
                v = vItr.hasNext() ? vItr.next() : null;
            }

            tPrevDate = t.getTimestamp();
        }

        // If no result was found, use a very old date.
        return ventStart != null ? ventStart : new Date(0);
    }
}
