package dk.sdu.mmmi.gc.control.light.photo.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.impl.entities.units.MMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public class PhotoSumAchievedPeriodSeries extends DoubleTimeSeries {

    private final ControlDomain g;

    public PhotoSumAchievedPeriodSeries(ControlDomain g) {
        this.g = g;
        setName("Photo sum achieved (today - 2 days)");
        setUnit("[mmol m^-2]");
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> r = new ArrayList<>();

        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        if (db != null) {
            List<Sample<MMolSqrMeter>> raw = db.selectPhotoSumAchievedPeriod(from, to);
            for (Sample<MMolSqrMeter> s : raw) {
                Date t = s.getTimestamp();
                double v = s.getSample().value();
                r.add(new DoubleTimeValue(t, v));
            }
        }

        return r;
    }
}
