package dk.sdu.mmmi.gc.control.commons.utils;

import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class LightUtil {

    public static Double NIGHT_LIMIT = 5.0;

    public static MolSqrMeter calcPARSum(List<Sample<UMolSqrtMeterSecond>> parLightLevels, Duration interpolationInterval) {

        Map<Long, Double> iPARLevels = MathUtil.interpolate(parLightLevels, interpolationInterval);

        MolSqrMeter sum = new MolSqrMeter(0d);

        for (Map.Entry<Long, Double> sample : iPARLevels.entrySet()) {
            UMolSqrtMeterSecond light = new UMolSqrtMeterSecond(sample.getValue());
            UMolSqrMeter par = light.times(interpolationInterval);
            sum = sum.add(par.toMol());
        }

        return sum;
    }

    public static MolSqrMeter calcLightplanPARSum(LightPlan lightPlan, UMolSqrtMeterSecond lampIntensity) {

        UMolSqrMeter artPARLightSum = new UMolSqrMeter(0d);

        for (int i = 0; i < lightPlan.size(); i++) {
            Switch s = lightPlan.getElement(i);
            if (s.isOn()) {
                artPARLightSum = artPARLightSum.add(lampIntensity.times(lightPlan.getLightInterval()));
            }
        }
        return artPARLightSum.toMol();
    }

    public static List<Sample<UMolSqrtMeterSecond>> convertToIndoorPAR(OutdoorLightForecast outdoorSunLightLevels, Percent transmission) {

        List<Sample<UMolSqrtMeterSecond>> indoorPARLevels = new ArrayList<>();
        for (Sample<WattSqrMeter> s : outdoorSunLightLevels.get()) {
            WattSqrMeter outdoorSunLight = s.getSample();
            UMolSqrtMeterSecond indoorPARLevel = fromSunToPAR(outdoorSunLight.times(transmission));
            indoorPARLevels.add(new Sample<>(s.getTimestamp(), indoorPARLevel));
        }
        return indoorPARLevels;
    }

    /**
     * sun spectra into umol m^-2 s^-1 in PAR spectra
     *
     * @return Level level in PAR spectra
     */
    public static UMolSqrtMeterSecond fromSunToPAR(WattSqrMeter wattSqrMeter) {
        return new UMolSqrtMeterSecond(wattSqrMeter.value() * 2.3);
    }

    /**
     * @param parNatLightLevels Natural PAR light
     * @param plan Art. light plan
     * @param lampIntensity
     * @return Return a list of combined Nat. and Art. light levels
     */
    public static List<Sample<UMolSqrtMeterSecond>> combineNatArtLight(List<Sample<UMolSqrtMeterSecond>> parNatLightLevels, LightPlan plan, UMolSqrtMeterSecond lampIntensity) {

        List<Sample<UMolSqrtMeterSecond>> parLightLevels = new ArrayList<>();

        // Add art. light to indoor light samples
        for (Sample<UMolSqrtMeterSecond> sample : parNatLightLevels) {

            Date sampleTime = sample.getTimestamp();

            if (plan.hasDate(sampleTime)) {

                Switch lightHour = plan.getElement(sampleTime);

                if (lightHour.isOn()) {
                    UMolSqrtMeterSecond parNatLight = sample.getSample();
                    UMolSqrtMeterSecond value = parNatLight.add(lampIntensity);
                    parLightLevels.add(new Sample<>(sample.getTimestamp(), value));

                } else {
                    parLightLevels.add(sample);

                }
            }
        }
        return parLightLevels;
    }
}
