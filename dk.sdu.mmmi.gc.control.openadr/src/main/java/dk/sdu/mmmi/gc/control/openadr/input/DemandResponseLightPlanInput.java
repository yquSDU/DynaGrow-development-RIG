/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.control.openadr.input;

import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRAction;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRSubscriber;
import dk.sdu.mmmi.controleum.impl.openadr.api.filter.LoadControlCapacityEventFilter;
import dk.sdu.mmmi.controleum.impl.openadr.api.filter.OpenADREventFilter;
import dk.sdu.mmmi.gc.impl.entities.results.FixedDayLightPlan;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ancla
 */
public class DemandResponseLightPlanInput extends AbstractInput<FixedDayLightPlan> implements OpenADRSubscriber {

    private final ControlDomain controlDomain;

    public DemandResponseLightPlanInput(ControlDomain g) {
        super("Demand Response light plan signal");
        this.controlDomain = g;
    }

    @Override
    public void doUpdateValue(Date t) {
        setValue(l);
    }

    //TODO Refactor rest of this class out to DB layer. DB layer will be responsible for storing the signal and implement the OpenADRSubscriber interface as well as creating light plans based on DR signals.
    FixedDayLightPlan l = new FixedDayLightPlan();;

    @Override
    public void notify(OpenADRAction packet) {
        Switch s;
        switch (packet.getSignal().intValue()) {
            case 1:
                s = Switch.ON;
                break;
            case 0:
                s = Switch.OFF;
                break;
            default:
                s = Switch.NA;
                break;
        }
        long num_hours = packet.getLength() / 3600000;
        for (int i = 0; i < num_hours; i++) {
            l.set(packet.getStartCalendar().get(Calendar.HOUR_OF_DAY) + i, s);
        }
    }

    OpenADREventFilter filter = new LoadControlCapacityEventFilter();

    @Override
    public OpenADREventFilter getFilter() {
        return filter;
    }

}
