package dk.sdu.mmmi.gc.control.light.par.input;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.control.commons.AbstractInput;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.utils.DialogUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author jcs
 */
public class PARSumDayGoalInput extends AbstractInput<MolSqrMeter> {

    private final ControlDomain g;
    private final Link<Action> action;

    public PARSumDayGoalInput(ControlDomain g) {
        super("PAR Day Light Integral (DLI) ");
        this.g = g;
        this.action = context(this).add(Action.class, new ConfigureAction());
    }

    @Override
    public synchronized void doUpdateValue(Date t) {
        MolSqrMeter v = retrieve(t);
        setValue(v);
        store(t, v);
    }

    private void store(Date t, MolSqrMeter result) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        db.insertPARSumDayGoal(new Sample<>(t, result));
    }

    private MolSqrMeter retrieve(Date t) {
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        Sample<MolSqrMeter> r = db.selectPARSumDayGoal(t);
        return r.getSample();
    }

    private class ConfigureAction extends AbstractAction {

        public ConfigureAction() {
            super("Configure");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            MolSqrMeter oldValue = PARSumDayGoalInput.this.getValue();
            MolSqrMeter newValue = DialogUtil.promtMolSqrMeter(oldValue, "PAR goal");
            if (newValue != null) {
                setValue(newValue);
                store(new Date(), newValue);
            }
        }
    }
}
