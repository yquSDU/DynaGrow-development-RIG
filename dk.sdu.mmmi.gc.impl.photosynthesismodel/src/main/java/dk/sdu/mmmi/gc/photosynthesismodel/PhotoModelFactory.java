package dk.sdu.mmmi.gc.photosynthesismodel;

/**
 * This interface is implemented as Service Provider Interface that can be
 * looked up by other components
 *
 * @author jcs
 */
public interface PhotoModelFactory {

    /**
     * Creates a photosynthesis optimization calculator given the following
     * parameters.
     * 
     * @param model Photosynthesis model
     * @param minTemp Min temp. of the temp. axis in the matrix
     * @param maxTemp Max. temp of the temp axis in the matrix
     * @param minCO2 Min CO2 of the CO2 axis in the matrix
     * @param maxCO2 Max CO2 of the CO2 axis in the matrix
     * @param searchCO2First Search direction of the matrix. If true then CO2 is
     * search first else it is temp.
     *
     * @return Photosynthesis calculator with specified model, minTemp, maxTemp, minCO2, maxCO2 and searchCO2First
     */
    PhotosynthesisCalculator getPhotosynthesisCalculator(PhotosynthesisModel model, double minTemp, double maxTemp, double minCO2, double maxCO2, boolean searchCO2First);

    /**
     * Creates a photosynthesis optimization calculator with a default
     * Photosynthesis model.
     * 
     * @param minTemp Min temp. of the temp. axis in the matrix
     * @param maxTemp Max. temp of the temp axis in the matrix
     * @param minCO2 Min CO2 of the CO2 axis in the matrix
     * @param maxCO2 Max CO2 of the CO2 axis in the matrix
     * @param searchCO2First Search direction of the matrix. If true then CO2 is
     * search first else it is temp.
     *
     * @return Photosynthesis calculator with default photosynthesis model and specified minTemp, maxTemp, minCO2, maxCO2 and searchCO2First
     */
    PhotosynthesisCalculator getPhotosynthesisCalculator(double minTemp, double maxTemp, double minCO2, double maxCO2, boolean searchCO2First);

    /**
     * minTemp = 14
     * maxTemp = 32
     * minCO2 = 350
     * maxCO2 = 1450
     * searchCO2First = true
     * 
     * @return Default implementaion of Photosynsthesis calculator with above configuration.
     */
    PhotosynthesisCalculator getPhotosynthesisCalculator();

    /**
     * @return Default implementation of Photosynthesis model.
     */
    PhotosynthesisModel getPhotosynthesisModel();

    /**
     * @return Default photosynthesis plant specific parameters
     */
    PlantSpecificPhotoParams getPlantSpecPhotoParams();

}
