package dk.sdu.mmmi.gc.control.basic.concern;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import static dk.sdu.mmmi.controleum.api.moea.Concern.HARD_PRIORITY;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import dk.sdu.mmmi.controleum.impl.entities.units.Percent;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.gc.api.climatedb.ClimateDataAccess;
import dk.sdu.mmmi.gc.control.commons.output.GeneralScreenPositionOutput;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple Ventilation Ramp
 *
 * @author jcs
 */
public class SimpleScreenRampConcern extends AbstractConcern {

    Date timeSp = new Date(0);
    private Sample<Percent> sp;
    private final double ramp = 5; // ramp rate 5 Percent/ctrl_interval
    private double diff;

    public SimpleScreenRampConcern(ControlDomain g) {
        super("Simple screen ramp", g, HARD_PRIORITY);
    }

    @Override
    public double evaluate(Solution s) {

        if (!timeSp.equals(s.getTime())) {
            readSp(s);
        }

        double proposedSp = s.getValue(GeneralScreenPositionOutput.class).asPercent();
        diff = Math.abs(proposedSp - sp.getSample().asPercent());

        return (diff <= ramp) ? 0 : 1;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        Map<Class, String> r = new HashMap<>();
        if (sp != null) {
            r.put(GeneralScreenPositionOutput.class,
                    String.format("diff is %.1f", diff));
        }
        return r;
    }

    private void readSp(Solution s) {
        // Read setpoint to find ramped set point
        timeSp = s.getTime();
        ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
        sp = db.selectGeneralScreenPosition(timeSp);

    }
}
