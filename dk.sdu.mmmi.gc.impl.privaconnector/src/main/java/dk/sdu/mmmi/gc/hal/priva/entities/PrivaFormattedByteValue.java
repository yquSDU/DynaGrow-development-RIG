package dk.sdu.mmmi.gc.hal.priva.entities;

/**
 *
 * @author ancla
 */
public class PrivaFormattedByteValue {

    private final byte[] value;
    private final byte[] attrib;

    public PrivaFormattedByteValue(byte[] value, byte[] attrib) {
        this.value = value;
        this.attrib = attrib;
    }

    /**
     * @return the value
     */
    public byte[] getValue() {
        return value;
    }

    /**
     * @return the attrib
     */
    public byte[] getAttrib() {
        return attrib;
    }
}
