package dk.sdu.mmmi.gc.impl.entities.results;

import dk.sdu.mmmi.controleum.impl.entities.units.MoneyMegaWattHour;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Represents a list of forecasted hourly electricity prices. Each price is in
 * given currency per MWh.
 *
 * @author jcs
 */
public class ElPriceMWhForecast {

    /**
     * Hourly el. price forecast
     */
    private final Map<Date, MoneyMegaWattHour> priceListMWh;
    private final String area;
    private final String currency;

    public ElPriceMWhForecast() {
        priceListMWh = new TreeMap<>();
        area = "";
        currency = "";
    }

    /**
     * @param forecast Hourly electricity price forecast in given currency per
     * MWh.
     * @param area Area code for the Danish electricity grid. E.g. "DKEAST",
     * "DKWEST" or "SYS"
     * @param currencyCode The ISO 4217 code of the currency
     */
    public ElPriceMWhForecast(Map<Date, Double> forecast, String area, String currencyCode) {

        this.area = area;
        this.currency = currencyCode;

        this.priceListMWh = new TreeMap<>();
        for (Date date : forecast.keySet()) {
            this.priceListMWh.put(startOfHour(date), new MoneyMegaWattHour(forecast.get(date), currencyCode));
        }
    }

    /**
     * @param data Hourly electricity price forecast in given currency per MWh.
     * @param area Area code for the Danish electricity grid. E.g. "DKEAST",
     * "DKWEST" or "SYS"
     * @param currency The ISO 4217 code of the currency
     */
    public ElPriceMWhForecast(List<Sample<MoneyMegaWattHour>> data, String area, String currency) {
        this.area = area;
        this.currency = currency;

        this.priceListMWh = new TreeMap<>();
        for (Sample<MoneyMegaWattHour> s : data) {
            this.priceListMWh.put(startOfHour(s.getTimestamp()), s.getSample());
        }
    }

    public List<Sample<MoneyMegaWattHour>> asListSampleMoneyMegaWattHour() {
        List<Sample<MoneyMegaWattHour>> r = new ArrayList();

        for (Map.Entry<Date, MoneyMegaWattHour> entrySet : priceListMWh.entrySet()) {                        
            r.add(new Sample(entrySet.getKey(), entrySet.getValue()));
        }

        return r;
    }

    public Map<Date, MoneyMegaWattHour> getForecastData() {
        return priceListMWh;
    }

    public String getArea() {
        return area;
    }

    public String getCurrency() {
        return currency;
    }

    public Set<Date> getDates() {
        return priceListMWh.keySet();
    }

    public double size() {
        return priceListMWh.size();
    }

    /**
     * TODO: Maybe use EnergyPriceDatabase.getPrice or calculatePrice instead?
     * Find the hour price for a date
     *
     * @param date
     * @return Price of electricity at the given time date
     */
    public MoneyMegaWattHour getPrice(Date date) {
        MoneyMegaWattHour m = priceListMWh.get(startOfHour(date));
        return m;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.priceListMWh);
        hash = 53 * hash + Objects.hashCode(this.area);
        hash = 53 * hash + Objects.hashCode(this.currency);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElPriceMWhForecast other = (ElPriceMWhForecast) obj;
        if (!Objects.equals(this.priceListMWh, other.priceListMWh)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");

        StringBuilder sb = new StringBuilder();
        sb.append("{area=");
        sb.append(area);
        sb.append(", forecast={");
        for (Date t : priceListMWh.keySet()) {
            sb.append(df.format(t));
            sb.append("=");
            MoneyMegaWattHour price = priceListMWh.get(t);
            sb.append(price);
            sb.append(", ");
        }
        sb.append("}}");

        return sb.toString();
    }
}
