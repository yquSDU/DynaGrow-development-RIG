package dk.sdu.mmmi.gc.gui.explorer;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import org.openide.nodes.AbstractNode;

/**
 * @author mrj & jcs
 */
public class ConcernsNode extends AbstractNode implements SearchObserver {

    private final ControlDomain g;

    public ConcernsNode(ControlDomain g) {
        super(new ConcernsNodeChildren(g));
        this.g = g;
        setDisplayName("Specifications");
        setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_smile_gray.png");

        context(g).add(SearchObserver.class, this);
    }

    // TODO: Refactor this to Greenhouse Service
    @Override
    public void onSearchCompleted(Solution s) {
        boolean found = false;

        // Set the emotion icon according to worse happiness
//        for (Concern concern : context(g).all(Concern.class)) {
//            if (concern.isEnabled() && !concern.isAccepted()) {
//                setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_unhappy_red.png");
//                found = true;
//                break;
//            }
//        }
        if (!found) {
            for (Concern concern : context(g).all(Concern.class)) {
                if (concern.isEnabled() && !(concern.getEvaluationResult() <= 0.01)) {
                    setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_neutral.png");
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            for (Concern concern : context(g).all(Concern.class)) {
                if (concern.isEnabled() && (concern.getEvaluationResult() <= 0.01)) {
                    setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_smile_green.png");
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            setIconBaseWithExtension("dk/sdu/mmmi/gc/silk/emoticon_smile_gray.png");
        }
    }
}
