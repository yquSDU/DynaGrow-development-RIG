/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.gc.hal.priva.acquaintance;

/**
 *
 * @author ancla
 */
public class PrivaServerException extends Exception {
    public PrivaServerException(String message)
    {
        super(message);
    }
    
    public PrivaServerException(String message, Throwable e)
    {
        super(message, e);
    }
}
