package dk.sdu.mmmi.gc.gui.actions;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.gc.impl.entities.config.ChartConfig;
import dk.sdu.mmmi.gc.gui.charts.ChartManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author jcs
 */
public class DeleteChartAction extends AbstractAction {

    private final ChartConfig cfg;
    private final ControlDomain g;

    public DeleteChartAction(ControlDomain g, ChartConfig cfg) {
        super("Delete Chart");
        this.g = g;
        this.cfg = cfg;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        ChartManager cm = context(g).one(ChartManager.class);
        cm.delete(cfg);
    }
}
